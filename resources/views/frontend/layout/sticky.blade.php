<div class="c-dropdown__inner | js-dropdown mobile_show">
    <div class="c-dropdown__content">
        <ul class="c-dropdown-menu c-dropdown-menu--main">
            <li class="c-dropdown-menu__item"><a href="{{ route('products') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">Products</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('about-us') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">About us</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('csr') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">CSR</div>
                </a></li>

            <li class="c-dropdown-menu__item"><a href="{{ route('research.and.development') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">R&D</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('events') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">Events</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('medical.care') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">Medical Care</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('go.green') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">Go Green</div>
                </a></li>
            <li class="c-dropdown-menu__item"><a href="{{ route('contact-us') }}"
                                                 class="c-dropdown-menu__link stroke-o-1 | js-mobile-link">
                    <div class="js-dropdown-menu__line--main">Contact Us</div>
                </a></li>
        </ul>
    </div>
    <div class="flaps__pre__footer abs bottom x">
        <div class="flap__med abs x bottom left | js-d-flap-one">
            <svg class="x y db" width="1440" height="367" viewBox="0 0 1440 367" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M-397.96 20.8043C-433.069 32.5067 -487.682 94.9195 -588.132 116.374C-664.762 132.741 -846.213 283.425 -939 367H2006C1978.37 354.647 1909.65 328.967 1855.81 325.066C1788.52 320.19 1707.58 246.075 1685.15 224.621C1662.72 203.166 1610.05 198.29 1580.8 192.439C1551.54 186.588 1524.23 154.406 1510.58 124.175C1496.93 93.944 1428.66 68.5888 1386.72 68.5888C1344.79 68.5888 1295.05 101.746 1250.19 108.572C1205.33 115.398 1119.51 84.192 1069.77 68.5888C1020.03 52.9856 985.902 43.2336 926.412 57.8616C866.922 72.4896 793.78 116.374 686.504 137.828C579.227 159.282 496.332 227.546 430.016 236.323C363.7 245.1 268.127 166.109 208.637 116.374C149.148 66.639 -9.81604 71.5144 -54.677 68.5888C-99.5378 65.6632 -165.854 38.3579 -208.764 12.0275C-251.675 -14.3029 -362.852 9.10187 -397.96 20.8043Z"
                    fill="#111111" fill-opacity="0.3"/>
            </svg>
        </div>
        <div class="flap__bottom abs x bottom left | js-d-flap-two">
            <svg class="x y db" width="1440" height="205" viewBox="0 0 1440 205" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M1215.79 11.6209C1235.4 18.1577 1265.91 53.0204 1322.01 65.0045C1364.82 74.1468 1466.17 158.317 1518 205H-127C-111.566 198.1 -73.1795 183.756 -43.1097 181.577C-5.5225 178.853 39.6911 137.453 52.2202 125.469C64.7492 113.485 94.1653 110.762 110.508 107.493C126.85 104.225 142.103 86.2488 149.729 69.3621C157.355 52.4755 195.487 38.3125 218.911 38.3125C242.335 38.3125 270.117 56.8334 295.175 60.6465C320.233 64.4596 368.171 47.0282 395.952 38.3125C423.734 29.5968 442.8 24.1495 476.03 32.3205C509.259 40.4914 550.115 65.0045 610.036 76.9884C669.958 88.9723 716.261 127.104 753.303 132.006C790.346 136.909 843.731 92.7856 876.96 65.0045C910.189 37.2234 998.982 39.9467 1024.04 38.3125C1049.1 36.6783 1086.14 21.4261 1110.11 6.71834C1134.08 -7.98938 1196.18 5.08415 1215.79 11.6209Z"
                    fill="#111111" fill-opacity="0.7"/>
            </svg>
        </div>
    </div>
</div>
