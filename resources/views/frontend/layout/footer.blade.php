<div class="theme_new_footer" style="background-image: url({{ asset('frontend/assets/imgs/players.png') }});">
    <div class="logo">
        <img src="{{ asset('frontend/assets/imgs/logo.png') }}">
    </div>
    <div class="icons-listing">
        <div class="social-icon">
            <img src="{{ asset('frontend/assets/imgs/fb.png') }}">
        </div>
        <div class="social-icon">
            <img src="{{ asset('frontend/assets/imgs/insta.png') }}">
        </div>
        <div class="social-icon">
            <img src="{{ asset('frontend/assets/imgs/twitter.png') }}">
        </div>
    </div>
    <div class="footer_tagline">
        <p>*Silver Star Group is proudly serving the world of sports for more than a century. An icon of
            excellence, uniqueness and serving as a role model for soccer balls, apparel and gloves
            manufact</p>
    </div>
</div>
