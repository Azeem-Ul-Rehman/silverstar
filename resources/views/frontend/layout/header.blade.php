<header class="header site-head | js-header">
    <div class="wrapper x y df jcb aic | js-inner-header">
        <a href="javascript:void(0)" class="nav-logo site-head__logo" id="logoClick">
            <img src="{{ asset('frontend/assets/imgs/logo.png') }}">
        </a>
        <nav class="desktop-nav site-head__nav">
            <ul class="site-head-menu">
                <div class="site-head-menu-inner df jcc aic rel">
                    {{--                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"--}}
                    {{--                                                        href="{{ route('index') }}">Home</a></li>--}}
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('about-us') }}">About us</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('products') }}">Products</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('csr') }}">CSR</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('research.and.development') }}">R&D</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('events') }}">EVENTS</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('medical.care') }}">Medical Care</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('go.green') }}">GO GREEN</a></li>
                    <li class="site-head-menu__item"><a class="link link-in site-head-menu__link | js-site-link"
                                                        href="{{ route('contact-us') }}">Contact Us</a></li>
                </div>
            </ul>
        </nav>
        <nav class="mobile-nav site-head__mobile">
            <ul class="site-head-menu">
                <div class="burger | js-menu-toggle js-menu-original">
                    <div class="burger__line | js-burger__line"></div>
                    <div class="burger__line | js-burger__line"></div>
                    <div class="burger__line | js-burger__line"></div>
                    <div class="burger-close">
                        <div class="burger-close__line | js-burger-close__line--1"></div>
                        <div class="burger-close__line | js-burger-close__line--2"></div>
                    </div>
                    <svg class="circ__path__mobile" xmlns="http://www.w3.org/2000/svg" width="54" height="58"
                         viewBox="0 0 54 58" fill="none">
                        <path
                            d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                            stroke="#FF5F09" stroke-width="2" stroke-linecap="square" stroke-linejoin="bevel"/>
                    </svg>
                </div>
            </ul>
        </nav>
    </div>
</header>
@push('js')

    <script>
        $('#logoClick').on('click', function () {
            window.location.href = '{{route('index')}}';
        })
    </script>
@endpush
