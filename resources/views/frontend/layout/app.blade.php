<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<!-- begin::Head -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage"
          content="{{ asset('frontend/themes/wp-idearocket/assets/static/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image"
          content="{{ asset('frontend/themes/wp-idearocket/assets/imgs/IR-Share.jpg') }}"/>
    <meta name="twitter:image"
          content="{{ asset('frontend/themes/wp-idearocket/assets/imgs/IR-Share.jpg') }}">
    <link type="text/css" media="all"
          href="{{ asset('frontend/cache/breeze-minification/css/breeze_85e3f6370737b5b0831917476d8c1e87.css') }}"
          rel="stylesheet"/>



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Silver Star | @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('frontend/cdn.plyr.io/3.6.2/plyr.css') }}"/>
    @stack('css')
    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">

</head>

<!-- end::Head -->

<!-- begin::Body -->
<body>
@include('frontend.layout.header')
@yield('content')


<script src="{{ asset('frontend/themes/wp-idearocket/build/main.js') }}"
        type="428105bd07a4a5eb26aef6bf-text/javascript"></script>
<script type="428105bd07a4a5eb26aef6bf-text/javascript"
        src='{{ asset('frontend/plugins/wp-smushit/app/assets/js/smush-lazy-load.mincd70.js') }}?ver=3.8.3'
        id='smush-lazy-load-js'></script>
<script type="428105bd07a4a5eb26aef6bf-text/javascript"
        src='{{ asset('frontend/js/wp-embed.min4c7e.js') }}?ver=5.6.2'
        id='wp-embed-js'></script>
<script
    src="{{ asset('frontend/ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js') }}"
    data-cf-settings="428105bd07a4a5eb26aef6bf-|49" defer=""></script>
<!--end::Page Scripts -->
<script type="text/javascript">

    function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        } else {
            return false;
        }
    }
</script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif
</script>
@stack('models')
@stack('js')
</body>
</html>
