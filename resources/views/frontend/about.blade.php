@extends('frontend.layout.app')
@section('title','About Us')
@section('content')
    @include('frontend.layout.sticky')

    <main class="rel z10 | js-smooth main-wrapper fonts-new ">
        <section data-router-wrapper>
            <article data-router-view="home">
                <section class="company-about-sec">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="company_about_pic">
                                <img src="{{ asset('frontend/assets/imgs/about-build.png') }}">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="company-about">
                                <div class="about-us">
                                    <h3><span>About</span> Us</h3>
                                    <p>From a humble in 1974, we have grown large and strong over the decades of the
                                        extent that now Sliver Star is
                                        confidently prepared to face my market challenge and is ready to serve around
                                        the globe.</p>
                                    <p>Today, with more than 4,000 workers, we are looking forward to elevate this
                                        family to a level where we are even more proud and spirited to sustain our
                                        position as
                                        the industry leader and prove to be the best partner for our global customer.
                                        As we are already producing high quality soccer balls, goalkeeping gloves,
                                        sports apparel and bags
                                        for the world's prominent brands, Sliver Star vision for the future revolves
                                        around becoming a
                                        one-stop solution for customer
                                        seeking quality and commitment all kind of team sports products.</p>
                                </div>
                                <div class="our-mission">
                                    <h3><span>Our</span> Mission</h3>
                                    <p>An icon of excellence, unique and role model Soccer Balls manufacturing and
                                        exporting company in South Asia, by using its
                                        own manufacturing facilities for casing material and bladders; aiming at the
                                        supreme compliance</p>
                                </div>

                                <div class="portion_icons_wrap">
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/shake.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/heart.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/group.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/calender.png') }}">
                                    </div>
                                </div>
                                <div class="portion_icons_wrap">
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/shake.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/heart.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/group.png') }}">
                                    </div>
                                    <div class="portion_icon">
                                        <img src="{{ asset('frontend/assets/imgs/calender.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="process" data-scroll data-scroll-offset="100">
                    <div class="process__container rel pt0 m:pt150 mb150 mb0">
                        <div data-scroll data-scroll-offset="100">
                            <div class="process__trigger rocket__trigger"></div>
                            <div class="process__trigger rocket__animation dn m:db" data-scroll data-scroll-offset="100"
                                 id="scroll__fixed"></div>
                            <div data-scroll data-scroll-sticky data-scroll-target="#scroll__fixed">
                                <div class="process__title__top enter-x delay-2" data-scroll data-scroll-offset="100">
                                    <h2 class="process__title stroke-w-2" data-scroll data-scroll-direction="horizontal"
                                        data-scroll-speed="1">
                                        <div><span class="">TimeLine</span></div>
                                    </h2>
                                </div>
                                <div class="process__container__info wrapper df jcc m:jcs aic m:pl100 m:pb100 rel z4"
                                     data-scroll data-scroll-offset="200">
                                    <h2 class="process__subtitle dn m:db pb100">
                                        <div class="history-title"><h3><span>Our</span> History</h3>
                                        </div>
                                        <!-- <div class="oh ng-mt-2 pl0 m:pl50"><span class="dib enter-y delay-7">production process</span>
                                        </div> -->
                                    </h2>
                                    <h2 class="process__subtitle db m:dn pb100">
                                        <!-- <div class="oh"><span class="mdum dib enter-y delay-5">Our animated</span></div>
                                        <div class="oh ng-mt-2 pl0 m:pl50"><span class="dib enter-y delay-7">video production</span>
                                        </div> -->
                                        <div class="oh ng-mt-2 pl0 m:pl50"><span
                                                class="dib enter-y delay-9"></span>
                                        </div>
                                    </h2>
                                </div>
                                <div class="process__line rel z4" data-scroll data-scroll-direction="horizontal"
                                     data-scroll-speed="25">
                                    <div class="process__grid df fr abs enter-y delay-4">
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">1</span><h4
                                                    class="process__grid__title">1900-1940</h4>
                                                <h5 class="generation">Generation</h5>
                                                <p class="process__grid__text"> Comment of business set the
                                                    foundation</p>

                                                <h6 class="own-name">Imam-ud-Din</h6>
                                            </div>
                                        </div>
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">2</span><h4
                                                    class="process__grid__title">1940-1970</h4>
                                                <h5 class="generation">Generation</h5>
                                                <p class="process__grid__text"> Cottage industry beginig towards
                                                    growth</p>
                                                <h6 class="own-name">Imam-ud-Din , Amir-ud-Din</h6>
                                            </div>
                                        </div>
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">3</span><h4
                                                    class="process__grid__title ml-20">1970-2000</h4>
                                                <h5 class="generation">Generation</h5>
                                                <p class="process__grid__text"> Expansion Growth Largest
                                                    infrastructure</p>
                                                <h6 class="own-name">Amir-ud-Din, Jahangir Iqbal Babar Hamayun, Arif
                                                    Hamayun</h6>
                                            </div>
                                        </div>
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">4</span><h4
                                                    class="process__grid__title">2000-2018</h4>
                                                <h5 class="generation">Generation</h5>
                                                <p class="process__grid__text"> Exploring New Horizons of Growth</p>
                                                <h6 class="own-name">Amir-ud-Din, Jahangir Iqbal Babar Hamayun, Arif
                                                    Hamayun</h6>
                                            </div>
                                        </div>
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">5</span><h4
                                                    class="process__grid__title">Animation</h4>
                                                <p class="process__grid__text"> We bring the characters and other <br>
                                                    visual elements to life with animation.</p></div>
                                        </div>
                                        <div class="process__grid__item rel">
                                            <div class="process__grid__inner__item rel x"><span
                                                    class="process__grid__num abs top left">6</span><h4
                                                    class="process__grid__title pb50">Sound Design</h4>
                                                <p class="process__grid__text"> We add music and sound effects to <br>
                                                    help
                                                    the image land. The video is <br> now ready to deliver!</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rocket__path dn m:db rel z1"
                                     style="position: relative;top: -150px; width: 115%;">
                                    <svg width="100%" height="100%" viewBox="0 0 1645 419" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path id="rocket__path" opacity="0.5"
                                              d="M1 378.5C62.1667 233.167 225.2 -44.6 388 7.00004C550.8 58.6 642 389 807 397C1018 397 1033.74 107.557 1188.5 82.5C1451 40 1489.5 397 1644 418.5"
                                              stroke="#023BA0"/>
                                    </svg>
                                    <div class="pen" id="rocket"
                                         style="width: 120px;height: 60px;pointer-events: none;position: absolute;"><img
                                            style="width: 100%; transform: rotate(11deg) scale(2.5); margin-top: 4px;    height: 50px;
                                        width: 50px;"
                                            src="{{ asset('frontend/assets/imgs/moving-arrow.png') }}"
                                            alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="fake__scroll dn m:db" style="height: 800px;"></div>
                        </div>
                    </div>
                </section>
                <section class="video-section">
                    <div class="focus__container__content rel df fc jcc aic m:pb150 m:pb200 is-inview" data-scroll=""
                         data-scroll-offset="100">
                        <h2 class="focus__container__title pb50 m:pb100 rel z5">
                            <div class=" oh">
                                <span class="dib enter-y delay-5 stroke-o-1">Cahirman's</span> Message
                            </div>
                            <!-- <div class="ng-mt-1 nn-mt oh">
                                <span class="dib enter-y delay-7">our founder's story</span>
                            </div> -->
                        </h2>
                        <div class="focus__watch df jcc aic enter-y delay-9 rel z5 | js-toggle-video"
                             data-title="Our founder's story" data-video-id="478140706">
                            <div class="g__btn__circ">
                                <div class="circ__head">
                                    <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                         height="58" viewBox="0 0 54 58" fill="none">
                                        <path
                                            d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                            stroke="#FF5F09" stroke-width="2" stroke-linecap="square"
                                            stroke-linejoin="bevel"></path>
                                        <path class="circ__icon-l"
                                              d="M37.2629 27.7081L37.0084 28.1385L37.0097 28.1393L37.2629 27.7081ZM26.2626 21.2052L26.517 20.7748L26.5169 20.7748L26.2626 21.2052ZM37.2629 30.2892L37.0091 29.8584L37.0085 29.8588L37.2629 30.2892ZM26.2626 36.789L26.0082 36.3585L26.0075 36.3589L26.2626 36.789ZM36.7566 29.4319L37.011 29.8623L37.0126 29.8613L36.7566 29.4319ZM25.7563 35.9316L26.0057 36.365L26.0106 36.362L25.7563 35.9316ZM36.7535 28.5694L36.4991 28.9998L36.4993 28.9999L36.7535 28.5694ZM25.7531 22.0665L25.4967 22.4958L25.4987 22.497L25.7531 22.0665ZM37.5173 27.2777L26.517 20.7748L26.0081 21.6357L37.0084 28.1385L37.5173 27.2777ZM37.5167 30.72C38.8248 29.9494 38.8309 28.0491 37.5161 27.277L37.0097 28.1393C37.6636 28.5233 37.6635 29.4729 37.0091 29.8584L37.5167 30.72ZM26.5169 37.2195L37.5172 30.7197L37.0085 29.8588L26.0082 36.3585L26.5169 37.2195ZM23.5 35.4984C23.5 37.0625 25.1977 38.0018 26.5176 37.219L26.0075 36.3589C25.3461 36.7512 24.5 36.278 24.5 35.4984H23.5ZM23.5 22.4958V35.4984H24.5V22.4958H23.5ZM26.5169 20.7748C25.9156 20.4195 25.17 20.4188 24.5738 20.7148C23.9583 21.0204 23.5 21.6435 23.5 22.4958H24.5C24.5 22.0419 24.7261 21.7557 25.0185 21.6105C25.3301 21.4558 25.7158 21.4629 26.0082 21.6357L26.5169 20.7748ZM36.5023 29.0014L25.5019 35.5011L26.0106 36.362L37.011 29.8623L36.5023 29.0014ZM36.4993 28.9999C36.5016 29.0013 36.5019 29.0018 36.5013 29.0012C36.5007 29.0006 36.5002 28.9999 36.4998 28.9993C36.4993 28.9982 36.5 28.9991 36.5 29.0018C36.5 29.0045 36.4993 29.0054 36.4999 29.0042C36.5003 29.0035 36.5009 29.0026 36.5016 29.0018C36.5024 29.001 36.5024 29.0013 36.5006 29.0024L37.0126 29.8613C37.664 29.473 37.6625 28.5254 37.0077 28.1388L36.4993 28.9999ZM25.4987 22.497L36.4991 28.9998L37.0079 28.139L26.0076 21.6361L25.4987 22.497ZM25.5 22.4978C25.5 22.4761 25.5027 22.4664 25.5029 22.4656C25.5031 22.4651 25.5015 22.4703 25.4962 22.4781C25.4909 22.4858 25.4845 22.4922 25.4785 22.4968C25.4728 22.5012 25.4691 22.5026 25.469 22.5027C25.469 22.5027 25.4697 22.5024 25.4712 22.502C25.4727 22.5016 25.4749 22.5012 25.4776 22.5008C25.4833 22.5 25.49 22.4998 25.4968 22.5003C25.5037 22.5008 25.5082 22.5019 25.5098 22.5023C25.5104 22.5025 25.5097 22.5023 25.5077 22.5015C25.5057 22.5007 25.502 22.4989 25.4967 22.4958L26.0096 21.6373C25.7514 21.4831 25.4005 21.4553 25.1036 21.5718C24.9443 21.6344 24.7872 21.7438 24.6715 21.9125C24.5549 22.0824 24.5 22.2839 24.5 22.4978H25.5ZM25.5 35.5003V22.4978H24.5V35.5003H25.5ZM25.5069 35.4982C25.5044 35.4996 25.5032 35.4999 25.5031 35.4999C25.503 35.5 25.5029 35.5 25.5026 35.5C25.5017 35.5 25.4996 35.4996 25.4976 35.4985C25.4957 35.4974 25.4967 35.4973 25.4984 35.5002C25.4993 35.5017 25.4999 35.5031 25.5002 35.5042C25.5005 35.5052 25.5 35.5043 25.5 35.5003H24.5C24.5 36.2989 25.3587 36.7372 26.0056 36.3649L25.5069 35.4982Z"
                                              fill="#FF5F09"></path>
                                    </svg>
                                </div>
                                <span class="circ__head__inner__text circ__inner__text-right">Watch video</span>
                            </div>
                        </div>
                        <div class="founder__image abs z1 rel"
                             style="top: 50%;left: 50%;transform: translate(-50%, -33%);">
                            <img class="x y"
                                 src="https://idearocketanimation.com/wp-content/themes/wp-idearocket/assets/imgs/IR-Cover-Founder.jpg"
                                 alt="idea_rocket_founder">
                        </div>
                    </div>
                </section>
                <section class="clients m:pt100">
                    <div class="clients__container rel pt0  pb150 m:pb200">
                        <div class="clients__container__info wrapper rel df jcc aic pb100" data-scroll
                             data-scroll-offset="100">
                            <div class="clients__title__top delay-2">
                                <h2 class="clients__title" data-scroll data-scroll-direction="horizontal"
                                    data-scroll-speed="-1">
                                    <div class=""><span class="stroke-o-2">Our</span>CLIENTS</div>
                                </h2>
                            </div>
                            <!-- <h2 class="clients__container__subtitle dn m:db">
                                <div class="oh"><span class="mdum dib enter-y delay-5">Our animation services</span></div>
                                <div class="oh ng-mt-2"><span class="dib enter-y delay-7">are trusted by</span></div>
                            </h2> -->
                            <!-- <h2 class="clients__container__subtitle db m:dn">
                                <div class="oh"><span class="mdum dib enter-y delay-5">Our animation</span></div>
                                <div class="oh ng-mt-2"><span class="mdum dib enter-y delay-7">services</span></div>
                                <div class="oh ng-mt-2"><span class="dib enter-y delay-9">are trusted by</span></div>
                            </h2> -->
                        </div>
                        <div class="our__clients dn m:df jcb aic" data-scroll data-scroll-offset="100">
                            <div class="x df fc jcc aic">
                                <div class="enter-x delay-1">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid df fr jcv aic" data-scroll
                                             data-scroll-direction="horizontal" data-scroll-speed="4">
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/nike-logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/roly.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Umbro-Logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/li-ning.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Jako-logo.png') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clients__separator wrapper x pt20 pb20 m:pt50 m:pb50">
                                    <svg class="x" width="1047" height="2" viewBox="0 0 1047 2" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.6" d="M1 1L1046 1.00009" stroke="#4E4E4E"
                                              stroke-linecap="round"
                                              stroke-dasharray="12 12"/>
                                    </svg>
                                </div>
                                <div class="enter-x-r delay-3">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid df fr jcv aic" data-scroll
                                             data-scroll-direction="horizontal" data-scroll-speed="-4">
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Jako-logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/li-ning.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Umbro-Logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/roly.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/nike-logo.png') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clients__separator wrapper x pt20 pb20 m:pt50 m:pb50">
                                    <svg class="x" width="1047" height="2" viewBox="0 0 1047 2" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.6" d="M1 1L1046 1.00009" stroke="#4E4E4E"
                                              stroke-linecap="round"
                                              stroke-dasharray="12 12"/>
                                    </svg>
                                </div>
                                <div class="enter-x-r delay-5">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid rel df fr jcv aic pb100" data-scroll
                                             data-scroll-direction="horizontal" data-scroll-speed="4">
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/nike-logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/roly.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Umbro-Logo.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/li-ning.png') }}">
                                            </div>
                                            <div class="pl80"><img
                                                    src="{{ asset('frontend/assets/imgs/Jako-logo.png') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="our__clients df m:dn jcb aic pen" style="user-select: none;" data-scroll
                             data-scroll-offset="100">
                            <div class="x df fc jcc aic">
                                <div class="enter-x delay-1">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid df fr jcv aic | js-client-grid-one" dir="rtl">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Alcatel_Lucent.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Bain_and_Company.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/bankofamerica.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/citi.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Deloitte.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/EA.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Ernst_%26_Young.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/bankofamerica.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Bain_and_Company.png') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clients__separator wrapper x pt20 pb20 m:pt50 m:pb50">
                                    <svg class="x" width="1047" height="2" viewBox="0 0 1047 2" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.6" d="M1 1L1046 1.00009" stroke="#4E4E4E"
                                              stroke-linecap="round"
                                              stroke-dasharray="12 12"/>
                                    </svg>
                                </div>
                                <div class="enter-x delay-3">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid df fr jcv aic | js-client-grid-two">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Exxon.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/ford_1.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/grey.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Lilly.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/metlife.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Exxon.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/newyorklife-web.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Lilly.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/new-york-city-knicks-2.png') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clients__separator wrapper x pt20 pb20 m:pt50 m:pb50">
                                    <svg class="x" width="1047" height="2" viewBox="0 0 1047 2" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.6" d="M1 1L1046 1.00009" stroke="#4E4E4E"
                                              stroke-linecap="round"
                                              stroke-dasharray="12 12"/>
                                    </svg>
                                </div>
                                <div class="enter-x delay-5">
                                    <div class="clients__wrap x rel">
                                        <div class="clients__grid df fr jcv aic | js-client-grid-three" dir="rtl">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/new-york-city-knicks-2.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/pngegg.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Ralph-Lauren.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/showtime.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Tiffany_Co.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Venmo.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/verizon.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/Ralph-Lauren.png') }}">
                                                </div>
                                                <div class="swiper-slide client__slide__mobile"><img
                                                        src="{{ asset('frontend/themes/wp-idearocket/assets/imgs/clients/showtime.png') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="awards rel" data-scroll data-scroll-call="scrollUpdate">
                    <div class="awards__container wrapper rel">
                        <div class="awards__container__info df jcc m:jcs aic m:pl100 pb50 m:pb0" data-scroll
                             data-scroll-offset="100">
                            <div class="awards__title__top enter-x delay-2">
                                <h2 class="awards__title" data-scroll data-scroll-direction="horizontal"
                                    data-scroll-speed="1">
                                    <div class="awards_stroke-text"><span class="">AWARDS</span></div>
                                </h2>
                            </div>
                            <h2 class="awards__subtitle">
                                <!-- <div class="oh"><span class="mdum dib enter-y delay-5">Our</span>Achievements</div> -->
                                <h6 class="stroke-award"><span>Our</span>Achievements</h6>
                            </h2>
                            <!-- <div class="oh ng-mt-2 pl0 m:pl50"><span class="dib enter-y delay-7">recognitions</span> -->
                        </div>

                    </div>
                    <div class="pb120 m:db" data-scroll data-scroll-offset="100">
                        <div class="awards__grid__wrap rel s:mb200">
                            <div class="awards__grid" data-scroll data-scroll-offset="100">
                                <div class="awards__grid__item df fc jcc ais enter-y delay-3">
                                    <div class="award__picture pb30"><img
                                            src="{{ asset('frontend/assets/imgs/award1.png') }}"
                                            alt="award"></div>
                                    <div class="award_info">
                                        <h4 class="award__title">
                                            <div class="oh"><span>PRESIDENT OF PAKISTAN</span></div>
                                        </h4>
                                        <p class="award__text"> SPECIAL MERIT EXPORT AWARD 2012 GIVEN BY</p>
                                    </div>
                                </div>
                                <div class="awards__grid__item df fc jcc ais enter-y delay-6">
                                    <div class="award__picture pb30"><img
                                            src="{{ asset('frontend/assets/imgs/award2.png') }}"
                                            alt="award"></div>
                                    <!-- <h4 class="award__title pb50">
                                        <div class="oh"><span>The communicator</span></div>
                                        <div class="oh"><span>awards</span></div>
                                    </h4>
                                    <p class="award__text"> Winner of Excellence for Santa <br> Monica Fair Housing,
                                        created for <br> the City of Santa Monica.</p> -->
                                    <div class="award_info">
                                        <h4 class="award__title">
                                            <div class="oh"><span>PRESIDENT OF PAKISTAN</span></div>
                                        </h4>
                                        <p class="award__text"> SPECIAL MERIT EXPORT AWARD 2012 GIVEN BY</p>
                                    </div>
                                </div>
                                <div class="awards__grid__item df fc jcc ais enter-y delay-9">
                                    <div class="award__picture pb30"><img
                                            src="{{ asset('frontend/assets/imgs/award3.png') }}"
                                            alt="award"></div>
                                    <div class="award_info">
                                        <h4 class="award__title">
                                            <div class="oh"><span>PRESIDENT OF PAKISTAN</span></div>
                                        </h4>
                                        <p class="award__text"> SPECIAL MERIT EXPORT AWARD 2012 GIVEN BY</p>
                                    </div>
                                    <!-- <h4 class="award__title pb50">
                                        <div class="oh"><span>AI-AP’s International</span></div>
                                        <div class="oh"><span>Motion Art Award</span>
                                        </div>
                                    </h4> -->
                                    <!-- <p class="award__text"> Winner for for its promotional web <br> video for SCANDIS, a
                                        retailer of <br> modern furniture.</p> -->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="theme_new_footer" style="background-image: url({{ asset('frontend/assets/imgs/players.png') }});">
                        <div class="logo">
                            <img src="{{ asset('frontend/assets/imgs/logo.png') }}">
                        </div>
                        <div class="icons-listing">
                            <div class="social-icon">
                                <img src="{{ asset('frontend/assets/imgs/fb.png') }}">
                            </div>
                            <div class="social-icon">
                                <img src="{{ asset('frontend/assets/imgs/insta.png') }}">
                            </div>
                            <div class="social-icon">
                                <img src="{{ asset('frontend/assets/imgs/twitter.png') }}">
                            </div>
                        </div>
                        <div class="footer_tagline">
                            <p>*Silver Star Group is proudly serving the world of sports for more than a century. An icon of
                                excellence, uniqueness and serving as a role model for soccer balls, apparel and gloves
                                manufact</p>
                        </div>
                    </div>

                </section>
            </article>
        </section>
    </main>
{{--    <div class="showreel fix top left oh x yvh dn m:db" style="z-index: 9999;">--}}
{{--        <div class="showreel__fixed | js-video-showreel desktop_show">--}}
{{--            <video id="ir-video-home" class="video__showreel" width="100%" autoplay muted playsinline>--}}
{{--                <source--}}
{{--                    data-src="//286832-886580-raikfcquaxqncofqfm.stackpathdns.com/wp-content/themes/wp-idearocket/assets/video/reel.mp4">--}}
{{--            </video>--}}
{{--        </div>--}}
{{--        <div class="ww-vv cp | js-sound-wave">--}}
{{--            <div class="wave__music rel z2">--}}
{{--                <div class="wave__bar"></div>--}}
{{--                <div class="wave__bar"></div>--}}
{{--                <div class="wave__bar"></div>--}}
{{--                <div class="wave__bar"></div>--}}
{{--            </div>--}}
{{--            <p class="soundOnOff">Sound <span class="isSound">on</span></p></div>--}}
{{--        <div class="scroll__to__scape df fc jcc aic | js-scroll-scape">--}}
{{--            <div class="arrow__bounce">--}}
{{--                <svg class="bouncing-arrow" width="21" height="10" viewBox="0 0 24 13" fill="none"--}}
{{--                     xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <path d="M1 12L12 1L23 12" stroke="#FFFFFF" stroke-width="2"></path>--}}
{{--                </svg>--}}
{{--            </div>--}}
{{--            <h2 class="mt15" style="color: #fff;">--}}
{{--                <div class="dib"><span>Scroll to escape</span></div>--}}
{{--                <div class="dib"><span> </span></div>--}}
{{--            </h2>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="galaxy" style="position: fixed; top: 0; left: 0; z-index: 1; width: 100%; height: 100%;">
        <div class="gal1"
             style="opacity: .75; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-img:     background-image: linear-gradient( 
180deg, black, #30318a);"></div> <!---- url('{{ asset('frontend/themes/wp-idearocket/assets/imgs/galaxy/1.png') }}'); --->
        <div class="gal2"
             style="z-index: 100; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-img:     background-image: linear-gradient( 
180deg, black, #30318a);"></div>
        <div class="gal3"
             style="z-index: 101; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-img:     background-image: linear-gradient( 
180deg, black, #30318a);"></div>
    </div>
    <div class="modal-video | js-modal-video">
        <div class="bg__modal__video"></div>
        <div class="modal__video__content">
            <div class="video__info">
                <div class="modal__close | js-modal-close">
                    <div class="g__btn__circ">
                        <div class="circ__head">
                            <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                 height="58"
                                 viewBox="0 0 54 58" fill="none">
                                <path
                                    d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                    stroke="#FF5F09" stroke-width="2" stroke-linecap="square" stroke-linejoin="bevel"/>
                            </svg>
                            <svg class="circ__close" width="16px" height="22px" viewBox="0 0 21 22" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path class="circ__close__1" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09" stroke-width="2"></path>
                                <path class="circ__close__2" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09" stroke-width="2"
                                      transform="translate(10.506504, 11.047541) rotate(-90.000000) translate(-10.506504, -11.047541)"></path>
                            </svg>
                        </div>
                        <span
                            class="circ__head__inner__text circ__inner__text-right circ__inner__text-close">Close</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m__newsletter | js-modal-newsletter" style="z-index: 250;">
        <div class="m__newsletter__container wrapper rel z10 x y df fc jcs aic s:pl100 s:pr100">
            <div class="m__newsletter__close x df jce aic mt50 mb50">
                <div class="m__newsletter__inner df jcc aic | js-close-m-news">
                    <div class="g__btn__circ">
                        <div class="circ__head">
                            <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                 height="58"
                                 viewBox="0 0 54 58" fill="none">
                                <path
                                    d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                    stroke="#FF5F09" stroke-width="2" stroke-linecap="square" stroke-linejoin="bevel"/>
                            </svg>
                            <svg class="circ__close" width="16px" height="22px" viewBox="0 0 21 22" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path class="circ__close__1" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09" stroke-width="2"></path>
                                <path class="circ__close__2" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09" stroke-width="2"
                                      transform="translate(10.506504, 11.047541) rotate(-90.000000) translate(-10.506504, -11.047541)"></path>
                            </svg>
                        </div>
                        <span
                            class="circ__head__inner__text circ__inner__text-right circ__inner__text-close">Close</span>
                    </div>
                </div>
            </div>
            <div class="m__newsletter__content x df fc m:fr jcc ais">
                <div class="x m:w60 rel">
                    <h2 class="m__newsletter__title dn s:db">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-title">Get awesome</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">content in</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">your inbox</span></div>
                        <div class="ng-mt-2 oh"><span class="dib | js-m-news-title">each month</span></div>
                    </h2>
                    <h2 class="m__newsletter__title db s:dn">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-title">Get</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">awesome</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">content in</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">your inbox</span></div>
                        <div class="ng-mt-2 oh"><span class="dib | js-m-news-title">each month</span></div>
                    </h2>
                    <h2 class="m__newsletter__title abs top left">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-thanks">Thanks for</span></div>
                        <div class="ng-mt-1 oh"><span class="dib | js-m-news-thanks">subscribing</span></div>
                    </h2>
                </div>
                <div class="x m:w40 mt50 m:mt20">
                    <form id="mNewsletter" class="m__newsletter__form x df fc jcc aic m:ais mb50 s:mb0" data-id="form"
                          data-url="https://idearocketanimation.com/thanks" data-ajax="true"
                          action="https://idearocketanimation.com/wp-admin/admin-ajax.php?action=subscribe">
                        <div class="newsletter__part x mb20 m:mb30 oh"><input class="js-m-news-input" type="text"
                                                                              name="name" placeholder="Firstname"
                                                                              required>
                        </div>
                        <div class="newsletter__part x mb20 m:mb30 oh"><input class="js-m-news-input" type="text"
                                                                              placeholder="Surname" required></div>
                        <div class="newsletter__part x mb60 oh"><input class="js-m-news-input" type="email" name="email"
                                                                       placeholder="e-mail" required></div>
                        <button type="submit" class="g__btn__circ formatBtn z10 | js-m-news-send">
                            <div class="circ__head">
                                <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                     height="58" viewBox="0 0 54 58" fill="none">
                                    <path
                                        d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                        stroke="#FF5F09" stroke-width="2" stroke-linecap="square"
                                        stroke-linejoin="bevel"/>
                                </svg>
                                <svg class="circ__icon-r" width="9" height="16" viewBox="0 0 9 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.828636 0.127011L0.124917 0.842339C-0.0416389 1.01165 -0.0416389 1.28618 0.124917 1.45552L6.54754 8L0.124917 14.5445C-0.0416389 14.7138 -0.0416389 14.9883 0.124917 15.1577L0.828636 15.873C0.995192 16.0423 1.26525 16.0423 1.43184 15.873L8.87508 8.30661C9.04164 8.13729 9.04164 7.86277 8.87508 7.69342L1.43184 0.127011C1.26525 -0.0423369 0.995192 -0.0423369 0.828636 0.127011Z"
                                        fill="#FF5F09"/>
                                </svg>
                                <svg class="circ__icon-r-h" width="9" height="16" viewBox="0 0 9 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.828636 0.127011L0.124917 0.842339C-0.0416389 1.01165 -0.0416389 1.28618 0.124917 1.45552L6.54754 8L0.124917 14.5445C-0.0416389 14.7138 -0.0416389 14.9883 0.124917 15.1577L0.828636 15.873C0.995192 16.0423 1.26525 16.0423 1.43184 15.873L8.87508 8.30661C9.04164 8.13729 9.04164 7.86277 8.87508 7.69342L1.43184 0.127011C1.26525 -0.0423369 0.995192 -0.0423369 0.828636 0.127011Z"
                                        fill="#FF5F09"/>
                                </svg>
                            </div>
                            <span class="circ__head__inner__text">Suscribe</span></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="flaps__pre__footer rel z1 x">
            <div class="flap__med abs x bottom left | js-m-flap-one">
                <svg class="x y db" width="1440" height="367" viewBox="0 0 1440 367" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M-397.96 20.8043C-433.069 32.5067 -487.682 94.9195 -588.132 116.374C-664.762 132.741 -846.213 283.425 -939 367H2006C1978.37 354.647 1909.65 328.967 1855.81 325.066C1788.52 320.19 1707.58 246.075 1685.15 224.621C1662.72 203.166 1610.05 198.29 1580.8 192.439C1551.54 186.588 1524.23 154.406 1510.58 124.175C1496.93 93.944 1428.66 68.5888 1386.72 68.5888C1344.79 68.5888 1295.05 101.746 1250.19 108.572C1205.33 115.398 1119.51 84.192 1069.77 68.5888C1020.03 52.9856 985.902 43.2336 926.412 57.8616C866.922 72.4896 793.78 116.374 686.504 137.828C579.227 159.282 496.332 227.546 430.016 236.323C363.7 245.1 268.127 166.109 208.637 116.374C149.148 66.639 -9.81604 71.5144 -54.677 68.5888C-99.5378 65.6632 -165.854 38.3579 -208.764 12.0275C-251.675 -14.3029 -362.852 9.10187 -397.96 20.8043Z"
                        fill="#111111" fill-opacity="0.3"/>
                </svg>
            </div>
            <div class="flap__bottom abs x bottom left | js-m-flap-two">
                <svg class="x y db" width="1440" height="205" viewBox="0 0 1440 205" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M1215.79 11.6209C1235.4 18.1577 1265.91 53.0204 1322.01 65.0045C1364.82 74.1468 1466.17 158.317 1518 205H-127C-111.566 198.1 -73.1795 183.756 -43.1097 181.577C-5.5225 178.853 39.6911 137.453 52.2202 125.469C64.7492 113.485 94.1653 110.762 110.508 107.493C126.85 104.225 142.103 86.2488 149.729 69.3621C157.355 52.4755 195.487 38.3125 218.911 38.3125C242.335 38.3125 270.117 56.8334 295.175 60.6465C320.233 64.4596 368.171 47.0282 395.952 38.3125C423.734 29.5968 442.8 24.1495 476.03 32.3205C509.259 40.4914 550.115 65.0045 610.036 76.9884C669.958 88.9723 716.261 127.104 753.303 132.006C790.346 136.909 843.731 92.7856 876.96 65.0045C910.189 37.2234 998.982 39.9467 1024.04 38.3125C1049.1 36.6783 1086.14 21.4261 1110.11 6.71834C1134.08 -7.98938 1196.18 5.08415 1215.79 11.6209Z"
                        fill="#111111" fill-opacity="0.7"/>
                </svg>
            </div>
        </div>
    </div>
    <div class="mask__transition fix top left x y pen">
        <div class="mask mask__2 abs top left x y z10 | js-mask-2"></div>
    </div>
    <div class="progress-top dn m:db fix top right y | js-progress-scroll"></div>
    <div class="loader fix x y top left | js-loader" style="display:none;">
        <div class="loader__wrapper | js-loader-content rel x y df fc jcc aic">
            <svg class="progress__logo-above abs js-logo-svg" width="150" height="150" viewBox="0 0 150 150">
                <circle style="stroke-dasharray: 439.822;stroke-dashoffset: 439.822;"
                        class="progress__logo__stroke | js-progress-svg" cx="75" cy="75" r="70"></circle>
            </svg>
            <svg style="opacity: 0 !important;" class="progress__logo abs js-logo-svg" width="150" height="150"
                 viewBox="0 0 150 150">
                <circle class="progress__logo__stroke | js-progress-last-svg" cx="75" cy="75" r="70"></circle>
            </svg>
            <svg class="logo__loader" width="185" height="31" viewBox="0 0 185 31" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd" d="M0 4H3V2.34212L0 1V4Z" fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd" d="M0 24H3V7H0V24Z" fill="white"/>
                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="5" y="0" width="19" height="26">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M5.02783 0.0300293H23.0553V25.1868H5.02783V0.0300293Z"
                          fill="white"/>
                </mask>
                <g mask="url(#mask0)">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M18.4495 20.6017C17.2979 21.7925 15.8709 22.3897 14.169 22.3897C12.4666 22.3897 11.0368 21.7925 9.88522 20.6017C8.73418 19.4077 8.16005 17.9399 8.16005 16.1928C8.16005 14.4446 8.73418 12.9741 9.88522 11.7834C11.0368 10.5894 12.4666 9.99536 14.169 9.99536C15.8709 9.99536 17.2979 10.5894 18.4495 11.7834C19.6011 12.9741 20.178 14.4446 20.178 16.1928C20.178 17.9399 19.6011 19.4077 18.4495 20.6017ZM19.9962 0.0299072V10.171C19.3489 9.22845 18.4816 8.49591 17.3888 7.97663C16.296 7.45791 15.1034 7.195 13.8083 7.195C11.3388 7.195 9.25567 8.06336 7.56487 9.799C5.87462 11.5352 5.02783 13.6663 5.02783 16.1928C5.02783 18.7417 5.86852 20.8783 7.54769 22.603C9.2263 24.3245 11.315 25.1868 13.8083 25.1868C15.0801 25.1868 16.2727 24.9043 17.3888 24.3392C18.5049 23.7708 19.375 23.0154 19.9962 22.0723V24.7974H23.0553V1.20154L19.9962 0.0299072Z"
                          fill="white"/>
                </g>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M28.2334 14.6353C28.377 13.4069 28.9838 12.3024 30.05 11.3217C31.1196 10.3415 32.3355 9.85386 33.7037 9.85386C35.1185 9.85386 36.3344 10.3066 37.3541 11.2153C38.3738 12.1262 38.9684 13.2657 39.1352 14.6353H28.2334ZM33.7741 7.19531C31.2809 7.19531 29.206 8.05168 27.5512 9.76495C25.8953 11.4749 25.0663 13.6175 25.0663 16.1926C25.0663 18.814 25.907 20.968 27.5861 22.6551C29.2647 24.3449 31.4006 25.1866 33.9908 25.1866C35.6927 25.1866 37.2045 24.7917 38.5262 24.0013C39.8446 23.2115 40.8754 22.1419 41.6202 20.7946L39.1003 19.5204C38.4763 20.4891 37.7819 21.2217 37.0144 21.7175C36.2463 22.2139 35.2504 22.4615 34.0257 22.4615C32.4906 22.4615 31.1545 21.9542 30.0146 20.9391C28.8752 19.924 28.2567 18.658 28.163 17.1471H42.2325L42.2675 16.0513C42.2675 13.5711 41.4645 11.4749 39.8562 9.76495C38.2474 8.05168 36.223 7.19531 33.7741 7.19531Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M57.3379 20.6017C56.1863 21.7924 54.7599 22.3897 53.0547 22.3897C51.3522 22.3897 49.9258 21.7924 48.7742 20.6017C47.6226 19.4077 47.0457 17.9399 47.0457 16.1928C47.0457 14.4446 47.6226 12.974 48.7742 11.7833C49.9258 10.5893 51.3522 9.99531 53.0547 9.99531C54.7599 9.99531 56.1863 10.5893 57.3379 11.7833C58.4895 12.974 59.0636 14.4446 59.0636 16.1928C59.0636 17.9399 58.4895 19.4077 57.3379 20.6017ZM58.8852 7.58767V10.2059C58.2379 9.26276 57.3673 8.52476 56.2772 7.99404C55.1844 7.46331 53.9924 7.19495 52.6972 7.19495C50.2245 7.19495 48.1441 8.06331 46.4538 9.79895C44.763 11.5351 43.9163 13.6662 43.9163 16.1928C43.9163 18.7417 44.7542 20.8782 46.4361 22.6029C48.1147 24.3244 50.2012 25.1868 52.6972 25.1868C53.9658 25.1868 55.1611 24.9042 56.2772 24.3391C57.3906 23.7708 58.2612 23.0039 58.8852 22.0346V24.7973H61.9437V7.58767H58.8852Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M100.34 7.19922C99.2063 7.19922 98.2027 7.48613 97.3288 8.0534C96.4548 8.62449 95.7831 9.40776 95.3099 10.407V7.58267H90.2114V24.8038H95.3476V17.7985C95.3476 16.101 95.7482 14.7641 96.5501 13.7878C97.352 12.8109 98.4737 12.3254 99.9157 12.3254C100.507 12.3254 100.943 12.3936 101.225 12.5327V7.30449C101.179 7.28158 101.067 7.25867 100.89 7.23304C100.711 7.21067 100.53 7.19922 100.34 7.19922Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M115.186 17.9373C114.949 18.4713 114.638 18.9366 114.249 19.331C113.856 19.7253 113.392 20.0406 112.847 20.2735C112.305 20.5059 111.739 20.6193 111.148 20.6193C110.534 20.6193 109.963 20.5059 109.43 20.2735C108.899 20.0406 108.438 19.7253 108.048 19.331C107.659 18.9366 107.348 18.4713 107.111 17.9373C106.874 17.4033 106.756 16.8219 106.756 16.1946C106.756 15.5673 106.874 14.9853 107.111 14.4519C107.348 13.9152 107.659 13.4526 108.048 13.055C108.438 12.6606 108.899 12.3481 109.43 12.1157C109.963 11.8828 110.534 11.7666 111.148 11.7666C111.739 11.7666 112.305 11.8828 112.847 12.1157C113.392 12.3481 113.856 12.6606 114.249 13.055C114.638 13.4526 114.949 13.9152 115.186 14.4519C115.422 14.9853 115.541 15.5673 115.541 16.1946C115.541 16.8219 115.422 17.4033 115.186 17.9373ZM117.914 9.74517C117.063 8.9308 116.06 8.29207 114.903 7.8268C113.747 7.36426 112.495 7.13135 111.148 7.13135C109.801 7.13135 108.55 7.36426 107.394 7.8268C106.237 8.29207 105.234 8.9308 104.383 9.74517C103.535 10.5573 102.866 11.5168 102.382 12.6208C101.9 13.7248 101.657 14.9144 101.657 16.1946C101.657 17.4715 101.9 18.6639 102.382 19.7679C102.866 20.8724 103.535 21.8292 104.383 22.6435C105.234 23.4552 106.237 24.0966 107.394 24.5597C108.55 25.025 109.801 25.2579 111.148 25.2579C112.495 25.2579 113.747 25.025 114.903 24.5597C116.06 24.0966 117.063 23.4552 117.914 22.6435C118.762 21.8292 119.431 20.8724 119.915 19.7679C120.397 18.6639 120.639 17.4715 120.639 16.1946C120.639 14.9144 120.397 13.7248 119.915 12.6208C119.431 11.5168 118.762 10.5573 117.914 9.74517Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M132.054 20.6566C131.37 20.6566 130.739 20.5458 130.159 20.3244C129.579 20.1029 129.081 19.7964 128.653 19.402C128.23 19.0044 127.898 18.5364 127.661 17.9882C127.425 17.4433 127.307 16.8444 127.307 16.1942C127.307 15.5446 127.425 14.9457 127.661 14.3975C127.898 13.8526 128.23 13.3818 128.653 12.9869C129.081 12.5926 129.579 12.2827 130.159 12.0618C130.739 11.8431 131.37 11.7324 132.054 11.7324C133.377 11.7324 134.64 12.1497 135.843 12.9869V8.20983C135.205 7.83783 134.505 7.56565 133.735 7.3922C132.968 7.21602 132.206 7.13147 131.451 7.13147C130.081 7.13147 128.823 7.35565 127.678 7.80947C126.534 8.26383 125.548 8.89656 124.722 9.70874C123.895 10.5231 123.252 11.4826 122.793 12.5866C122.332 13.6906 122.101 14.8917 122.101 16.1942C122.101 17.5431 122.332 18.7747 122.793 19.8902C123.252 21.0057 123.895 21.9564 124.722 22.7484C125.548 23.5377 126.528 24.1535 127.661 24.5964C128.795 25.036 130.035 25.258 131.381 25.258C132.137 25.258 132.898 25.1697 133.666 24.9967C134.433 24.8206 135.171 24.5369 135.88 24.142V19.3311C135.336 19.7255 134.733 20.0462 134.073 20.2906C133.412 20.5344 132.74 20.6566 132.054 20.6566Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M154.975 7.58246H148.812L143.004 13.7194V2.38537L137.868 0.418457V24.8035H143.004V18.809L143.89 17.9374L149.239 24.8035H155.437L147.751 15.2183L154.975 7.58246Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M159.483 14.2757C159.529 13.8584 159.665 13.4641 159.89 13.0921C160.115 12.7201 160.403 12.3824 160.757 12.0819C161.112 11.7775 161.513 11.5392 161.96 11.3663C162.41 11.1901 162.894 11.105 163.414 11.105C164.475 11.105 165.36 11.4006 166.07 11.9935C166.779 12.5864 167.192 13.3473 167.31 14.2757H159.483ZM169.894 9.70862C169.115 8.89698 168.178 8.26371 167.079 7.80989C165.98 7.35553 164.772 7.13135 163.448 7.13135C162.15 7.13135 160.951 7.35553 159.855 7.80989C158.756 8.26371 157.799 8.89698 156.986 9.70862C156.169 10.523 155.532 11.4824 155.073 12.5864C154.612 13.691 154.381 14.8915 154.381 16.1946C154.381 17.5173 154.612 18.7375 155.073 19.8535C155.532 20.969 156.169 21.9279 156.986 22.7313C157.799 23.5321 158.774 24.1533 159.907 24.5963C161.04 25.0364 162.291 25.2579 163.662 25.2579C165.574 25.2579 167.22 24.8521 168.602 24.0372C169.983 23.2223 171.039 22.1068 171.771 20.6908L167.417 18.9819C167.013 19.6348 166.531 20.1453 165.963 20.5173C165.398 20.8893 164.628 21.0737 163.662 21.0737C163.117 21.0737 162.603 20.9859 162.122 20.8124C161.637 20.6395 161.199 20.3952 160.81 20.0804C160.42 19.7679 160.109 19.4019 159.872 18.9819C159.636 18.5646 159.495 18.0988 159.445 17.5883H172.302C172.328 17.4715 172.345 17.3101 172.356 17.1001C172.368 16.8895 172.374 16.6343 172.374 16.3332C172.374 14.9853 172.155 13.7477 171.719 12.6208C171.281 11.4939 170.672 10.523 169.894 9.70862Z"
                      fill="white"/>
                <mask id="mask1" mask-type="alpha" maskUnits="userSpaceOnUse" x="174" y="2" width="10" height="24">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M174.15 2.52979H183.713V25.2234H174.15V2.52979Z"
                          fill="white"/>
                </mask>
                <g mask="url(#mask1)">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M174.15 11.7665V19.158C174.15 21.0851 174.683 22.578 175.745 23.6367C176.809 24.6927 178.26 25.2234 180.103 25.2234C180.927 25.2234 181.637 25.1525 182.228 25.0134C182.816 24.8743 183.313 24.7096 183.713 24.5252V20.1343C183.431 20.3187 183.125 20.4523 182.793 20.5347C182.461 20.6138 182.133 20.6563 181.801 20.6563C180.951 20.6563 180.319 20.4578 179.907 20.0629C179.494 19.6685 179.286 19.0047 179.286 18.0763V11.7665H183.713V7.58233H179.286V4.48306L174.187 2.52979H174.15V7.58233V11.7665Z"
                          fill="white"/>
                </g>
                <g class="js-l-rocketJump">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M76.7881 22.0259C76.3846 21.554 75.5811 21.554 75.1776 22.0259L72.6628 24.967C72.3857 25.291 72.3857 25.7513 72.6628 26.0753L75.2419 29.9791C75.6221 30.4231 76.3442 30.4231 76.7238 29.9791L79.303 26.0753C79.58 25.7513 79.58 25.291 79.303 24.967L76.7881 22.0259Z"
                          fill="#FF5F09"/>
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M77.779 11.1053L76.2705 12.9615C76.1231 13.1426 75.8427 13.1426 75.6952 12.9615L74.1868 11.1053C74.0793 10.9728 74.0793 10.7851 74.1868 10.6526L75.6952 8.79641C75.8427 8.61532 76.1231 8.61532 76.2705 8.79641L77.779 10.6526C77.8865 10.7851 77.8865 10.9728 77.779 11.1053ZM84.3704 11.1168L76.8934 3.38986C76.3907 2.87005 75.575 2.87005 75.0723 3.38986L67.5959 11.1168C67.193 11.5324 66.9669 12.097 66.9669 12.6855V22.2871C66.9669 23.407 68.2232 24.026 69.0656 23.3219L75.1715 18.2131C75.6448 17.8177 76.3209 17.8177 76.7942 18.2131L82.9007 23.3219C83.7425 24.026 84.9988 23.407 84.9988 22.2871V12.6855C84.9988 12.097 84.7727 11.5324 84.3704 11.1168Z"
                          fill="#FF5F09"/>
                </g>
            </svg>
        </div>
    </div>
@endsection
