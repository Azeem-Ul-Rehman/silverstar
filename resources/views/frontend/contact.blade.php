@extends('frontend.layout.app')
@section('title','Contact Us')
@push('css')
    {{--    <style>--}}
    {{--        body {--}}
    {{--            overflow-y: scroll;--}}
    {{--            overflow-x: hidden;--}}
    {{--        }--}}
    {{--    </style>--}}
@endpush
@section('content')
    @include('frontend.layout.sticky')

    <main class="rel z10 contc | js-smooth fonts-new">
        <section data-router-wrapper>
            <article data-router-view="contact">
                <section class="contact rel">
                    <div class="contact__container wrapper rel z5">
                        <div class="contact__container__content df fc m:fr ais" data-scroll>
                            <div class="contact__select df m:dn fr ais pb80">
                                <div class="select__button mr50 enter-y delay-4">
                                    <h5 class="select__title select-active | js-select-form-btn js-project-form-btn"
                                        data-form-id="projects">
                                        <div class="oh"><span class="dib">Get in Touch</span></div>
                                    </h5>
                                </div>
                                {{--                                <div class="select__button enter-y delay-6">--}}
                                {{--                                    <h5 class="select__title join | js-select-form-btn js-join-form-btn"--}}
                                {{--                                        data-form-id="jobs">--}}
                                {{--                                        <div class="oh"><span class="dib">Join our team</span></div>--}}
                                {{--                                    </h5>--}}
                                {{--                                </div>--}}
                            </div>
                            <div class="x m:w50 pt150 m-np">
                                <div class="contact__title__wrap rel lg:ml100">
                                    <h2 class="contact__big__title | js-p-h2">
                                        <div class="stroke-o-1 ng-mt-2 ng-mt-m-1 oh"> <span
                                                class="dib enter-y delay-5 mbl-mt-0"> <span
                                                    class="dib | js-p-title resp-font">Get in touch</span> </span></div>
                                        <!-- <div class="stroke-o-1 ng-mt-2 ng-mt-m-1 oh"> <span class="dib enter-y delay-7"> <span
                                              class="dib | js-p-title">take off?</span> </span></div> -->
                                        <div class="ng-mt-2 ng-mt-m-1 oh "> <span class="dib enter-y delay-9 mbl-mt-0"> <span
                                                    class="dib | js-p-title today">With us today. </span> </span></div>
                                    </h2>
                                    <h2 class="contact__big__title abs top left | js-j-h2" style="opacity: 0;">
                                        <div class="stroke-o-1 ng-mt-2 ng-mt-m-1 oh"> <span
                                                class="dib enter-y delay-2"> <span
                                                    class="dib | js-j-title">Interested</span> </span></div>
                                        <div class="stroke-o-1 ng-mt-2 ng-mt-m-1 oh"> <span
                                                class="dib enter-y delay-3"> <span
                                                    class="dib | js-j-title">in working</span> </span></div>
                                        <div class="stroke-o-1 ng-mt-2 ng-mt-m-1 oh"> <span
                                                class="dib enter-y delay-4"> <span
                                                    class="dib | js-j-title">with us?</span> </span></div>
                                        <div class="ng-mt-2 ng-mt-m-1 oh"> <span class="dib enter-y delay-5"> <span
                                                    class="dib | js-j-title">Send us a</span> </span></div>
                                        <div class="ng-mt-2 ng-mt-m-1 oh"> <span class="dib enter-y delay-6"> <span
                                                    class="dib | js-j-title">note!</span> </span></div>
                                    </h2>
                                </div>
                            </div>
                            <div class="x m:w50">
                                <div class="contact__forms df fc ais">
                                    <div class="contact__select dn m:df fr ais pb80">
                                        <div class="select__button mr50 enter-y delay-4">
                                            <h5 class="select__title select-active | js-select-form-btn js-project-form-btn"
                                                data-form-id="projects">
                                                <div class="oh"><span class="dib">Get in Touch</span></div>
                                            </h5>
                                        </div>
                                        {{--                                        <div class="select__button enter-y delay-6">--}}
                                        {{--                                            <h5 class="select__title join | js-select-form-btn js-join-form-btn"--}}
                                        {{--                                                data-form-id="jobs">--}}
                                        {{--                                                <div class="oh"><span class="dib">Join our team</span></div>--}}
                                        {{--                                            </h5>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="project__form__container rel">
                                        <form
                                              method="post" action="{{ route('contacts.store') }}"
                                              enctype="multipart/form-data"
                                              class="project__form df fc jcc aic m:ais mt50 m:mt0 enter-y delay-3 | js-p-form">
                                            @csrf
                                            <div class="project__part mb20 m:mb30 df fc m:fr jcb ais m:aic oh">
                                                <input
                                                    class="dib input__half mb20 m:mb0 mr0 m:mr30 | js-p-input @error('full_name') is-invalid @enderror"
                                                    name="full_name"
                                                    value="{{ old('full_name') }}" autocomplete="full_name"
                                                    type="text"
                                                    placeholder="Full name">
                                                @error('full_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <input class="dib input__half | js-p-input @error('phone_number') is-invalid @enderror"
                                                       value="{{ old('phone_number') }}" autocomplete="phone_number"  type="text" name="phone_number"
                                                       placeholder="Phone Number">
                                                @error('phone_number')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="project__part mb20 m:mb30 df fc m:fr jcb ais m:aic oh">
                                                <input class="dib input__half mb20 m:mb0 mr0 m:mr30 | js-p-input @error('email') is-invalid @enderror"
                                                       value="{{ old('email') }}" autocomplete="email"  type="email" name="email" placeholder="Enter Email">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <input class="dib input__half | js-p-input" type="text"
                                                       name="company_name" placeholder="Company (optional)">
                                            </div>
                                            <div class="project__part x mb50 oh">
                                                <input class="dib x | js-p-input @error('message') is-invalid @enderror"
                                                value="{{ old('message') }}" autocomplete="message"  type="text" name="message" placeholder="Message">>
                                                @error('message')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="submit_btn_contact">
                                                <button type="submit">Submit</button>
                                            </div>
                                        </form>
                                        {{--                                        <form id="joinForm"--}}
                                        {{--                                              class="project__form abs top left df fc jcc aic m:ais mt150 m:mt0 | js-j-form"--}}
                                        {{--                                              style="opacity: 0; pointer-events: none;" data-id="careers"--}}
                                        {{--                                              data-url="https://idearocketanimation.com/thanks" data-ajax="true"--}}
                                        {{--                                              action="https://idearocketanimation.com/wp-admin/admin-ajax.php?action=form">--}}
                                        {{--                                            <input type="hidden"--}}
                                        {{--                                                   value="careers" name="type"/>--}}
                                        {{--                                            <div class="project__part mb20 m:mb30 df fc m:fr jcb ais m:aic oh">--}}
                                        {{--                                                <input class="dib input__half mb20 m:mb0 mr0 m:mr30 | js-j-input"--}}
                                        {{--                                                    type="text"--}}
                                        {{--                                                    name="name"--}}
                                        {{--                                                    placeholder="Full name" required>--}}
                                        {{--                                                <input--}}
                                        {{--                                                    class="dib input__half | js-j-input" type="text"--}}
                                        {{--                                                    name="phone" placeholder="Telephone (optional)">--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="project__part mb20 m:mb30 df fc m:fr jcb ais m:aic oh">--}}
                                        {{--                                                <input class="dib input__half mb20 m:mb0 mr0 m:mr30 | js-j-input"--}}
                                        {{--                                                    type="email"--}}
                                        {{--                                                    name="email"--}}
                                        {{--                                                    placeholder="E-mail" required>--}}
                                        {{--                                                <input class="dib input__half | js-j-input" type="text"--}}
                                        {{--                                                    name="link" placeholder="Link to creative portfolio">--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="project__part x mb80 oh">--}}
                                        {{--                                                <input class="dib x | js-j-input"--}}
                                        {{--                                                       type="text" name="message"--}}
                                        {{--                                                       placeholder="Message">--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <button class="g__btn__circ formatBtn z10 | js-j-btn"--}}
                                        {{--                                                    type="submit">--}}
                                        {{--                                                <div class="circ__head">--}}
                                        {{--                                                    <svg class="circ__path abs top left"--}}
                                        {{--                                                         xmlns="http://www.w3.org/2000/svg"--}}
                                        {{--                                                         width="54" height="58" viewBox="0 0 54 58" fill="none">--}}
                                        {{--                                                        <path--}}
                                        {{--                                                            d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"--}}
                                        {{--                                                            stroke="#FF5F09" stroke-width="2" stroke-linecap="square"--}}
                                        {{--                                                            stroke-linejoin="bevel"/>--}}
                                        {{--                                                    </svg>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <span class="circ__head__inner__text">Get started</span>--}}
                                        {{--                                            </button>--}}
                                        {{--                                        </form>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact__orbitmoon dn m:db z1" style="position: absolute; top: 15%; left: -25%;"><img
                            src="{{ asset('frontend/assets/imgs/Orbit.png') }}"
                            alt="orbit-moon"> <img class="rotating__orbit abs top left"
                                                   src="{{ asset('frontend/assets/imgs/Contact-ball.png') }}"
                                                   alt="orbit-moon"></div>
                    <div class="flaps__pre__footer rel x pen" style="height: 35rem;">
                    </div>
                    @include('frontend.layout.footer')
                </section>


            </article>
        </section>
    </main>
    <div class="showreel fix top left oh x yvh dn m:db" style="z-index: 9999;">
        <div class="showreel__fixed | js-video-showreel desktop_show"></div>
        <div class="ww-vv cp | js-sound-wave">
            <div class="wave__music rel z2">
                <div class="wave__bar"></div>
                <div class="wave__bar"></div>
                <div class="wave__bar"></div>
                <div class="wave__bar"></div>
            </div>
            <p class="soundOnOff">Sound <span class="isSound">on</span></p>
        </div>
    </div>
    <div class="galaxy" style="position: fixed; top: 0; left: 0; z-index: 1; width: 100%; height: 100%;">
        <div class="gal1"
             style="opacity: .75; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: url({{ asset('frontend/themes/wp-idearocket/assets/imgs/galaxy/1.png') }});">
        </div>
        <div class="gal2"
             style="z-index: 100; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: url({{ asset('frontend/themes/wp-idearocket/assets/imgs/galaxy/2.png') }})">
        </div>
        <div class="gal3"
             style="z-index: 101; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: url({{ asset('frontend/themes/wp-idearocket/assets/imgs/galaxy/3.png') }});">
        </div>
    </div>
    <div class="modal-video | js-modal-video">
        <div class="bg__modal__video"></div>
        <div class="modal__video__content">
            <div class="video__info">
                <div class="modal__close | js-modal-close">
                    <div class="g__btn__circ">
                        <div class="circ__head">
                            <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                 height="58" viewBox="0 0 54 58" fill="none">
                                <path
                                    d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                    stroke="#FF5F09" stroke-width="2" stroke-linecap="square" stroke-linejoin="bevel"/>
                            </svg>
                            <svg class="circ__close" width="16px" height="22px" viewBox="0 0 21 22" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path class="circ__close__1" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09"
                                      stroke-width="2"></path>
                                <path class="circ__close__2" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09"
                                      stroke-width="2"
                                      transform="translate(10.506504, 11.047541) rotate(-90.000000) translate(-10.506504, -11.047541)">
                                </path>
                            </svg>
                        </div>
                        <span
                            class="circ__head__inner__text circ__inner__text-right circ__inner__text-close">Close</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m__newsletter | js-modal-newsletter" style="z-index: 250;">
        <div class="m__newsletter__container wrapper rel z10 x y df fc jcs aic s:pl100 s:pr100">
            <div class="m__newsletter__close x df jce aic mt50 mb50">
                <div class="m__newsletter__inner df jcc aic | js-close-m-news">
                    <div class="g__btn__circ">
                        <div class="circ__head">
                            <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg" width="54"
                                 height="58" viewBox="0 0 54 58" fill="none">
                                <path
                                    d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                    stroke="#FF5F09" stroke-width="2" stroke-linecap="square" stroke-linejoin="bevel"/>
                            </svg>
                            <svg class="circ__close" width="16px" height="22px" viewBox="0 0 21 22" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path class="circ__close__1" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09"
                                      stroke-width="2"></path>
                                <path class="circ__close__2" d="M0.506503868,1.04754092 L20.5065039,21.0475409"
                                      stroke="#FF5F09"
                                      stroke-width="2"
                                      transform="translate(10.506504, 11.047541) rotate(-90.000000) translate(-10.506504, -11.047541)">
                                </path>
                            </svg>
                        </div>
                        <span
                            class="circ__head__inner__text circ__inner__text-right circ__inner__text-close">Close</span>
                    </div>
                </div>
            </div>
            <div class="m__newsletter__content x df fc m:fr jcc ais">
                <div class="x m:w60 rel">
                    <h2 class="m__newsletter__title dn s:db">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-title">Get awesome</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">content in</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">your inbox</span></div>
                        <div class="ng-mt-2 oh"><span class="dib | js-m-news-title">each month</span></div>
                    </h2>
                    <h2 class="m__newsletter__title db s:dn">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-title">Get</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">awesome</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">content in</span></div>
                        <div class="stroke-o-1 ng-mt-2 oh"><span class="dib | js-m-news-title">your inbox</span></div>
                        <div class="ng-mt-2 oh"><span class="dib | js-m-news-title">each month</span></div>
                    </h2>
                    <h2 class="m__newsletter__title abs top left">
                        <div class="stroke-o-1 oh"><span class="dib | js-m-news-thanks">Thanks for</span></div>
                        <div class="ng-mt-1 oh"><span class="dib | js-m-news-thanks">subscribing</span></div>
                    </h2>
                </div>
                <div class="x m:w40 mt50 m:mt20">
                    <form id="mNewsletter" class="m__newsletter__form x df fc jcc aic m:ais mb50 s:mb0" data-id="form"
                          data-url="https://idearocketanimation.com/thanks" data-ajax="true"
                          action="https://idearocketanimation.com/wp-admin/admin-ajax.php?action=subscribe">
                        <div class="newsletter__part x mb20 m:mb30 oh"><input class="js-m-news-input" type="text"
                                                                              name="name"
                                                                              placeholder="Firstname" required></div>
                        <div class="newsletter__part x mb20 m:mb30 oh"><input class="js-m-news-input" type="text"
                                                                              placeholder="Surname" required></div>
                        <div class="newsletter__part x mb60 oh"><input class="js-m-news-input" type="email" name="email"
                                                                       placeholder="e-mail" required></div>
                        <button type="submit"
                                class="g__btn__circ formatBtn z10 | js-m-news-send">
                            <div class="circ__head">
                                <svg class="circ__path abs top left" xmlns="http://www.w3.org/2000/svg"
                                     width="54" height="58" viewBox="0 0 54 58" fill="none">
                                    <path
                                        d="M50.2177 46.7594C46.5064 51.3227 41.4604 54.5862 35.8005 56.0837C30.1406 57.5813 24.1563 57.2364 18.7024 55.0982C13.2484 52.96 8.60381 49.1379 5.43197 44.1779C2.26012 39.2179 0.723279 33.3736 1.04095 27.4799C1.35862 21.5861 3.51456 15.9444 7.20082 11.3605C10.8871 6.77661 15.9151 3.48503 21.5667 1.95592C27.2183 0.426809 33.2044 0.738387 38.67 2.84615C44.1356 4.95391 48.801 8.75004 52 13.6923"
                                        stroke="#FF5F09" stroke-width="2" stroke-linecap="square"
                                        stroke-linejoin="bevel"/>
                                </svg>
                                <svg class="circ__icon-r" width="9" height="16" viewBox="0 0 9 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.828636 0.127011L0.124917 0.842339C-0.0416389 1.01165 -0.0416389 1.28618 0.124917 1.45552L6.54754 8L0.124917 14.5445C-0.0416389 14.7138 -0.0416389 14.9883 0.124917 15.1577L0.828636 15.873C0.995192 16.0423 1.26525 16.0423 1.43184 15.873L8.87508 8.30661C9.04164 8.13729 9.04164 7.86277 8.87508 7.69342L1.43184 0.127011C1.26525 -0.0423369 0.995192 -0.0423369 0.828636 0.127011Z"
                                        fill="#FF5F09"/>
                                </svg>
                                <svg class="circ__icon-r-h" width="9" height="16" viewBox="0 0 9 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.828636 0.127011L0.124917 0.842339C-0.0416389 1.01165 -0.0416389 1.28618 0.124917 1.45552L6.54754 8L0.124917 14.5445C-0.0416389 14.7138 -0.0416389 14.9883 0.124917 15.1577L0.828636 15.873C0.995192 16.0423 1.26525 16.0423 1.43184 15.873L8.87508 8.30661C9.04164 8.13729 9.04164 7.86277 8.87508 7.69342L1.43184 0.127011C1.26525 -0.0423369 0.995192 -0.0423369 0.828636 0.127011Z"
                                        fill="#FF5F09"/>
                                </svg>
                            </div>
                            <span class="circ__head__inner__text">Suscribe</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="flaps__pre__footer rel z1 x">

        </div>
    </div>
    <div class="mask__transition fix top left x y pen">
        <div class="mask mask__2 abs top left x y z10 | js-mask-2"></div>
    </div>
    <div class="progress-top dn m:db fix top right y | js-progress-scroll"></div>
    <div class="loader fix x y top left | js-loader" style="display:none;">
        <div class="loader__wrapper | js-loader-content rel x y df fc jcc aic">
            <svg
                class="progress__logo-above abs js-logo-svg" width="150" height="150" viewBox="0 0 150 150">
                <circle style="stroke-dasharray: 439.822;stroke-dashoffset: 439.822;"
                        class="progress__logo__stroke | js-progress-svg" cx="75" cy="75" r="70"></circle>
            </svg>
            <svg style="opacity: 0 !important;" class="progress__logo abs js-logo-svg" width="150" height="150"
                 viewBox="0 0 150 150">
                <circle class="progress__logo__stroke | js-progress-last-svg" cx="75" cy="75" r="70"></circle>
            </svg>
            <svg class="logo__loader" width="185" height="31" viewBox="0 0 185 31" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd" d="M0 4H3V2.34212L0 1V4Z" fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd" d="M0 24H3V7H0V24Z" fill="white"/>
                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="5" y="0" width="19" height="26">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M5.02783 0.0300293H23.0553V25.1868H5.02783V0.0300293Z"
                          fill="white"/>
                </mask>
                <g mask="url(#mask0)">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M18.4495 20.6017C17.2979 21.7925 15.8709 22.3897 14.169 22.3897C12.4666 22.3897 11.0368 21.7925 9.88522 20.6017C8.73418 19.4077 8.16005 17.9399 8.16005 16.1928C8.16005 14.4446 8.73418 12.9741 9.88522 11.7834C11.0368 10.5894 12.4666 9.99536 14.169 9.99536C15.8709 9.99536 17.2979 10.5894 18.4495 11.7834C19.6011 12.9741 20.178 14.4446 20.178 16.1928C20.178 17.9399 19.6011 19.4077 18.4495 20.6017ZM19.9962 0.0299072V10.171C19.3489 9.22845 18.4816 8.49591 17.3888 7.97663C16.296 7.45791 15.1034 7.195 13.8083 7.195C11.3388 7.195 9.25567 8.06336 7.56487 9.799C5.87462 11.5352 5.02783 13.6663 5.02783 16.1928C5.02783 18.7417 5.86852 20.8783 7.54769 22.603C9.2263 24.3245 11.315 25.1868 13.8083 25.1868C15.0801 25.1868 16.2727 24.9043 17.3888 24.3392C18.5049 23.7708 19.375 23.0154 19.9962 22.0723V24.7974H23.0553V1.20154L19.9962 0.0299072Z"
                          fill="white"/>
                </g>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M28.2334 14.6353C28.377 13.4069 28.9838 12.3024 30.05 11.3217C31.1196 10.3415 32.3355 9.85386 33.7037 9.85386C35.1185 9.85386 36.3344 10.3066 37.3541 11.2153C38.3738 12.1262 38.9684 13.2657 39.1352 14.6353H28.2334ZM33.7741 7.19531C31.2809 7.19531 29.206 8.05168 27.5512 9.76495C25.8953 11.4749 25.0663 13.6175 25.0663 16.1926C25.0663 18.814 25.907 20.968 27.5861 22.6551C29.2647 24.3449 31.4006 25.1866 33.9908 25.1866C35.6927 25.1866 37.2045 24.7917 38.5262 24.0013C39.8446 23.2115 40.8754 22.1419 41.6202 20.7946L39.1003 19.5204C38.4763 20.4891 37.7819 21.2217 37.0144 21.7175C36.2463 22.2139 35.2504 22.4615 34.0257 22.4615C32.4906 22.4615 31.1545 21.9542 30.0146 20.9391C28.8752 19.924 28.2567 18.658 28.163 17.1471H42.2325L42.2675 16.0513C42.2675 13.5711 41.4645 11.4749 39.8562 9.76495C38.2474 8.05168 36.223 7.19531 33.7741 7.19531Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M57.3379 20.6017C56.1863 21.7924 54.7599 22.3897 53.0547 22.3897C51.3522 22.3897 49.9258 21.7924 48.7742 20.6017C47.6226 19.4077 47.0457 17.9399 47.0457 16.1928C47.0457 14.4446 47.6226 12.974 48.7742 11.7833C49.9258 10.5893 51.3522 9.99531 53.0547 9.99531C54.7599 9.99531 56.1863 10.5893 57.3379 11.7833C58.4895 12.974 59.0636 14.4446 59.0636 16.1928C59.0636 17.9399 58.4895 19.4077 57.3379 20.6017ZM58.8852 7.58767V10.2059C58.2379 9.26276 57.3673 8.52476 56.2772 7.99404C55.1844 7.46331 53.9924 7.19495 52.6972 7.19495C50.2245 7.19495 48.1441 8.06331 46.4538 9.79895C44.763 11.5351 43.9163 13.6662 43.9163 16.1928C43.9163 18.7417 44.7542 20.8782 46.4361 22.6029C48.1147 24.3244 50.2012 25.1868 52.6972 25.1868C53.9658 25.1868 55.1611 24.9042 56.2772 24.3391C57.3906 23.7708 58.2612 23.0039 58.8852 22.0346V24.7973H61.9437V7.58767H58.8852Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M100.34 7.19922C99.2063 7.19922 98.2027 7.48613 97.3288 8.0534C96.4548 8.62449 95.7831 9.40776 95.3099 10.407V7.58267H90.2114V24.8038H95.3476V17.7985C95.3476 16.101 95.7482 14.7641 96.5501 13.7878C97.352 12.8109 98.4737 12.3254 99.9157 12.3254C100.507 12.3254 100.943 12.3936 101.225 12.5327V7.30449C101.179 7.28158 101.067 7.25867 100.89 7.23304C100.711 7.21067 100.53 7.19922 100.34 7.19922Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M115.186 17.9373C114.949 18.4713 114.638 18.9366 114.249 19.331C113.856 19.7253 113.392 20.0406 112.847 20.2735C112.305 20.5059 111.739 20.6193 111.148 20.6193C110.534 20.6193 109.963 20.5059 109.43 20.2735C108.899 20.0406 108.438 19.7253 108.048 19.331C107.659 18.9366 107.348 18.4713 107.111 17.9373C106.874 17.4033 106.756 16.8219 106.756 16.1946C106.756 15.5673 106.874 14.9853 107.111 14.4519C107.348 13.9152 107.659 13.4526 108.048 13.055C108.438 12.6606 108.899 12.3481 109.43 12.1157C109.963 11.8828 110.534 11.7666 111.148 11.7666C111.739 11.7666 112.305 11.8828 112.847 12.1157C113.392 12.3481 113.856 12.6606 114.249 13.055C114.638 13.4526 114.949 13.9152 115.186 14.4519C115.422 14.9853 115.541 15.5673 115.541 16.1946C115.541 16.8219 115.422 17.4033 115.186 17.9373ZM117.914 9.74517C117.063 8.9308 116.06 8.29207 114.903 7.8268C113.747 7.36426 112.495 7.13135 111.148 7.13135C109.801 7.13135 108.55 7.36426 107.394 7.8268C106.237 8.29207 105.234 8.9308 104.383 9.74517C103.535 10.5573 102.866 11.5168 102.382 12.6208C101.9 13.7248 101.657 14.9144 101.657 16.1946C101.657 17.4715 101.9 18.6639 102.382 19.7679C102.866 20.8724 103.535 21.8292 104.383 22.6435C105.234 23.4552 106.237 24.0966 107.394 24.5597C108.55 25.025 109.801 25.2579 111.148 25.2579C112.495 25.2579 113.747 25.025 114.903 24.5597C116.06 24.0966 117.063 23.4552 117.914 22.6435C118.762 21.8292 119.431 20.8724 119.915 19.7679C120.397 18.6639 120.639 17.4715 120.639 16.1946C120.639 14.9144 120.397 13.7248 119.915 12.6208C119.431 11.5168 118.762 10.5573 117.914 9.74517Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M132.054 20.6566C131.37 20.6566 130.739 20.5458 130.159 20.3244C129.579 20.1029 129.081 19.7964 128.653 19.402C128.23 19.0044 127.898 18.5364 127.661 17.9882C127.425 17.4433 127.307 16.8444 127.307 16.1942C127.307 15.5446 127.425 14.9457 127.661 14.3975C127.898 13.8526 128.23 13.3818 128.653 12.9869C129.081 12.5926 129.579 12.2827 130.159 12.0618C130.739 11.8431 131.37 11.7324 132.054 11.7324C133.377 11.7324 134.64 12.1497 135.843 12.9869V8.20983C135.205 7.83783 134.505 7.56565 133.735 7.3922C132.968 7.21602 132.206 7.13147 131.451 7.13147C130.081 7.13147 128.823 7.35565 127.678 7.80947C126.534 8.26383 125.548 8.89656 124.722 9.70874C123.895 10.5231 123.252 11.4826 122.793 12.5866C122.332 13.6906 122.101 14.8917 122.101 16.1942C122.101 17.5431 122.332 18.7747 122.793 19.8902C123.252 21.0057 123.895 21.9564 124.722 22.7484C125.548 23.5377 126.528 24.1535 127.661 24.5964C128.795 25.036 130.035 25.258 131.381 25.258C132.137 25.258 132.898 25.1697 133.666 24.9967C134.433 24.8206 135.171 24.5369 135.88 24.142V19.3311C135.336 19.7255 134.733 20.0462 134.073 20.2906C133.412 20.5344 132.74 20.6566 132.054 20.6566Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M154.975 7.58246H148.812L143.004 13.7194V2.38537L137.868 0.418457V24.8035H143.004V18.809L143.89 17.9374L149.239 24.8035H155.437L147.751 15.2183L154.975 7.58246Z"
                      fill="white"/>
                <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                      d="M159.483 14.2757C159.529 13.8584 159.665 13.4641 159.89 13.0921C160.115 12.7201 160.403 12.3824 160.757 12.0819C161.112 11.7775 161.513 11.5392 161.96 11.3663C162.41 11.1901 162.894 11.105 163.414 11.105C164.475 11.105 165.36 11.4006 166.07 11.9935C166.779 12.5864 167.192 13.3473 167.31 14.2757H159.483ZM169.894 9.70862C169.115 8.89698 168.178 8.26371 167.079 7.80989C165.98 7.35553 164.772 7.13135 163.448 7.13135C162.15 7.13135 160.951 7.35553 159.855 7.80989C158.756 8.26371 157.799 8.89698 156.986 9.70862C156.169 10.523 155.532 11.4824 155.073 12.5864C154.612 13.691 154.381 14.8915 154.381 16.1946C154.381 17.5173 154.612 18.7375 155.073 19.8535C155.532 20.969 156.169 21.9279 156.986 22.7313C157.799 23.5321 158.774 24.1533 159.907 24.5963C161.04 25.0364 162.291 25.2579 163.662 25.2579C165.574 25.2579 167.22 24.8521 168.602 24.0372C169.983 23.2223 171.039 22.1068 171.771 20.6908L167.417 18.9819C167.013 19.6348 166.531 20.1453 165.963 20.5173C165.398 20.8893 164.628 21.0737 163.662 21.0737C163.117 21.0737 162.603 20.9859 162.122 20.8124C161.637 20.6395 161.199 20.3952 160.81 20.0804C160.42 19.7679 160.109 19.4019 159.872 18.9819C159.636 18.5646 159.495 18.0988 159.445 17.5883H172.302C172.328 17.4715 172.345 17.3101 172.356 17.1001C172.368 16.8895 172.374 16.6343 172.374 16.3332C172.374 14.9853 172.155 13.7477 171.719 12.6208C171.281 11.4939 170.672 10.523 169.894 9.70862Z"
                      fill="white"/>
                <mask id="mask1" mask-type="alpha" maskUnits="userSpaceOnUse" x="174" y="2" width="10" height="24">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M174.15 2.52979H183.713V25.2234H174.15V2.52979Z"
                          fill="white"/>
                </mask>
                <g mask="url(#mask1)">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M174.15 11.7665V19.158C174.15 21.0851 174.683 22.578 175.745 23.6367C176.809 24.6927 178.26 25.2234 180.103 25.2234C180.927 25.2234 181.637 25.1525 182.228 25.0134C182.816 24.8743 183.313 24.7096 183.713 24.5252V20.1343C183.431 20.3187 183.125 20.4523 182.793 20.5347C182.461 20.6138 182.133 20.6563 181.801 20.6563C180.951 20.6563 180.319 20.4578 179.907 20.0629C179.494 19.6685 179.286 19.0047 179.286 18.0763V11.7665H183.713V7.58233H179.286V4.48306L174.187 2.52979H174.15V7.58233V11.7665Z"
                          fill="white"/>
                </g>
                <g class="js-l-rocketJump">
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M76.7881 22.0259C76.3846 21.554 75.5811 21.554 75.1776 22.0259L72.6628 24.967C72.3857 25.291 72.3857 25.7513 72.6628 26.0753L75.2419 29.9791C75.6221 30.4231 76.3442 30.4231 76.7238 29.9791L79.303 26.0753C79.58 25.7513 79.58 25.291 79.303 24.967L76.7881 22.0259Z"
                          fill="#FF5F09"/>
                    <path class="js-l-logo" fill-rule="evenodd" clip-rule="evenodd"
                          d="M77.779 11.1053L76.2705 12.9615C76.1231 13.1426 75.8427 13.1426 75.6952 12.9615L74.1868 11.1053C74.0793 10.9728 74.0793 10.7851 74.1868 10.6526L75.6952 8.79641C75.8427 8.61532 76.1231 8.61532 76.2705 8.79641L77.779 10.6526C77.8865 10.7851 77.8865 10.9728 77.779 11.1053ZM84.3704 11.1168L76.8934 3.38986C76.3907 2.87005 75.575 2.87005 75.0723 3.38986L67.5959 11.1168C67.193 11.5324 66.9669 12.097 66.9669 12.6855V22.2871C66.9669 23.407 68.2232 24.026 69.0656 23.3219L75.1715 18.2131C75.6448 17.8177 76.3209 17.8177 76.7942 18.2131L82.9007 23.3219C83.7425 24.026 84.9988 23.407 84.9988 22.2871V12.6855C84.9988 12.097 84.7727 11.5324 84.3704 11.1168Z"
                          fill="#FF5F09"/>
                </g>
            </svg>
        </div>
    </div>


@endsection
@push('js')
@endpush
