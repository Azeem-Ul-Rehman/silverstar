@extends('layouts.master')
@section('title','Settings')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('Settings') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('admin.settings.update', $setting->id) }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            @method('patch')
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Logo') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{$setting->logo}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURLLogo(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($setting->logo) ? 'block' : 'none'}};"
                                                 id="img"
                                                 src="{{ asset('/uploads/logo/'.$setting->logo) }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="footer_logo"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Banner') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{$setting->banner}}" type="file"
                                                   class="form-control @error('footer_logo') is-invalid @enderror"
                                                   onchange="readURLFooter(this)" id="footer_logo"
                                                   name="footer_logo" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($setting->banner) ? 'block' : 'none'}};"
                                                 id="imgFooter"
                                                 src="{{ asset('/uploads/banner/'.$setting->banner) }}"
                                                 alt="{{$setting->banner}}"/>

                                            @error('footer_logo')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }}</label>

                                            <select id="status"
                                                    class="form-control @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status" autofocus>
                                                <option
                                                    value="active" {{ $setting && $setting->status == 'active' ? 'selected' : '' }}>
                                                    Active
                                                </option>
                                                <option
                                                    value="inactive" {{ $setting && $setting->status == 'inactive' ? 'selected' : '' }}>
                                                    In Active
                                                </option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        SAVE
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function readURLFooter(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    $('#imgFooter').attr('src', e.target.result);
                    $('#imgFooter').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLLogo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    $('#img').attr('src', e.target.result);
                    $('#img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
