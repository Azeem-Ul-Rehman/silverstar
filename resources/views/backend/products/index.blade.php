@extends('layouts.master')
@section('title','Products')
@push('css')
    <style>
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Products
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.products.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Products</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr No.</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th style="width: 10%;">Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($products))
                        @foreach($products as $product)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($product->title)}}</td>
                                <td>{{ucfirst($product->type)}}</td>
                                <td>
                                    <img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($product->image) ? '' : 'none'}};
                                             object-fit: contain;"
                                         id="img"
                                         src="{{ $product->image_path}}"
                                         alt="your image"/>
                                </td>
                                <td nowrap style="width: 30%;">
                                    <a href="{{route('admin.products.edit',$product->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post" action="{{ route('admin.products.destroy', $product->id) }}"
                                          id="delete_{{ $product->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$product->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [3, 4]}
            ],
        });


    </script>
@endpush
