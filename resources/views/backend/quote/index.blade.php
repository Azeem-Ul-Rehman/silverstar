@extends('layouts.master')
@section('title','Get a Quote')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Get a Quote
                        </h3>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body">
                <div class="responsiveTable">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                        <tr>

                            <th> Sr No</th>
                            <th> Full Name</th>
                            <th> Email</th>
                            <th> Mobile Number</th>
                            <th> Company Name</th>
                            <th> Message</th>
                            <th> Category</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($contacts))
                            @foreach($contacts as $contact)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ucfirst($contact->name)}}</td>
                                    <td>{{$contact->email}}</td>
                                    <td>{{$contact->phone_number}}</td>
                                    <td>{{$contact->company_name}}</td>
                                    <td>{{$contact->message}}</td>
                                    <td>{{$contact->category}}</td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({});


    </script>
@endpush
