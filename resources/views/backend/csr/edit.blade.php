@extends('layouts.master')
@section('title','CSR')
@push('css')
    <style>
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('CSR') }}
                        </h3>
                    </div>
                </div>

            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.csr.update',$csr->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title',$csr->title) }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Description') }}
                                                <span class="mandatorySign">*</span></label>
                                            <textarea id="description" type="text"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description"
                                                      autocomplete="description"
                                                      autofocus>{{ old('description',$csr->description) }}</textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}</label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 id="img"
                                                 style="display:{{($csr->image) ? 'block' : 'none'}};"
                                                 src="{{ $csr->image_path }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="background_image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Background Image') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{old('background_image')}}" type="file"
                                                   class="form-control @error('background_image') is-invalid @enderror"
                                                   onchange="readURLBG(this)" id="background_image"
                                                   name="background_image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="background_image-thumbnail"
                                                 id="background_image_img"
                                                 style="display:{{($csr->background_image) ? 'block' : 'none'}};"
                                                 src="{{ $csr->background_image_path }}"
                                                 alt="your image"/>

                                            @error('background_image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <hr>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="icon_one"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Icon One') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{old('icon_one')}}" type="file"
                                                   class="form-control @error('icon_one') is-invalid @enderror"
                                                   onchange="readURLIconOne(this)" id="icon_one"
                                                   name="icon_one" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="icon_one-thumbnail"
                                                 style="display:{{($csr->icon_one) ? 'block' : 'none'}};"
                                                 src="{{ $csr->icon_one_path }}"
                                                 id="icon_one_img"
                                                 alt="your image"/>

                                            @error('icon_one')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="icon_two"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Icon Two') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{old('icon_two')}}" type="file"
                                                   class="form-control @error('icon_two') is-invalid @enderror"
                                                   onchange="readURLIconTwo(this)" id="icon_two"
                                                   name="icon_two" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="icon_two-thumbnail"
                                                 style="display:{{($csr->icon_two) ? 'block' : 'none'}};"
                                                 src="{{ $csr->icon_two_path }}"
                                                 id="icon_two_img"
                                                 alt="your image"/>

                                            @error('icon_two')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="icon_three"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Icon Three') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{old('icon_three')}}" type="file"
                                                   class="form-control @error('icon_three') is-invalid @enderror"
                                                   onchange="readURLIconThree(this)" id="icon_three"
                                                   name="icon_three" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="icon_three-thumbnail"
                                                 style="display:{{($csr->icon_three) ? 'block' : 'none'}};"
                                                 src="{{ $csr->icon_three_path }}"
                                                 id="icon_three_img"
                                                 alt="your image"/>

                                            @error('icon_three')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="icon_four"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Icon Four') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input value="{{old('icon_four')}}" type="file"
                                                   class="form-control @error('icon_four') is-invalid @enderror"
                                                   onchange="readURLIconFour(this)" id="icon_four"
                                                   name="icon_four" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="icon_four-thumbnail"
                                                 style="display:{{($csr->icon_four) ? 'block' : 'none'}};"
                                                 src="{{ $csr->icon_four_path }}"
                                                 id="icon_four_img"
                                                 alt="your image"/>

                                            @error('icon_four')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.csr.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')


    <script type="text/javascript">
        function readURLBG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#background_image_img').attr('src', e.target.result);
                    $('#background_image_img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLIconOne(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#icon_one_img').attr('src', e.target.result);
                    $('#icon_one_img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLIconTwo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#icon_two_img').attr('src', e.target.result);
                    $('#icon_two_img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLIconThree(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#icon_three_img').attr('src', e.target.result);
                    $('#icon_three_img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLIconFour(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#icon_four_img').attr('src', e.target.result);
                    $('#icon_four_img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
