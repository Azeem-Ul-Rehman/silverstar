@extends('layouts.master')
@section('title','Product Variants')
@push('css')
    <style>
        .table-hover > tbody > tr:hover, .table-hover > tbody > tr:hover > td  {
            background: unset !important;
            color: unset !important;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Product Variants
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.product-variants.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Product Variants</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr No.</th>
                        <th>Product Title</th>
                        <th>Product Type</th>
                        <th>Color</th>
                        <th style="width: 10%;">Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($variants))
                        @foreach($variants as $variant)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($variant->product->title)}}</td>
                                <td>{{ucfirst($variant->product->type)}}</td>
                                <td style="background-color:{{ $variant->color ?? '#FFFFFF' }}"></td>
                                <td>
                                    <img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($variant->image) ? '' : 'none'}};
                                             object-fit: contain;"
                                         id="img"
                                         src="{{ $variant->image_path}}"
                                         alt="your image"/>
                                </td>
                                <td nowrap style="width: 30%;">
                                    <a href="{{route('admin.product-variants.edit',$variant->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post"
                                          action="{{ route('admin.product-variants.destroy', $variant->id) }}"
                                          id="delete_{{ $variant->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$variant->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [4, 5]}
            ],
        });


    </script>
@endpush
