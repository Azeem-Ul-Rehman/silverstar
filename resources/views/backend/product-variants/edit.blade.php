@extends('layouts.master')
@section('title','Product Variants')
@push('css')
    <style>
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Product Variant') }}
                        </h3>
                    </div>
                </div>

            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('admin.product-variants.update',$variant->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body" id="enterPress">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="type"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Product Type') }}
                                                <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="type"
                                                    class="form-control product-type @error('type') is-invalid @enderror"
                                                    name="type" autocomplete="type">
                                                <option value="" {{ (old('type') == '') ? 'selected' : '' }}>Select an
                                                    option
                                                </option>
                                                <option
                                                    value="apparels" {{ (old('type',$variant->product->type) == 'apparels') ? 'selected' : '' }}>
                                                    Apparels
                                                </option>
                                                <option
                                                    value="balls" {{ (old('type',$variant->product->type) == 'balls') ? 'selected' : '' }}>
                                                    Balls
                                                </option>
                                                <option
                                                    value="gloves" {{ (old('type',$variant->product->type) == 'gloves') ? 'selected' : '' }}>
                                                    Gloves
                                                </option>
                                                <option
                                                    value="bags" {{ (old('type',$variant->product->type) == 'bags') ? 'selected' : '' }}>
                                                    Bags
                                                </option>
                                            </select>

                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="product_id"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Product Category') }}
                                                <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="product_id"
                                                    class="form-control product-category @error('product_id') is-invalid @enderror"
                                                    name="product_id" autocomplete="product_id">
                                                <option value="{{$variant->product_id}}" selected>
                                                    {{ $variant->product->title }}
                                                </option>

                                            </select>

                                            @error('product_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="color"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Color') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="color" type="text"
                                                   class="form-control @error('color') is-invalid @enderror colorpicker"
                                                   name="color" value="{{ old('color',$variant->color) }}"
                                                   autocomplete="off" autofocus>

                                            @error('color')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}</label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 id="img"
                                                 style="display:{{($variant->image) ? 'block' : 'none'}};"
                                                 src="{{ $variant->image_path }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.product-variants.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css"
          rel="stylesheet">
    <style>
        .colorpicker {
            padding: 11px !important;
            margin-top: 0px !important;
        }
    </style>
@endpush

@push('js')
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
    <script>
        $('.colorpicker').colorpicker();
    </script>
@endpush
@push('js')
    <script type="text/javascript">
        $("#enterPress").keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });
    </script>


    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display", "block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $('.product-type').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.product-category';
            var type = $(this).val();
            var request = "type=" + type;

            if (type !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.product.category') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.products, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.title + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Product Category</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Product Category</option>");
            }
        });
    </script>
@endpush
