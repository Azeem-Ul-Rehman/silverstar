@extends('layouts.master')
@section('title','Events')
@push('css')
    <style>
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Events
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.events.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Event</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr NO.</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th style="width: 10%;">Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($events))
                        @foreach($events as $event)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($event->title)}}</td>
                                <td>{{\Illuminate\Support\Str::limit($event->description,200)}}</td>
                                <td>
                                    <img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($event->image) ? '' : 'none'}};
                                             object-fit: contain;"
                                         id="img"
                                         src="{{ $event->image_path}}"
                                         alt="your image"/>
                                </td>
                                <td nowrap style="width: 30%;">
                                    <a href="{{route('admin.csr.edit',$event->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post" action="{{ route('admin.events.destroy', $event->id) }}"
                                          id="delete_{{ $event->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$event->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [3, 4]}
            ],
        });


    </script>
@endpush
