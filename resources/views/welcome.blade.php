<!DOCTYPE html PUBLIC>
<html xmlns="" xml:lang="en" itemscope itemtype="">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ asset('assets/vues/commun/img/commun/favicon/favicon.ico') }}" rel="icon">
    <meta name="description" content="Silver Star - manufacturing top class apparels and sports products for the world">
    <title>Silver Star - Manufacturing top class Apparels and Sports Products for the World</title>

    <script src="{{ asset('assets/includes/js/jquery-1.12.0.min.js') }}"></script>
    <script src="{{ asset('assets/variables/js/custom.js') }}"></script>


    <!-- font awesome cdn -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- styling files source -->
    <link href="{{ asset('assets/variables/css/page.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/variables/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/html-css.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/variables/css/lib/cssshake-1.5.0.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ asset('assets/vues/commun/img/commun/favicon/favicon.ico') }}">

    {{--    <script>--}}
    {{--        $(function () {--}}
    {{--            $("#mySidenav").load("header.html");--}}
    {{--        });--}}
    {{--    </script>--}}

    <title>Silver Star</title>

</head>

<body>
<!-- sidebar -->
<div id="mySidenav" class="sidenav">

    <ul>
        <li class="active"><a href="{{ route('index') }}"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="{{ route('about-us') }}"><i class="fa fa-user"></i>About Us</a></li>
        <li><a href="{{ route('products') }}"><img src="{{ asset('frontend/assets/imgs/prodct.png') }}">Products</a></li>
        <li><a href="{{ route('csr') }}"><img src="{{ asset('frontend/assets/imgs/csr.png') }}">CSR</a></li>
        <li><a href="{{ route('research.and.development') }}"><img src="{{ asset('frontend/assets/imgs/research.png') }}">R&D</a></li>
        <li><a href="{{ route('events') }}"><img src="{{ asset('frontend/assets/imgs/event.png') }}">Events</a></li>
        <li><a href="{{ route('medical.care') }}"><img src="{{ asset('frontend/assets/imgs/medical.png') }}">Medical Care</a></li>
        <li><a href="{{ route('go.green') }}"><img src="{{ asset('frontend/assets/imgs/go-green.png') }}">Go green</a></li>
        <li><a href="{{ route('contact-us') }}"><img src="{{ asset('frontend/assets/imgs/sms.png') }}">Contact</a></li>
    </ul>
</div>

<div id="bodyOverlay"></div>


<div id="main">
    <!-- sidebar open hamburger -->
    <div class="sidebar_toogle_btn" onclick="openNav()">
        <div></div>
        <div></div>
        <div></div>
        <!-- <i class="fa fa-bars"></i> -->
    </div>
    <!-- main contennt -->
    <div id="main-container">
        <div id="devWaiona" class="devWaiona-en_GB" data-display-cle="">
            <div class="dW_preloader">
                <div></div>
            </div>
            <div class="dW_animFlash">
                <div class="dW_1"></div>
                <div class="dW_2"></div>
            </div>
            <div class="dW_hover dW_generic__hover dW_generic__hidden">
                <div data-cle="stronger"></div>
                <div data-cle="faster"></div>
            </div>
            <div class="dW_header">
                <div class="dW_logo">
                    <h3><span>Silver Star Group Pvt Ltd</span></h3>
                </div>
            </div>
            <section id="dW_intro">
                <div class="dW_background" data-cle="stronger">
                    <div class="dW_content">
                        <div class="dW_titre">
                            <h3 class="shake shake-constant"><span>Silver</span></h3>
                        </div>
                        <div class="dW_visuels">
                            <ul>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="dW_background" data-cle="faster">
                    <div class="dW_content">
                        <div class="dW_titre">
                            <h3 class="shake shake-constant"><span>Star</span></h3>
                        </div>
                        <div class="dW_visuels">
                            <ul>

                            </ul>
                        </div>
                    </div>
                </div>

            </section>
            <section id="dW_page" class="dW_generic__anim" data-hover-cle="">
                <div class="dW_cover">
                    <div class="dW_scroll-down">
                        <p><a href="#dW_story" title="See the rest"><span>See the rest</span></a></p>
                    </div>
                    <div class="dW_silhouette dW_generic__animOnScroll">
                        <div class="dW_visuels" data-cle="stronger">
                            <ul>
                                <li data-index="1"><img
                                        src="{{ asset('assets/variables/img/generic/1ptrans_3-16.gif') }}"
                                        width="240"
                                        height="1280" alt="" title=""/></li>
                                <li data-index="2"><img
                                        src="{{ asset('assets/variables/img/generic/1ptrans_3-16.gif') }}"
                                        width="240"
                                        height="1280" alt="" title=""/></li>
                            </ul>
                        </div>
                        <div class="dW_visuels" data-cle="faster">
                            <ul>
                                <li data-index="1"><img
                                        src="{{ asset('assets/variables/img/generic/1ptrans_3-16.gif') }}"
                                        width="240"
                                        height="1280" alt="" title=""/></li>
                                <li data-index="2"><img
                                        src="{{ asset('assets/variables/img/generic/1ptrans_3-16.gif') }}"
                                        width="240"
                                        height="1280" alt="" title=""/></li>
                            </ul>
                        </div>
                    </div>
                    <div class="dW_titre apparel-text-bg">
                        <h1><span
                                class="dW_generic__dib"><span>A</span><span>P</span><span>P</span><span>A</span><span>R</span><span>E</span><span>L</span><span>S</span></span>
                        </h1>
                    </div>
                    <div class="dW_illus"><img src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                               width="1280" height="640"
                                               alt="" title=""/></div>
                    <div class="dW_background">
                        <div class="dW_visuel" data-cle="stronger"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="640" height="640" alt="" title=""/></div>
                        <div class="dW_visuel" data-cle="faster"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="640" height="640" alt="" title=""/></div>
                    </div>
                </div>
                <div id="dW_story" class="dW_story">
                    <div class="dW_titre dW_generic__animOnScroll">
                        <div class="dW_1">
                            <h2>SOCCER</h2>
                        </div>
                        <div class="dW_2">
                            <h3>BALLS</h3>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="1">
                        <div class="dW_texte">
                            <p>
                                <span class="dW_generic__dib">
                                    <span class="dW_s1"></span>
                                    <span class="dW_s2"></span>
                                </span>
                                <br/>
                                <span class="dW_generic__dib">
                                    <span class="dW_s3">World Class</span>
                                </span>
                                <br/>
                                <span class="dW_generic__dib">
                                    <span class="dW_s3"></span>
                                </span>
                                <br/>
                                <span class="dW_generic__dib">
                                    <span class="dW_s4">Production of Sports Apparel,
                                            Soccer
                                            Balls, <br/> Gloves and Bags
                                    </span>
                                </span>
                                <br/>

                            </p>
                        </div>
                        <div class="dW_illus dW_generic__eventOnScroll" data-parallax-ratio=".175">
                            <div class="dW_visuel" data-index="1"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_8-7.gif') }}"
                                    width="320" height="280" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'bg','url':'{{ asset('assets/variables/img/story/story_item-1-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-1-illus.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="2"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_8-7.gif') }}"
                                    width="320" height="280" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'bg','url':'{{ asset('assets/variables/img/story/story_item-1-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-1-illus.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="3"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_8-7.gif') }}"
                                    width="320" height="280" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'bg','url':'{{ asset('assets/variables/img/story/story_item-1-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-1-illus.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="4"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_8-7.gif') }}"
                                    width="320" height="280" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'bg','url':'{{ asset('assets/variables/img/story/story_item-1-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-1-illus.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="5"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_8-7.gif') }}"
                                    width="320" height="280" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'bg','url':'{{ asset('assets/variables/img/story/story_item-1-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-1-illus.webp') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="2">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">Largest
                                            Sports</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">Apparel Manufacturing <br/>Infrastructure in
                                            Region</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4"></span></span>
                            </p>
                        </div>

                        <div class="dW_illus dW_generic__eventOnScroll" data-index="2" data-parallax-ratio=".215">
                            <div class="dW_visuel"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_47-30.gif') }}" width="376"
                                    height="240" style="object-fit: contain;" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/ball.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/ball.png') }}?ver='}}"/>
                            </div>
                        </div>

                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="3">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">Sustainable &
                                            Futuristic</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">Apparel Production Under One Roof</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s4"></span></span></p>
                        </div>
                        <div class="dW_illus dW_generic__eventOnScroll" data-parallax-ratio=".125">
                            <div class="dW_visuel"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_159-127.gif') }}"
                                    width="795"
                                    height="635" alt="" title="" class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item-3-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/map.png') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="4">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">Region's</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s4">Largest Hand-Sewn sports bag<br/>
                                            Production Unit</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">by Premisse's company</span></span></p>
                        </div>

                        <div class="dW_illus dW_generic__eventOnScroll ball_in_hand_wrap" data-index="2"
                             data-parallax-ratio=".195">
                            <div class="dW_visuel sports-bag-visuel"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_317-140.gif') }}"
                                    width="539"
                                    height="238" style="object-fit: contain;" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/sports-bag.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/sports-bag.png') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="5">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">Hi-tech
                                        </span></span><br/><span class="dW_generic__dib"><span class="dW_s4">Soccer
                                            Gloves
                                            Production Unit<br/> for Goalkeepers and more</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s4"></span></span></p>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll dW_generic__animOnScroll" data-index="6">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1">1</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">large-scale
                                            test</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">took
                                            place at the </span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">"Silver Star Group"</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s4">headquarters of
                                        </span></span><br/><span class="dW_generic__dib"><span class="dW_s4">Sports
                                            Production Sialkot,Pakistan</span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s4"></span></span></p>
                        </div>
                        <div class="dW_illus dW_generic__eventOnScroll" data-parallax-ratio=".115">
                            <div class="dW_visuel" data-index="1"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_90-67.gif') }}"
                                    width="540" height="402" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="2"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_90-67.gif') }}"
                                    width="540" height="402" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="3"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_90-67.gif') }}"
                                    width="540" height="402" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver='}}"/>
                            </div>
                            <div class="dW_visuel" data-index="4"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_90-67.gif') }}"
                                    width="540" height="402" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item.jpg') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="dW_item dW_generic__displayOnScroll" data-index="7">
                        <div class="dW_texte">
                            <p><span class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                    class="dW_generic__dib"><span class="dW_s3">In-House
                                            Testing</span></span><br/><span class="dW_generic__dib"><span
                                        class="dW_s4">Lab
                                            for Maintaining <br/>International Standard Quality</span></span><br/>
                                <span class="dW_generic__dib"><span class="dW_s4">
                                        </span></span>
                            </p>
                        </div>
                        <div class="dW_illus dW_generic__eventOnScroll" data-parallax-ratio=".135">
                            <div class="dW_visuel"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_40-73.gif') }}" width="400"
                                    height="730" alt="" title="" class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/story_item-6-illus.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/story_item-6-illus.webp') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dW_versus">
                    <div class="dW_titre dW_generic__animOnScroll">
                        <div class="dW_1">
                            <h2 style="line-height: 68px;">BEST <span>SELLINGS</span></h2>
                        </div>
                        <div class="dW_2">
                        </div>
                    </div>
                    <div class="dW_intro">
                        <p class="dW_generic__animOnScroll"><span class="dW_generic__dib"><span
                                    class="dW_s1">Hand-sewn
                                    </span> <span class="dW_s2"><span>soccer balls</span></span></span><br/><span
                                class="dW_generic__dib"><span class="dW_s1">for better durability and softer
                                        feel.
                                    </span></span></p>
                        <p class="dW_generic__animOnScroll"><span class="dW_generic__dib"><span
                                    class="dW_s1">100%</span>
                                    <span class="dW_s2"><span>Organic pima</span></span> <span class="dW_s1">cotton
                                        sports
                                    </span></span><br/><span class="dW_generic__dib"><span class="dW_s1">
                                        apparel manufacturing</span></span></p>
                    </div>
                    <div class="dW_content dW_generic__eventOnScroll">
                        <div class="dW_terrain" data-cle="stronger">
                            <div class="dW_nav">
                                <p><a title="Voir la galerie Stronger"><span class="dW_generic__dib"><span
                                                class="dW_s1">INTERCEPT !</span></span><br/><span
                                            class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                            class="dW_generic__dib"><span
                                                class="dW_s2">DISCOVER</span></span><br/><span
                                            class="dW_generic__dib"><span class="dW_s2">Sports
                                                    Apparels...</span></span></a></p>
                            </div>
                            <div class="dW_infos dW_generic__animOnScroll">
                                <p><span class="dW_generic__dib dW_s1"><span>COMFIRT</span></span><br/><span
                                        class="dW_generic__dib dW_s2">ON YOUR BODY !</span></p>
                            </div>
                            <div class="dW_packshots dW_generic__animOnScroll socc_left">
                                <ul class="left-comp">
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/g-1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/g-1.png') }}?ver='}}"/>
                                    </li>
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/g2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/g2.png') }}?ver='}}"/>
                                    </li>
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/g3.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/g3.png') }}?ver='}}"/>
                                    </li>
                                </ul>
                            </div>
                            <div class="dW_titre dW_generic__animOnScroll"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="253"
                                    height="253" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_terrain-titre-stronger.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_terrain-titre-stronger.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_background dW_generic__animOnScroll"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="610"
                                    height="400" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_terrain-stronger.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/right-soccer.png') }}?ver='}}"/>
                            </div>
                        </div>
                        <div class="dW_terrain" data-cle="faster">
                            <div class="dW_nav">
                                <p><a title="Voir la galerie Faster"><span class="dW_generic__dib"><span
                                                class="dW_s1">DRIBBLE !</span></span><br/><span
                                            class="dW_generic__dib"><span class="dW_s1"></span></span><br/><span
                                            class="dW_generic__dib"><span
                                                class="dW_s2">DISCOVER</span></span><br/><span
                                            class="dW_generic__dib"><span class="dW_s2">SOCCER
                                                    BALLS..</span></span></a>
                                </p>
                            </div>
                            <div class="dW_infos dW_generic__animOnScroll">
                                <p><span class="dW_generic__dib dW_s1"><span>GO FASTER</span></span><br/><span
                                        class="dW_generic__dib dW_s2">TO THE GOAL !</span></p>
                            </div>
                            <div class="dW_packshots dW_generic__animOnScroll socc_right">
                                <ul class="right-comp">
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/b1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/b1.png')}}?ver='}}"/>
                                    </li>
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/b2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/b3.png') }}?ver='}}"/>
                                    </li>
                                    <li><img src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="500"
                                             height="500" alt=""
                                             title="" class="dW_generic__preloadOnScroll"
                                             data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/b3.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/b3.png') }}?ver='}}"/>
                                    </li>
                                </ul>
                            </div>
                            <div class="dW_titre dW_generic__animOnScroll"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="182"
                                    height="182" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_terrain-titre-faster.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_terrain-titre-faster.webp') }}?ver='}}"/>
                            </div>
                            <div class="dW_background dW_generic__animOnScroll"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans_61-40.gif') }}" width="610"
                                    height="400" alt=""
                                    title="" class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_terrain-faster.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/left-soccer.png') }}?ver='}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="dW_prix dW_generic__animOnScroll">
                        <div class="dW_valeur dW_generic__animOnScroll">
                            <div class="dW_content">
                                <p><span class="dW_generic__dib"><span class="dW_counter" data-start="120"
                                                                       data-end="75">75</span><span class="dW_devise">&euro;</span><span
                                            class="dW_asterisque">*</span></span></p>
                            </div>
                        </div>
                        <div class="dW_asterisque">
                            <p><span class="dW_generic__dib">price in July 2020</span></p>
                        </div>
                    </div>
                </div>
                <div class="dW_avis-expert">
                    <div class="dW_titre dW_generic__animOnScroll">
                        <div class="dW_1">
                            <h2>Silver Star's</h2>
                        </div>
                        <div class="dW_2">
                            <h3>Chairman</h3>
                        </div>
                    </div>
                    <div class="dW_avis dW_generic__animOnScroll">
                        <p class="dW_texte"><span class="dW_generic__dib">We owe our success to the support
                                    ofour
                                    customers, the commitment of our management in sustaining our vision and the
                                    dedication
                                    of our workers, without whom nothing would have been possible.</span>
                        </p>
                        <p class="dW_signature"><span class="dW_generic__dib"><span class="dW_s1"><strong>Sheikh
                                            Jahangir iqbal</strong>
                                    </span></span><br/><span class="dW_generic__dib"><span class="dW_s2">Chairman
                                        Silver
                                        Star Group</span></span></p>
                    </div>
                    <div class="dW_visuel dW_generic__animOnScroll" data-cle="expert">
                        <div class="dW_content" data-index="1"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans_82-61.gif') }}"
                                width="820" height="610" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver='}}"/>
                        </div>
                        <div class="dW_content" data-index="2"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans_82-61.gif') }}"
                                width="820" height="610" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver='}}"/>
                        </div>
                        <div class="dW_content" data-index="3"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans_82-61.gif') }}"
                                width="820" height="610" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver='}}"/>
                        </div>
                        <div class="dW_content" data-index="4"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans_82-61.gif') }}"
                                width="820" height="610" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/chairman-photo.jpg') }}?ver='}}"/>
                        </div>
                    </div>
                    <div class="dW_visuel dW_generic__animOnScroll" data-cle="ballon"><img
                            src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="492" height="492"
                            alt="" title=""
                            class="dW_generic__preloadOnScroll"
                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/gamme/pakshot-ball1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/gamme/pakshot-ball1.png') }}?ver='}}"/>
                    </div>
                </div>
                <div class="dW_gamme">
                    <div class="dW_titre dW_generic__animOnScroll">
                        <div class="dW_1">
                            <h2>HOT</h2>
                        </div>
                        <div class="dW_2">
                            <h3>Products</h3>
                        </div>
                    </div>
                    <div class="dW_produits dW_generic__displayOnScroll">
                        <div class="dW_liste" data-cle="stronger">
                            <ul class="hot-products-listing">
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/gamme/gamme-packshot-stronger-1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/gamme/packshot-bag.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">SPORTS BAGS</span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the MEN RED MAGMA H900 STRONGER model"><span>Discover
                                                        the SPORTS BAGS</span></a>
                                        </p>
                                    </div>
                                </li>
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/gamme/gamme-packshot-stronger-2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/b2.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">SOCCER BALLS</span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the SOCCER BALLS model"><span>Discover
                                                        the SOCCER BALLS</span></a>
                                        </p>
                                    </div>
                                </li>
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/glove1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/glove1.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">GOALKEEPER GLOVES</span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the GOALKEEPER GLOVES model"><span>Discover
                                                        the SPORTS GLOVES</span></a>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                            <br class="dW_nofloat"/>
                        </div>
                        <div class="dW_liste" data-cle="faster">
                            <ul class="hot-products-listing">
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/gamme/packshot-bag1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/gamme/packshot-bag1.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">SPORTS BAGS</span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the MEN BLACK H900 FASTER model"><span>Discover
                                                        the
                                                        SPORTS BAGS</span></a>
                                        </p>
                                    </div>
                                </li>
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/gamme/pakshot-ball1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/gamme/pakshot-ball1.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">SOCCER BALLS</span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the MEN BLUE H900 FASTER model"><span>Discover
                                                        the
                                                        SOCCER BALLS</span></a>
                                        </p>
                                    </div>
                                </li>
                                <li class="dW_generic__hover dW_generic__animOnScroll">
                                    <div class="dW_visuel"><img
                                            src="{{ asset('assets/variables/img/generic/1ptrans_2-1.gif') }}"
                                            width="448" height="224" alt="" title=""
                                            class="dW_generic__preloadOnScroll"
                                            data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/glove2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/glove2.png') }}?ver='}}"/>
                                    </div>
                                    <div class="dW_titre">
                                        <h3><span class="dW_generic__dib">GOALKEEPER GLOVES </span></h3>
                                    </div>
                                    <div class="dW_cta">
                                        <p>
                                            <a href="#" target="_blank"
                                               title="Discover the WOMEN H900 FASTER  model"><span>Discover the
                                                        GOALKEEPER GLOVES</span></a>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                            <br class="dW_nofloat"/>
                        </div>
                        <div class="dW_background dW_generic__eventOnScroll" data-parallax-ratio=".25"></div>
                        <br class="dW_generic__nofloat"/>
                    </div>
                    <div class="dW_cta dW_generic__animOnScroll">
                        <p>
                            <a href="#" target="_blank" title="Discover all the sports Products "
                               class="dW_generic__hover"><span>Discover all the sports Products </span></a>
                        </p>
                    </div>
                </div>
                <div class="dW_footer">
                    <div class="dW_logo dW_generic__animOnScroll">
                        <h3><span>Silver Star Football</span></h3>
                    </div>
                    <div class="dW_reseaux-sociaux dW_generic__animOnScroll">
                        <ul>
                            <li class="dW_generic__hover"><a href="" target="_blank" class="dW_icon-youtube"
                                                             title="Follow silver star on YouTube"><span>Follow silver star on
                                            YouTube</span></a>
                            </li>
                            <li class="dW_generic__hover"><a href="#" target="_blank" class="dW_icon-twitter"
                                                             title="Follow silver star on Twitter"><span>Follow silver star on
                                            Twitter</span></a>
                            </li>
                            <li class="dW_generic__hover"><a href="#" target="_blank" class="dW_icon-facebook"
                                                             title="Follow silver start on Facebook"><span>Follow silver star on
                                            Facebook</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="dW_asterisque dW_generic__animOnScroll">
                        <p><span class="dW_generic__dib">*Silver Star Group is proudly serving the world of
                                    sports
                                    for
                                    more than a century. An icon of excellence, uniqueness
                                    and serving as a role model for soccer balls, apparel and gloves manufact</span>
                        </p>
                    </div>
                </div>
                <div class="dW_back">
                    <div class="dW_1"></div>
                    <div class="dW_2"></div>
                </div>
            </section>
            <section id="dW_galleries" class="dW_generic__anim" data-hover-cle="">
                <div class="dW_close">
                    <p><a title="Close the gallery" class="dW_generic__hover"><span>Close the gallery</span></a>
                    </p>
                </div>
                <div class="dW_item" data-cle="stronger">
                    <div class="dW_content">
                        <div class="dW_scroll">
                            <p><a title="See the photo gallery"><span>See the photo gallery</span></a></p>
                        </div>
                        <div class="dW_visuel" data-index="1"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-1.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="2"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-2.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="3"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="327" height="490" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-3.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-3.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="4"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-4.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-4.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="5"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="440" height="294" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-5.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-5.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="6"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="366" height="244" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-6.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-6.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="7"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="327" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-7.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-7.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="8"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-8.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-visuel-8.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_video" data-index="1" data-video-cle="8bNqjL8fr9M">
                            <div class="dW_player dW_generic__hidden"></div>
                            <div class="dW_thumbnail dW_generic__hover"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                    width="540" height="304" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_stronger-video-1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_stronger-video-1.webp') }}?ver='}}"/>
                            </div>
                        </div>
                        <div class="dW_packshot gallery_img" data-index="1"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="435"
                                height="345"
                                alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/apparels.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/apparels.png') }}?ver='}}"/>
                        </div>
                        <div class="dW_packshot gallery_img" data-index="2"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="502"
                                height="397"
                                alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/apparels.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/apparels.png') }}?ver='}}"/>
                        </div>
                        <div class="dW_packshot gallery_img" data-index="3"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}" width="401"
                                height="394"
                                alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/story/apparels.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/story/apparels.png') }}?ver='}}"/>
                        </div>
                        <div class="dW_titre">
                            <h2><span class="dW_generic__dib"><span class="dW_s2"></span><span class="dW_s1">
                                            Apparels</span></span></h2>
                        </div>

                    </div>
                </div>
                <div class="dW_item" data-cle="faster">
                    <div class="dW_content">
                        <div class="dW_scroll">
                            <p><a title="See the photo gallery"><span>See the photo gallery</span></a></p>
                        </div>
                        <div class="dW_visuel" data-index="1"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/generic/1ptrans.gif') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-1.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="2"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="366" height="244" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('variables/img/versus/versus_faster-visuel-2.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-2.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="3"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="440" height="294" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_faster-visuel-3.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-3.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="4"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_faster-visuel-4.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-4.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="5"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="327" height="490" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_faster-visuel-5.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-5.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_visuel" data-index="6"><img
                                src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                width="490" height="327" alt="" title=""
                                class="dW_generic__preloadOnScroll"
                                data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_faster-visuel-6.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-visuel-6.webp') }}?ver='}}"/>
                        </div>
                        <div class="dW_video" data-index="1" data-video-cle="bjjCsbdcxAM">
                            <div class="dW_player dW_generic__hidden"></div>
                            <div class="dW_thumbnail dW_generic__hover"><img
                                    src="{{ asset('assets/variables/img/generic/1ptrans.gif') }}"
                                    width="540" height="304" alt="" title=""
                                    class="dW_generic__preloadOnScroll"
                                    data-paramsOnScroll="{'preload':{'type':'img','url':'{{ asset('assets/variables/img/versus/versus_faster-video-1.png') }}?ver=','urlWebp':'{{ asset('assets/variables/img/versus/versus_faster-video-1.webp') }}?ver='}}"/>
                            </div>
                        </div>
                        <div class="dW_titre">
                            <h2><span class="dW_generic__dib"><span class="dW_s1">SOCCER BALLS</span><span
                                        class="dW_s2">.</span></span>
                            </h2>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <script type="text/javascript">
            var DevWaiona_datas = {
                urls: {livrables: '{{ asset('assets/variables') }}'},
                evts: {
                    animationEnd: 'webkitAnimationEnd oanimationend msAnimationEnd animationend',
                    transitionEnd: 'webkitTransitionEnd oTransitionEnd msTransitionEnd transitionend'
                },
                params: {locale: 'en_GB', consoleLog: false},
                imagesToPreload: [],
                imagesToLoad: {
                    'all': [{
                        'path': '{{ asset('assets/variables/img/generic/preloader.png') }}',
                        'filesize': '4632'
                    }, {
                        'path': '{{ asset('assets/variables/img/generic/preloader.svg') }}',
                        'filesize': '1309'
                    }, {
                        'path': '{{ asset('assets/variables/img/generic/1ptrans_3-2.gif') }}',
                        'filesize': '43'
                    }, {
                        'path': '{{ asset('assets/variables/img/generic/1ptrans.gif') }}',
                        'filesize': '43'
                    }],
                    'no-webp': [{
                        'path': '{{ asset('assets/variables/img/logo-decathlon-atorka.png') }}',
                        'filesize': '14437'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover_h900.png') }}',
                        'filesize': '45601'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-01.jpg') }}',
                        'filesize': '70344'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-02.jpg') }}',
                        'filesize': '48068'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-03.jpg') }}',
                        'filesize': '65623'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-04.jpg') }}',
                        'filesize': '54663'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-05.jpg') }}',
                        'filesize': '80570'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-06.jpg') }}',
                        'filesize': '64975'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-07.jpg') }}',
                        'filesize': '95191'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-08.jpg') }}',
                        'filesize': '58809'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-09.jpg') }}',
                        'filesize': '56691'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-10.jpg') }}',
                        'filesize': '84335'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-11.jpg') }}',
                        'filesize': '66647'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-12.jpg') }}',
                        'filesize': '110092'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-01.jpg') }}',
                        'filesize': '47247'
                    }, {
                        'path': '{{ asset('variables/img/intro/cover-ambiance-faster-02.jpg') }}',
                        'filesize': '54221'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-03.jpg') }}',
                        'filesize': '59239'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-04.jpg') }}',
                        'filesize': '48935'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-05.jpg') }}',
                        'filesize': '47398'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-06.jpg') }}',
                        'filesize': '42074'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-07.jpg') }}',
                        'filesize': '48427'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-08.jpg') }}',
                        'filesize': '53720'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-shadow.png') }}',
                        'filesize': '43468'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-1.png') }}',
                        'filesize': '179942'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-1.png') }}',
                        'filesize': '246436'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-2.png') }}',
                        'filesize': '193852'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-2.png') }}',
                        'filesize': '241888'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-3.png') }}',
                        'filesize': '193114'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-3.png') }}',
                        'filesize': '252954'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-stronger-1.png') }}',
                        'filesize': '70633'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-stronger-2.png') }}',
                        'filesize': '85140'
                    }, {
                        'path': '{{ asset('asssets/variables/img/cover/cover-silhouette-faster-1.png') }}',
                        'filesize': '81632'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-faster-2.png') }}',
                        'filesize': '67406'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-illus.png') }}',
                        'filesize': '30720'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_resistance.png') }}',
                        'filesize': '5046'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_resistance-fleche.png') }}',
                        'filesize': '3164'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_terrain-titre-stronger-ombre.png') }}',
                        'filesize': '3601'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_legerete.png') }}',
                        'filesize': '4352'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_legerete-fleche.png') }}',
                        'filesize': '2938'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_terrain-titre-faster-ombre.png') }}',
                        'filesize': '3328'
                    }, {
                        'path': '{{ asset('assets/variables/img/avis-expert/avis-expert_quote.png') }}',
                        'filesize': '1637'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme_background.png')}}',
                        'filesize': '13267'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-ombre.png') }}',
                        'filesize': '7789'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-background.png') }}',
                        'filesize': '27627'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-background-mobile.png') }}',
                        'filesize': '27670'
                    }],
                    'webp': [{
                        'path': '{{ asset('assets/variables/img/logo-decathlon-atorka.webp') }}',
                        'filesize': '5318'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover_h900.webp') }}',
                        'filesize': '36934'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-01.webp') }}',
                        'filesize': '17584'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-02.webp') }}',
                        'filesize': '11746'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-03.webp') }}',
                        'filesize': '17458'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-04.webp') }}',
                        'filesize': '13080'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-05.webp') }}',
                        'filesize': '21894'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-06.webp') }}',
                        'filesize': '16068'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-07.webp') }}',
                        'filesize': '26298'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-08.webp') }}',
                        'filesize': '14864'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-09.webp') }}',
                        'filesize': '12540'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-10.webp') }}',
                        'filesize': '20708'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-11.webp') }}',
                        'filesize': '16786'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-stronger-12.webp') }}',
                        'filesize': '29974'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-01.webp') }}',
                        'filesize': '11472'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-02.webp') }}',
                        'filesize': '13170'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-03.webp') }}',
                        'filesize': '14542'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-04.webp') }}',
                        'filesize': '12978'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-05.webp') }}',
                        'filesize': '13270'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-06.webp') }}',
                        'filesize': '11150'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-07.webp') }}',
                        'filesize': '13576'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-ambiance-faster-08.webp') }}',
                        'filesize': '14716'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-shadow.webp') }}',
                        'filesize': '29464'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-1.webp') }}',
                        'filesize': '151152'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-1.webp') }}',
                        'filesize': '218312'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-2.webp') }}',
                        'filesize': '164022'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-2.webp') }}',
                        'filesize': '206514'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-stronger-3.webp') }}',
                        'filesize': '163634'
                    }, {
                        'path': '{{ asset('assets/variables/img/intro/cover-packshot-faster-3.webp') }}',
                        'filesize': '224152'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-stronger-1.webp') }}',
                        'filesize': '61904'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-stronger-2.webp') }}',
                        'filesize': '75294'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-faster-1.webp') }}',
                        'filesize': '71812'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-silhouette-faster-2.webp') }}',
                        'filesize': '60692'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-illus.webp') }}',
                        'filesize': '13516'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-background-stronger.webp') }}',
                        'filesize': '219318'
                    }, {
                        'path': '{{ asset('assets/variables/img/cover/cover-background-faster.webp') }}',
                        'filesize': '78312'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_resistance.webp') }}',
                        'filesize': '4406'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_resistance-fleche.webp') }}',
                        'filesize': '1270'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_terrain-titre-stronger-ombre.webp') }}',
                        'filesize': '1120'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_legerete.webp') }}',
                        'filesize': '3894'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_legerete-fleche.webp') }}',
                        'filesize': '1258'
                    }, {
                        'path': '{{ asset('assets/variables/img/versus/versus_terrain-titre-faster-ombre.webp') }}',
                        'filesize': '888'
                    }, {
                        'path': '{{ asset('assets/variables/img/avis-expert/avis-expert_quote.webp') }}',
                        'filesize': '1354'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme_background.webp') }}',
                        'filesize': '6314'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-ombre.webp') }}',
                        'filesize': '4300'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-background.webp') }}',
                        'filesize': '17824'
                    }, {
                        'path': '{{ asset('assets/variables/img/gamme/gamme-packshot-background-mobile.webp') }}',
                        'filesize': '17976'
                    }]
                }
            };
        </script>
        <script type="text/javascript" src="{{ asset('assets/variables/js/page.js') }}" async defer></script>

    </div>
    <!-- footer -->
    <!-- <div class="landing_footer">
        <footer>
            <div class="footer_brands_wrap">
                <div class="row">
                    <
                </div>
            </div>
        </footer>
    </div> -->
</div>
<div class="block-footer">
    <div class="container-services"></div>
</div>


<script type="text/javascript" async defer>
    jQuery(window).on('scroll', function () {
        if ((parseInt(jQuery(window).scrollTop(), 10) > 0)) {
            setTimeout(function () {
                jQuery('#header').addClass('sticky');
            }, 1150);
        } else {
            jQuery('#header').removeClass('sticky');
        }
    });
</script>

<script>
    function openNav() {
        if ($('#bodyOverlay').hasClass('main-overlay')) {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            $('#bodyOverlay').removeClass('main-overlay');
        } else {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
            $('#bodyOverlay').addClass('main-overlay');
        }
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
        $('#bodyOverlay').removeClass('main-overlay');
    }

    $('#showChildNav').click(function (event) {
        if ($('.ChildNav').hasClass('dismiss')) {
            $('.ChildNav').removeClass('dismiss').addClass('selected').show();
        }
        event.preventDefault();
    });

    $('#hideChildNav').click(function (event) {
        if ($('.ChildNav').hasClass('selected')) {
            $('.ChildNav').removeClass('selected').addClass('dismiss');
        }
        event.preventDefault();
    });

</script>
<style>
    .main-overlay {
        display: none;
    }
</style>


</body>

</html>
