<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    return 'Commands run successfully Cleared.';
});
//Frontend Routes

Route::get('/', 'FrontendController@index')->name('index');
Route::get('about-us', 'FrontendController@aboutUs')->name('about-us');
Route::get('products', 'FrontendController@products')->name('products');
Route::get('contact-us', 'FrontendController@contactUs')->name('contact-us');
Route::get('csr', 'FrontendController@csr')->name('csr');
Route::get('product/detail/{name}', 'FrontendController@productDetail')->name('product.detail');
Route::get('go-green', 'FrontendController@goGreen')->name('go.green');
Route::get('research-&-development', 'FrontendController@researchAndDevelopment')->name('research.and.development');
Route::get('events', 'FrontendController@events')->name('events');
Route::get('medical-care', 'FrontendController@medicalCare')->name('medical.care');
Route::resource('/contacts', 'GetInTouchController', [
    'only' => ['create', 'store']
]);
Route::resource('/getquote', 'GetAQuoteController', [
    'only' => ['create', 'store']
]);


//Backend Routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('forget-password', 'AccountController@getEmail');
Route::post('forget-password', 'AccountController@postEmail');
Route::get('reset-password/{token}', 'AccountController@getPassword');
Route::post('reset-password', 'AccountController@updatePassword');
Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('settings', 'Backend\SettingController', [
            'only' => ['index', 'update']
        ]);
        Route::resource('contacts', 'GetInTouchController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('getquote', 'GetAQuoteController', [
            'only' => ['index']
        ]);
        Route::resource('csr', 'Backend\CSRController');
        Route::resource('events', 'Backend\EventController');
        Route::resource('products', 'Backend\ProductController');
        Route::resource('product-variants', 'Backend\ProductVariantController');
    });
});


//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('product/category', 'Backend\AjaxController@productCategory')->name('product.category');
});




