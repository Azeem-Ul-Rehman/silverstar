<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGetAQuoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('get_a_quote', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('phone_number');
            $table->text('email');
            $table->text('company_name')->nullable();
            $table->longText('message');
            $table->longText('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('get_a_quote');
    }
}
