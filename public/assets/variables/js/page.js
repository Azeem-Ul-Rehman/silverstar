jQuery.fn.extend({
    hasAttr: function (name) {
        return (typeof jQuery(this).attr(name) !== typeof undefined && jQuery(this).attr(name) !== false);
    }
});

jQuery.fn.extend({
    hScroll: function (amount) {
        amount = amount || 120;
        jQuery(this).bind("DOMMouseScroll mousewheel", function (event) {
            if (DevWaiona_Page.currents.windowWidth > 999) {
                var oEvent = event.originalEvent,
                    direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta,
                    position = jQuery(this).scrollLeft();
                position += direction > 0 ? -amount : amount;
                jQuery(this).scrollLeft(position);
                event.preventDefault();
            }
        })
    }
});

var DevWaiona_Page = {
    currents: {
        bClickEnable: true,
        bInitialized: false,
        bStart: false,
        bImagesLoaded: false,
        bTouch: false,
        bWebP: null,
        documentWidth: 0,
        documentHeight: 0,
        loadTimestamp: 0,
        onLoadTimer: 0,
        windowHeight: 0,
        windowWidth: 0,
        touchEvent: {
            active: true,
        },
        scroll: {
            bInit: false,
            currLeft: null,
            currTop: null,
            precTop: null,
            eltsDisplay: [],
            eltsAnim: [],
            eltsEvent: [],
            eltsPreload: [],
            eltsToDisplay: [],
            toDisplayStarted: false,
            toDisplayDelay: 125,
            toDisplayTimer: null,
            isHuman: false
        },
        scrollTo: {
            timeByPixel: 0.25,
            margeTop: 50
        },
        mouseMove: {
            bInit: false,
            currPos: {
                mouseX: null,
                mouseY: null
            },
            evts: []
        },
        preloader: {
            nbImagesToLoad: 0,
            imagesizeToLoad: 0,
            filesizeToLoad: 0,
            nbImagesLoaded: 0,
            imagesizeLoaded: 0,
            filesizeLoaded: 0,
            nbApiToLoad: 0,
            nbApiLoaded: 0,
            apiLoadedCallbackFnk: null
        },
        bufferedInterval: {
            videos: {}
        },
        timers: {},
        rafs: {},
        parallaxDatas: {},
        youtubePlayer: {
            eltPlayer: null,
            objPlayer: null
        },
        versus: {
            prix: {
                delay: 15
            }
        }
    },
    datas: {},
    elements: {
        main: {
            root: null,
            intro: {
                root: null
            },
            page: {
                root: null,
                cover: {
                    root: null
                },
                story: {
                    root: null
                },
                versus: {
                    root: null
                },
                avisExpert: {
                    root: null
                },
                gamme: {
                    root: null
                },
                footer: {
                    root: null
                }
            },
            galleries: {
                root: null
            },
            animFlash: null,
            hover: null
        }
    },
    //
    init: function () {
        DevWaiona_Page.consoleLog('DevWaiona_Page.init();');
        DevWaiona_Page.currents.loadTimestamp = Date.now();

        DevWaiona_Page.elements.main.root = jQuery('#devWaiona');
        DevWaiona_Page.elements.main.intro.root = jQuery('#dW_intro');
        DevWaiona_Page.elements.main.page.root = jQuery('#dW_page');
        DevWaiona_Page.elements.main.page.cover.root = jQuery('div.dW_cover', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.page.story.root = jQuery('div.dW_story', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.page.versus.root = jQuery('div.dW_versus', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.page.avisExpert.root = jQuery('div.dW_avis-expert', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.page.gamme.root = jQuery('div.dW_gamme', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.page.footer.root = jQuery('div.dW_footer', DevWaiona_Page.elements.main.page.root);
        DevWaiona_Page.elements.main.galleries.root = jQuery('#dW_galleries');
        //
        DevWaiona_Page.elements.main.animFlash = jQuery('div.dW_animFlash', DevWaiona_Page.elements.main.root);
        DevWaiona_Page.elements.main.hover = jQuery('div.dW_hover', DevWaiona_Page.elements.main.root);
        //
        if (DevWaiona_Page.hasTouchEvent()) {
            DevWaiona_Page.initTouchYes();
        } else {
            DevWaiona_Page.initTouchNo();
        }
        //
        if (DevWaiona_Page.isCss3()) {
            DevWaiona_Page.elements.main.root.addClass('css3-yes');
        } else {
            DevWaiona_Page.elements.main.root.addClass('css3-no');
        }
        //
        DevWaiona_Page.getDocumentHeight();
        DevWaiona_Page.getWindowHeight();
        DevWaiona_Page.getWindowWidth();
        //
        DevWaiona_Page.setRootDimensions();
        DevWaiona_Page.setPageDimensions();
        //
        DevWaiona_Page.setVersusGalleriesElementsSizes();
        //
        jQuery(document).trigger('dW_Global_Page_Init');
        //
        DevWaiona_Page.initDom();
        //
        DevWaiona_Page.initOnScrollHandlers();
        DevWaiona_Page.initOnMouseMoveHandlers();
        DevWaiona_Page.initPageHandlers();
        //
        DevWaiona_Page.initGlobalEvents();
        DevWaiona_Page.initPageEvents();
        //
        DevWaiona_Page.initParallax();
        //
        jQuery(document).on('dW_Global_GoogleWebPDetection_Ended', {}, function () {
            //
            DevWaiona_Page.initPreload(function () {
                DevWaiona_Page.loadImages(
                    function () {
                        setTimeout(function () {
                            DevWaiona_Page.start();
                        }, 125);
                    });
            });
        });

        DevWaiona_Page.detectWebP('alpha');
        return false;
    },
    //
    start: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.start();');
        //
        jQuery(document)
            .on(
                'dW_Global_ScrollTo_Ended',
                {},
                function () {
                    //
                    jQuery(document).off('dW_Global_ScrollTo_Ended');
                    //
                    DevWaiona_Page.currents.scroll.bInit = true; // On considère la page comme déjà initialisée
                    DevWaiona_Page.initOnScroll();
                    //
                    DevWaiona_Page.currents.mouseMove.bInit = true;
                    DevWaiona_Page.initOnMouseMove();
                    //
                    var _currDelta = Date.now() - DevWaiona_Page.currents.loadTimestamp;
                    if (_currDelta <= 1500) {
                        setTimeout(function () {
                                DevWaiona_Page.starting();
                            }, (1500 - _currDelta)
                        );
                    } else {
                        //
                        DevWaiona_Page.starting();
                    }
                });
        //
        DevWaiona_Page.scrollTo(DevWaiona_Page.elements.main.root, 0);
        //
        return false;
    },
    //
    starting: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.starting();');
        //
        jQuery(document)
            .trigger(
                'dW_Global_PageStart',
                []
            )
        ;
        //
        // Gestion de l'intro
        //
        DevWaiona_Page.elements.main.intro.root
            .on(
                DevWaiona_datas.evts.transitionEnd,
                {},
                function (pEvt) {
                    //
                    if (pEvt.originalEvent.target == this) {
                        //
                        pEvt.stopImmediatePropagation();
                        //
                        jQuery(this)
                            .off(DevWaiona_datas.evts.transitionEnd)
                        ;
                        DevWaiona_Page.elements.main.root
                            .removeClass('dW_scr-intro')
                            .addClass('dW_scr-page')
                        ;
                        //
                        jQuery('div.dW_1', DevWaiona_Page.elements.main.animFlash)
                            .off(DevWaiona_datas.evts.transitionEnd)
                            .on(
                                DevWaiona_datas.evts.transitionEnd,
                                {},
                                function (pEvt) {
                                    //
                                    if (pEvt.originalEvent.target == this) {
                                        //
                                        jQuery(this)
                                            .off(DevWaiona_datas.evts.transitionEnd)
                                            .removeClass('dW_generic__animFlash')
                                        ;
                                        //
                                        jQuery('div.dW_cover', DevWaiona_Page.elements.main.page.root)
                                            .addClass('dW_generic__display')
                                        ;
                                    }
                                }
                            )
                            .addClass('dW_generic__animFlash')
                        ;
                    }
                }
            )
        ;
        DevWaiona_Page.elements.main.root
            .addClass('dW_scr-intro')
        ;
        //
        jQuery('div.dW_preloader', DevWaiona_Page.elements.main.root)
            .on(
                DevWaiona_datas.evts.transitionEnd,
                {},
                function (pEvt) {
                    //
                    if (pEvt.originalEvent.target == this) {
                        //
                        pEvt.stopImmediatePropagation();
                        //
                        jQuery(this)
                            .off(DevWaiona_datas.evts.transitionEnd)
                        ;
                        //
                        if (DevWaiona_Page.currents.windowWidth < 768) {
                            //
                            DevWaiona_Page.elements.main.root
                                .addClass('dW_generic__displayMobile')
                            ;
                        } else {
                            if (DevWaiona_Page.currents.windowWidth < 1000) {
                                //
                                DevWaiona_Page.elements.main.root
                                    .addClass('dW_generic__displayTablet')
                                ;
                            }
                        }
                        //
                        jQuery(this)
                            .hide()
                        ;
                        //
                        DevWaiona_Page.currents.bStart = true;
                        //
                        jQuery(window)
                            .trigger('scroll')
                        ;
                    }
                }
            )
        ;
        //
        DevWaiona_Page.closePreload();
        //
        DevWaiona_Page.currents.bInitialized = true;
        //
        return false;
    },
    //
    initDom: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initDom();');
        //
        jQuery('div.dW_produits div.dW_liste ul li', DevWaiona_Page.elements.main.page.gamme.root)
            .each(
                function () {
                    var _ctaElt = jQuery('div.dW_cta p a', jQuery(this));
                    jQuery(this)
                        // .attr('data-href', _ctaElt.attr('href'))
                        .attr('data-target', _ctaElt.attr('target'))
                        .addClass('dW_generic__link')
                    ;
                    _ctaElt
                        // .removeAttr('href')
                        .removeAttr('target')
                    ;
                    delete _ctaElt;
                }
            )
        ;


        //
        return false;
    },
    //
    initParallax: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initParallax();');
        //
        var _parallaxCounter = 0;
        //
        // dW_story
        //
        jQuery('div.dW_item div.dW_illus.dW_generic__eventOnScroll[data-parallax-ratio]', DevWaiona_Page.elements.main.page.story.root)
            .each(
                function () {
                    //
                    var _elt = jQuery(this);
                    //
                    var _parallaxIndex = 'story_' + _parallaxCounter;
                    _parallaxCounter++;
                    //
                    DevWaiona_Page.currents.parallaxDatas[_parallaxIndex] = {
                        eltForCalc: _elt,
                        eltToMove: _elt,
                        ratio: parseFloat(_elt.attr('data-parallax-ratio')),
                        m: null,
                        p: null
                    };
                    //
                    _elt
                        .on(
                            'dW_PageScroll_EventOnScroll',
                            {
                                _parallaxIndex: _parallaxIndex
                            },
                            function (pEvt, pParams) {
                                //
                                if (pParams.index == jQuery(this).attr('data-indexonscroll')) {
                                    //
// 									pParams.scrollTop = parseInt(pParams.scrollTop, 10);
// 									//
// 									var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
// 									var _elt = _eventObj.elt;
// 									//
// 									Waiona_T_Global.consoleLog('dW_PageScroll_EventOnScroll_Elt : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
// 									//
                                    var _currEltDatas = DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex];
                                    //
                                    if ((!DevWaiona_Page.currents.bTouch)
                                        && (DevWaiona_Page.currents.windowWidth > 999)
                                    ) {
                                        //
                                        if (_currEltDatas.m == null) {
                                            //
                                            // Y=mX+p
                                            //      X = scroll de la page
                                            //      Y = deplacement de l'image
                                            //
                                            var _point1 = {
                                                scroll: Math.max(pParams.eventObj.pos.start, 0),
                                                depl: 0
                                            };
                                            var _point2 = {
                                                scroll: pParams.eventObj.pos.stop,
                                                depl: _currEltDatas.eltForCalc.height() * _currEltDatas.ratio
                                            };
                                            _currEltDatas.m = (_point2.depl - _point1.depl) / (_point2.scroll - _point1.scroll);
                                            _currEltDatas.p = (_point2.depl - (_currEltDatas.m * _point2.scroll));
                                            //
                                        }
                                        //
                                        _currEltDatas.eltToMove
                                            .css(
                                                {
                                                    'transform': 'translate3d(0,' + ((pParams.scrollTop * _currEltDatas.m) + _currEltDatas.p) + 'px,0)'
                                                }
                                            )
                                        ;
                                    } else {
                                        //
                                        _currEltDatas.eltToMove
                                            .css(
                                                {
                                                    'transform': 'translate3d(0,0,0)'
                                                }
                                            )
                                        ;
                                    }
                                    //
                                    delete _currEltDatas;
//									//
//									delete _elt;
//									delete _eventObj;
                                }
                            }
                        )
                        .on(
                            'dW_PageScroll_EventOnScroll_Stopped',
                            {
                                _parallaxIndex: _parallaxIndex
                            },
                            function (pEvt, pParams) {
                                //
                                if (pParams.index == jQuery(this).attr('data-indexonscroll')) {
//									//
//									pParams.scrollTop = parseInt(pParams.scrollTop, 10);
//									//
//									var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
//									var _elt = _eventObj.elt;
//									//
//									DevWaiona_Page.consoleLog('dW_PageScroll_EventOnScroll_Elt_Stopped : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
//									//
                                    DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex].m = null;
                                    DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex].p = null;
//									//
//									delete _elt;
//									delete _eventObj;
                                }
                            }
                        )
                    ;
                    //
                    delete _parallaxIndex;
                    delete _elt;
                }
            );
        //
        // dW_gamme
        //
        jQuery('div.dW_produits div.dW_background.dW_generic__eventOnScroll[data-parallax-ratio]', DevWaiona_Page.elements.main.page.gamme.root)
            .each(
                function () {
                    //
                    var _elt = jQuery(this);
                    //
                    var _parallaxIndex = 'gamme_' + _parallaxCounter;
                    _parallaxCounter++;
                    //
                    DevWaiona_Page.currents.parallaxDatas[_parallaxIndex] = {
                        eltForCalc: _elt,
                        eltToMove: _elt,
                        ratio: parseFloat(_elt.attr('data-parallax-ratio')),
                        m: null,
                        p: null
                    };
                    //
                    _elt
                        .on(
                            'dW_PageScroll_EventOnScroll',
                            {
                                _parallaxIndex: _parallaxIndex
                            },
                            function (pEvt, pParams) {
                                //
                                if (pParams.index == jQuery(this).attr('data-indexonscroll')) {
                                    //
// 									pParams.scrollTop = parseInt(pParams.scrollTop, 10);
// 									//
// 									var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
// 									var _elt = _eventObj.elt;
// 									//
// 									Waiona_T_Global.consoleLog('dW_PageScroll_EventOnScroll_Elt : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
// 									//
                                    var _currEltDatas = DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex];
                                    //
                                    if ((!DevWaiona_Page.currents.bTouch)
                                        && (DevWaiona_Page.currents.windowWidth > 999)
                                    ) {
                                        //
                                        if (_currEltDatas.m == null) {
                                            //
                                            // Y=mX+p
                                            //      X = scroll de la page
                                            //      Y = deplacement de l'image
                                            //
                                            var _point1 = {
                                                scroll: Math.max(pParams.eventObj.pos.start, 0),
                                                depl: 0
                                            };
                                            var _point2 = {
                                                scroll: pParams.eventObj.pos.stop,
                                                depl: _currEltDatas.eltForCalc.height() * _currEltDatas.ratio
                                            };
                                            _currEltDatas.m = (_point2.depl - _point1.depl) / (_point2.scroll - _point1.scroll);
                                            _currEltDatas.p = (_point2.depl - (_currEltDatas.m * _point2.scroll));
                                            //
                                        }
                                        //
                                        _currEltDatas.eltToMove
                                            .css(
                                                {
                                                    'transform': 'translate3d(0,' + ((pParams.scrollTop * _currEltDatas.m) + _currEltDatas.p) + 'px,0)'
                                                }
                                            )
                                        ;
                                    } else {
                                        //
                                        _currEltDatas.eltToMove
                                            .css(
                                                {
                                                    'transform': 'translate3d(0,0,0)'
                                                }
                                            )
                                        ;
                                    }
                                    //
                                    delete _currEltDatas;
//									//
//									delete _elt;
//									delete _eventObj;
                                }
                            }
                        )
                        .on(
                            'dW_PageScroll_EventOnScroll_Stopped',
                            {
                                _parallaxIndex: _parallaxIndex
                            },
                            function (pEvt, pParams) {
                                //
                                if (pParams.index == jQuery(this).attr('data-indexonscroll')) {
//									//
//									pParams.scrollTop = parseInt(pParams.scrollTop, 10);
//									//
//									var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
//									var _elt = _eventObj.elt;
//									//
//									DevWaiona_Page.consoleLog('dW_PageScroll_EventOnScroll_Elt_Stopped : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
//									//
                                    DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex].m = null;
                                    DevWaiona_Page.currents.parallaxDatas[pEvt.data._parallaxIndex].p = null;
//									//
//									delete _elt;
//									delete _eventObj;
                                }
                            }
                        )
                    ;
                    //
                    delete _parallaxIndex;
                    delete _elt;
                }
            )
        ;//
        delete _parallaxCounter;
        //
        return false;
    },
    //
    initPageHandlers: function () {
        //i
        DevWaiona_Page.consoleLog('DevWaiona_Page.initPageHandlers();');
        //
        jQuery(document)
            .on(
                'dW_Global_VersusAnim_MouseMove',
                {},
                function (pEvt, pParams) {
                    //
//					DevWaiona_Page.consoleLog('dW_Global_VersusAnim_MouseMove : ' + pParams.evtIndex + ' / ' + pParams.mouseX + ' / ' + pParams.mouseX + ' / ' + DevWaiona_Page.currents.scroll.currLeft + ' / ' + DevWaiona_Page.currents.scroll.currTop);
                    //
                    if (DevWaiona_Page.currents.windowWidth > 999) {
                        //
                        if (!DevWaiona_Page.elements.main.root.hasClass('dW_generic__display-galerie')) {
                            //
                            var _halfWidth = DevWaiona_Page.currents.windowWidth / 2;
                            //
                            var _currHoverCleIn = 'stronger';
                            var _currHoverCleOut = 'faster';
                            var _mouseXPercent = (_halfWidth - DevWaiona_Page.currents.mouseMove.currPos.mouseX) * 100 / _halfWidth;
                            if (DevWaiona_Page.currents.mouseMove.currPos.mouseX > _halfWidth) {
                                _currHoverCleIn = 'faster';
                                _currHoverCleOut = 'stronger';
                                _mouseXPercent = (DevWaiona_Page.currents.mouseMove.currPos.mouseX - _halfWidth) * 100 / _halfWidth;
                            }
                            var _deplPixel = (_mouseXPercent * 350 / 100);
                            delete _mouseXPercent;
                            DevWaiona_Page.elements.main.page.root
                                .attr('data-hover-cle', _currHoverCleIn)
                            ;
                            DevWaiona_Page.elements.main.galleries.root
                                .attr('data-hover-cle', _currHoverCleIn)
                            ;
                            //
                            var _initialMove = false;
                            if (DevWaiona_Page.elements.main.page.root.hasClass('dW_generic__anim')) {
                                _initialMove = true;
                            }
                            //
                            if ((_initialMove)
                            ) {
                                DevWaiona_Page.elements.main.page.root
                                    .on(
                                        DevWaiona_datas.evts.transitionEnd,
                                        {
                                            evtIndex: pParams.evtIndex
                                        },
                                        function (pEvt) {
                                            if (pEvt.originalEvent.target == this) {
                                                pEvt.stopImmediatePropagation();
                                                jQuery(this)
                                                    .off(DevWaiona_datas.evts.transitionEnd)
                                                    .removeClass('dW_generic__anim')
                                                ;
                                                DevWaiona_Page.elements.main.galleries.root
                                                    .removeClass('dW_generic__anim')
                                                ;
                                                if ((pEvt.data.evtIndex in DevWaiona_Page.currents.mouseMove.evts)
                                                ) {
                                                    DevWaiona_Page.currents.mouseMove.evts[pEvt.data.evtIndex].trig = false;
                                                }
                                            }
                                        }
                                    )
                                ;
                            }
                            //
                            switch (_currHoverCleIn) {
                                //
                                case 'stronger':
                                    //
                                    DevWaiona_Page.elements.main.page.root
                                        .css(
                                            {
                                                transform: 'translate3d(' + _deplPixel + 'px,0,0)'
                                            }
                                        )
                                    ;
                                    //
                                    var _eltNav = jQuery('> div.dW_content div.dW_terrain[data-cle="stronger"] div.dW_nav', DevWaiona_Page.elements.main.page.versus.root);
                                    if (_deplPixel < 280) {
                                        _eltNav
                                            .css(
                                                {
                                                    left: '-' + _deplPixel + 'px',
                                                    width: (_deplPixel + _halfWidth) + 'px'
                                                }
                                            )
                                        ;
                                    }
                                    if (_deplPixel > 230) {
                                        _eltNav.addClass('dW_generic__display-title');
                                    } else {
                                        _eltNav.removeClass('dW_generic__display-title');
                                    }
                                    delete _eltNav;
                                    //
                                    break;
                                //
                                case 'faster':
                                    //
                                    DevWaiona_Page.elements.main.page.root
                                        .css(
                                            {
                                                transform: 'translate3d(-' + _deplPixel + 'px,0,0)'
                                            }
                                        )
                                    ;
                                    //
                                    var _eltNav = jQuery('> div.dW_content div.dW_terrain[data-cle="faster"] div.dW_nav', DevWaiona_Page.elements.main.page.versus.root);
                                    if (_deplPixel < 280) {
                                        _eltNav
                                            .css(
                                                {
                                                    right: '-' + _deplPixel + 'px',
                                                    width: (_deplPixel + _halfWidth) + 'px'
                                                }
                                            )
                                        ;
                                    }
                                    //
                                    if (_deplPixel > 230) {
                                        _eltNav.addClass('dW_generic__display-title');
                                    } else {
                                        _eltNav.removeClass('dW_generic__display-title');
                                    }
                                    delete _eltNav;
                                    //
                                    break;
                            }
                            //
                            delete _deplPixel;
                            delete _halfWidth;
                            //
                            if ((!_initialMove)
                                && (pParams !== undefined)
                                && ('evtIndex' in pParams)
                            ) {
                                DevWaiona_Page.currents.mouseMove.evts[pParams.evtIndex].trig = false;
                            }
                            //
                            delete _initialMove;
                            delete _currHoverCle;
                        }
                    }
                }
            )
        ;
        //
        DevWaiona_Page.elements.main.hover
            .on(
                'dW_Global_Hover_Over',
                {},
                function (pEvt, pParams) {
                    //
                    if (DevWaiona_Page.currents.windowWidth > 999) {
                        //
                        jQuery(document)
                            .trigger(
                                'dW_Global_MouseMove_AddEvent',
                                [
                                    {
                                        'evt': 'dW_Global_VersusAnim_MouseMove'
                                    }
                                ]
                            )
                        ;
                        //
                        for (var _index = 0; _index < DevWaiona_Page.currents.mouseMove.evts.length; _index++) {
                            if (DevWaiona_Page.currents.mouseMove.evts[_index].evt == 'dW_Global_VersusAnim_MouseMove') {
                                //
                                DevWaiona_Page.currents.mouseMove.evts[_index].trig = true;
                                //
                                jQuery(document)
                                    .trigger(
                                        'dW_Global_VersusAnim_MouseMove',
                                        [
                                            {
                                                evtIndex: _index
                                            }
                                        ]
                                    )
                                ;
                                //
                                break;
                            }
                        }
                    } else {
                        jQuery(this).trigger('dW_Global_Hover_Out');
                    }
                }
            )
            .on(
                'dW_Global_Hover_Out',
                {},
                function (pEvt, pParams) {
                    //
                    DevWaiona_Page.elements.main.page.root
                        .attr('data-hover-cle', '')
                        .addClass('dW_generic__anim')
                        .css(
                            {
                                transform: 'translate3d(0,0,0)'
                            }
                        )
                    ;
                    //
                    DevWaiona_Page.elements.main.galleries.root
                        .attr('data-hover-cle', '')
                        .addClass('dW_generic__anim')
                    ;
                    jQuery('div.dW_item', DevWaiona_Page.elements.main.galleries.root)
                        .css(
                            {
                                transform: 'translate3d(0,0,0)'
                            }
                        )
                    ;
                    //
                    jQuery(document)
                        .trigger(
                            'dW_Global_MouseMove_RemoveEvent',
                            [
                                {
                                    'evt': 'dW_Global_VersusAnim_MouseMove'
                                }
                            ]
                        )
                    ;
                }
            )
        ;
        jQuery('div.dW_content', DevWaiona_Page.elements.main.page.versus.root)
            .on(
                'dW_PageScroll_EventOnScroll',
                {},
                function (pEvt, pParams) {
                    //
                    pParams.scrollTop = parseInt(pParams.scrollTop, 10);
                    //
                    var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
                    //					var _elt = _eventObj.elt;
                    //
                    //                  DevWaiona_Page.consoleLog('dW_PageScroll_EventOnScroll : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
                    //
                    if ((pParams.scrollTop > (_eventObj.pos.start + _eventObj.pos.height - 180))
                        && (pParams.scrollTop < (_eventObj.pos.top - jQuery('#container-header').height() + 300))
                    ) {
                        DevWaiona_Page.elements.main.hover
                            .removeClass('dW_generic__hidden')
                        ;
                        jQuery('div.dW_content div.dW_terrain div.dW_nav', DevWaiona_Page.elements.main.page.versus.root)
                            .addClass('dW_generic__display')
                        ;
                    } else {
                        DevWaiona_Page.elements.main.hover
                            .addClass('dW_generic__hidden')
                        ;
                        jQuery('div.dW_content div.dW_terrain div.dW_nav', DevWaiona_Page.elements.main.page.versus.root)
                            .removeClass('dW_generic__display')
                        ;
                    }
                    //
                    //					delete _elt;
                    delete _eventObj;
                }
            )
            .on(
                'dW_PageScroll_EventOnScroll_Stopped',
                {},
                function (pEvt, pParams) {
                    //
//					pParams.scrollTop = parseInt(pParams.scrollTop, 10);
                    //
//					var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
//					var _elt = _eventObj.elt;
                    //
// 					DevWaiona_Page.consoleLog('dW_PageScroll_EventOnScroll_Stopped : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
                    //
                    DevWaiona_Page.elements.main.hover
                        .addClass('dW_generic__hidden')
                    ;
                    //
//					delete _elt;
//					delete _eventObj;
                }
            )
        ;
        //
        jQuery('div.dW_prix div.dW_valeur', DevWaiona_Page.elements.main.page.versus.root)
            .on(
                'dW_PageScroll_AnimOnScroll',
                {},
                function (pEvt, pParams) {
                    //
//					pParams.scrollTop = parseInt(pParams.scrollTop, 10);
                    //
//					var _animObj = DevWaiona_Page.currents.scroll.eltsAnim[pParams.index];
//					var _elt = _animObj.elt;
                    //
// 					DevWaiona_Page.consoleLog('dW_PageScroll_AnimOnScroll : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_animObj.pos));
                    //
                    jQuery('div.dW_content p span.dW_counter', jQuery(this))
                        .removeClass('dW_generic__playing')
                    ;
                    DevWaiona_Page.currents.timers.prixCounter = window.setInterval(
                        function () {
                            var _eltCounter = jQuery('div.dW_prix div.dW_valeur div.dW_content p span.dW_counter', DevWaiona_Page.elements.main.page.versus.root);
                            if (!_eltCounter.hasClass('dW_generic__playing')) {
                                _eltCounter
                                    .html(_eltCounter.attr('data-start'))
                                    .addClass('dW_generic__playing')
                                ;
                            } else {
                                if (_eltCounter.html() != _eltCounter.attr('data-end')) {
                                    var _currValue = parseInt(_eltCounter.html());
                                    _currValue -= 1;
                                    if (_currValue >= parseInt(_eltCounter.attr('data-end'))) {
                                        _eltCounter.html(_currValue);
                                    }
                                    delete _currValue;
                                } else {
                                    clearTimeout(DevWaiona_Page.currents.timers.prixCounter);
                                    DevWaiona_Page.currents.timers.prixCounter = null;
                                }
                            }
                            delete _eltCounter;
                        },
                        DevWaiona_Page.currents.versus.prix.delay
                    );
                    //
//					delete _elt;
//					delete _animObj;
                }
            )
        ;
        //


        //
        return false;
    },
    //
    initPageEvents: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initPageEvents();');
        //
        jQuery('div', DevWaiona_Page.elements.main.hover)
            .on(
                'click',
                {},
                function (pEvt) {
                    //
                    pEvt.stopImmediatePropagation();
                    //
                    DevWaiona_Page.elements.main.root
                        .attr('data-display-cle', jQuery(this).attr('data-cle'))
                        .addClass('dW_generic__display-galerie')
                    ;
                    //
                    jQuery('div.dW_item[data-cle="' + DevWaiona_Page.elements.main.page.root.attr('data-hover-cle') + '"]', DevWaiona_Page.elements.main.galleries.root).hScroll(20);
                }
            )
        ;
        //
        jQuery('div.dW_scroll-down p a', DevWaiona_Page.elements.main.page.cover.root)
            .on(
                'click',
                {},
                function (pEvt) {
                    //
                    pEvt.stopImmediatePropagation();
                    //
                    DevWaiona_Page.scrollTo(DevWaiona_Page.elements.main.page.story.root, 20);
                }
            )
            .removeAttr('href')
        ;
        //
        jQuery('div.dW_close', DevWaiona_Page.elements.main.galleries.root)
            .on(
                'click',
                {},
                function (pEvt) {
                    //
                    pEvt.stopImmediatePropagation();
                    //
                    switch (DevWaiona_Page.elements.main.root.attr('data-display-cle')) {
                        //
                        case 'stronger':
                            jQuery('div.dW_item[data-cle="stronger"]', DevWaiona_Page.elements.main.galleries.root).scrollLeft(-1 * (jQuery('div.dW_item[data-cle="stronger"]', DevWaiona_Page.elements.main.galleries.root).width() - jQuery('div.dW_item[data-cle="stronger"] div.dW_content', DevWaiona_Page.elements.main.galleries.root).width()));
                            break;
                        //
                        case 'faster':
                            jQuery('div.dW_item[data-cle="faster"]', DevWaiona_Page.elements.main.galleries.root).scrollLeft(0);
                    }
                    //
                    DevWaiona_Page.elements.main.root
                        .attr('data-display-cle', '')
                        .removeClass('dW_generic__display-galerie')
                    ;
                }
            )
        ;
        //
        jQuery('div.dW_item', DevWaiona_Page.elements.main.galleries.root)
            .each(
                function () {

                    jQuery('div.dW_content div.dW_video', jQuery(this))
                        .on(
                            'click',
                            {
                                _gammeCle: jQuery(this).attr('data-cle')
                            },
                            function (pEvt) {
                                //
                                pEvt.stopImmediatePropagation();
                                //
                                DevWaiona_Page.currents.youtubePlayer.eltPlayer = jQuery('div.dW_player', jQuery(this));
                                //
                                if ((DevWaiona_Page.currents.youtubePlayer.eltPlayer != null)
                                ) {
                                    //
                                    var _newId = DevWaiona_Page.uniqId();
                                    //
                                    DevWaiona_Page.currents.youtubePlayer.eltPlayer
                                        .append('<div id="' + _newId + '"></div>')
                                    ;
                                    //
                                    DevWaiona_Page.currents.youtubePlayer.objPlayer = new YT.Player(_newId, {
                                        height: jQuery(this).height(),
                                        width: jQuery(this).width(),
                                        videoId: jQuery(this).attr('data-video-cle'),
                                        playerVars: {
                                            controls: 0
                                        },
                                        events: {
                                            'onReady': onYoutubePlayerReady,
                                            'onStateChange': onYoutubePlayerStateChange
                                        }
                                    });
                                    //
                                    delete _newId;
                                }
                            }
                        )
                    ;
                }
            )
        ;


        //
        return false;
    },
    //
    initGlobalEvents: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initGlobalEvents();');
        //
        // Ecouteurs de touch
        //
        jQuery('a', DevWaiona_Page.elements.main.root)
            .on(
                'touchstart',
                {},
                function (pEvt) {
                    //
                    pEvt.stopImmediatePropagation();
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'click',
                {},
                function (pEvt) {
                    //
                    pEvt.stopImmediatePropagation();
                }
            )
        ;
        //
        jQuery('.dW_generic__hover-withPropagation')
            .addClass('dW_generic__hover')
        ;
        jQuery('.dW_generic__hover')
            .on(
                'mouseenter',
                {},
                function (pEvt) {
                    //
                    if (!jQuery(this).hasClass('dW_generic__hover-withPropagation')) {
                        pEvt.stopPropagation();
                    }
                    //
                    if ((!DevWaiona_Page.currents.bTouch)
                        || (DevWaiona_Page.currents.windowWidth > 640)
                    ) {
                        //
                        jQuery(this)
                            .on(
                                'mouseleave',
                                {},
                                function (pEvt) {
                                    //
                                    if (!jQuery(this).hasClass('dW_generic__hover-withPropagation')) {
                                        pEvt.stopPropagation();
                                    }
                                    //
                                    jQuery(this)
                                        .off('mouseleave')
                                        .removeClass('dW_generic__hover-over')
                                        .addClass('dW_generic__hover-out')
                                        .trigger(
                                            'dW_Global_Hover_Out'
                                        )
                                    ;
                                }
                            )
                            .removeClass('dW_generic__hover-out')
                            .addClass('dW_generic__hover-over')
                            .trigger(
                                'dW_Global_Hover_Over'
                            )
                        ;
                    }
                }
            )
            .addClass('dW_generic__hover-out')
            .removeClass('dW_generic__hover')
        ;
        //
        jQuery('.dW_generic__link')
            .on(
                'click',
                {},
                function (pEvt) {
                    pEvt.stopImmediatePropagation();
                    var _href = jQuery(this).attr('data-href');
                    var _target = jQuery(this).attr('data-target');
                    if (!DevWaiona_Page.isBlankOrNull(_href)) {
                        switch (_target) {
                            //
                            case '_self':
                                document.location = _href;
                                break;
                            //
                            case '_blank':
                                window.open(_href);
                                break;
                        }
                    }
                    //
                    delete _target;
                    delete _href;
                }
            )
        ;
        //
        return false;
    },
    //
    initOnScrollHandlers: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScrollHandlers();');
        //
        jQuery(document)
            .on(
                'dW_Global_PageScroll_PreloadOnScroll',
                {},
                function (pEvt, pParams) {
                    //
                    var _preloadObj = DevWaiona_Page.currents.scroll.eltsPreload[pParams.index];
                    var _elt = _preloadObj.elt;
                    //
//					DevWaiona_Page.consoleLog('dW_Global_PageScroll_PreloadOnScroll : ' + pParams.index + ' (' + _elt.attr('class') + ') : ' + JSON.stringify(_preloadObj.params));
                    //
                    _elt
                        .addClass('dW_generic__startPreloadOnScroll')
                        .addClass('dW_generic__preloadLoader')
                    ;
                    //
                    // Chargement des images en //
                    //
                    var _curr_img = new Image();
                    _curr_img['indexPreloadOnScroll'] = pParams.index;
                    _curr_img.onload = function () {
                        var _preloadObj = DevWaiona_Page.currents.scroll.eltsPreload[this.indexPreloadOnScroll];
                        var _elt = _preloadObj.elt;
                        //
                        switch (_preloadObj.params.type) {
                            //
                            case 'img':
                                _elt
                                    .attr('src', this.src)
                                ;
                                break;
                            //
                            case 'bg':
                                _elt.css(
                                    {
                                        'background-image': 'url(' + this.src + ')'
                                    }
                                );
                                break;
                        }
                        _elt
                            .removeClass('dW_generic__preloadLoader')
                        ;
                        //
                        setTimeout(
                            function () {
                                _elt
                                    .addClass('dW_generic__preloadOnScrollEnded')
                                ;
                            },
                            1250
                        );
                        //
                        delete _elt;
                        delete _preloadObj;
                    };
                    _curr_img.onerror = function () {
                        var _preloadObj = DevWaiona_Page.currents.scroll.eltsPreload[this.indexPreloadOnScroll];
                        _preloadObj.elt
                            .removeClass('dW_generic__preloadLoader')
                        ;
                        delete _preloadObj;
                    };
                    if ((DevWaiona_Page.currents.bWebP)
                        && (_preloadObj.params.hasOwnProperty('urlWebp'))
                        && (_preloadObj.params.urlWebp != '')
                    ) {
                        _curr_img.src = _preloadObj.params.urlWebp;
                    } else {
                        _curr_img.src = _preloadObj.params.url;
                    }
                    //
                    delete _elt;
                    delete _preloadObj;
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'dW_Global_PageScroll_EventOnScroll',
                {},
                function (pEvt, pParams) {
                    //
                    pParams.scrollTop = parseInt(pParams.scrollTop, 10);
                    //
                    var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
                    var _elt = _eventObj.elt;
                    //
//					DevWaiona_Page.consoleLog('dW_Global_PageScroll_EventOnScroll : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
                    //
                    _elt
                        .addClass('dW_generic__startEventOnScroll')
                    ;
                    //
                    _elt
                        .trigger(
                            'dW_PageScroll_EventOnScroll',
                            [
                                {
                                    index: pParams.index,
                                    scrollTop: pParams.scrollTop,
                                    eventObj: _eventObj
                                }
                            ]
                        )
                    ;
                    //
                    delete _elt;
                    delete _eventObj;
                }
            )
        ;
        jQuery(document)
            .on(
                'dW_Global_PageScroll_EventOnScroll_Stopped',
                {},
                function (pEvt, pParams) {
                    //
                    pParams.scrollTop = parseInt(pParams.scrollTop, 10);
                    //
                    var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[pParams.index];
                    var _elt = _eventObj.elt;
                    //
//					DevWaiona_Page.consoleLog('dW_Global_PageScroll_EventOnScroll_Stopped : ' + pParams.index + ' (' + _elt.attr('class') + ') ' + pParams.scrollTop + ' : ' + JSON.stringify(_eventObj.pos));
                    //
                    _elt
                        .removeClass('dW_generic__startEventOnScroll')
                    ;
                    //
                    _elt
                        .trigger(
                            'dW_PageScroll_EventOnScroll_Stopped',
                            [
                                {
                                    index: pParams.index,
                                    scrollTop: pParams.scrollTop,
                                    eventObj: _eventObj
                                }
                            ]
                        )
                    ;
                    //
                    delete _elt;
                    delete _eventObj;
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'dW_Global_PageScroll_DisplayOnScroll',
                {},
                function (pEvt) {
                    //
                    DevWaiona_Page.consoleLog('dW_Global_PageScroll_DisplayOnScroll : ' + DevWaiona_Page.currents.scroll.eltsToDisplay[0].elt.attr('data-indexOnScroll'));
                    //
                    if (DevWaiona_Page.currents.scroll.toDisplayTimer != null) {
                        window.clearTimeout(DevWaiona_Page.currents.scroll.toDisplayTimer);
                        DevWaiona_Page.currents.scroll.toDisplayTimer = null;
                    }
                    //
                    DevWaiona_Page.currents.scroll.eltsToDisplay[0].elt
                        .addClass('dW_generic__startDisplayOnScroll')
                        .on(
                            DevWaiona_datas.evts.transitionEnd,
                            function (pEvt) {
                                if (pEvt.originalEvent.target == this) {
                                    pEvt.stopImmediatePropagation();
                                    jQuery(this)
                                        .off(DevWaiona_datas.evts.transitionEnd)
                                        .addClass('dW_generic__displayOnScrollEnded')
                                    ;
                                }
                            }
                        )
                        .removeClass('dW_generic__displayOnScroll')
                    ;
                    //
                    DevWaiona_Page.currents.scroll.eltsToDisplay.shift();
                    //
                    if (DevWaiona_Page.currents.scroll.eltsToDisplay.length > 0) {
                        var _cleanFile = true;
                        while (_cleanFile) {
                            //
                            _cleanFile = false;
                            //
                            if ((DevWaiona_Page.currents.scroll.eltsToDisplay[0].params.prevElt == 1)
                                || (DevWaiona_Page.currents.scroll.currTop > DevWaiona_Page.currents.scroll.eltsToDisplay[0].pos.stop)
                            ) {
                                //
                                DevWaiona_Page.currents.scroll.eltsToDisplay[0].elt
                                    .addClass('dW_generic__startDisplayOnScroll')
                                    .on(
                                        DevWaiona_datas.evts.transitionEnd,
                                        function (pEvt) {
                                            if (pEvt.originalEvent.target == this) {
                                                pEvt.stopImmediatePropagation();
                                                jQuery(this)
                                                    .off(DevWaiona_datas.evts.transitionEnd)
                                                    .addClass('dW_generic__displayOnScrollEnded')
                                                ;
                                            }
                                        }
                                    )
                                    .removeClass('dW_generic__displayOnScroll')
                                ;
                                //
                                DevWaiona_Page.currents.scroll.eltsToDisplay.shift();
                                //
                                if (DevWaiona_Page.currents.scroll.eltsToDisplay.length > 0) {
                                    _cleanFile = true;
                                }
                            }
                        }
                        delete _cleanFile;
                    }
                    //
                    if (DevWaiona_Page.currents.scroll.eltsToDisplay.length > 0) {
                        if (DevWaiona_Page.currents.scroll.isHuman) {
                            DevWaiona_Page.currents.scroll.toDisplayTimer = window.setTimeout(
                                function () {
                                    jQuery(document)
                                        .trigger('dW_Global_PageScroll_DisplayOnScroll')
                                    ;
                                },
                                DevWaiona_Page.currents.scroll.toDisplayDelay
                            );
                        } else {
                            jQuery(document)
                                .trigger('dW_Global_PageScroll_DisplayOnScroll')
                            ;
                        }
                    } else {
                        DevWaiona_Page.currents.scroll.toDisplayStarted = false;
                    }
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'dW_Global_PageScroll',
                {},
                function (pEvt, pParams) {
                    //
//					DevWaiona_Page.consoleLog('dW_Global_PageScroll : ' + pParams.scrollTop);
                    //
                    DevWaiona_Page.currents.scroll.currTop = parseInt(pParams.scrollTop, 10);
                    //
                    if (!DevWaiona_Page.currents.scroll.isHuman) {
                        if ((DevWaiona_Page.currents.scroll.currTop > 0)
                            || ((DevWaiona_Page.currents.scroll.precTop != null)
                                && (DevWaiona_Page.currents.scroll.precTop != DevWaiona_Page.currents.scroll.currTop)
                            )
                        ) {
                            DevWaiona_Page.currents.scroll.isHuman = true;
                        } else {
                            DevWaiona_Page.currents.scroll.precTop = DevWaiona_Page.currents.scroll.currTop;
                        }
                    } else {
                        jQuery('.dW_generic__toDisplayOnScroll', DevWaiona_Page.elements.main.root)
                            .addClass('dW_generic__prepareDisplayOnScroll')
                        ;
                    }
                    //
                    if (DevWaiona_Page.currents.bStart) {
                        //
                        // Display
                        //
                        for (var _index in DevWaiona_Page.currents.scroll.eltsDisplay) {
                            var _displayObj = DevWaiona_Page.currents.scroll.eltsDisplay[_index];
                            //
                            var _inMediaQueries = true;
                            if (((_displayObj.params.mediaQueries.width.min != null)
                                    && (DevWaiona_Page.currents.windowWidth <= _displayObj.params.mediaQueries.width.min)
                                )
                                || ((_displayObj.params.mediaQueries.width.max != null)
                                    && (DevWaiona_Page.currents.windowWidth >= _displayObj.params.mediaQueries.width.max)
                                )
                            ) {
                                _inMediaQueries = false;
                            }
                            //
                            if (_inMediaQueries) {
                                if (DevWaiona_Page.currents.scroll.currTop >= _displayObj.pos.start) {
                                    //
                                    /*
									_displayObj.elt.prepend('windowWidth : '+DevWaiona_Page.currents.windowWidth+' / '+'windowHeight : '+DevWaiona_Page.currents.windowHeight+' / '+'currTop : '+DevWaiona_Page.currents.scroll.currTop+' / '+'start : '+_displayObj.pos.start+' / '+'stop : '+_displayObj.pos.stop);
									*/
                                    //
                                    if (DevWaiona_Page.currents.scroll.bInit) {
                                        //
                                        if (DevWaiona_Page.currents.scroll.currTop >= _displayObj.pos.stop) {
                                            //
                                            _displayObj.elt
                                                .addClass('dW_generic__prepareDisplayOnScroll')
                                            ;
                                            // animFlash
                                            jQuery('div.dW_' + _displayObj.elt.attr('data-indexOnScroll'), DevWaiona_Page.elements.main.animFlash)
                                                .off(DevWaiona_datas.evts.transitionEnd)
                                                .on(
                                                    DevWaiona_datas.evts.transitionEnd,
                                                    {
                                                        _displayObj: _displayObj
                                                    },
                                                    function (pEvt) {
                                                        //
                                                        if (pEvt.originalEvent.target == this) {
                                                            //
                                                            jQuery(this)
                                                                .off(DevWaiona_datas.evts.transitionEnd)
                                                                .removeClass('dW_generic__animFlash')
                                                            ;
                                                            //
                                                            pEvt.data._displayObj.elt
                                                                .addClass('dW_generic__startDisplayOnScroll')
                                                                .on(
                                                                    DevWaiona_datas.evts.transitionEnd,
                                                                    function (pEvt) {
                                                                        if (pEvt.originalEvent.target == this) {
                                                                            pEvt.stopImmediatePropagation();
                                                                            jQuery(this)
                                                                                .off(DevWaiona_datas.evts.transitionEnd)
                                                                                .addClass('dW_generic__displayOnScrollEnded')
                                                                            ;
                                                                        }
                                                                    }
                                                                )
                                                                .removeClass('dW_generic__displayOnScroll')
                                                            ;
                                                        }
                                                    }
                                                )
                                                .addClass('dW_generic__animFlash')
                                            ;
                                        } else {
                                            //
                                            DevWaiona_Page.currents.scroll.eltsToDisplay.push(
                                                _displayObj
                                            );
                                        }
                                        //
                                    } else {
                                        //
                                        _displayObj.elt
                                            .addClass('dW_generic__prepareDisplayOnScroll')
                                            .addClass('dW_generic__startDisplayOnScroll')
                                        ;
                                    }
                                    delete DevWaiona_Page.currents.scroll.eltsDisplay[_index];
                                }
                            } else {
                                _displayObj.elt
                                    .addClass('dW_generic__displayOnScrollEnded')
                                ;
                            }
                            delete _inMediaQueries;
                            //
                            delete _displayObj;
                        }
                        if ((DevWaiona_Page.currents.scroll.eltsToDisplay.length > 0)
                            && (DevWaiona_Page.currents.scroll.toDisplayStarted == false)
                        ) {
                            //
                            for (var _indexToDisplay = 0; _indexToDisplay < DevWaiona_Page.currents.scroll.eltsToDisplay.length; _indexToDisplay++) {
                                //
                                DevWaiona_Page.currents.scroll.eltsToDisplay[_indexToDisplay].elt
                                    .addClass('dW_generic__prepareDisplayOnScroll')
                                ;
                                // animFlash
                                jQuery('div.dW_' + DevWaiona_Page.currents.scroll.eltsToDisplay[_indexToDisplay].elt.attr('data-indexOnScroll'), DevWaiona_Page.elements.main.animFlash)
                                    .off(DevWaiona_datas.evts.transitionEnd)
                                    .on(
                                        DevWaiona_datas.evts.transitionEnd,
                                        {
                                            _indexToDisplay: _indexToDisplay
                                        },
                                        function (pEvt) {
                                            //
                                            if (pEvt.originalEvent.target == this) {
                                                //
                                                jQuery(this)
                                                    .off(DevWaiona_datas.evts.transitionEnd)
                                                    .removeClass('dW_generic__animFlash')
                                                ;
                                                if (pEvt.data._indexToDisplay == (DevWaiona_Page.currents.scroll.eltsToDisplay.length - 1)) {
                                                    //
                                                    DevWaiona_Page.currents.scroll.toDisplayStarted = true;
                                                    //
                                                    jQuery(document)
                                                        .trigger('dW_Global_PageScroll_DisplayOnScroll')
                                                    ;
                                                }
                                            }
                                        }
                                    )
                                    .addClass('dW_generic__animFlash')
                                ;
                            }
                        }
                        //
                        // Anim
                        //
                        for (var _index in DevWaiona_Page.currents.scroll.eltsAnim) {
                            var _animObj = DevWaiona_Page.currents.scroll.eltsAnim[_index];
                            //
                            var _inMediaQueries = true;
                            if (((_animObj.params.mediaQueries.width.min != null)
                                    && (DevWaiona_Page.currents.windowWidth <= _animObj.params.mediaQueries.width.min)
                                )
                                || ((_animObj.params.mediaQueries.width.max != null)
                                    && (DevWaiona_Page.currents.windowWidth >= _animObj.params.mediaQueries.width.max)
                                )
                            ) {
                                _inMediaQueries = false;
                            }
                            //
                            var _triggered = false;
                            //
                            if (_inMediaQueries) {
                                if ((DevWaiona_Page.currents.scroll.currTop >= _animObj.pos.start)
                                    && (DevWaiona_Page.currents.scroll.currTop <= _animObj.pos.stop)
                                ) {
                                    //
                                    var _firstLaunch = true;
                                    //
                                    if (_animObj.elt.hasClass('dW_generic__startAnimOnScroll')) {
                                        _firstLaunch = false;
                                    }
                                    //
                                    _animObj.elt
                                        .addClass('dW_generic__startAnimOnScroll')
                                    ;
                                    //
                                    _triggered = true;
                                    //
                                    if (_firstLaunch) {
                                        _animObj.elt
                                            .trigger(
                                                'dW_PageScroll_AnimOnScroll',
                                                [
                                                    {
                                                        index: _index,
                                                        scrollTop: DevWaiona_Page.currents.scroll.currTop,
                                                        animObj: _animObj
                                                    }
                                                ]
                                            )
                                        ;
                                    }
                                    delete _firstLaunch;
                                }
                            }
                            delete _inMediaQueries;
                            //
                            if ((!_triggered)
//								&& (_animObj.elt.hasClass('dW_generic__startAnimOnScroll'))
                            ) {
                                _animObj.elt
                                    .removeClass('dW_generic__startAnimOnScroll')
                                ;
                            }
                            delete _triggered;
                            //
                            delete _animObj;
                        }
                        //
                        // Events
                        //
                        for (var _index in DevWaiona_Page.currents.scroll.eltsEvent) {
                            var _eventObj = DevWaiona_Page.currents.scroll.eltsEvent[_index];
                            //
                            var _inMediaQueries = true;
                            if (((_eventObj.params.mediaQueries.width.min != null)
                                    && (DevWaiona_Page.currents.windowWidth <= _eventObj.params.mediaQueries.width.min)
                                )
                                || ((_eventObj.params.mediaQueries.width.max != null)
                                    && (DevWaiona_Page.currents.windowWidth >= _eventObj.params.mediaQueries.width.max)
                                )
                            ) {
                                _inMediaQueries = false;
                            }
                            //
                            var _triggered = false;
                            //
                            if (_inMediaQueries) {
                                if ((DevWaiona_Page.currents.scroll.currTop >= _eventObj.pos.start)
                                    && (DevWaiona_Page.currents.scroll.currTop <= _eventObj.pos.stop)
                                ) {
                                    jQuery(document)
                                        .trigger('dW_Global_PageScroll_EventOnScroll', [{
                                            index: _index,
                                            scrollTop: DevWaiona_Page.currents.scroll.currTop
                                        }])
                                    ;
                                    _triggered = true;
                                }
                            }
                            delete _inMediaQueries;
                            //
                            if ((!_triggered)
                                && (_eventObj.elt.hasClass('dW_generic__startEventOnScroll'))
                            ) {
                                jQuery(document)
                                    .trigger('dW_Global_PageScroll_EventOnScroll_Stopped', [{
                                        index: _index,
                                        scrollTop: DevWaiona_Page.currents.scroll.currTop
                                    }])
                                ;
                            }
                            delete _triggered;
                            //
                            delete _eventObj;
                        }
                        //
                        // Preloads
                        //
                        for (var _index in DevWaiona_Page.currents.scroll.eltsPreload) {
                            var _preloadObj = DevWaiona_Page.currents.scroll.eltsPreload[_index];
                            //
                            var _inMediaQueries = true;
                            if (((_preloadObj.params.mediaQueries.width.min != null)
                                    && (DevWaiona_Page.currents.windowWidth <= _preloadObj.params.mediaQueries.width.min)
                                )
                                || ((_preloadObj.params.mediaQueries.width.max != null)
                                    && (DevWaiona_Page.currents.windowWidth >= _preloadObj.params.mediaQueries.width.max)
                                )
                            ) {
                                _inMediaQueries = false;
                            }
                            //
                            if (_inMediaQueries) {
                                if ((!_preloadObj.elt.hasClass('dW_generic__startPreloadOnScroll'))
                                    && (DevWaiona_Page.currents.scroll.currTop >= _preloadObj.pos.start)
                                ) {
                                    jQuery(document)
                                        .trigger('dW_Global_PageScroll_PreloadOnScroll', [{index: _index}])
                                    ;
                                }
                            }
                            delete _inMediaQueries;
                            //
                            delete _preloadObj;
                        }
                        //
                        if (!DevWaiona_Page.currents.scroll.bInit) {
                            DevWaiona_Page.currents.scroll.bInit = true;
                        }
                        //
                        /*
						 * Cas spécifiques
						 */
                        //
                        // Divers
                        //
                    }
                }
            )
        ;
        //
        /*
		setInterval(
			function(){
				window.requestAnimationFrame(
					function() {
						//
						jQuery(document)
							.trigger(
								'dW_Global_PageScroll',
								[
								 	{
								 		'scrollTop': parseInt(jQuery(document).scrollTop(), 10)
								 	}
								]
							)
						;
					}
				);
			},
			10
		);
		*/
        //
        jQuery(window)
            .on(
                'scroll',
                function () {
                    //
                    jQuery(document)
                        .trigger(
                            'dW_Global_PageScroll',
                            [
                                {
                                    'scrollTop': parseInt(jQuery(document).scrollTop(), 10)
                                }
                            ]
                        )
                    ;
                }
            )
        ;
    },
    //
    initOnScroll: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll();');
        //
        var _eltsToDisplay = [];
        var _eltsToAnim = [];
        var _eltsToEvent = [];
        var _eltsToPreload = [];
        //
        jQuery('div[class^="dW_aos_"]', DevWaiona_Page.elements.main.animFlash)
            .remove()
        ;
        //
        var _displayTopPrev = null;
        jQuery('.dW_generic__displayOnScroll, .dW_generic__animOnScroll, .dW_generic__eventOnScroll, .dW_generic__preloadOnScroll', DevWaiona_Page.elements.main.root)
            .each(
                function () {
                    //
                    var _elt = jQuery(this);
                    //
                    if (((_elt.hasClass('dW_generic__displayOnScroll'))
                            && (!_elt.hasClass('dW_generic__startDisplayOnScroll'))
                        )
                        || (_elt.hasClass('dW_generic__animOnScroll'))
                        || (_elt.hasClass('dW_generic__eventOnScroll'))
                        || ((_elt.hasClass('dW_generic__preloadOnScroll'))
                            && (!_elt.hasClass('dW_generic__startPreloadOnScroll'))
                        )
                    ) {
                        //
                        var _params = {
                            display: {
                                prevElt: false,
                                mediaQueries: {
                                    width: {
                                        min: null,
                                        max: null
                                    }
                                }
                            },
                            anim: {
                                mediaQueries: {
                                    width: {
                                        min: null,
                                        max: null
                                    }
                                }
                            },
                            event: {
                                mediaQueries: {
                                    width: {
                                        min: null,
                                        max: null
                                    }
                                }
                            },
                            preload: {
                                type: null,
                                url: null,
                                urlWebP: null,
                                mediaQueries: {
                                    width: {
                                        min: null,
                                        max: null
                                    }
                                }
                            }
                        };
                        //
                        if ((_elt.hasAttr('data-paramsOnScroll'))
                            && (_elt.attr('data-paramsOnScroll') != '')
                        ) {
                            var _datasParams = _elt.attr('data-paramsOnScroll');
                            _datasParams = _datasParams.replace(/'/g, '"');
                            _params = jQuery.extend(true, {}, _params, JSON.parse(_datasParams));
                        }
                        //
//						DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll() : ' + _elt.attr('class') + ' : ' + JSON.stringify(_params));
                        //
                        // DisplayOnScroll
                        //
                        if ((_elt.hasClass('dW_generic__displayOnScroll'))
                            && (!_elt.hasClass('dW_generic__startDisplayOnScroll'))
                        ) {
                            //
                            _elt.removeAttr('data-indexOnScroll');
                            //
                            _elt.removeClass('dW_generic__displayOnScroll');
                            var _position = _elt.offset();
                            _elt.addClass('dW_generic__displayOnScroll');
                            //
                            var _top = _position.top;
                            var _left = _position.left;
                            //
                            if (_params.display.prevElt) {
                                _top = _displayTopPrev;
                            }
                            _displayTopPrev = _top;
                            //
                            var _indexs = [parseInt(_top, 10), parseInt(_left, 10)];
                            //
                            if (_eltsToDisplay[_indexs[0]] === undefined) {
                                _eltsToDisplay[_indexs[0]] = [];
                            }
                            if (_eltsToDisplay[_indexs[0]][_indexs[1]] === undefined) {
                                _eltsToDisplay[_indexs[0]][_indexs[1]] = [];
                            }
                            //
//							DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll() : DisplayOnScroll : ' + _elt.attr('class') + ' : ' + _top + ' / ' + _left + ' => ' + _indexs[0] + ' / ' + _indexs[1]);
                            //
                            var _width = Math.max(_elt.outerWidth(false), _elt.outerWidth(true));
                            var _height = Math.max(_elt.outerHeight(false), _elt.outerHeight(true));
                            //
                            _eltsToDisplay[_indexs[0]][_indexs[1]].push(
                                {
                                    elt: _elt,
                                    params: _params.display,
                                    pos: {
                                        top: _top,
                                        left: _left,
                                        width: _width,
                                        height: _height,
                                        start: parseInt((_top - DevWaiona_Page.currents.windowHeight), 10),
                                        stop: parseInt((_top + _height), 10)
                                    }
                                }
                            );
                            delete _height;
                            delete _indexs;
                            delete _left;
                            delete _top;
                            //
                            delete _position;
                        }
                        //
                        // AnimOnScroll
                        //
                        if (_elt.hasClass('dW_generic__animOnScroll')) {
                            //
                            _elt.removeAttr('data-indexOnScroll');
                            //
                            _elt.removeClass('dW_generic__animOnScroll');
                            var _position = _elt.offset();
                            _elt.addClass('dW_generic__animOnScroll');
                            //
                            var _top = _position.top;
                            var _left = _position.left;
                            //
                            var _indexs = [parseInt(_top, 10), parseInt(_left, 10)];
                            //
                            if (_eltsToAnim[_indexs[0]] === undefined) {
                                _eltsToAnim[_indexs[0]] = [];
                            }
                            if (_eltsToAnim[_indexs[0]][_indexs[1]] === undefined) {
                                _eltsToAnim[_indexs[0]][_indexs[1]] = [];
                            }
                            //
//							DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll() : AnimOnScroll : ' + _elt.attr('class') + ' : ' + _top + ' / ' + _left + ' => ' + _indexs[0] + ' / ' + _indexs[1]);
                            //
                            var _width = Math.max(_elt.outerWidth(false), _elt.outerWidth(true));
                            var _height = Math.max(_elt.outerHeight(false), _elt.outerHeight(true));
                            //
                            _eltsToAnim[_indexs[0]][_indexs[1]].push(
                                {
                                    elt: _elt,
                                    params: _params.event,
                                    pos: {
                                        top: _top,
                                        left: _left,
                                        width: _width,
                                        height: _height,
                                        start: parseInt((_top - DevWaiona_Page.currents.windowHeight), 10),
                                        stop: parseInt((_top + _height), 10)
                                    }
                                }
                            );
                            delete _height;
                            delete _indexs;
                            delete _left;
                            delete _top;
                            //
                            delete _position;
                        }
                        //
                        // EventOnScroll
                        //
                        if (_elt.hasClass('dW_generic__eventOnScroll')) {
                            //
                            _elt.removeAttr('data-indexOnScroll');
                            //
                            _elt.removeClass('dW_generic__eventOnScroll');
                            var _position = _elt.offset();
                            _elt.addClass('dW_generic__eventOnScroll');
                            //
                            var _top = _position.top;
                            var _left = _position.left;
                            //
                            var _indexs = [parseInt(_top, 10), parseInt(_left, 10)];
                            //
                            if (_eltsToEvent[_indexs[0]] === undefined) {
                                _eltsToEvent[_indexs[0]] = [];
                            }
                            if (_eltsToEvent[_indexs[0]][_indexs[1]] === undefined) {
                                _eltsToEvent[_indexs[0]][_indexs[1]] = [];
                            }
                            //
//							DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll() : EventOnScroll : ' + _elt.attr('class') + ' : ' + _top + ' / ' + _left + ' => ' + _indexs[0] + ' / ' + _indexs[1]);
                            //
                            var _width = Math.max(_elt.outerWidth(false), _elt.outerWidth(true));
                            var _height = Math.max(_elt.outerHeight(false), _elt.outerHeight(true));
                            //
                            _eltsToEvent[_indexs[0]][_indexs[1]].push(
                                {
                                    elt: _elt,
                                    params: _params.event,
                                    pos: {
                                        top: _top,
                                        left: _left,
                                        width: _width,
                                        height: _height,
                                        start: parseInt((_top - DevWaiona_Page.currents.windowHeight), 10),
                                        stop: parseInt((_top + _height), 10)
                                    }
                                }
                            );
                            delete _height;
                            delete _indexs;
                            delete _left;
                            delete _top;
                            //
                            delete _position;
                        }
                        //
                        // PreloadOnScroll
                        //
                        if ((_elt.hasClass('dW_generic__preloadOnScroll'))
                            && (!_elt.hasClass('dW_generic__startPreloadOnScroll'))
                        ) {
                            _elt.removeAttr('data-indexOnScroll');
                            //
                            _elt.removeClass('dW_generic__preloadOnScroll');
                            var _position = _elt.offset();
                            _elt.addClass('dW_generic__preloadOnScroll');
                            //
                            var _top = _position.top;
                            var _left = _position.left;
                            //
                            var _indexs = [parseInt(_top, 10), parseInt(_left, 10)];
                            //
                            if (_eltsToPreload[_indexs[0]] === undefined) {
                                _eltsToPreload[_indexs[0]] = [];
                            }
                            if (_eltsToPreload[_indexs[0]][_indexs[1]] === undefined) {
                                _eltsToPreload[_indexs[0]][_indexs[1]] = [];
                            }
                            //
//							DevWaiona_Page.consoleLog('DevWaiona_Page.initOnScroll() : PreloadOnScroll : ' + _elt.attr('class') + ' : ' + _top + ' / ' + _left + ' => ' + _indexs[0] + ' / ' + _indexs[1]);
                            //
                            var _width = Math.max(_elt.outerWidth(false), _elt.outerWidth(true));
                            var _height = Math.max(_elt.outerHeight(false), _elt.outerHeight(true));
                            //
                            _eltsToPreload[_indexs[0]][_indexs[1]].push(
                                {
                                    elt: _elt,
                                    params: _params.preload,
                                    pos: {
                                        top: _top,
                                        left: _left,
                                        width: _width,
                                        height: _height,
                                        start: parseInt((_top - (2 * DevWaiona_Page.currents.windowHeight)), 10)
                                    }
                                }
                            );
                            delete _height;
                            delete _indexs;
                            delete _left;
                            delete _top;
                            //
                            delete _position;
                        }
                    }
                    delete _elt;
                }
            )
        ;
        delete _displayTopPrev;
        //
        var _counter = 0;
        //
        // DisplayOnScroll
        //
        for (var _index in _eltsToDisplay) {
            var _eltsToDisplayTop = _eltsToDisplay[_index];
            if (_eltsToDisplayTop != null) {
                for (var _jndex in _eltsToDisplayTop) {
                    var _eltsToDisplayLeft = _eltsToDisplayTop[_jndex];
                    if (_eltsToDisplayLeft != null) {
                        for (var _kndex = 0; _kndex < _eltsToDisplayLeft.length; _kndex++) {
                            var _datas = _eltsToDisplayLeft[_kndex];
                            if (!_datas.elt.hasAttr('data-indexOnScroll')) {
                                _datas.elt.attr('data-indexOnScroll', 'aos_' + _counter);
                                _counter++;
                            }
                            DevWaiona_Page.currents.scroll.eltsDisplay[_datas.elt.attr('data-indexOnScroll')] = _datas;
                            DevWaiona_Page.elements.main.animFlash.append('<div class="dW_' + _datas.elt.attr('data-indexOnScroll') + '"></div>');
                            if (!_datas.elt.hasClass('dW_generic__startDisplayOnScroll')) {
                                _datas.elt.addClass('dW_generic__toDisplayOnScroll');
                                //
                                /*
								if ((DevWaiona_Page.currents.bMobile)
									|| (DevWaiona_Page.currents.windowWidth < 641)
								) {
									_datas.elt.addClass('dW_generic__prepareDisplayOnScroll');
								}
								*/
                            }
                            delete _datas;
                        }
                    }
                    delete _eltsToDisplayLeft;
                }
            }
            delete _eltsToDisplayTop;
        }
        delete _eltsToDisplay;
        //
        // AnimOnScroll
        //
        for (var _index in _eltsToAnim) {
            var _eltsToAnimTop = _eltsToAnim[_index];
            if (_eltsToAnimTop != null) {
                for (var _jndex in _eltsToAnimTop) {
                    var _eltsToAnimLeft = _eltsToAnimTop[_jndex];
                    if (_eltsToAnimLeft != null) {
                        for (var _kndex = 0; _kndex < _eltsToAnimLeft.length; _kndex++) {
                            var _datas = _eltsToAnimLeft[_kndex];
                            if (!_datas.elt.hasAttr('data-indexOnScroll')) {
                                _datas.elt.attr('data-indexOnScroll', 'aos_' + _counter);
                            }
                            _counter++;
                            DevWaiona_Page.currents.scroll.eltsAnim[_datas.elt.attr('data-indexOnScroll')] = _datas;
                            DevWaiona_Page.elements.main.animFlash.append('<div class="dW_' + _datas.elt.attr('data-indexOnScroll') + '"></div>');
                            _datas.elt.addClass('dW_generic__toAnimOnScroll');
                            delete _datas;
                        }
                    }
                    delete _eltsToAnimLeft;
                }
            }
            delete _eltsToAnimTop;
        }
        delete _eltsToAnim;
        //
        // EventOnScroll
        //
        for (var _index in _eltsToEvent) {
            var _eltsToEventTop = _eltsToEvent[_index];
            if (_eltsToEventTop != null) {
                for (var _jndex in _eltsToEventTop) {
                    var _eltsToEventLeft = _eltsToEventTop[_jndex];
                    if (_eltsToEventLeft != null) {
                        for (var _kndex = 0; _kndex < _eltsToEventLeft.length; _kndex++) {
                            var _datas = _eltsToEventLeft[_kndex];
                            if (!_datas.elt.hasAttr('data-indexOnScroll')) {
                                _datas.elt.attr('data-indexOnScroll', 'aos_' + _counter);
                            }
                            _counter++;
                            DevWaiona_Page.currents.scroll.eltsEvent[_datas.elt.attr('data-indexOnScroll')] = _datas;
                            DevWaiona_Page.elements.main.animFlash.append('<div class="dW_' + _datas.elt.attr('data-indexOnScroll') + '"></div>');
                            _datas.elt.addClass('dW_generic__toEventOnScroll');
                            delete _datas;
                        }
                    }
                    delete _eltsToEventLeft;
                }
            }
            delete _eltsToEventTop;
        }
        delete _eltsToEvent;
        //
        // PreloadOnScroll
        //
        for (var _index in _eltsToPreload) {
            var _eltsToPreloadTop = _eltsToPreload[_index];
            if (_eltsToPreloadTop != null) {
                for (var _jndex in _eltsToPreloadTop) {
                    var _eltsToPreloadLeft = _eltsToPreloadTop[_jndex];
                    if (_eltsToPreloadLeft != null) {
                        for (var _kndex = 0; _kndex < _eltsToPreloadLeft.length; _kndex++) {
                            var _datas = _eltsToPreloadLeft[_kndex];
                            if (!_datas.elt.hasAttr('data-indexOnScroll')) {
                                _datas.elt.attr('data-indexOnScroll', 'aos_' + _counter);
                                _counter++;
                            }
                            DevWaiona_Page.currents.scroll.eltsPreload[_datas.elt.attr('data-indexOnScroll')] = _datas;
                            DevWaiona_Page.elements.main.animFlash.append('<div class="dW_' + _datas.elt.attr('data-indexOnScroll') + '"></div>');
                            _datas.elt.addClass('dW_generic__toPreloadOnScroll');
                            delete _datas;
                        }
                    }
                    delete _eltsToPreloadLeft;
                }
            }
            delete _eltsToPreloadTop;
        }
        delete _eltsToPreload;
        //
        delete _counter;
    },
    //
    initOnMouseMoveHandlers: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initOnMouseMoveHandlers();');
        //
        jQuery(document)
            .on(
                'dW_Global_MouseMove_AddEvent',
                {},
                function (pEvt, pParams) {
                    //
//					DevWaiona_Page.consoleLog('dW_Global_MouseMove_AddEvent : ' + pParams.evt);
                    //
                    var _evtExists = false;
                    for (var _index = 0; _index < DevWaiona_Page.currents.mouseMove.evts.length; _index++) {
                        if (DevWaiona_Page.currents.mouseMove.evts[_index].evt == pParams.evt) {
                            _evtExists = true;
                            break;
                        }
                    }
                    if (!_evtExists) {
                        DevWaiona_Page.currents.mouseMove.evts
                            .push(
                                {
                                    'evt': pParams.evt,
                                    'trig': false
                                }
                            )
                        ;
                        DevWaiona_Page.initOnMouseMove();
                    }
                    delete _evtExists;
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'dW_Global_MouseMove_RemoveEvent',
                {},
                function (pEvt, pParams) {
                    //
//					DevWaiona_Page.consoleLog('dW_Global_MouseMove_RemoveEvent : ' + pParams.evt);
                    //
                    for (var _evtIndex = 0; _evtIndex < DevWaiona_Page.currents.mouseMove.evts.length; _evtIndex++) {
                        //
                        if (DevWaiona_Page.currents.mouseMove.evts[_evtIndex].evt === pParams.evt) {
                            //
                            DevWaiona_Page.currents.mouseMove.evts.splice(_evtIndex, 1);
                            //
                            DevWaiona_Page.initOnMouseMove();
                            //
                            break;
                        }
                    }
                }
            )
        ;
        //
        jQuery(document)
            .on(
                'dW_Global_MouseMove_TriggerEvents',
                {},
                function (pEvt, pParams) {
                    //
//					DevWaiona_Page.consoleLog('dW_Global_MouseMove_TriggerEvents');
                    //
                    if (DevWaiona_Page.currents.mouseMove.evts.length > 0) {
                        //
                        for (var _evtIndex = 0; _evtIndex < DevWaiona_Page.currents.mouseMove.evts.length; _evtIndex++) {
                            //
                            if (!DevWaiona_Page.currents.mouseMove.evts[_evtIndex].trig) {
                                //
                                DevWaiona_Page.currents.mouseMove.evts[_evtIndex].trig = true;
                                //
                                jQuery(document)
                                    .trigger(
                                        DevWaiona_Page.currents.mouseMove.evts[_evtIndex].evt,
                                        [
                                            {
                                                'evtIndex': _evtIndex,
                                                'mouseX': DevWaiona_Page.currents.mouseMove.currPos.mouseX,
                                                'mouseY': DevWaiona_Page.currents.mouseMove.currPos.mouseY
                                            }
                                        ]
                                    )
                                ;
                            }
                        }
                    }
                }
            )
        ;
    },
    //
    initOnMouseMove: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initOnMouseMove();');
        //
        if ((!DevWaiona_Page.currents.bTouch)
//			&& (DevWaiona_Page.currents.mouseMove.evts.length > 0)
        ) {
            //
            jQuery(window)
                .on(
                    'mousemove',
                    function (pEvt) {
                        //
                        DevWaiona_Page.currents.mouseMove.currPos.mouseX = parseInt(pEvt.pageX, 10) - DevWaiona_Page.currents.scroll.currLeft;
                        DevWaiona_Page.currents.mouseMove.currPos.mouseY = parseInt(pEvt.pageY, 10) - DevWaiona_Page.currents.scroll.currTop;
                        //
                        if (DevWaiona_Page.currents.mouseMove.evts.length > 0) {
                            //
                            jQuery(document)
                                .trigger(
                                    'dW_Global_MouseMove_TriggerEvents',
                                    [
                                        {}
                                    ]
                                )
                            ;
                        }
                    }
                )
            ;
        } else {
            //
            jQuery(window)
                .off(
                    'mousemove'
                )
            ;
        }
        //
    },
    //
    initPreload: function (pCompleteFnk) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initPreload();');
        //
        //
        if (!DevWaiona_Page.elements.main.root.hasClass('dW_generic__preloading')) {
            DevWaiona_Page.elements.main.root.addClass('dW_generic__preloading');
        }
        //
        if (typeof pCompleteFnk == 'function') {
            pCompleteFnk.call(this);
        }
        //
        return false;
    },
    //
    preloaded: function (pCompleteFnk) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.preloaded();');
        //
        //
        if (typeof pCompleteFnk == 'function') {
            pCompleteFnk.call(this);
        }
        //
        return false;
    },
    //
    closePreload: function (pCompleteFnk) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.closePreload();');
        //
        DevWaiona_Page.elements.main.root
            .removeClass('dW_generic__preloading')
            .addClass('dW_generic__preloaded')
        ;
        //
        return false;
    },
    //
    loadImages: function (pCompleteFnk) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.loadImages();');
        //
        var completeFnk = pCompleteFnk;
        //
        for (var _imagesToLoadIndex = 0; _imagesToLoadIndex < DevWaiona_datas.imagesToLoad['all'].length; _imagesToLoadIndex++) {
            DevWaiona_datas.imagesToPreload
                .push(DevWaiona_datas.imagesToLoad['all'][_imagesToLoadIndex])
            ;
        }
        if (DevWaiona_Page.currents.bWebP) {
            for (var _imagesToLoadIndex = 0; _imagesToLoadIndex < DevWaiona_datas.imagesToLoad['webp'].length; _imagesToLoadIndex++) {
                DevWaiona_datas.imagesToPreload
                    .push(DevWaiona_datas.imagesToLoad['webp'][_imagesToLoadIndex])
                ;
            }
        } else {
            for (var _imagesToLoadIndex = 0; _imagesToLoadIndex < DevWaiona_datas.imagesToLoad['no-webp'].length; _imagesToLoadIndex++) {
                DevWaiona_datas.imagesToPreload
                    .push(DevWaiona_datas.imagesToLoad['no-webp'][_imagesToLoadIndex])
                ;
            }
        }
        //
        DevWaiona_Page.currents.preloader.nbImagesToLoad = DevWaiona_datas.imagesToPreload.length;
        //
        if (DevWaiona_Page.currents.preloader.nbImagesToLoad > 0) {
            //
            for (var index_image = 0; index_image < DevWaiona_Page.currents.preloader.nbImagesToLoad; index_image++) {
                DevWaiona_Page.currents.preloader.imagesizeToLoad += parseInt(DevWaiona_datas.imagesToPreload[index_image].filesize);
            }
            //
            for (var index_image = 0; index_image < DevWaiona_Page.currents.preloader.nbImagesToLoad; index_image++) {
                //
                var _curr_img = new Image();
                _curr_img['indexImageToLoad'] = index_image;
                _curr_img.onload = function () {
                    //
                    DevWaiona_Page.currents.preloader.nbImagesLoaded++;
                    DevWaiona_Page.currents.preloader.imagesizeLoaded += parseInt(DevWaiona_datas.imagesToPreload[this.indexImageToLoad].filesize);
//            					var _percentage = Math.floor((DevWaiona_Page.currents.preloader.imagesizeLoaded * 100) / DevWaiona_Page.currents.preloader.imagesizeToLoad);
                    //
                    var _completeFnk = null;
                    if (DevWaiona_Page.currents.preloader.nbImagesLoaded == DevWaiona_Page.currents.preloader.nbImagesToLoad) {
                        //
                        DevWaiona_datas.imagesToPreload.splice(0, DevWaiona_Page.currents.preloader.nbImagesToLoad);
                        //
                        delete (DevWaiona_datas.imagesToPreload);
                        //
                        _completeFnk = completeFnk;
                    }
                    //
                    DevWaiona_Page.preloaded(_completeFnk);
                };
                _curr_img.src = DevWaiona_datas.imagesToPreload[_curr_img['indexImageToLoad']].path;
            }
            //
        } else {
            //
            DevWaiona_Page.preloaded(completeFnk);
        }
        //
        delete completeFnk;
        //
        return false;
    },
    //
    isMobile: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.isMobile();');
        //
        return /Mobile/i.test(navigator.userAgent);
    },
    //
    initMobileYes: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initMobileYes();');
        //
        //
        DevWaiona_Page.currents.bMobile = true;
        DevWaiona_Page.elements.main.root.addClass('dW_generic__mobile-yes');
        //
        DevWaiona_Page.initTouchYesEvents();
        //
        return false;
    },
    //
    initMobileNo: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initMobileNo();');
        //
        //
        DevWaiona_Page.currents.bMobile = false;
        DevWaiona_Page.elements.main.root.addClass('dW_generic__mobile-no');
        //
        return false;
    },
    //
    hasTouchEvent: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.hasTouchEvent();');
        //
        return 'ontouchstart' in window || navigator.msMaxTouchPoints;
    },
    //
    initTouchYes: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initTouchYes();');
        //
        //
        DevWaiona_Page.currents.bTouch = true;
        DevWaiona_Page.elements.main.root.addClass('dW_generic__touch-yes');
        //
        DevWaiona_Page.initTouchYesEvents();
        //
        return false;
    },
    //
    initTouchYesEvents: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initTouchYesEvents();');
        //
        //
        return false;
    },
    //
    initTouchNo: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.initTouchNo();');
        //
        //
        DevWaiona_Page.currents.bTouch = false;
        DevWaiona_Page.elements.main.root.addClass('dW_generic__touch-no');
        //
        return false;
    },
    //
    isCss3: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.isCss3();');
        //
        //
        if (this.bCss3 == null) {
            //
            var body = document.body || document.documentElement;
            var bodyStyle = body.style;

            this.bCss3 = false;

            this.bCss3 = bodyStyle.WebkitTransition !== undefined || bodyStyle.MozTransition !== undefined || bodyStyle.transition !== undefined;

            if (document.documentMode) {
                if (document.documentMode == 9) {
                    this.bCss3 = false;
                }
            }
        }
        return this.bCss3;
    },
    // Google WebP detection
    detectWebP: function (feature) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.detectWebP(' + feature + ');');
        //
        var kTestImages = {
            lossy: "UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA",
            lossless: "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==",
            alpha: "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==",
            animation: "UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA"
        };
        var img = new Image();
        img.onload = function () {
            var result = (img.width > 0) && (img.height > 0);
//            callback(feature, result);
            if (result) {
                DevWaiona_Page.currents.bWebP = true;
                DevWaiona_Page.elements.main.root.addClass('dW_generic__webp-yes');
            } else {
                DevWaiona_Page.currents.bWebP = false;
                DevWaiona_Page.elements.main.root.addClass('dW_generic__webp-no');
            }
            jQuery(document)
                .trigger('dW_Global_GoogleWebPDetection_Ended')
            ;
        };
        img.onerror = function () {
//            callback(feature, false);
            DevWaiona_Page.currents.bWebP = false;
            DevWaiona_Page.elements.main.root.addClass('dW_generic__webp-no');
            jQuery(document)
                .trigger('dW_Global_GoogleWebPDetection_Ended')
            ;
        };
        img.src = "data:image/webp;base64," + kTestImages[feature];
    },
    //
    getDocumentHeight: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.getDocumentHeight();');
        //
        DevWaiona_Page.currents.documentHeight = Math.max(
            document.body.scrollHeight,
            document.documentElement.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.offsetHeight,
            document.body.clientHeight,
            document.documentElement.clientHeight
        );
        //
        return false;
    },
    //
    getWindowHeight: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.getWindowHeight();');
        //
        //
        if (window.innerHeight) {
            DevWaiona_Page.currents.windowHeight = window.innerHeight;
        } else {
            DevWaiona_Page.currents.windowHeight = jQuery(window).height();
        }
        //
        return false;
    },
    //
    getWindowWidth: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.getWindowWidth();');
        //
        //
        if (window.innerWidth) {
            DevWaiona_Page.currents.windowWidth = window.innerWidth;
        } else {
            DevWaiona_Page.currents.windowWidth = jQuery(window).width();
        }
        //
        return false;
    },
    //
    setRootDimensions: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.setRootDimensions();');
        //
        DevWaiona_Page.elements.main.root.removeAttr('style');
        if (DevWaiona_Page.currents.windowWidth < 1000) {
            var _minHeight = (DevWaiona_Page.currents.windowHeight - DevWaiona_Page.elements.main.root.offset().top);
            if ((DevWaiona_Page.currents.windowWidth > 767)
                && (_minHeight < 500)
            ) {
                _minHeight = 500;
            }
            DevWaiona_Page.elements.main.root.css(
                {
                    'min-height': _minHeight + 'px'
                }
            );
            delete _minHeight;
        }
        //
        return false;
    },
    //
    setPageDimensions: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.setPageDimensions();');
        //
        DevWaiona_Page.elements.main.intro.root.removeAttr('style');
        DevWaiona_Page.elements.main.page.cover.root.removeAttr('style');
        //
//		if (DevWaiona_Page.currents.windowWidth > 999) {
        //
        var _newHeight = DevWaiona_Page.currents.windowHeight - jQuery('#container-header').height();
        //
        DevWaiona_Page.elements.main.intro.root
            .css(
                {
                    height: _newHeight,
                    top: jQuery('#container-header').height()
                }
            )
        ;
        jQuery('div.dW_close', DevWaiona_Page.elements.main.galleries.root)
            .css(
                {
                    marginTop: jQuery('#container-header').height()
                }
            )
        ;
        jQuery('div.dW_item', DevWaiona_Page.elements.main.galleries.root)
            .css(
                {
                    height: _newHeight,
                    top: jQuery('#container-header').height()
                }
            )
        ;
        DevWaiona_Page.elements.main.page.cover.root
            .css(
                {
                    height: _newHeight
                }
            )
        ;
        //
        delete _newHeight;
//		}
        //
        return false;
    },
    //
    setVersusGalleriesElementsSizes: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.setVersusGalleriesElementsSizes();');
        //
        if ((DevWaiona_Page.currents.windowWidth > 999)
        ) {
            //
            jQuery('div.dW_item', DevWaiona_Page.elements.main.galleries.root)
                .each(
                    function () {
                        //
                        var _init = true;
                        if (jQuery(this).hasClass('dW_generic__init')) {
                            _init = false;
                        }
                        //
                        var _eltContent = jQuery('div.dW_content', jQuery(this));
                        //
                        var _height = jQuery(this).height();
                        //
                        if (_init) {
                            _eltContent.attr('data-width', _eltContent.width() / DevWaiona_Page.currents.windowHeight);
                        }
                        _eltContent.width(_height * parseFloat(_eltContent.attr('data-width')));
                        //
                        switch (jQuery(this).attr('data-cle')) {
                            //
                            case 'stronger':
                                $(this).scrollLeft(-1 * ($(this).width() - _eltContent.width()));
                                break;
                            //
                            case 'faster':
                                $(this).scrollLeft(0);
                                break;
                        }
                        //
                        jQuery('div.dW_visuel, div.dW_video, div.dW_packshot, div.dW_background, div.dW_titre', _eltContent)
                            .each(
                                function () {
                                    //
                                    if (_init) {
                                        jQuery(this).attr('data-left', jQuery(this).position().left / DevWaiona_Page.currents.windowHeight);
                                    }
                                    jQuery(this)
                                        .css(
                                            {
                                                left: (_height * parseFloat(jQuery(this).attr('data-left'))) + 'px'
                                            }
                                        )
                                    ;
                                }
                            )
                        ;
                        jQuery('div.dW_visuel, div.dW_video, div.dW_packshot, div.dW_background', _eltContent)
                            .each(
                                function () {
                                    //
                                    if (_init) {
                                        jQuery(this).attr('data-width', jQuery(this).width() / DevWaiona_Page.currents.windowHeight);
                                    }
                                    jQuery(this).width(_height * parseFloat(jQuery(this).attr('data-width')));
                                }
                            )
                        ;
                        jQuery('div.dW_scroll', _eltContent)
                            .each(
                                function () {
                                    //
                                    if (_init) {
                                        jQuery(this).attr('data-right', jQuery(this).position().right / DevWaiona_Page.currents.windowHeight);
                                    }
                                    jQuery(this)
                                        .css(
                                            {
                                                right: (_height * parseFloat(jQuery(this).attr('data-right'))) + 'px'
                                            }
                                        )
                                    ;
                                }
                            )
                        ;
                        //
                        delete _height;
                        delete _init;
                        //
                        delete _eltContent;
                        //
                        jQuery(this).addClass('dW_generic__init');
                    }
                )
            ;
        } else {
            //
            jQuery('div.dW_visuel, div.dW_video, div.dW_packshot, div.dW_background, div.dW_titre', jQuery('div.dW_item', DevWaiona_Page.elements.main.galleries.root))
                .removeAttr('style')
            ;
        }
        //
        return false;
    },
    //
    resize: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.resize();');
        //
        var _prevWidth = DevWaiona_Page.currents.windowWidth;
        var _prevHeight = DevWaiona_Page.currents.windowHeight;
        //
        DevWaiona_Page.getDocumentHeight();
        DevWaiona_Page.getWindowHeight();
        DevWaiona_Page.getWindowWidth();
        //
        DevWaiona_Page.setRootDimensions();
        DevWaiona_Page.setPageDimensions();
        //
        if ((DevWaiona_Page.currents.windowWidth > 999)
            && (DevWaiona_Page.currents.windowHeight != _prevHeight)
        ) {
            DevWaiona_Page.setVersusGalleriesElementsSizes();
        }
        //
        if ((!DevWaiona_Page.currents.bTouch)
            || (DevWaiona_Page.currents.windowWidth != _prevWidth)
        ) {
            //
            for (var _parallaxIndex in DevWaiona_Page.currents.parallaxDatas) {
                DevWaiona_Page.currents.parallaxDatas[_parallaxIndex].m = null;
                DevWaiona_Page.currents.parallaxDatas[_parallaxIndex].p = null;
            }
            //
            /*
			 * Resize fnk
			 */
            //
            DevWaiona_Page.initOnScroll();
            //
            jQuery(window)
                .trigger('scroll')
            ;
            //
        }
        //
        jQuery(document)
            .trigger(
                'dW_Global_PageResize',
                []
            )
        ;
        //
        DevWaiona_Page.elements.main.root.removeClass('dW_generic__resizing');
        //
        delete _prevWidth;
        //
        return false;
    },
    //
    isBlankOrNull: function (pValue) {
        if (pValue === null) {
            return true;
        } else if (pValue === undefined) {
            return true;
        } else if (typeof (pValue) === 'string') {
            if (pValue === '') {
                return true;
            }
        }
        return false;
    },
    //
    consoleLog: function (text) {
        if (DevWaiona_datas.params.consoleLog) {
            if (typeof (console.log(text)) != 'undefined') {
                console.log(text);
            }
        }
    },
    //
    getUrlHashtag: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.getUrlHashtag();');
        //
        var _value = '';
        var _url = window.location.toString();
        if (_url.indexOf('#') != -1) {
            _value = _url.substr(_url.indexOf('#') + 1);
        }
        delete (_url);
        //
        return _value;
    },
    //
    changeUrlHashtag: function (pValue) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.changeUrlHashtag(' + pValue + ');');
        //
        var _url = window.location.toString();
        if (_url.indexOf('#') != -1) {
            _url = _url.substr(0, _url.indexOf('#'));
        }
        //
        DevWaiona_Page.currents.hashtag = pValue;
        //
        window.location = _url + '#' + DevWaiona_Page.currents.hashtag;
    },
    //
    scrollToHashtag: function (pHashtag) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.scrollToHashtag(' + pHashtag + ');');
        //
        DevWaiona_Page.scrollTo(jQuery('.dW_generic__hashtag[data-hashtag="' + pHashtag + '"]', DevWaiona_Page.elements.main.root), parseInt(jQuery('body').css('padding-top')));
        //
        DevWaiona_Page.changeUrlHashtag(pHashtag);
        //
        return false;
    },
    //
    scrollTo: function (pElt, pMargeTop) {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.scrollTo();');
        //
        if (pElt.length > 0) {
            //
//			if (DevWaiona_Page.currents.windowWidth > 999) {
            //
            pMargeTop += jQuery('#container-header').height();
//			}
            //
            var _topDest = parseInt((pElt.offset().top - pMargeTop), 10);
            var _deltaToScroll = parseInt((jQuery(document).scrollTop() - _topDest), 10);
            var _timeToScroll = parseInt((Math.abs(_deltaToScroll) * DevWaiona_Page.currents.scrollTo.timeByPixel), 10);
            //
            var _callBackFnk = function () {
                //
                jQuery(document)
                    .trigger('dW_Global_ScrollTo_Ended')
                ;
                //
                jQuery(window)
                    .trigger('scroll')
                ;
            };
            //
            if (_deltaToScroll == 0) {
                //
                _callBackFnk.call(this);
            } else {
                //
                if (_timeToScroll == 0) {
                    _timeToScroll = DevWaiona_Page.currents.scrollTo.timeByPixel;
                }
                //
                jQuery('html, body')
                    .animate(
                        {
                            scrollTop: _topDest
                        },
                        _timeToScroll,
                        _callBackFnk
                    )
                ;
            }
            //
            delete _callBackFnk;
            //
            delete _timeToScroll;
            delete _deltaToScroll;
            delete _topDest;
        }
        //
        return false;
    },
    //
    uniqId: function () {
        //
        DevWaiona_Page.consoleLog('DevWaiona_Page.uniqId();');
        //
        var _uniqId = null;
        while (_uniqId == null) {
            _uniqId = 'dW_' + Math.round(new Date().getTime() + (Math.random() * 100));
            if (jQuery('#' + _uniqId).length > 0) {
                _uniqId = null;
            }
        }
        //
        return _uniqId;
    },
    //
    getRandomInt: function (pMinVal, pMaxVal) {
        pMinVal = Math.ceil(pMinVal);
        pMaxVal = Math.floor(pMaxVal);
        return Math.floor(Math.random() * (pMaxVal - pMinVal + 1)) + pMinVal;
    }
};
//
jQuery(window)
    .resize(
        function () {
            if (!DevWaiona_Page.currents.bTouch) {
                DevWaiona_Page.elements.main.root.addClass('dW_generic__resizing');
            }
            DevWaiona_Page.resize();
        }
    )
;
//
jQuery(document)
    .ready(
        function () {
            DevWaiona_Page.init();
        }
    )
;


function onYoutubePlayerReady(event) {
    if ((DevWaiona_Page.currents.youtubePlayer.eltPlayer != null)
    ) {
        DevWaiona_Page.currents.youtubePlayer.eltPlayer
            .removeClass('dW_generic__hidden')
        ;
    }
    event.target.playVideo();
}

function onYoutubePlayerStateChange(event) {
    if (event.data === 0) {
        if ((DevWaiona_Page.currents.youtubePlayer.eltPlayer != null)
        ) {
            DevWaiona_Page.currents.youtubePlayer.eltPlayer
                .addClass('dW_generic__hidden')
                .html('')
            ;
            DevWaiona_Page.currents.youtubePlayer.eltPlayer = null;
        }
    }
}

//
