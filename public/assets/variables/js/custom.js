jQuery(window).on('scroll', function () {
    if ((parseInt(jQuery(window).scrollTop(), 10) > 0)) {
        setTimeout(function () {
            jQuery('#header').addClass('sticky');
        }, 1150);
    } else {
        jQuery('#header').removeClass('sticky');
    }
});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("openNav").style.display = 'none';
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("openNav").style.display = "inline-block";
    document.body.style.backgroundColor = "white";
}

// With JQuery
// $("#ex2").slider({});

// Without JQuery
// var slider = new Slider('#ex2', {});
