!function (e) {
    var t = {};

    function i(n) {
        if (t[n]) return t[n].exports;
        var r = t[n] = {i: n, l: !1, exports: {}};
        return e[n].call(r.exports, r, r.exports, i), r.l = !0, r.exports
    }

    i.m = e, i.c = t, i.d = function (e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {enumerable: !0, get: n})
    }, i.r = function (e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(e, "__esModule", {value: !0})
    }, i.t = function (e, t) {
        if (1 & t && (e = i(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var n = Object.create(null);
        if (i.r(n), Object.defineProperty(n, "default", {
            enumerable: !0,
            value: e
        }), 2 & t && "string" != typeof e) for (var r in e) i.d(n, r, function (t) {
            return e[t]
        }.bind(null, r));
        return n
    }, i.n = function (e) {
        var t = e && e.__esModule ? function () {
            return e.default
        } : function () {
            return e
        };
        return i.d(t, "a", t), t
    }, i.o = function (e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, i.p = "/", i(i.s = 59)
}([function (e, t, i) {
    "use strict";
    var n = i(7), r = Object.prototype.toString;

    function s(e) {
        return "[object Array]" === r.call(e)
    }

    function o(e) {
        return void 0 === e
    }

    function a(e) {
        return null !== e && "object" == typeof e
    }

    function l(e) {
        if ("[object Object]" !== r.call(e)) return !1;
        var t = Object.getPrototypeOf(e);
        return null === t || t === Object.prototype
    }

    function u(e) {
        return "[object Function]" === r.call(e)
    }

    function c(e, t) {
        if (null != e) if ("object" != typeof e && (e = [e]), s(e)) for (var i = 0, n = e.length; i < n; i++) t.call(null, e[i], i, e); else for (var r in e) Object.prototype.hasOwnProperty.call(e, r) && t.call(null, e[r], r, e)
    }

    e.exports = {
        isArray: s, isArrayBuffer: function (e) {
            return "[object ArrayBuffer]" === r.call(e)
        }, isBuffer: function (e) {
            return null !== e && !o(e) && null !== e.constructor && !o(e.constructor) && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e)
        }, isFormData: function (e) {
            return "undefined" != typeof FormData && e instanceof FormData
        }, isArrayBufferView: function (e) {
            return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
        }, isString: function (e) {
            return "string" == typeof e
        }, isNumber: function (e) {
            return "number" == typeof e
        }, isObject: a, isPlainObject: l, isUndefined: o, isDate: function (e) {
            return "[object Date]" === r.call(e)
        }, isFile: function (e) {
            return "[object File]" === r.call(e)
        }, isBlob: function (e) {
            return "[object Blob]" === r.call(e)
        }, isFunction: u, isStream: function (e) {
            return a(e) && u(e.pipe)
        }, isURLSearchParams: function (e) {
            return "undefined" != typeof URLSearchParams && e instanceof URLSearchParams
        }, isStandardBrowserEnv: function () {
            return ("undefined" == typeof navigator || "ReactNative" !== navigator.product && "NativeScript" !== navigator.product && "NS" !== navigator.product) && "undefined" != typeof window && "undefined" != typeof document
        }, forEach: c, merge: function e() {
            var t = {};

            function i(i, n) {
                l(t[n]) && l(i) ? t[n] = e(t[n], i) : l(i) ? t[n] = e({}, i) : s(i) ? t[n] = i.slice() : t[n] = i
            }

            for (var n = 0, r = arguments.length; n < r; n++) c(arguments[n], i);
            return t
        }, extend: function (e, t, i) {
            return c(t, function (t, r) {
                e[r] = i && "function" == typeof t ? n(t, i) : t
            }), e
        }, trim: function (e) {
            return e.replace(/^\s*/, "").replace(/\s*$/, "")
        }, stripBOM: function (e) {
            return 65279 === e.charCodeAt(0) && (e = e.slice(1)), e
        }
    }
}, function (e, t, i) {
    e.exports = i(41)
}, function (e, t, i) {
    "use strict";
    var n = i(34);

    function r() {
        var e = navigator.userAgent.toLowerCase(), t = navigator.appVersion.toLowerCase(),
            i = /windows phone|iemobile|wpdesktop/.test(e), n = !i && /android.*mobile/.test(e),
            r = !i && !n && /android/i.test(e), s = n || r, o = !i && /ip(hone|od|ad)/i.test(e) && !window.MSStream,
            a = !i && /ipad/i.test(e) && o, l = r || a, u = n || o && !a || i, c = u || l,
            h = e.indexOf("firefox") > -1, d = !!e.match(/version\/[\d\.]+.*safari/), p = e.indexOf("opr") > -1,
            f = !window.ActiveXObject && "ActiveXObject" in window,
            m = t.indexOf("msie") > -1 || f || t.indexOf("edge") > -1, g = e.indexOf("edge") > -1,
            v = null !== window.chrome && void 0 !== window.chrome && "google inc." == navigator.vendor.toLowerCase() && !p && !g;
        this.infos = {
            isDroid: s,
            isDroidPhone: n,
            isDroidTablet: r,
            isWindowsPhone: i,
            isIos: o,
            isIpad: a,
            isDevice: c,
            isEdge: g,
            isIE: m,
            isIE11: f,
            isPhone: u,
            isTablet: l,
            isFirefox: h,
            isSafari: d,
            isOpera: p,
            isChrome: v,
            isDesktop: !u && !l
        }, Object.keys(this.infos).forEach(function (e) {
            Object.defineProperty(this, e, {
                get: function () {
                    return this.infos[e]
                }
            })
        }, this), Object.freeze(this)
    }

    e.exports = new r, r.prototype.addClasses = function (e) {
        Object.keys(this.infos).forEach(function (t) {
            this.infos[t] && function (e, t) {
                e.addClass ? e.addClass(t) : e.classList ? e.classList.add(t) : e.className += " " + t
            }(e, n(t))
        }, this)
    }, r.prototype.getInfos = function () {
        return e = this.infos, JSON.parse(JSON.stringify(e));
        var e
    }
}, function (e, t, i) {
    var n, r;
    !function (s, o) {
        "use strict";
        n = [i(58)], void 0 === (r = function (e) {
            return function (e, t) {
                var i = e.jQuery, n = e.console;

                function r(e, t) {
                    for (var i in t) e[i] = t[i];
                    return e
                }

                var s = Array.prototype.slice;

                function o(e, t, a) {
                    if (!(this instanceof o)) return new o(e, t, a);
                    var l, u = e;
                    "string" == typeof e && (u = document.querySelectorAll(e)), u ? (this.elements = (l = u, Array.isArray(l) ? l : "object" == typeof l && "number" == typeof l.length ? s.call(l) : [l]), this.options = r({}, this.options), "function" == typeof t ? a = t : r(this.options, t), a && this.on("always", a), this.getImages(), i && (this.jqDeferred = new i.Deferred), setTimeout(this.check.bind(this))) : n.error("Bad element for imagesLoaded " + (u || e))
                }

                o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
                    this.images = [], this.elements.forEach(this.addElementImages, this)
                }, o.prototype.addElementImages = function (e) {
                    "IMG" == e.nodeName && this.addImage(e), !0 === this.options.background && this.addElementBackgroundImages(e);
                    var t = e.nodeType;
                    if (t && a[t]) {
                        for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
                            var r = i[n];
                            this.addImage(r)
                        }
                        if ("string" == typeof this.options.background) {
                            var s = e.querySelectorAll(this.options.background);
                            for (n = 0; n < s.length; n++) {
                                var o = s[n];
                                this.addElementBackgroundImages(o)
                            }
                        }
                    }
                };
                var a = {1: !0, 9: !0, 11: !0};

                function l(e) {
                    this.img = e
                }

                function u(e, t) {
                    this.url = e, this.element = t, this.img = new Image
                }

                return o.prototype.addElementBackgroundImages = function (e) {
                    var t = getComputedStyle(e);
                    if (t) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) {
                        var r = n && n[2];
                        r && this.addBackground(r, e), n = i.exec(t.backgroundImage)
                    }
                }, o.prototype.addImage = function (e) {
                    var t = new l(e);
                    this.images.push(t)
                }, o.prototype.addBackground = function (e, t) {
                    var i = new u(e, t);
                    this.images.push(i)
                }, o.prototype.check = function () {
                    var e = this;

                    function t(t, i, n) {
                        setTimeout(function () {
                            e.progress(t, i, n)
                        })
                    }

                    this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? this.images.forEach(function (e) {
                        e.once("progress", t), e.check()
                    }) : this.complete()
                }, o.prototype.progress = function (e, t, i) {
                    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && n && n.log("progress: " + i, e, t)
                }, o.prototype.complete = function () {
                    var e = this.hasAnyBroken ? "fail" : "done";
                    if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
                        var t = this.hasAnyBroken ? "reject" : "resolve";
                        this.jqDeferred[t](this)
                    }
                }, l.prototype = Object.create(t.prototype), l.prototype.check = function () {
                    this.getIsImageComplete() ? this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.proxyImage.src = this.img.src)
                }, l.prototype.getIsImageComplete = function () {
                    return this.img.complete && this.img.naturalWidth
                }, l.prototype.confirm = function (e, t) {
                    this.isLoaded = e, this.emitEvent("progress", [this, this.img, t])
                }, l.prototype.handleEvent = function (e) {
                    var t = "on" + e.type;
                    this[t] && this[t](e)
                }, l.prototype.onload = function () {
                    this.confirm(!0, "onload"), this.unbindEvents()
                }, l.prototype.onerror = function () {
                    this.confirm(!1, "onerror"), this.unbindEvents()
                }, l.prototype.unbindEvents = function () {
                    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
                }, u.prototype = Object.create(l.prototype), u.prototype.check = function () {
                    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url, this.getIsImageComplete() && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
                }, u.prototype.unbindEvents = function () {
                    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
                }, u.prototype.confirm = function (e, t) {
                    this.isLoaded = e, this.emitEvent("progress", [this, this.element, t])
                }, o.makeJQueryPlugin = function (t) {
                    (t = t || e.jQuery) && ((i = t).fn.imagesLoaded = function (e, t) {
                        return new o(this, e, t).jqDeferred.promise(i(this))
                    })
                }, o.makeJQueryPlugin(), o
            }(s, e)
        }.apply(t, n)) || (e.exports = r)
    }("undefined" != typeof window ? window : this)
}, function (e, t, i) {
    (function (t) {
        "object" == typeof navigator && (e.exports = function () {
            "use strict";

            function e(e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }

            function i(e, t) {
                for (var i = 0; i < t.length; i++) {
                    var n = t[i];
                    n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                }
            }

            function n(e, t, n) {
                return t && i(e.prototype, t), n && i(e, n), e
            }

            function r(e, t, i) {
                return t in e ? Object.defineProperty(e, t, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = i, e
            }

            function s(e, t) {
                var i = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter(function (t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function o(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var i = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? s(Object(i), !0).forEach(function (t) {
                        r(e, t, i[t])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : s(Object(i)).forEach(function (t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(i, t))
                    })
                }
                return e
            }

            function a(e, t) {
                return function (e) {
                    if (Array.isArray(e)) return e
                }(e) || function (e, t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) {
                        var i = [], n = !0, r = !1, s = void 0;
                        try {
                            for (var o, a = e[Symbol.iterator](); !(n = (o = a.next()).done) && (i.push(o.value), !t || i.length !== t); n = !0) ;
                        } catch (e) {
                            r = !0, s = e
                        } finally {
                            try {
                                n || null == a.return || a.return()
                            } finally {
                                if (r) throw s
                            }
                        }
                        return i
                    }
                }(e, t) || u(e, t) || function () {
                    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }()
            }

            function l(e) {
                return function (e) {
                    if (Array.isArray(e)) return c(e)
                }(e) || function (e) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) return Array.from(e)
                }(e) || u(e) || function () {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }()
            }

            function u(e, t) {
                if (e) {
                    if ("string" == typeof e) return c(e, t);
                    var i = Object.prototype.toString.call(e).slice(8, -1);
                    return "Object" === i && e.constructor && (i = e.constructor.name), "Map" === i || "Set" === i ? Array.from(e) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? c(e, t) : void 0
                }
            }

            function c(e, t) {
                (null == t || t > e.length) && (t = e.length);
                for (var i = 0, n = new Array(t); i < t; i++) n[i] = e[i];
                return n
            }

            function h(e, t) {
                for (var i = 0; i < t.length; i++) {
                    var n = t[i];
                    n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                }
            }

            function d(e, t, i) {
                return t in e ? Object.defineProperty(e, t, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = i, e
            }

            function p(e, t) {
                var i = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter(function (t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function f(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var i = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? p(Object(i), !0).forEach(function (t) {
                        d(e, t, i[t])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : p(Object(i)).forEach(function (t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(i, t))
                    })
                }
                return e
            }

            var m = {addCSS: !0, thumbWidth: 15, watch: !0};
            var g, v, y, b = function (e) {
                return null != e ? e.constructor : null
            }, w = function (e, t) {
                return !!(e && t && e instanceof t)
            }, D = function (e) {
                return b(e) === String
            }, T = function (e) {
                return Array.isArray(e)
            }, C = function (e) {
                return w(e, NodeList)
            }, k = D, E = T, _ = C, x = function (e) {
                return w(e, Element)
            }, S = function (e) {
                return w(e, Event)
            }, P = function (e) {
                return function (e) {
                    return null == e
                }(e) || (D(e) || T(e) || C(e)) && !e.length || function (e) {
                    return b(e) === Object
                }(e) && !Object.keys(e).length
            }, O = function () {
                function e(t, i) {
                    (function (e, t) {
                        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                    })(this, e), x(t) ? this.element = t : k(t) && (this.element = document.querySelector(t)), x(this.element) && P(this.element.rangeTouch) && (this.config = f({}, m, {}, i), this.init())
                }

                return function (e, t, i) {
                    t && h(e.prototype, t), i && h(e, i)
                }(e, [{
                    key: "init", value: function () {
                        e.enabled && (this.config.addCSS && (this.element.style.userSelect = "none", this.element.style.webKitUserSelect = "none", this.element.style.touchAction = "manipulation"), this.listeners(!0), this.element.rangeTouch = this)
                    }
                }, {
                    key: "destroy", value: function () {
                        e.enabled && (this.config.addCSS && (this.element.style.userSelect = "", this.element.style.webKitUserSelect = "", this.element.style.touchAction = ""), this.listeners(!1), this.element.rangeTouch = null)
                    }
                }, {
                    key: "listeners", value: function (e) {
                        var t = this, i = e ? "addEventListener" : "removeEventListener";
                        ["touchstart", "touchmove", "touchend"].forEach(function (e) {
                            t.element[i](e, function (e) {
                                return t.set(e)
                            }, !1)
                        })
                    }
                }, {
                    key: "get", value: function (t) {
                        if (!e.enabled || !S(t)) return null;
                        var i, n = t.target, r = t.changedTouches[0], s = parseFloat(n.getAttribute("min")) || 0,
                            o = parseFloat(n.getAttribute("max")) || 100, a = parseFloat(n.getAttribute("step")) || 1,
                            l = n.getBoundingClientRect(), u = 100 / l.width * (this.config.thumbWidth / 2) / 100;
                        return 0 > (i = 100 / l.width * (r.clientX - l.left)) ? i = 0 : 100 < i && (i = 100), 50 > i ? i -= (100 - 2 * i) * u : 50 < i && (i += 2 * (i - 50) * u), s + function (e, t) {
                            if (1 > t) {
                                var i = function (e) {
                                    var t = "".concat(e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                                    return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
                                }(t);
                                return parseFloat(e.toFixed(i))
                            }
                            return Math.round(e / t) * t
                        }(i / 100 * (o - s), a)
                    }
                }, {
                    key: "set", value: function (t) {
                        e.enabled && S(t) && !t.target.disabled && (t.preventDefault(), t.target.value = this.get(t), function (e, t) {
                            if (e && t) {
                                var i = new Event(t, {bubbles: !0});
                                e.dispatchEvent(i)
                            }
                        }(t.target, "touchend" === t.type ? "change" : "input"))
                    }
                }], [{
                    key: "setup", value: function (t) {
                        var i = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}, n = null;
                        if (P(t) || k(t) ? n = Array.from(document.querySelectorAll(k(t) ? t : 'input[type="range"]')) : x(t) ? n = [t] : _(t) ? n = Array.from(t) : E(t) && (n = t.filter(x)), P(n)) return null;
                        var r = f({}, m, {}, i);
                        k(t) && r.watch && new MutationObserver(function (i) {
                            Array.from(i).forEach(function (i) {
                                Array.from(i.addedNodes).forEach(function (i) {
                                    x(i) && function (e, t) {
                                        return function () {
                                            return Array.from(document.querySelectorAll(t)).includes(this)
                                        }.call(e, t)
                                    }(i, t) && new e(i, r)
                                })
                            })
                        }).observe(document.body, {childList: !0, subtree: !0});
                        return n.map(function (t) {
                            return new e(t, i)
                        })
                    }
                }, {
                    key: "enabled", get: function () {
                        return "ontouchstart" in document.documentElement
                    }
                }]), e
            }(), A = function (e) {
                return null != e ? e.constructor : null
            }, M = function (e, t) {
                return Boolean(e && t && e instanceof t)
            }, L = function (e) {
                return null == e
            }, F = function (e) {
                return A(e) === Object
            }, j = function (e) {
                return A(e) === String
            }, B = function (e) {
                return A(e) === Function
            }, I = function (e) {
                return Array.isArray(e)
            }, R = function (e) {
                return M(e, NodeList)
            }, N = function (e) {
                return L(e) || (j(e) || I(e) || R(e)) && !e.length || F(e) && !Object.keys(e).length
            }, z = L, H = F, V = function (e) {
                return A(e) === Number && !Number.isNaN(e)
            }, q = j, W = function (e) {
                return A(e) === Boolean
            }, X = B, Y = I, U = R, G = function (e) {
                return M(e, Element)
            }, $ = function (e) {
                return M(e, Event)
            }, K = function (e) {
                return M(e, KeyboardEvent)
            }, J = function (e) {
                return M(e, TextTrack) || !L(e) && j(e.kind)
            }, Q = function (e) {
                if (M(e, window.URL)) return !0;
                if (!j(e)) return !1;
                var t = e;
                e.startsWith("http://") && e.startsWith("https://") || (t = "http://".concat(e));
                try {
                    return !N(new URL(t).hostname)
                } catch (e) {
                    return !1
                }
            }, Z = N, ee = (g = document.createElement("span"), v = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            }, y = Object.keys(v).find(function (e) {
                return void 0 !== g.style[e]
            }), !!q(y) && v[y]);

            function te(e, t) {
                setTimeout(function () {
                    try {
                        e.hidden = !0, e.offsetHeight, e.hidden = !1
                    } catch (e) {
                    }
                }, t)
            }

            var ie = {
                isIE: !!document.documentMode,
                isEdge: window.navigator.userAgent.includes("Edge"),
                isWebkit: "WebkitAppearance" in document.documentElement.style && !/Edge/.test(navigator.userAgent),
                isIPhone: /(iPhone|iPod)/gi.test(navigator.platform),
                isIos: /(iPad|iPhone|iPod)/gi.test(navigator.platform)
            };

            function ne(e, t) {
                return t.split(".").reduce(function (e, t) {
                    return e && e[t]
                }, e)
            }

            function re() {
                for (var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = arguments.length, i = new Array(t > 1 ? t - 1 : 0), n = 1; n < t; n++) i[n - 1] = arguments[n];
                if (!i.length) return e;
                var s = i.shift();
                return H(s) ? (Object.keys(s).forEach(function (t) {
                    H(s[t]) ? (Object.keys(e).includes(t) || Object.assign(e, r({}, t, {})), re(e[t], s[t])) : Object.assign(e, r({}, t, s[t]))
                }), re.apply(void 0, [e].concat(i))) : e
            }

            function se(e, t) {
                var i = e.length ? e : [e];
                Array.from(i).reverse().forEach(function (e, i) {
                    var n = i > 0 ? t.cloneNode(!0) : t, r = e.parentNode, s = e.nextSibling;
                    n.appendChild(e), s ? r.insertBefore(n, s) : r.appendChild(n)
                })
            }

            function oe(e, t) {
                G(e) && !Z(t) && Object.entries(t).filter(function (e) {
                    var t = a(e, 2)[1];
                    return !z(t)
                }).forEach(function (t) {
                    var i = a(t, 2), n = i[0], r = i[1];
                    return e.setAttribute(n, r)
                })
            }

            function ae(e, t, i) {
                var n = document.createElement(e);
                return H(t) && oe(n, t), q(i) && (n.innerText = i), n
            }

            function le(e, t, i, n) {
                G(t) && t.appendChild(ae(e, i, n))
            }

            function ue(e) {
                U(e) || Y(e) ? Array.from(e).forEach(ue) : G(e) && G(e.parentNode) && e.parentNode.removeChild(e)
            }

            function ce(e) {
                if (G(e)) for (var t = e.childNodes.length; t > 0;) e.removeChild(e.lastChild), t -= 1
            }

            function he(e, t) {
                return G(t) && G(t.parentNode) && G(e) ? (t.parentNode.replaceChild(e, t), e) : null
            }

            function de(e, t) {
                if (!q(e) || Z(e)) return {};
                var i = {}, n = re({}, t);
                return e.split(",").forEach(function (e) {
                    var t = e.trim(), r = t.replace(".", ""), s = t.replace(/[[\]]/g, "").split("="), o = a(s, 1)[0],
                        l = s.length > 1 ? s[1].replace(/["']/g, "") : "";
                    switch (t.charAt(0)) {
                        case".":
                            q(n.class) ? i.class = "".concat(n.class, " ").concat(r) : i.class = r;
                            break;
                        case"#":
                            i.id = t.replace("#", "");
                            break;
                        case"[":
                            i[o] = l
                    }
                }), re(n, i)
            }

            function pe(e, t) {
                if (G(e)) {
                    var i = t;
                    W(i) || (i = !e.hidden), e.hidden = i
                }
            }

            function fe(e, t, i) {
                if (U(e)) return Array.from(e).map(function (e) {
                    return fe(e, t, i)
                });
                if (G(e)) {
                    var n = "toggle";
                    return void 0 !== i && (n = i ? "add" : "remove"), e.classList[n](t), e.classList.contains(t)
                }
                return !1
            }

            function me(e, t) {
                return G(e) && e.classList.contains(t)
            }

            function ge(e, t) {
                var i = Element.prototype;
                return (i.matches || i.webkitMatchesSelector || i.mozMatchesSelector || i.msMatchesSelector || function () {
                    return Array.from(document.querySelectorAll(t)).includes(this)
                }).call(e, t)
            }

            function ve(e) {
                return this.elements.container.querySelectorAll(e)
            }

            function ye(e) {
                return this.elements.container.querySelector(e)
            }

            function be() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                    t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                G(e) && (e.focus({preventScroll: !0}), t && fe(e, this.config.classNames.tabFocus))
            }

            var we, De = {
                "audio/ogg": "vorbis",
                "audio/wav": "1",
                "video/webm": "vp8, vorbis",
                "video/mp4": "avc1.42E01E, mp4a.40.2",
                "video/ogg": "theora"
            }, Te = {
                audio: "canPlayType" in document.createElement("audio"),
                video: "canPlayType" in document.createElement("video"),
                check: function (e, t, i) {
                    var n = ie.isIPhone && i && Te.playsinline, r = Te[e] || "html5" !== t;
                    return {api: r, ui: r && Te.rangeInput && ("video" !== e || !ie.isIPhone || n)}
                },
                pip: !(ie.isIPhone || !X(ae("video").webkitSetPresentationMode) && (!document.pictureInPictureEnabled || ae("video").disablePictureInPicture)),
                airplay: X(window.WebKitPlaybackTargetAvailabilityEvent),
                playsinline: "playsInline" in document.createElement("video"),
                mime: function (e) {
                    if (Z(e)) return !1;
                    var t = a(e.split("/"), 1)[0], i = e;
                    if (!this.isHTML5 || t !== this.type) return !1;
                    Object.keys(De).includes(i) && (i += '; codecs="'.concat(De[e], '"'));
                    try {
                        return Boolean(i && this.media.canPlayType(i).replace(/no/, ""))
                    } catch (e) {
                        return !1
                    }
                },
                textTracks: "textTracks" in document.createElement("video"),
                rangeInput: (we = document.createElement("input"), we.type = "range", "range" === we.type),
                touch: "ontouchstart" in document.documentElement,
                transitions: !1 !== ee,
                reducedMotion: "matchMedia" in window && window.matchMedia("(prefers-reduced-motion)").matches
            }, Ce = function () {
                var e = !1;
                try {
                    var t = Object.defineProperty({}, "passive", {
                        get: function () {
                            return e = !0, null
                        }
                    });
                    window.addEventListener("test", null, t), window.removeEventListener("test", null, t)
                } catch (e) {
                }
                return e
            }();

            function ke(e, t, i) {
                var n = this, r = arguments.length > 3 && void 0 !== arguments[3] && arguments[3],
                    s = !(arguments.length > 4 && void 0 !== arguments[4]) || arguments[4],
                    o = arguments.length > 5 && void 0 !== arguments[5] && arguments[5];
                if (e && "addEventListener" in e && !Z(t) && X(i)) {
                    var a = t.split(" "), l = o;
                    Ce && (l = {passive: s, capture: o}), a.forEach(function (t) {
                        n && n.eventListeners && r && n.eventListeners.push({
                            element: e,
                            type: t,
                            callback: i,
                            options: l
                        }), e[r ? "addEventListener" : "removeEventListener"](t, i, l)
                    })
                }
            }

            function Ee(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                    i = arguments.length > 2 ? arguments[2] : void 0,
                    n = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
                    r = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];
                ke.call(this, e, t, i, !0, n, r)
            }

            function _e(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                    i = arguments.length > 2 ? arguments[2] : void 0,
                    n = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
                    r = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];
                ke.call(this, e, t, i, !1, n, r)
            }

            function xe(e) {
                var t = this, i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                    n = arguments.length > 2 ? arguments[2] : void 0,
                    r = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
                    s = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];
                ke.call(this, e, i, function o() {
                    _e(e, i, o, r, s);
                    for (var a = arguments.length, l = new Array(a), u = 0; u < a; u++) l[u] = arguments[u];
                    n.apply(t, l)
                }, !0, r, s)
            }

            function Se(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                    i = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
                    n = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {};
                if (G(e) && !Z(t)) {
                    var r = new CustomEvent(t, {bubbles: i, detail: o(o({}, n), {}, {plyr: this})});
                    e.dispatchEvent(r)
                }
            }

            function Pe(e) {
                (function (e) {
                    return M(e, Promise) && B(e.then)
                })(e) && e.then(null, function () {
                })
            }

            function Oe(e) {
                return !!(Y(e) || q(e) && e.includes(":")) && (Y(e) ? e : e.split(":")).map(Number).every(V)
            }

            function Ae(e) {
                if (!Y(e) || !e.every(V)) return null;
                var t = a(e, 2), i = t[0], n = t[1], r = function e(t, i) {
                    return 0 === i ? t : e(i, t % i)
                }(i, n);
                return [i / r, n / r]
            }

            function Me(e) {
                var t = function (e) {
                    return Oe(e) ? e.split(":").map(Number) : null
                }, i = t(e);
                if (null === i && (i = t(this.config.ratio)), null === i && !Z(this.embed) && Y(this.embed.ratio) && (i = this.embed.ratio), null === i && this.isHTML5) {
                    var n = this.media;
                    i = Ae([n.videoWidth, n.videoHeight])
                }
                return i
            }

            function Le(e) {
                if (!this.isVideo) return {};
                var t = this.elements.wrapper, i = Me.call(this, e), n = a(Y(i) ? i : [0, 0], 2), r = 100 / n[0] * n[1];
                if (t.style.paddingBottom = "".concat(r, "%"), this.isVimeo && !this.config.vimeo.premium && this.supported.ui) {
                    var s = 100 / this.media.offsetWidth * parseInt(window.getComputedStyle(this.media).paddingBottom, 10),
                        o = (s - r) / (s / 50);
                    this.media.style.transform = "translateY(-".concat(o, "%)")
                } else this.isHTML5 && t.classList.toggle(this.config.classNames.videoFixedRatio, null !== i);
                return {padding: r, ratio: i}
            }

            var Fe = {
                getSources: function () {
                    var e = this;
                    return this.isHTML5 ? Array.from(this.media.querySelectorAll("source")).filter(function (t) {
                        var i = t.getAttribute("type");
                        return !!Z(i) || Te.mime.call(e, i)
                    }) : []
                }, getQualityOptions: function () {
                    return this.config.quality.forced ? this.config.quality.options : Fe.getSources.call(this).map(function (e) {
                        return Number(e.getAttribute("size"))
                    }).filter(Boolean)
                }, setup: function () {
                    if (this.isHTML5) {
                        var e = this;
                        e.options.speed = e.config.speed.options, Z(this.config.ratio) || Le.call(e), Object.defineProperty(e.media, "quality", {
                            get: function () {
                                var t = Fe.getSources.call(e).find(function (t) {
                                    return t.getAttribute("src") === e.source
                                });
                                return t && Number(t.getAttribute("size"))
                            }, set: function (t) {
                                if (e.quality !== t) {
                                    if (e.config.quality.forced && X(e.config.quality.onChange)) e.config.quality.onChange(t); else {
                                        var i = Fe.getSources.call(e).find(function (e) {
                                            return Number(e.getAttribute("size")) === t
                                        });
                                        if (!i) return;
                                        var n = e.media, r = n.currentTime, s = n.paused, o = n.preload,
                                            a = n.readyState, l = n.playbackRate;
                                        e.media.src = i.getAttribute("src"), ("none" !== o || a) && (e.once("loadedmetadata", function () {
                                            e.speed = l, e.currentTime = r, s || Pe(e.play())
                                        }), e.media.load())
                                    }
                                    Se.call(e, e.media, "qualitychange", !1, {quality: t})
                                }
                            }
                        })
                    }
                }, cancelRequests: function () {
                    this.isHTML5 && (ue(Fe.getSources.call(this)), this.media.setAttribute("src", this.config.blankVideo), this.media.load(), this.debug.log("Cancelled network requests"))
                }
            };

            function je(e) {
                return Y(e) ? e.filter(function (t, i) {
                    return e.indexOf(t) === i
                }) : e
            }

            function Be(e) {
                for (var t = arguments.length, i = new Array(t > 1 ? t - 1 : 0), n = 1; n < t; n++) i[n - 1] = arguments[n];
                return Z(e) ? e : e.toString().replace(/{(\d+)}/g, function (e, t) {
                    return i[t].toString()
                })
            }

            var Ie = function () {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                    t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                    i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "";
                return e.replace(new RegExp(t.toString().replace(/([.*+?^=!:${}()|[\]\/\\])/g, "\\$1"), "g"), i.toString())
            }, Re = function () {
                return (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "").toString().replace(/\w\S*/g, function (e) {
                    return e.charAt(0).toUpperCase() + e.substr(1).toLowerCase()
                })
            };

            function Ne(e) {
                var t = document.createElement("div");
                return t.appendChild(e), t.innerHTML
            }

            var ze = {pip: "PIP", airplay: "AirPlay", html5: "HTML5", vimeo: "Vimeo", youtube: "YouTube"},
                He = function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    if (Z(e) || Z(t)) return "";
                    var i = ne(t.i18n, e);
                    if (Z(i)) return Object.keys(ze).includes(e) ? ze[e] : "";
                    var n = {"{seektime}": t.seekTime, "{title}": t.title};
                    return Object.entries(n).forEach(function (e) {
                        var t = a(e, 2), n = t[0], r = t[1];
                        i = Ie(i, n, r)
                    }), i
                }, Ve = function () {
                    function t(i) {
                        e(this, t), this.enabled = i.config.storage.enabled, this.key = i.config.storage.key
                    }

                    return n(t, [{
                        key: "get", value: function (e) {
                            if (!t.supported || !this.enabled) return null;
                            var i = window.localStorage.getItem(this.key);
                            if (Z(i)) return null;
                            var n = JSON.parse(i);
                            return q(e) && e.length ? n[e] : n
                        }
                    }, {
                        key: "set", value: function (e) {
                            if (t.supported && this.enabled && H(e)) {
                                var i = this.get();
                                Z(i) && (i = {}), re(i, e), window.localStorage.setItem(this.key, JSON.stringify(i))
                            }
                        }
                    }], [{
                        key: "supported", get: function () {
                            try {
                                return "localStorage" in window && (window.localStorage.setItem("___test", "___test"), window.localStorage.removeItem("___test"), !0)
                            } catch (e) {
                                return !1
                            }
                        }
                    }]), t
                }();

            function qe(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "text";
                return new Promise(function (i, n) {
                    try {
                        var r = new XMLHttpRequest;
                        if (!("withCredentials" in r)) return;
                        r.addEventListener("load", function () {
                            if ("text" === t) try {
                                i(JSON.parse(r.responseText))
                            } catch (e) {
                                i(r.responseText)
                            } else i(r.response)
                        }), r.addEventListener("error", function () {
                            throw new Error(r.status)
                        }), r.open("GET", e, !0), r.responseType = t, r.send()
                    } catch (e) {
                        n(e)
                    }
                })
            }

            function We(e, t) {
                if (q(e)) {
                    var i = q(t), n = function () {
                        return null !== document.getElementById(t)
                    }, r = function (e, t) {
                        e.innerHTML = t, i && n() || document.body.insertAdjacentElement("afterbegin", e)
                    };
                    if (!i || !n()) {
                        var s = Ve.supported, o = document.createElement("div");
                        if (o.setAttribute("hidden", ""), i && o.setAttribute("id", t), s) {
                            var a = window.localStorage.getItem("".concat("cache", "-").concat(t));
                            if (null !== a) {
                                var l = JSON.parse(a);
                                r(o, l.content)
                            }
                        }
                        qe(e).then(function (e) {
                            Z(e) || (s && window.localStorage.setItem("".concat("cache", "-").concat(t), JSON.stringify({content: e})), r(o, e))
                        }).catch(function () {
                        })
                    }
                }
            }

            var Xe = function (e) {
                return Math.trunc(e / 60 / 60 % 60, 10)
            }, Ye = function (e) {
                return Math.trunc(e / 60 % 60, 10)
            }, Ue = function (e) {
                return Math.trunc(e % 60, 10)
            };

            function Ge() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                    t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                    i = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
                if (!V(e)) return Ge(void 0, t, i);
                var n = function (e) {
                    return "0".concat(e).slice(-2)
                }, r = Xe(e), s = Ye(e), o = Ue(e);
                return r = t || r > 0 ? "".concat(r, ":") : "", "".concat(i && e > 0 ? "-" : "").concat(r).concat(n(s), ":").concat(n(o))
            }

            var $e = {
                getIconUrl: function () {
                    var e = new URL(this.config.iconUrl, window.location).host !== window.location.host || ie.isIE && !window.svg4everybody;
                    return {url: this.config.iconUrl, cors: e}
                }, findElements: function () {
                    try {
                        return this.elements.controls = ye.call(this, this.config.selectors.controls.wrapper), this.elements.buttons = {
                            play: ve.call(this, this.config.selectors.buttons.play),
                            pause: ye.call(this, this.config.selectors.buttons.pause),
                            restart: ye.call(this, this.config.selectors.buttons.restart),
                            rewind: ye.call(this, this.config.selectors.buttons.rewind),
                            fastForward: ye.call(this, this.config.selectors.buttons.fastForward),
                            mute: ye.call(this, this.config.selectors.buttons.mute),
                            pip: ye.call(this, this.config.selectors.buttons.pip),
                            airplay: ye.call(this, this.config.selectors.buttons.airplay),
                            settings: ye.call(this, this.config.selectors.buttons.settings),
                            captions: ye.call(this, this.config.selectors.buttons.captions),
                            fullscreen: ye.call(this, this.config.selectors.buttons.fullscreen)
                        }, this.elements.progress = ye.call(this, this.config.selectors.progress), this.elements.inputs = {
                            seek: ye.call(this, this.config.selectors.inputs.seek),
                            volume: ye.call(this, this.config.selectors.inputs.volume)
                        }, this.elements.display = {
                            buffer: ye.call(this, this.config.selectors.display.buffer),
                            currentTime: ye.call(this, this.config.selectors.display.currentTime),
                            duration: ye.call(this, this.config.selectors.display.duration)
                        }, G(this.elements.progress) && (this.elements.display.seekTooltip = this.elements.progress.querySelector(".".concat(this.config.classNames.tooltip))), !0
                    } catch (e) {
                        return this.debug.warn("It looks like there is a problem with your custom controls HTML", e), this.toggleNativeControls(!0), !1
                    }
                }, createIcon: function (e, t) {
                    var i = $e.getIconUrl.call(this),
                        n = "".concat(i.cors ? "" : i.url, "#").concat(this.config.iconPrefix),
                        r = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    oe(r, re(t, {"aria-hidden": "true", focusable: "false"}));
                    var s = document.createElementNS("http://www.w3.org/2000/svg", "use"),
                        o = "".concat(n, "-").concat(e);
                    return "href" in s && s.setAttributeNS("http://www.w3.org/1999/xlink", "href", o), s.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", o), r.appendChild(s), r
                }, createLabel: function (e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, i = He(e, this.config);
                    return ae("span", o(o({}, t), {}, {class: [t.class, this.config.classNames.hidden].filter(Boolean).join(" ")}), i)
                }, createBadge: function (e) {
                    if (Z(e)) return null;
                    var t = ae("span", {class: this.config.classNames.menu.value});
                    return t.appendChild(ae("span", {class: this.config.classNames.menu.badge}, e)), t
                }, createButton: function (e, t) {
                    var i = this, n = re({}, t), r = function () {
                        var e = (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "").toString();
                        return (e = function () {
                            var e = (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "").toString();
                            return e = Ie(e, "-", " "), e = Ie(e, "_", " "), e = Re(e), Ie(e, " ", "")
                        }(e)).charAt(0).toLowerCase() + e.slice(1)
                    }(e), s = {
                        element: "button",
                        toggle: !1,
                        label: null,
                        icon: null,
                        labelPressed: null,
                        iconPressed: null
                    };
                    switch (["element", "icon", "label"].forEach(function (e) {
                        Object.keys(n).includes(e) && (s[e] = n[e], delete n[e])
                    }), "button" !== s.element || Object.keys(n).includes("type") || (n.type = "button"), Object.keys(n).includes("class") ? n.class.split(" ").some(function (e) {
                        return e === i.config.classNames.control
                    }) || re(n, {class: "".concat(n.class, " ").concat(this.config.classNames.control)}) : n.class = this.config.classNames.control, e) {
                        case"play":
                            s.toggle = !0, s.label = "play", s.labelPressed = "pause", s.icon = "play", s.iconPressed = "pause";
                            break;
                        case"mute":
                            s.toggle = !0, s.label = "mute", s.labelPressed = "unmute", s.icon = "volume", s.iconPressed = "muted";
                            break;
                        case"captions":
                            s.toggle = !0, s.label = "enableCaptions", s.labelPressed = "disableCaptions", s.icon = "captions-off", s.iconPressed = "captions-on";
                            break;
                        case"fullscreen":
                            s.toggle = !0, s.label = "enterFullscreen", s.labelPressed = "exitFullscreen", s.icon = "enter-fullscreen", s.iconPressed = "exit-fullscreen";
                            break;
                        case"play-large":
                            n.class += " ".concat(this.config.classNames.control, "--overlaid"), r = "play", s.label = "play", s.icon = "play";
                            break;
                        default:
                            Z(s.label) && (s.label = r), Z(s.icon) && (s.icon = e)
                    }
                    var o = ae(s.element);
                    return s.toggle ? (o.appendChild($e.createIcon.call(this, s.iconPressed, {class: "icon--pressed"})), o.appendChild($e.createIcon.call(this, s.icon, {class: "icon--not-pressed"})), o.appendChild($e.createLabel.call(this, s.labelPressed, {class: "label--pressed"})), o.appendChild($e.createLabel.call(this, s.label, {class: "label--not-pressed"}))) : (o.appendChild($e.createIcon.call(this, s.icon)), o.appendChild($e.createLabel.call(this, s.label))), re(n, de(this.config.selectors.buttons[r], n)), oe(o, n), "play" === r ? (Y(this.elements.buttons[r]) || (this.elements.buttons[r] = []), this.elements.buttons[r].push(o)) : this.elements.buttons[r] = o, o
                }, createRange: function (e, t) {
                    var i = ae("input", re(de(this.config.selectors.inputs[e]), {
                        type: "range",
                        min: 0,
                        max: 100,
                        step: .01,
                        value: 0,
                        autocomplete: "off",
                        role: "slider",
                        "aria-label": He(e, this.config),
                        "aria-valuemin": 0,
                        "aria-valuemax": 100,
                        "aria-valuenow": 0
                    }, t));
                    return this.elements.inputs[e] = i, $e.updateRangeFill.call(this, i), O.setup(i), i
                }, createProgress: function (e, t) {
                    var i = ae("progress", re(de(this.config.selectors.display[e]), {
                        min: 0,
                        max: 100,
                        value: 0,
                        role: "progressbar",
                        "aria-hidden": !0
                    }, t));
                    if ("volume" !== e) {
                        i.appendChild(ae("span", null, "0"));
                        var n = {played: "played", buffer: "buffered"}[e], r = n ? He(n, this.config) : "";
                        i.innerText = "% ".concat(r.toLowerCase())
                    }
                    return this.elements.display[e] = i, i
                }, createTime: function (e, t) {
                    var i = de(this.config.selectors.display[e], t), n = ae("div", re(i, {
                        class: "".concat(i.class ? i.class : "", " ").concat(this.config.classNames.display.time, " ").trim(),
                        "aria-label": He(e, this.config)
                    }), "00:00");
                    return this.elements.display[e] = n, n
                }, bindMenuItemShortcuts: function (e, t) {
                    var i = this;
                    Ee.call(this, e, "keydown keyup", function (n) {
                        if ([32, 38, 39, 40].includes(n.which) && (n.preventDefault(), n.stopPropagation(), "keydown" !== n.type)) {
                            var r, s = ge(e, '[role="menuitemradio"]');
                            !s && [32, 39].includes(n.which) ? $e.showMenuPanel.call(i, t, !0) : 32 !== n.which && (40 === n.which || s && 39 === n.which ? (r = e.nextElementSibling, G(r) || (r = e.parentNode.firstElementChild)) : (r = e.previousElementSibling, G(r) || (r = e.parentNode.lastElementChild)), be.call(i, r, !0))
                        }
                    }, !1), Ee.call(this, e, "keyup", function (e) {
                        13 === e.which && $e.focusFirstMenuItem.call(i, null, !0)
                    })
                }, createMenuItem: function (e) {
                    var t = this, i = e.value, n = e.list, r = e.type, s = e.title, o = e.badge,
                        a = void 0 === o ? null : o, l = e.checked, u = void 0 !== l && l,
                        c = de(this.config.selectors.inputs[r]), h = ae("button", re(c, {
                            type: "button",
                            role: "menuitemradio",
                            class: "".concat(this.config.classNames.control, " ").concat(c.class ? c.class : "").trim(),
                            "aria-checked": u,
                            value: i
                        })), d = ae("span");
                    d.innerHTML = s, G(a) && d.appendChild(a), h.appendChild(d), Object.defineProperty(h, "checked", {
                        enumerable: !0,
                        get: function () {
                            return "true" === h.getAttribute("aria-checked")
                        },
                        set: function (e) {
                            e && Array.from(h.parentNode.children).filter(function (e) {
                                return ge(e, '[role="menuitemradio"]')
                            }).forEach(function (e) {
                                return e.setAttribute("aria-checked", "false")
                            }), h.setAttribute("aria-checked", e ? "true" : "false")
                        }
                    }), this.listeners.bind(h, "click keyup", function (e) {
                        if (!K(e) || 32 === e.which) {
                            switch (e.preventDefault(), e.stopPropagation(), h.checked = !0, r) {
                                case"language":
                                    t.currentTrack = Number(i);
                                    break;
                                case"quality":
                                    t.quality = i;
                                    break;
                                case"speed":
                                    t.speed = parseFloat(i)
                            }
                            $e.showMenuPanel.call(t, "home", K(e))
                        }
                    }, r, !1), $e.bindMenuItemShortcuts.call(this, h, r), n.appendChild(h)
                }, formatTime: function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                        t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    return V(e) ? Ge(e, Xe(this.duration) > 0, t) : e
                }, updateTimeDisplay: function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
                        i = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
                    G(e) && V(t) && (e.innerText = $e.formatTime(t, i))
                }, updateVolume: function () {
                    this.supported.ui && (G(this.elements.inputs.volume) && $e.setRange.call(this, this.elements.inputs.volume, this.muted ? 0 : this.volume), G(this.elements.buttons.mute) && (this.elements.buttons.mute.pressed = this.muted || 0 === this.volume))
                }, setRange: function (e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
                    G(e) && (e.value = t, $e.updateRangeFill.call(this, e))
                }, updateProgress: function (e) {
                    var t = this;
                    if (this.supported.ui && $(e)) {
                        var i, n, r = 0;
                        if (e) switch (e.type) {
                            case"timeupdate":
                            case"seeking":
                            case"seeked":
                                i = this.currentTime, n = this.duration, r = 0 === i || 0 === n || Number.isNaN(i) || Number.isNaN(n) ? 0 : (i / n * 100).toFixed(2), "timeupdate" === e.type && $e.setRange.call(this, this.elements.inputs.seek, r);
                                break;
                            case"playing":
                            case"progress":
                                !function (e, i) {
                                    var n = V(i) ? i : 0, r = G(e) ? e : t.elements.display.buffer;
                                    if (G(r)) {
                                        r.value = n;
                                        var s = r.getElementsByTagName("span")[0];
                                        G(s) && (s.childNodes[0].nodeValue = n)
                                    }
                                }(this.elements.display.buffer, 100 * this.buffered)
                        }
                    }
                }, updateRangeFill: function (e) {
                    var t = $(e) ? e.target : e;
                    if (G(t) && "range" === t.getAttribute("type")) {
                        if (ge(t, this.config.selectors.inputs.seek)) {
                            t.setAttribute("aria-valuenow", this.currentTime);
                            var i = $e.formatTime(this.currentTime), n = $e.formatTime(this.duration),
                                r = He("seekLabel", this.config);
                            t.setAttribute("aria-valuetext", r.replace("{currentTime}", i).replace("{duration}", n))
                        } else if (ge(t, this.config.selectors.inputs.volume)) {
                            var s = 100 * t.value;
                            t.setAttribute("aria-valuenow", s), t.setAttribute("aria-valuetext", "".concat(s.toFixed(1), "%"))
                        } else t.setAttribute("aria-valuenow", t.value);
                        ie.isWebkit && t.style.setProperty("--value", "".concat(t.value / t.max * 100, "%"))
                    }
                }, updateSeekTooltip: function (e) {
                    var t = this;
                    if (this.config.tooltips.seek && G(this.elements.inputs.seek) && G(this.elements.display.seekTooltip) && 0 !== this.duration) {
                        var i = "".concat(this.config.classNames.tooltip, "--visible"), n = function (e) {
                            return fe(t.elements.display.seekTooltip, i, e)
                        };
                        if (this.touch) n(!1); else {
                            var r = 0, s = this.elements.progress.getBoundingClientRect();
                            if ($(e)) r = 100 / s.width * (e.pageX - s.left); else {
                                if (!me(this.elements.display.seekTooltip, i)) return;
                                r = parseFloat(this.elements.display.seekTooltip.style.left, 10)
                            }
                            r < 0 ? r = 0 : r > 100 && (r = 100), $e.updateTimeDisplay.call(this, this.elements.display.seekTooltip, this.duration / 100 * r), this.elements.display.seekTooltip.style.left = "".concat(r, "%"), $(e) && ["mouseenter", "mouseleave"].includes(e.type) && n("mouseenter" === e.type)
                        }
                    }
                }, timeUpdate: function (e) {
                    var t = !G(this.elements.display.duration) && this.config.invertTime;
                    $e.updateTimeDisplay.call(this, this.elements.display.currentTime, t ? this.duration - this.currentTime : this.currentTime, t), e && "timeupdate" === e.type && this.media.seeking || $e.updateProgress.call(this, e)
                }, durationUpdate: function () {
                    if (this.supported.ui && (this.config.invertTime || !this.currentTime)) {
                        if (this.duration >= Math.pow(2, 32)) return pe(this.elements.display.currentTime, !0), void pe(this.elements.progress, !0);
                        G(this.elements.inputs.seek) && this.elements.inputs.seek.setAttribute("aria-valuemax", this.duration);
                        var e = G(this.elements.display.duration);
                        !e && this.config.displayDuration && this.paused && $e.updateTimeDisplay.call(this, this.elements.display.currentTime, this.duration), e && $e.updateTimeDisplay.call(this, this.elements.display.duration, this.duration), $e.updateSeekTooltip.call(this)
                    }
                }, toggleMenuButton: function (e, t) {
                    pe(this.elements.settings.buttons[e], !t)
                }, updateSetting: function (e, t, i) {
                    var n = this.elements.settings.panels[e], r = null, s = t;
                    if ("captions" === e) r = this.currentTrack; else {
                        if (r = Z(i) ? this[e] : i, Z(r) && (r = this.config[e].default), !Z(this.options[e]) && !this.options[e].includes(r)) return void this.debug.warn("Unsupported value of '".concat(r, "' for ").concat(e));
                        if (!this.config[e].options.includes(r)) return void this.debug.warn("Disabled value of '".concat(r, "' for ").concat(e))
                    }
                    if (G(s) || (s = n && n.querySelector('[role="menu"]')), G(s)) {
                        this.elements.settings.buttons[e].querySelector(".".concat(this.config.classNames.menu.value)).innerHTML = $e.getLabel.call(this, e, r);
                        var o = s && s.querySelector('[value="'.concat(r, '"]'));
                        G(o) && (o.checked = !0)
                    }
                }, getLabel: function (e, t) {
                    switch (e) {
                        case"speed":
                            return 1 === t ? He("normal", this.config) : "".concat(t, "&times;");
                        case"quality":
                            if (V(t)) {
                                var i = He("qualityLabel.".concat(t), this.config);
                                return i.length ? i : "".concat(t, "p")
                            }
                            return Re(t);
                        case"captions":
                            return Qe.getLabel.call(this);
                        default:
                            return null
                    }
                }, setQualityMenu: function (e) {
                    var t = this;
                    if (G(this.elements.settings.panels.quality)) {
                        var i = this.elements.settings.panels.quality.querySelector('[role="menu"]');
                        Y(e) && (this.options.quality = je(e).filter(function (e) {
                            return t.config.quality.options.includes(e)
                        }));
                        var n = !Z(this.options.quality) && this.options.quality.length > 1;
                        if ($e.toggleMenuButton.call(this, "quality", n), ce(i), $e.checkMenu.call(this), n) {
                            var r = function (e) {
                                var i = He("qualityBadge.".concat(e), t.config);
                                return i.length ? $e.createBadge.call(t, i) : null
                            };
                            this.options.quality.sort(function (e, i) {
                                var n = t.config.quality.options;
                                return n.indexOf(e) > n.indexOf(i) ? 1 : -1
                            }).forEach(function (e) {
                                $e.createMenuItem.call(t, {
                                    value: e,
                                    list: i,
                                    type: "quality",
                                    title: $e.getLabel.call(t, "quality", e),
                                    badge: r(e)
                                })
                            }), $e.updateSetting.call(this, "quality", i)
                        }
                    }
                }, setCaptionsMenu: function () {
                    var e = this;
                    if (G(this.elements.settings.panels.captions)) {
                        var t = this.elements.settings.panels.captions.querySelector('[role="menu"]'),
                            i = Qe.getTracks.call(this), n = Boolean(i.length);
                        if ($e.toggleMenuButton.call(this, "captions", n), ce(t), $e.checkMenu.call(this), n) {
                            var r = i.map(function (i, n) {
                                return {
                                    value: n,
                                    checked: e.captions.toggled && e.currentTrack === n,
                                    title: Qe.getLabel.call(e, i),
                                    badge: i.language && $e.createBadge.call(e, i.language.toUpperCase()),
                                    list: t,
                                    type: "language"
                                }
                            });
                            r.unshift({
                                value: -1,
                                checked: !this.captions.toggled,
                                title: He("disabled", this.config),
                                list: t,
                                type: "language"
                            }), r.forEach($e.createMenuItem.bind(this)), $e.updateSetting.call(this, "captions", t)
                        }
                    }
                }, setSpeedMenu: function () {
                    var e = this;
                    if (G(this.elements.settings.panels.speed)) {
                        var t = this.elements.settings.panels.speed.querySelector('[role="menu"]');
                        this.options.speed = this.options.speed.filter(function (t) {
                            return t >= e.minimumSpeed && t <= e.maximumSpeed
                        });
                        var i = !Z(this.options.speed) && this.options.speed.length > 1;
                        $e.toggleMenuButton.call(this, "speed", i), ce(t), $e.checkMenu.call(this), i && (this.options.speed.forEach(function (i) {
                            $e.createMenuItem.call(e, {
                                value: i,
                                list: t,
                                type: "speed",
                                title: $e.getLabel.call(e, "speed", i)
                            })
                        }), $e.updateSetting.call(this, "speed", t))
                    }
                }, checkMenu: function () {
                    var e = this.elements.settings.buttons, t = !Z(e) && Object.values(e).some(function (e) {
                        return !e.hidden
                    });
                    pe(this.elements.settings.menu, !t)
                }, focusFirstMenuItem: function (e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    if (!this.elements.settings.popup.hidden) {
                        var i = e;
                        G(i) || (i = Object.values(this.elements.settings.panels).find(function (e) {
                            return !e.hidden
                        }));
                        var n = i.querySelector('[role^="menuitem"]');
                        be.call(this, n, t)
                    }
                }, toggleMenu: function (e) {
                    var t = this.elements.settings.popup, i = this.elements.buttons.settings;
                    if (G(t) && G(i)) {
                        var n = t.hidden, r = n;
                        if (W(e)) r = e; else if (K(e) && 27 === e.which) r = !1; else if ($(e)) {
                            var s = X(e.composedPath) ? e.composedPath()[0] : e.target, o = t.contains(s);
                            if (o || !o && e.target !== i && r) return
                        }
                        i.setAttribute("aria-expanded", r), pe(t, !r), fe(this.elements.container, this.config.classNames.menu.open, r), r && K(e) ? $e.focusFirstMenuItem.call(this, null, !0) : r || n || be.call(this, i, K(e))
                    }
                }, getMenuSize: function (e) {
                    var t = e.cloneNode(!0);
                    t.style.position = "absolute", t.style.opacity = 0, t.removeAttribute("hidden"), e.parentNode.appendChild(t);
                    var i = t.scrollWidth, n = t.scrollHeight;
                    return ue(t), {width: i, height: n}
                }, showMenuPanel: function () {
                    var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                        i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                        n = this.elements.container.querySelector("#plyr-settings-".concat(this.id, "-").concat(t));
                    if (G(n)) {
                        var r = n.parentNode, s = Array.from(r.children).find(function (e) {
                            return !e.hidden
                        });
                        if (Te.transitions && !Te.reducedMotion) {
                            r.style.width = "".concat(s.scrollWidth, "px"), r.style.height = "".concat(s.scrollHeight, "px");
                            var o = $e.getMenuSize.call(this, n);
                            Ee.call(this, r, ee, function t(i) {
                                i.target === r && ["width", "height"].includes(i.propertyName) && (r.style.width = "", r.style.height = "", _e.call(e, r, ee, t))
                            }), r.style.width = "".concat(o.width, "px"), r.style.height = "".concat(o.height, "px")
                        }
                        pe(s, !0), pe(n, !1), $e.focusFirstMenuItem.call(this, n, i)
                    }
                }, setDownloadUrl: function () {
                    var e = this.elements.buttons.download;
                    G(e) && e.setAttribute("href", this.download)
                }, create: function (e) {
                    var t = this, i = $e.bindMenuItemShortcuts, n = $e.createButton, r = $e.createProgress,
                        s = $e.createRange, o = $e.createTime, a = $e.setQualityMenu, l = $e.setSpeedMenu,
                        u = $e.showMenuPanel;
                    this.elements.controls = null, Y(this.config.controls) && this.config.controls.includes("play-large") && this.elements.container.appendChild(n.call(this, "play-large"));
                    var c = ae("div", de(this.config.selectors.controls.wrapper));
                    this.elements.controls = c;
                    var h = {class: "plyr__controls__item"};
                    return je(Y(this.config.controls) ? this.config.controls : []).forEach(function (a) {
                        if ("restart" === a && c.appendChild(n.call(t, "restart", h)), "rewind" === a && c.appendChild(n.call(t, "rewind", h)), "play" === a && c.appendChild(n.call(t, "play", h)), "fast-forward" === a && c.appendChild(n.call(t, "fast-forward", h)), "progress" === a) {
                            var l = ae("div", {class: "".concat(h.class, " plyr__progress__container")}),
                                d = ae("div", de(t.config.selectors.progress));
                            if (d.appendChild(s.call(t, "seek", {id: "plyr-seek-".concat(e.id)})), d.appendChild(r.call(t, "buffer")), t.config.tooltips.seek) {
                                var p = ae("span", {class: t.config.classNames.tooltip}, "00:00");
                                d.appendChild(p), t.elements.display.seekTooltip = p
                            }
                            t.elements.progress = d, l.appendChild(t.elements.progress), c.appendChild(l)
                        }
                        if ("current-time" === a && c.appendChild(o.call(t, "currentTime", h)), "duration" === a && c.appendChild(o.call(t, "duration", h)), "mute" === a || "volume" === a) {
                            var f = t.elements.volume;
                            if (G(f) && c.contains(f) || (f = ae("div", re({}, h, {class: "".concat(h.class, " plyr__volume").trim()})), t.elements.volume = f, c.appendChild(f)), "mute" === a && f.appendChild(n.call(t, "mute")), "volume" === a && !ie.isIos) {
                                var m = {max: 1, step: .05, value: t.config.volume};
                                f.appendChild(s.call(t, "volume", re(m, {id: "plyr-volume-".concat(e.id)})))
                            }
                        }
                        if ("captions" === a && c.appendChild(n.call(t, "captions", h)), "settings" === a && !Z(t.config.settings)) {
                            var g = ae("div", re({}, h, {class: "".concat(h.class, " plyr__menu").trim(), hidden: ""}));
                            g.appendChild(n.call(t, "settings", {
                                "aria-haspopup": !0,
                                "aria-controls": "plyr-settings-".concat(e.id),
                                "aria-expanded": !1
                            }));
                            var v = ae("div", {
                                    class: "plyr__menu__container",
                                    id: "plyr-settings-".concat(e.id),
                                    hidden: ""
                                }), y = ae("div"), b = ae("div", {id: "plyr-settings-".concat(e.id, "-home")}),
                                w = ae("div", {role: "menu"});
                            b.appendChild(w), y.appendChild(b), t.elements.settings.panels.home = b, t.config.settings.forEach(function (n) {
                                var r = ae("button", re(de(t.config.selectors.buttons.settings), {
                                    type: "button",
                                    class: "".concat(t.config.classNames.control, " ").concat(t.config.classNames.control, "--forward"),
                                    role: "menuitem",
                                    "aria-haspopup": !0,
                                    hidden: ""
                                }));
                                i.call(t, r, n), Ee.call(t, r, "click", function () {
                                    u.call(t, n, !1)
                                });
                                var s = ae("span", null, He(n, t.config)),
                                    o = ae("span", {class: t.config.classNames.menu.value});
                                o.innerHTML = e[n], s.appendChild(o), r.appendChild(s), w.appendChild(r);
                                var a = ae("div", {id: "plyr-settings-".concat(e.id, "-").concat(n), hidden: ""}),
                                    l = ae("button", {
                                        type: "button",
                                        class: "".concat(t.config.classNames.control, " ").concat(t.config.classNames.control, "--back")
                                    });
                                l.appendChild(ae("span", {"aria-hidden": !0}, He(n, t.config))), l.appendChild(ae("span", {class: t.config.classNames.hidden}, He("menuBack", t.config))), Ee.call(t, a, "keydown", function (e) {
                                    37 === e.which && (e.preventDefault(), e.stopPropagation(), u.call(t, "home", !0))
                                }, !1), Ee.call(t, l, "click", function () {
                                    u.call(t, "home", !1)
                                }), a.appendChild(l), a.appendChild(ae("div", {role: "menu"})), y.appendChild(a), t.elements.settings.buttons[n] = r, t.elements.settings.panels[n] = a
                            }), v.appendChild(y), g.appendChild(v), c.appendChild(g), t.elements.settings.popup = v, t.elements.settings.menu = g
                        }
                        if ("pip" === a && Te.pip && c.appendChild(n.call(t, "pip", h)), "airplay" === a && Te.airplay && c.appendChild(n.call(t, "airplay", h)), "download" === a) {
                            var D = re({}, h, {element: "a", href: t.download, target: "_blank"});
                            t.isHTML5 && (D.download = "");
                            var T = t.config.urls.download;
                            !Q(T) && t.isEmbed && re(D, {
                                icon: "logo-".concat(t.provider),
                                label: t.provider
                            }), c.appendChild(n.call(t, "download", D))
                        }
                        "fullscreen" === a && c.appendChild(n.call(t, "fullscreen", h))
                    }), this.isHTML5 && a.call(this, Fe.getQualityOptions.call(this)), l.call(this), c
                }, inject: function () {
                    var e = this;
                    if (this.config.loadSprite) {
                        var t = $e.getIconUrl.call(this);
                        t.cors && We(t.url, "sprite-plyr")
                    }
                    this.id = Math.floor(1e4 * Math.random());
                    var i = null;
                    this.elements.controls = null;
                    var n, r, s = {id: this.id, seektime: this.config.seekTime, title: this.config.title}, o = !0;
                    if (X(this.config.controls) && (this.config.controls = this.config.controls.call(this, s)), this.config.controls || (this.config.controls = []), G(this.config.controls) || q(this.config.controls) ? i = this.config.controls : (i = $e.create.call(this, {
                        id: this.id,
                        seektime: this.config.seekTime,
                        speed: this.speed,
                        quality: this.quality,
                        captions: Qe.getLabel.call(this)
                    }), o = !1), o && q(this.config.controls) && (n = i, Object.entries(s).forEach(function (e) {
                        var t = a(e, 2), i = t[0], r = t[1];
                        n = Ie(n, "{".concat(i, "}"), r)
                    }), i = n), q(this.config.selectors.controls.container) && (r = document.querySelector(this.config.selectors.controls.container)), G(r) || (r = this.elements.container), r[G(i) ? "insertAdjacentElement" : "insertAdjacentHTML"]("afterbegin", i), G(this.elements.controls) || $e.findElements.call(this), !Z(this.elements.buttons)) {
                        var l = function (t) {
                            var i = e.config.classNames.controlPressed;
                            Object.defineProperty(t, "pressed", {
                                enumerable: !0, get: function () {
                                    return me(t, i)
                                }, set: function () {
                                    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                                    fe(t, i, e)
                                }
                            })
                        };
                        Object.values(this.elements.buttons).filter(Boolean).forEach(function (e) {
                            Y(e) || U(e) ? Array.from(e).filter(Boolean).forEach(l) : l(e)
                        })
                    }
                    if (ie.isEdge && te(r), this.config.tooltips.controls) {
                        var u = this.config, c = u.classNames, h = u.selectors,
                            d = "".concat(h.controls.wrapper, " ").concat(h.labels, " .").concat(c.hidden),
                            p = ve.call(this, d);
                        Array.from(p).forEach(function (t) {
                            fe(t, e.config.classNames.hidden, !1), fe(t, e.config.classNames.tooltip, !0)
                        })
                    }
                }
            };

            function Ke(e) {
                var t = e;
                if (!(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1]) {
                    var i = document.createElement("a");
                    i.href = t, t = i.href
                }
                try {
                    return new URL(t)
                } catch (e) {
                    return null
                }
            }

            function Je(e) {
                var t = new URLSearchParams;
                return H(e) && Object.entries(e).forEach(function (e) {
                    var i = a(e, 2), n = i[0], r = i[1];
                    t.set(n, r)
                }), t
            }

            var Qe = {
                setup: function () {
                    if (this.supported.ui) if (!this.isVideo || this.isYouTube || this.isHTML5 && !Te.textTracks) Y(this.config.controls) && this.config.controls.includes("settings") && this.config.settings.includes("captions") && $e.setCaptionsMenu.call(this); else {
                        if (G(this.elements.captions) || (this.elements.captions = ae("div", de(this.config.selectors.captions)), function (e, t) {
                            G(e) && G(t) && t.parentNode.insertBefore(e, t.nextSibling)
                        }(this.elements.captions, this.elements.wrapper)), ie.isIE && window.URL) {
                            var e = this.media.querySelectorAll("track");
                            Array.from(e).forEach(function (e) {
                                var t = e.getAttribute("src"), i = Ke(t);
                                null !== i && i.hostname !== window.location.href.hostname && ["http:", "https:"].includes(i.protocol) && qe(t, "blob").then(function (t) {
                                    e.setAttribute("src", window.URL.createObjectURL(t))
                                }).catch(function () {
                                    ue(e)
                                })
                            })
                        }
                        var t = je((navigator.languages || [navigator.language || navigator.userLanguage || "en"]).map(function (e) {
                                return e.split("-")[0]
                            })),
                            i = (this.storage.get("language") || this.config.captions.language || "auto").toLowerCase();
                        "auto" === i && (i = a(t, 1)[0]);
                        var n = this.storage.get("captions");
                        if (W(n) || (n = this.config.captions.active), Object.assign(this.captions, {
                            toggled: !1,
                            active: n,
                            language: i,
                            languages: t
                        }), this.isHTML5) {
                            var r = this.config.captions.update ? "addtrack removetrack" : "removetrack";
                            Ee.call(this, this.media.textTracks, r, Qe.update.bind(this))
                        }
                        setTimeout(Qe.update.bind(this), 0)
                    }
                }, update: function () {
                    var e = this, t = Qe.getTracks.call(this, !0), i = this.captions, n = i.active, r = i.language,
                        s = i.meta, o = i.currentTrackNode, a = Boolean(t.find(function (e) {
                            return e.language === r
                        }));
                    this.isHTML5 && this.isVideo && t.filter(function (e) {
                        return !s.get(e)
                    }).forEach(function (t) {
                        e.debug.log("Track added", t), s.set(t, {default: "showing" === t.mode}), "showing" === t.mode && (t.mode = "hidden"), Ee.call(e, t, "cuechange", function () {
                            return Qe.updateCues.call(e)
                        })
                    }), (a && this.language !== r || !t.includes(o)) && (Qe.setLanguage.call(this, r), Qe.toggle.call(this, n && a)), fe(this.elements.container, this.config.classNames.captions.enabled, !Z(t)), Y(this.config.controls) && this.config.controls.includes("settings") && this.config.settings.includes("captions") && $e.setCaptionsMenu.call(this)
                }, toggle: function (e) {
                    var t = this, i = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                    if (this.supported.ui) {
                        var n = this.captions.toggled, r = this.config.classNames.captions.active, s = z(e) ? !n : e;
                        if (s !== n) {
                            if (i || (this.captions.active = s, this.storage.set({captions: s})), !this.language && s && !i) {
                                var o = Qe.getTracks.call(this),
                                    a = Qe.findTrack.call(this, [this.captions.language].concat(l(this.captions.languages)), !0);
                                return this.captions.language = a.language, void Qe.set.call(this, o.indexOf(a))
                            }
                            this.elements.buttons.captions && (this.elements.buttons.captions.pressed = s), fe(this.elements.container, r, s), this.captions.toggled = s, $e.updateSetting.call(this, "captions"), Se.call(this, this.media, s ? "captionsenabled" : "captionsdisabled")
                        }
                        setTimeout(function () {
                            s && t.captions.toggled && (t.captions.currentTrackNode.mode = "hidden")
                        })
                    }
                }, set: function (e) {
                    var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
                        i = Qe.getTracks.call(this);
                    if (-1 !== e) if (V(e)) if (e in i) {
                        if (this.captions.currentTrack !== e) {
                            this.captions.currentTrack = e;
                            var n = i[e], r = (n || {}).language;
                            this.captions.currentTrackNode = n, $e.updateSetting.call(this, "captions"), t || (this.captions.language = r, this.storage.set({language: r})), this.isVimeo && this.embed.enableTextTrack(r), Se.call(this, this.media, "languagechange")
                        }
                        Qe.toggle.call(this, !0, t), this.isHTML5 && this.isVideo && Qe.updateCues.call(this)
                    } else this.debug.warn("Track not found", e); else this.debug.warn("Invalid caption argument", e); else Qe.toggle.call(this, !1, t)
                }, setLanguage: function (e) {
                    var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                    if (q(e)) {
                        var i = e.toLowerCase();
                        this.captions.language = i;
                        var n = Qe.getTracks.call(this), r = Qe.findTrack.call(this, [i]);
                        Qe.set.call(this, n.indexOf(r), t)
                    } else this.debug.warn("Invalid language argument", e)
                }, getTracks: function () {
                    var e = this, t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    return Array.from((this.media || {}).textTracks || []).filter(function (i) {
                        return !e.isHTML5 || t || e.captions.meta.has(i)
                    }).filter(function (e) {
                        return ["captions", "subtitles"].includes(e.kind)
                    })
                }, findTrack: function (e) {
                    var t, i = this, n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                        r = Qe.getTracks.call(this), s = function (e) {
                            return Number((i.captions.meta.get(e) || {}).default)
                        }, o = Array.from(r).sort(function (e, t) {
                            return s(t) - s(e)
                        });
                    return e.every(function (e) {
                        return !(t = o.find(function (t) {
                            return t.language === e
                        }))
                    }), t || (n ? o[0] : void 0)
                }, getCurrentTrack: function () {
                    return Qe.getTracks.call(this)[this.currentTrack]
                }, getLabel: function (e) {
                    var t = e;
                    return !J(t) && Te.textTracks && this.captions.toggled && (t = Qe.getCurrentTrack.call(this)), J(t) ? Z(t.label) ? Z(t.language) ? He("enabled", this.config) : e.language.toUpperCase() : t.label : He("disabled", this.config)
                }, updateCues: function (e) {
                    if (this.supported.ui) if (G(this.elements.captions)) if (z(e) || Array.isArray(e)) {
                        var t = e;
                        if (!t) {
                            var i = Qe.getCurrentTrack.call(this);
                            t = Array.from((i || {}).activeCues || []).map(function (e) {
                                return e.getCueAsHTML()
                            }).map(Ne)
                        }
                        var n = t.map(function (e) {
                            return e.trim()
                        }).join("\n");
                        if (n !== this.elements.captions.innerHTML) {
                            ce(this.elements.captions);
                            var r = ae("span", de(this.config.selectors.caption));
                            r.innerHTML = n, this.elements.captions.appendChild(r), Se.call(this, this.media, "cuechange")
                        }
                    } else this.debug.warn("updateCues: Invalid input", e); else this.debug.warn("No captions element to render to")
                }
            }, Ze = {
                enabled: !0,
                title: "",
                debug: !1,
                autoplay: !1,
                autopause: !0,
                playsinline: !0,
                seekTime: 10,
                volume: 1,
                muted: !1,
                duration: null,
                displayDuration: !0,
                invertTime: !0,
                toggleInvert: !0,
                ratio: null,
                clickToPlay: !0,
                hideControls: !0,
                resetOnEnd: !1,
                disableContextMenu: !0,
                loadSprite: !0,
                iconPrefix: "plyr",
                iconUrl: "https://cdn.plyr.io/3.6.2/plyr.svg",
                blankVideo: "https://cdn.plyr.io/static/blank.mp4",
                quality: {
                    default: 576,
                    options: [4320, 2880, 2160, 1440, 1080, 720, 576, 480, 360, 240],
                    forced: !1,
                    onChange: null
                },
                loop: {active: !1},
                speed: {selected: 1, options: [.5, .75, 1, 1.25, 1.5, 1.75, 2, 4]},
                keyboard: {focused: !0, global: !1},
                tooltips: {controls: !1, seek: !0},
                captions: {active: !1, language: "auto", update: !1},
                fullscreen: {enabled: !0, fallback: !0, iosNative: !1},
                storage: {enabled: !0, key: "plyr"},
                controls: ["play-large", "play", "progress", "current-time", "mute", "volume", "captions", "settings", "pip", "airplay", "fullscreen"],
                settings: ["captions", "quality", "speed"],
                i18n: {
                    restart: "Restart",
                    rewind: "Rewind {seektime}s",
                    play: "Play",
                    pause: "Pause",
                    fastForward: "Forward {seektime}s",
                    seek: "Seek",
                    seekLabel: "{currentTime} of {duration}",
                    played: "Played",
                    buffered: "Buffered",
                    currentTime: "Current time",
                    duration: "Duration",
                    volume: "Volume",
                    mute: "Mute",
                    unmute: "Unmute",
                    enableCaptions: "Enable captions",
                    disableCaptions: "Disable captions",
                    download: "Download",
                    enterFullscreen: "Enter fullscreen",
                    exitFullscreen: "Exit fullscreen",
                    frameTitle: "Player for {title}",
                    captions: "Captions",
                    settings: "Settings",
                    pip: "PIP",
                    menuBack: "Go back to previous menu",
                    speed: "Speed",
                    normal: "Normal",
                    quality: "Quality",
                    loop: "Loop",
                    start: "Start",
                    end: "End",
                    all: "All",
                    reset: "Reset",
                    disabled: "Disabled",
                    enabled: "Enabled",
                    advertisement: "Ad",
                    qualityBadge: {2160: "4K", 1440: "HD", 1080: "HD", 720: "HD", 576: "SD", 480: "SD"}
                },
                urls: {
                    download: null,
                    vimeo: {
                        sdk: "https://player.vimeo.com/api/player.js",
                        iframe: "https://player.vimeo.com/video/{0}?{1}",
                        api: "https://vimeo.com/api/v2/video/{0}.json"
                    },
                    youtube: {
                        sdk: "https://www.youtube.com/iframe_api",
                        api: "https://noembed.com/embed?url=https://www.youtube.com/watch?v={0}"
                    },
                    googleIMA: {sdk: "https://imasdk.googleapis.com/js/sdkloader/ima3.js"}
                },
                listeners: {
                    seek: null,
                    play: null,
                    pause: null,
                    restart: null,
                    rewind: null,
                    fastForward: null,
                    mute: null,
                    volume: null,
                    captions: null,
                    download: null,
                    fullscreen: null,
                    pip: null,
                    airplay: null,
                    speed: null,
                    quality: null,
                    loop: null,
                    language: null
                },
                events: ["ended", "progress", "stalled", "playing", "waiting", "canplay", "canplaythrough", "loadstart", "loadeddata", "loadedmetadata", "timeupdate", "volumechange", "play", "pause", "error", "seeking", "seeked", "emptied", "ratechange", "cuechange", "download", "enterfullscreen", "exitfullscreen", "captionsenabled", "captionsdisabled", "languagechange", "controlshidden", "controlsshown", "ready", "statechange", "qualitychange", "adsloaded", "adscontentpause", "adscontentresume", "adstarted", "adsmidpoint", "adscomplete", "adsallcomplete", "adsimpression", "adsclick"],
                selectors: {
                    editable: "input, textarea, select, [contenteditable]",
                    container: ".plyr",
                    controls: {container: null, wrapper: ".plyr__controls"},
                    labels: "[data-plyr]",
                    buttons: {
                        play: '[data-plyr="play"]',
                        pause: '[data-plyr="pause"]',
                        restart: '[data-plyr="restart"]',
                        rewind: '[data-plyr="rewind"]',
                        fastForward: '[data-plyr="fast-forward"]',
                        mute: '[data-plyr="mute"]',
                        captions: '[data-plyr="captions"]',
                        download: '[data-plyr="download"]',
                        fullscreen: '[data-plyr="fullscreen"]',
                        pip: '[data-plyr="pip"]',
                        airplay: '[data-plyr="airplay"]',
                        settings: '[data-plyr="settings"]',
                        loop: '[data-plyr="loop"]'
                    },
                    inputs: {
                        seek: '[data-plyr="seek"]',
                        volume: '[data-plyr="volume"]',
                        speed: '[data-plyr="speed"]',
                        language: '[data-plyr="language"]',
                        quality: '[data-plyr="quality"]'
                    },
                    display: {
                        currentTime: ".plyr__time--current",
                        duration: ".plyr__time--duration",
                        buffer: ".plyr__progress__buffer",
                        loop: ".plyr__progress__loop",
                        volume: ".plyr__volume--display"
                    },
                    progress: ".plyr__progress",
                    captions: ".plyr__captions",
                    caption: ".plyr__caption"
                },
                classNames: {
                    type: "plyr--{0}",
                    provider: "plyr--{0}",
                    video: "plyr__video-wrapper",
                    embed: "plyr__video-embed",
                    videoFixedRatio: "plyr__video-wrapper--fixed-ratio",
                    embedContainer: "plyr__video-embed__container",
                    poster: "plyr__poster",
                    posterEnabled: "plyr__poster-enabled",
                    ads: "plyr__ads",
                    control: "plyr__control",
                    controlPressed: "plyr__control--pressed",
                    playing: "plyr--playing",
                    paused: "plyr--paused",
                    stopped: "plyr--stopped",
                    loading: "plyr--loading",
                    hover: "plyr--hover",
                    tooltip: "plyr__tooltip",
                    cues: "plyr__cues",
                    hidden: "plyr__sr-only",
                    hideControls: "plyr--hide-controls",
                    isIos: "plyr--is-ios",
                    isTouch: "plyr--is-touch",
                    uiSupported: "plyr--full-ui",
                    noTransition: "plyr--no-transition",
                    display: {time: "plyr__time"},
                    menu: {value: "plyr__menu__value", badge: "plyr__badge", open: "plyr--menu-open"},
                    captions: {enabled: "plyr--captions-enabled", active: "plyr--captions-active"},
                    fullscreen: {enabled: "plyr--fullscreen-enabled", fallback: "plyr--fullscreen-fallback"},
                    pip: {supported: "plyr--pip-supported", active: "plyr--pip-active"},
                    airplay: {supported: "plyr--airplay-supported", active: "plyr--airplay-active"},
                    tabFocus: "plyr__tab-focus",
                    previewThumbnails: {
                        thumbContainer: "plyr__preview-thumb",
                        thumbContainerShown: "plyr__preview-thumb--is-shown",
                        imageContainer: "plyr__preview-thumb__image-container",
                        timeContainer: "plyr__preview-thumb__time-container",
                        scrubbingContainer: "plyr__preview-scrubbing",
                        scrubbingContainerShown: "plyr__preview-scrubbing--is-shown"
                    }
                },
                attributes: {embed: {provider: "data-plyr-provider", id: "data-plyr-embed-id"}},
                ads: {enabled: !1, publisherId: "", tagUrl: ""},
                previewThumbnails: {enabled: !1, src: ""},
                vimeo: {
                    byline: !1,
                    portrait: !1,
                    title: !1,
                    speed: !0,
                    transparent: !1,
                    premium: !1,
                    referrerPolicy: null
                },
                youtube: {noCookie: !0, rel: 0, showinfo: 0, iv_load_policy: 3, modestbranding: 1}
            }, et = "picture-in-picture", tt = {html5: "html5", youtube: "youtube", vimeo: "vimeo"}, it = function () {
            }, nt = function () {
                function t() {
                    var i = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    e(this, t), this.enabled = window.console && i, this.enabled && this.log("Debugging enabled")
                }

                return n(t, [{
                    key: "log", get: function () {
                        return this.enabled ? Function.prototype.bind.call(console.log, console) : it
                    }
                }, {
                    key: "warn", get: function () {
                        return this.enabled ? Function.prototype.bind.call(console.warn, console) : it
                    }
                }, {
                    key: "error", get: function () {
                        return this.enabled ? Function.prototype.bind.call(console.error, console) : it
                    }
                }]), t
            }(), rt = function () {
                function t(i) {
                    var n = this;
                    e(this, t), this.player = i, this.prefix = t.prefix, this.property = t.property, this.scrollPosition = {
                        x: 0,
                        y: 0
                    }, this.forceFallback = "force" === i.config.fullscreen.fallback, this.player.elements.fullscreen = i.config.fullscreen.container && function (e, t) {
                        return (Element.prototype.closest || function () {
                            var e = this;
                            do {
                                if (ge.matches(e, t)) return e;
                                e = e.parentElement || e.parentNode
                            } while (null !== e && 1 === e.nodeType);
                            return null
                        }).call(e, t)
                    }(this.player.elements.container, i.config.fullscreen.container), Ee.call(this.player, document, "ms" === this.prefix ? "MSFullscreenChange" : "".concat(this.prefix, "fullscreenchange"), function () {
                        n.onChange()
                    }), Ee.call(this.player, this.player.elements.container, "dblclick", function (e) {
                        G(n.player.elements.controls) && n.player.elements.controls.contains(e.target) || n.toggle()
                    }), Ee.call(this, this.player.elements.container, "keydown", function (e) {
                        return n.trapFocus(e)
                    }), this.update()
                }

                return n(t, [{
                    key: "onChange", value: function () {
                        if (this.enabled) {
                            var e = this.player.elements.buttons.fullscreen;
                            G(e) && (e.pressed = this.active), Se.call(this.player, this.target, this.active ? "enterfullscreen" : "exitfullscreen", !0)
                        }
                    }
                }, {
                    key: "toggleFallback", value: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                        if (e ? this.scrollPosition = {
                            x: window.scrollX || 0,
                            y: window.scrollY || 0
                        } : window.scrollTo(this.scrollPosition.x, this.scrollPosition.y), document.body.style.overflow = e ? "hidden" : "", fe(this.target, this.player.config.classNames.fullscreen.fallback, e), ie.isIos) {
                            var t = document.head.querySelector('meta[name="viewport"]'), i = "viewport-fit=cover";
                            t || (t = document.createElement("meta")).setAttribute("name", "viewport");
                            var n = q(t.content) && t.content.includes(i);
                            e ? (this.cleanupViewport = !n, n || (t.content += ",".concat(i))) : this.cleanupViewport && (t.content = t.content.split(",").filter(function (e) {
                                return e.trim() !== i
                            }).join(","))
                        }
                        this.onChange()
                    }
                }, {
                    key: "trapFocus", value: function (e) {
                        if (!ie.isIos && this.active && "Tab" === e.key && 9 === e.keyCode) {
                            var t = document.activeElement,
                                i = ve.call(this.player, "a[href], button:not(:disabled), input:not(:disabled), [tabindex]"),
                                n = a(i, 1)[0], r = i[i.length - 1];
                            t !== r || e.shiftKey ? t === n && e.shiftKey && (r.focus(), e.preventDefault()) : (n.focus(), e.preventDefault())
                        }
                    }
                }, {
                    key: "update", value: function () {
                        var e;
                        this.enabled ? (e = this.forceFallback ? "Fallback (forced)" : t.native ? "Native" : "Fallback", this.player.debug.log("".concat(e, " fullscreen enabled"))) : this.player.debug.log("Fullscreen not supported and fallback disabled"), fe(this.player.elements.container, this.player.config.classNames.fullscreen.enabled, this.enabled)
                    }
                }, {
                    key: "enter", value: function () {
                        this.enabled && (ie.isIos && this.player.config.fullscreen.iosNative ? this.target.webkitEnterFullscreen() : !t.native || this.forceFallback ? this.toggleFallback(!0) : this.prefix ? Z(this.prefix) || this.target["".concat(this.prefix, "Request").concat(this.property)]() : this.target.requestFullscreen({navigationUI: "hide"}))
                    }
                }, {
                    key: "exit", value: function () {
                        if (this.enabled) if (ie.isIos && this.player.config.fullscreen.iosNative) this.target.webkitExitFullscreen(), Pe(this.player.play()); else if (!t.native || this.forceFallback) this.toggleFallback(!1); else if (this.prefix) {
                            if (!Z(this.prefix)) {
                                var e = "moz" === this.prefix ? "Cancel" : "Exit";
                                document["".concat(this.prefix).concat(e).concat(this.property)]()
                            }
                        } else (document.cancelFullScreen || document.exitFullscreen).call(document)
                    }
                }, {
                    key: "toggle", value: function () {
                        this.active ? this.exit() : this.enter()
                    }
                }, {
                    key: "usingNative", get: function () {
                        return t.native && !this.forceFallback
                    }
                }, {
                    key: "enabled", get: function () {
                        return (t.native || this.player.config.fullscreen.fallback) && this.player.config.fullscreen.enabled && this.player.supported.ui && this.player.isVideo
                    }
                }, {
                    key: "active", get: function () {
                        if (!this.enabled) return !1;
                        if (!t.native || this.forceFallback) return me(this.target, this.player.config.classNames.fullscreen.fallback);
                        var e = this.prefix ? document["".concat(this.prefix).concat(this.property, "Element")] : document.fullscreenElement;
                        return e && e.shadowRoot ? e === this.target.getRootNode().host : e === this.target
                    }
                }, {
                    key: "target", get: function () {
                        return ie.isIos && this.player.config.fullscreen.iosNative ? this.player.media : this.player.elements.fullscreen || this.player.elements.container
                    }
                }], [{
                    key: "native", get: function () {
                        return !!(document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled)
                    }
                }, {
                    key: "prefix", get: function () {
                        if (X(document.exitFullscreen)) return "";
                        var e = "";
                        return ["webkit", "moz", "ms"].some(function (t) {
                            return !(!X(document["".concat(t, "ExitFullscreen")]) && !X(document["".concat(t, "CancelFullScreen")]) || (e = t, 0))
                        }), e
                    }
                }, {
                    key: "property", get: function () {
                        return "moz" === this.prefix ? "FullScreen" : "Fullscreen"
                    }
                }]), t
            }();

            function st(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1;
                return new Promise(function (i, n) {
                    var r = new Image, s = function () {
                        delete r.onload, delete r.onerror, (r.naturalWidth >= t ? i : n)(r)
                    };
                    Object.assign(r, {onload: s, onerror: s, src: e})
                })
            }

            var ot = {
                addStyleHook: function () {
                    fe(this.elements.container, this.config.selectors.container.replace(".", ""), !0), fe(this.elements.container, this.config.classNames.uiSupported, this.supported.ui)
                }, toggleNativeControls: function () {
                    arguments.length > 0 && void 0 !== arguments[0] && arguments[0] && this.isHTML5 ? this.media.setAttribute("controls", "") : this.media.removeAttribute("controls")
                }, build: function () {
                    var e = this;
                    if (this.listeners.media(), !this.supported.ui) return this.debug.warn("Basic support only for ".concat(this.provider, " ").concat(this.type)), void ot.toggleNativeControls.call(this, !0);
                    G(this.elements.controls) || ($e.inject.call(this), this.listeners.controls()), ot.toggleNativeControls.call(this), this.isHTML5 && Qe.setup.call(this), this.volume = null, this.muted = null, this.loop = null, this.quality = null, this.speed = null, $e.updateVolume.call(this), $e.timeUpdate.call(this), ot.checkPlaying.call(this), fe(this.elements.container, this.config.classNames.pip.supported, Te.pip && this.isHTML5 && this.isVideo), fe(this.elements.container, this.config.classNames.airplay.supported, Te.airplay && this.isHTML5), fe(this.elements.container, this.config.classNames.isIos, ie.isIos), fe(this.elements.container, this.config.classNames.isTouch, this.touch), this.ready = !0, setTimeout(function () {
                        Se.call(e, e.media, "ready")
                    }, 0), ot.setTitle.call(this), this.poster && ot.setPoster.call(this, this.poster, !1).catch(function () {
                    }), this.config.duration && $e.durationUpdate.call(this)
                }, setTitle: function () {
                    var e = He("play", this.config);
                    if (q(this.config.title) && !Z(this.config.title) && (e += ", ".concat(this.config.title)), Array.from(this.elements.buttons.play || []).forEach(function (t) {
                        t.setAttribute("aria-label", e)
                    }), this.isEmbed) {
                        var t = ye.call(this, "iframe");
                        if (!G(t)) return;
                        var i = Z(this.config.title) ? "video" : this.config.title, n = He("frameTitle", this.config);
                        t.setAttribute("title", n.replace("{title}", i))
                    }
                }, togglePoster: function (e) {
                    fe(this.elements.container, this.config.classNames.posterEnabled, e)
                }, setPoster: function (e) {
                    var t = this;
                    return (!(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1]) && this.poster ? Promise.reject(new Error("Poster already set")) : (this.media.setAttribute("data-poster", e), function () {
                        var e = this;
                        return new Promise(function (t) {
                            return e.ready ? setTimeout(t, 0) : Ee.call(e, e.elements.container, "ready", t)
                        }).then(function () {
                        })
                    }.call(this).then(function () {
                        return st(e)
                    }).catch(function (i) {
                        throw e === t.poster && ot.togglePoster.call(t, !1), i
                    }).then(function () {
                        if (e !== t.poster) throw new Error("setPoster cancelled by later call to setPoster")
                    }).then(function () {
                        return Object.assign(t.elements.poster.style, {
                            backgroundImage: "url('".concat(e, "')"),
                            backgroundSize: ""
                        }), ot.togglePoster.call(t, !0), e
                    }))
                }, checkPlaying: function (e) {
                    var t = this;
                    fe(this.elements.container, this.config.classNames.playing, this.playing), fe(this.elements.container, this.config.classNames.paused, this.paused), fe(this.elements.container, this.config.classNames.stopped, this.stopped), Array.from(this.elements.buttons.play || []).forEach(function (e) {
                        Object.assign(e, {pressed: t.playing}), e.setAttribute("aria-label", He(t.playing ? "pause" : "play", t.config))
                    }), $(e) && "timeupdate" === e.type || ot.toggleControls.call(this)
                }, checkLoading: function (e) {
                    var t = this;
                    this.loading = ["stalled", "waiting"].includes(e.type), clearTimeout(this.timers.loading), this.timers.loading = setTimeout(function () {
                        fe(t.elements.container, t.config.classNames.loading, t.loading), ot.toggleControls.call(t)
                    }, this.loading ? 250 : 0)
                }, toggleControls: function (e) {
                    var t = this.elements.controls;
                    if (t && this.config.hideControls) {
                        var i = this.touch && this.lastSeekTime + 2e3 > Date.now();
                        this.toggleControls(Boolean(e || this.loading || this.paused || t.pressed || t.hover || i))
                    }
                }, migrateStyles: function () {
                    var e = this;
                    Object.values(o({}, this.media.style)).filter(function (e) {
                        return !Z(e) && e.startsWith("--plyr")
                    }).forEach(function (t) {
                        e.elements.container.style.setProperty(t, e.media.style.getPropertyValue(t)), e.media.style.removeProperty(t)
                    }), Z(this.media.style) && this.media.removeAttribute("style")
                }
            }, at = function () {
                function t(i) {
                    e(this, t), this.player = i, this.lastKey = null, this.focusTimer = null, this.lastKeyDown = null, this.handleKey = this.handleKey.bind(this), this.toggleMenu = this.toggleMenu.bind(this), this.setTabFocus = this.setTabFocus.bind(this), this.firstTouch = this.firstTouch.bind(this)
                }

                return n(t, [{
                    key: "handleKey", value: function (e) {
                        var t = this.player, i = t.elements, n = e.keyCode ? e.keyCode : e.which,
                            r = "keydown" === e.type, s = r && n === this.lastKey;
                        if (!(e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) && V(n)) if (r) {
                            var o = document.activeElement;
                            if (G(o)) {
                                var a = t.config.selectors.editable;
                                if (o !== i.inputs.seek && ge(o, a)) return;
                                if (32 === e.which && ge(o, 'button, [role^="menuitem"]')) return
                            }
                            switch ([32, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 56, 57, 67, 70, 73, 75, 76, 77, 79].includes(n) && (e.preventDefault(), e.stopPropagation()), n) {
                                case 48:
                                case 49:
                                case 50:
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                case 57:
                                    s || (t.currentTime = t.duration / 10 * (n - 48));
                                    break;
                                case 32:
                                case 75:
                                    s || Pe(t.togglePlay());
                                    break;
                                case 38:
                                    t.increaseVolume(.1);
                                    break;
                                case 40:
                                    t.decreaseVolume(.1);
                                    break;
                                case 77:
                                    s || (t.muted = !t.muted);
                                    break;
                                case 39:
                                    t.forward();
                                    break;
                                case 37:
                                    t.rewind();
                                    break;
                                case 70:
                                    t.fullscreen.toggle();
                                    break;
                                case 67:
                                    s || t.toggleCaptions();
                                    break;
                                case 76:
                                    t.loop = !t.loop
                            }
                            27 === n && !t.fullscreen.usingNative && t.fullscreen.active && t.fullscreen.toggle(), this.lastKey = n
                        } else this.lastKey = null
                    }
                }, {
                    key: "toggleMenu", value: function (e) {
                        $e.toggleMenu.call(this.player, e)
                    }
                }, {
                    key: "firstTouch", value: function () {
                        var e = this.player, t = e.elements;
                        e.touch = !0, fe(t.container, e.config.classNames.isTouch, !0)
                    }
                }, {
                    key: "setTabFocus", value: function (e) {
                        var t = this.player, i = t.elements;
                        if (clearTimeout(this.focusTimer), "keydown" !== e.type || 9 === e.which) {
                            "keydown" === e.type && (this.lastKeyDown = e.timeStamp);
                            var n, r = e.timeStamp - this.lastKeyDown <= 20;
                            ("focus" !== e.type || r) && (n = t.config.classNames.tabFocus, fe(ve.call(t, ".".concat(n)), n, !1), "focusout" !== e.type && (this.focusTimer = setTimeout(function () {
                                var e = document.activeElement;
                                i.container.contains(e) && fe(document.activeElement, t.config.classNames.tabFocus, !0)
                            }, 10)))
                        }
                    }
                }, {
                    key: "global", value: function () {
                        var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], t = this.player;
                        t.config.keyboard.global && ke.call(t, window, "keydown keyup", this.handleKey, e, !1), ke.call(t, document.body, "click", this.toggleMenu, e), xe.call(t, document.body, "touchstart", this.firstTouch), ke.call(t, document.body, "keydown focus blur focusout", this.setTabFocus, e, !1, !0)
                    }
                }, {
                    key: "container", value: function () {
                        var e = this.player, t = e.config, i = e.elements, n = e.timers;
                        !t.keyboard.global && t.keyboard.focused && Ee.call(e, i.container, "keydown keyup", this.handleKey, !1), Ee.call(e, i.container, "mousemove mouseleave touchstart touchmove enterfullscreen exitfullscreen", function (t) {
                            var r = i.controls;
                            r && "enterfullscreen" === t.type && (r.pressed = !1, r.hover = !1);
                            var s = 0;
                            ["touchstart", "touchmove", "mousemove"].includes(t.type) && (ot.toggleControls.call(e, !0), s = e.touch ? 3e3 : 2e3), clearTimeout(n.controls), n.controls = setTimeout(function () {
                                return ot.toggleControls.call(e, !1)
                            }, s)
                        });
                        var r = function (t) {
                            if (!t) return Le.call(e);
                            var n = i.container.getBoundingClientRect(), r = n.width, s = n.height;
                            return Le.call(e, "".concat(r, ":").concat(s))
                        }, s = function () {
                            clearTimeout(n.resized), n.resized = setTimeout(r, 50)
                        };
                        Ee.call(e, i.container, "enterfullscreen exitfullscreen", function (t) {
                            var n = e.fullscreen, o = n.target, l = n.usingNative;
                            if (o === i.container && (e.isEmbed || !Z(e.config.ratio))) {
                                var u = "enterfullscreen" === t.type, c = r(u);
                                c.padding, function (t, i, n) {
                                    if (e.isVimeo && !e.config.vimeo.premium) {
                                        var r = e.elements.wrapper.firstChild, s = a(t, 2)[1], o = a(Me.call(e), 2),
                                            l = o[0], u = o[1];
                                        r.style.maxWidth = n ? "".concat(s / u * l, "px") : null, r.style.margin = n ? "0 auto" : null
                                    }
                                }(c.ratio, 0, u), l || (u ? Ee.call(e, window, "resize", s) : _e.call(e, window, "resize", s))
                            }
                        })
                    }
                }, {
                    key: "media", value: function () {
                        var e = this, t = this.player, i = t.elements;
                        if (Ee.call(t, t.media, "timeupdate seeking seeked", function (e) {
                            return $e.timeUpdate.call(t, e)
                        }), Ee.call(t, t.media, "durationchange loadeddata loadedmetadata", function (e) {
                            return $e.durationUpdate.call(t, e)
                        }), Ee.call(t, t.media, "ended", function () {
                            t.isHTML5 && t.isVideo && t.config.resetOnEnd && (t.restart(), t.pause())
                        }), Ee.call(t, t.media, "progress playing seeking seeked", function (e) {
                            return $e.updateProgress.call(t, e)
                        }), Ee.call(t, t.media, "volumechange", function (e) {
                            return $e.updateVolume.call(t, e)
                        }), Ee.call(t, t.media, "playing play pause ended emptied timeupdate", function (e) {
                            return ot.checkPlaying.call(t, e)
                        }), Ee.call(t, t.media, "waiting canplay seeked playing", function (e) {
                            return ot.checkLoading.call(t, e)
                        }), t.supported.ui && t.config.clickToPlay && !t.isAudio) {
                            var n = ye.call(t, ".".concat(t.config.classNames.video));
                            if (!G(n)) return;
                            Ee.call(t, i.container, "click", function (r) {
                                ([i.container, n].includes(r.target) || n.contains(r.target)) && (t.touch && t.config.hideControls || (t.ended ? (e.proxy(r, t.restart, "restart"), e.proxy(r, function () {
                                    Pe(t.play())
                                }, "play")) : e.proxy(r, function () {
                                    Pe(t.togglePlay())
                                }, "play")))
                            })
                        }
                        t.supported.ui && t.config.disableContextMenu && Ee.call(t, i.wrapper, "contextmenu", function (e) {
                            e.preventDefault()
                        }, !1), Ee.call(t, t.media, "volumechange", function () {
                            t.storage.set({volume: t.volume, muted: t.muted})
                        }), Ee.call(t, t.media, "ratechange", function () {
                            $e.updateSetting.call(t, "speed"), t.storage.set({speed: t.speed})
                        }), Ee.call(t, t.media, "qualitychange", function (e) {
                            $e.updateSetting.call(t, "quality", null, e.detail.quality)
                        }), Ee.call(t, t.media, "ready qualitychange", function () {
                            $e.setDownloadUrl.call(t)
                        });
                        var r = t.config.events.concat(["keyup", "keydown"]).join(" ");
                        Ee.call(t, t.media, r, function (e) {
                            var n = e.detail, r = void 0 === n ? {} : n;
                            "error" === e.type && (r = t.media.error), Se.call(t, i.container, e.type, !0, r)
                        })
                    }
                }, {
                    key: "proxy", value: function (e, t, i) {
                        var n = this.player, r = n.config.listeners[i], s = !0;
                        X(r) && (s = r.call(n, e)), !1 !== s && X(t) && t.call(n, e)
                    }
                }, {
                    key: "bind", value: function (e, t, i, n) {
                        var r = this, s = !(arguments.length > 4 && void 0 !== arguments[4]) || arguments[4],
                            o = this.player, a = o.config.listeners[n], l = X(a);
                        Ee.call(o, e, t, function (e) {
                            return r.proxy(e, i, n)
                        }, s && !l)
                    }
                }, {
                    key: "controls", value: function () {
                        var e = this, t = this.player, i = t.elements, n = ie.isIE ? "change" : "input";
                        if (i.buttons.play && Array.from(i.buttons.play).forEach(function (i) {
                            e.bind(i, "click", function () {
                                Pe(t.togglePlay())
                            }, "play")
                        }), this.bind(i.buttons.restart, "click", t.restart, "restart"), this.bind(i.buttons.rewind, "click", t.rewind, "rewind"), this.bind(i.buttons.fastForward, "click", t.forward, "fastForward"), this.bind(i.buttons.mute, "click", function () {
                            t.muted = !t.muted
                        }, "mute"), this.bind(i.buttons.captions, "click", function () {
                            return t.toggleCaptions()
                        }), this.bind(i.buttons.download, "click", function () {
                            Se.call(t, t.media, "download")
                        }, "download"), this.bind(i.buttons.fullscreen, "click", function () {
                            t.fullscreen.toggle()
                        }, "fullscreen"), this.bind(i.buttons.pip, "click", function () {
                            t.pip = "toggle"
                        }, "pip"), this.bind(i.buttons.airplay, "click", t.airplay, "airplay"), this.bind(i.buttons.settings, "click", function (e) {
                            e.stopPropagation(), e.preventDefault(), $e.toggleMenu.call(t, e)
                        }, null, !1), this.bind(i.buttons.settings, "keyup", function (e) {
                            var i = e.which;
                            [13, 32].includes(i) && (13 !== i ? (e.preventDefault(), e.stopPropagation(), $e.toggleMenu.call(t, e)) : $e.focusFirstMenuItem.call(t, null, !0))
                        }, null, !1), this.bind(i.settings.menu, "keydown", function (e) {
                            27 === e.which && $e.toggleMenu.call(t, e)
                        }), this.bind(i.inputs.seek, "mousedown mousemove", function (e) {
                            var t = i.progress.getBoundingClientRect(), n = 100 / t.width * (e.pageX - t.left);
                            e.currentTarget.setAttribute("seek-value", n)
                        }), this.bind(i.inputs.seek, "mousedown mouseup keydown keyup touchstart touchend", function (e) {
                            var i = e.currentTarget, n = e.keyCode ? e.keyCode : e.which;
                            if (!K(e) || 39 === n || 37 === n) {
                                t.lastSeekTime = Date.now();
                                var r = i.hasAttribute("play-on-seeked"),
                                    s = ["mouseup", "touchend", "keyup"].includes(e.type);
                                r && s ? (i.removeAttribute("play-on-seeked"), Pe(t.play())) : !s && t.playing && (i.setAttribute("play-on-seeked", ""), t.pause())
                            }
                        }), ie.isIos) {
                            var r = ve.call(t, 'input[type="range"]');
                            Array.from(r).forEach(function (t) {
                                return e.bind(t, n, function (e) {
                                    return te(e.target)
                                })
                            })
                        }
                        this.bind(i.inputs.seek, n, function (e) {
                            var i = e.currentTarget, n = i.getAttribute("seek-value");
                            Z(n) && (n = i.value), i.removeAttribute("seek-value"), t.currentTime = n / i.max * t.duration
                        }, "seek"), this.bind(i.progress, "mouseenter mouseleave mousemove", function (e) {
                            return $e.updateSeekTooltip.call(t, e)
                        }), this.bind(i.progress, "mousemove touchmove", function (e) {
                            var i = t.previewThumbnails;
                            i && i.loaded && i.startMove(e)
                        }), this.bind(i.progress, "mouseleave touchend click", function () {
                            var e = t.previewThumbnails;
                            e && e.loaded && e.endMove(!1, !0)
                        }), this.bind(i.progress, "mousedown touchstart", function (e) {
                            var i = t.previewThumbnails;
                            i && i.loaded && i.startScrubbing(e)
                        }), this.bind(i.progress, "mouseup touchend", function (e) {
                            var i = t.previewThumbnails;
                            i && i.loaded && i.endScrubbing(e)
                        }), ie.isWebkit && Array.from(ve.call(t, 'input[type="range"]')).forEach(function (i) {
                            e.bind(i, "input", function (e) {
                                return $e.updateRangeFill.call(t, e.target)
                            })
                        }), t.config.toggleInvert && !G(i.display.duration) && this.bind(i.display.currentTime, "click", function () {
                            0 !== t.currentTime && (t.config.invertTime = !t.config.invertTime, $e.timeUpdate.call(t))
                        }), this.bind(i.inputs.volume, n, function (e) {
                            t.volume = e.target.value
                        }, "volume"), this.bind(i.controls, "mouseenter mouseleave", function (e) {
                            i.controls.hover = !t.touch && "mouseenter" === e.type
                        }), i.fullscreen && Array.from(i.fullscreen.children).filter(function (e) {
                            return !e.contains(i.container)
                        }).forEach(function (n) {
                            e.bind(n, "mouseenter mouseleave", function (e) {
                                i.controls.hover = !t.touch && "mouseenter" === e.type
                            })
                        }), this.bind(i.controls, "mousedown mouseup touchstart touchend touchcancel", function (e) {
                            i.controls.pressed = ["mousedown", "touchstart"].includes(e.type)
                        }), this.bind(i.controls, "focusin", function () {
                            var n = t.config, r = t.timers;
                            fe(i.controls, n.classNames.noTransition, !0), ot.toggleControls.call(t, !0), setTimeout(function () {
                                fe(i.controls, n.classNames.noTransition, !1)
                            }, 0);
                            var s = e.touch ? 3e3 : 4e3;
                            clearTimeout(r.controls), r.controls = setTimeout(function () {
                                return ot.toggleControls.call(t, !1)
                            }, s)
                        }), this.bind(i.inputs.volume, "wheel", function (e) {
                            var i = e.webkitDirectionInvertedFromDevice, n = a([e.deltaX, -e.deltaY].map(function (e) {
                                return i ? -e : e
                            }), 2), r = n[0], s = n[1], o = Math.sign(Math.abs(r) > Math.abs(s) ? r : s);
                            t.increaseVolume(o / 50);
                            var l = t.media.volume;
                            (1 === o && l < 1 || -1 === o && l > 0) && e.preventDefault()
                        }, "volume", !1)
                    }
                }]), t
            }();
            "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : void 0 !== t || "undefined" != typeof self && self;
            var lt = function (e, t) {
                return function (e, t) {
                    e.exports = function () {
                        var e = function () {
                        }, t = {}, i = {}, n = {};

                        function r(e, t) {
                            if (e) {
                                var r = n[e];
                                if (i[e] = t, r) for (; r.length;) r[0](e, t), r.splice(0, 1)
                            }
                        }

                        function s(t, i) {
                            t.call && (t = {success: t}), i.length ? (t.error || e)(i) : (t.success || e)(t)
                        }

                        function o(t, i, n, r) {
                            var s, a, l = document, u = n.async, c = (n.numRetries || 0) + 1, h = n.before || e,
                                d = t.replace(/[\?|#].*$/, ""), p = t.replace(/^(css|img)!/, "");
                            r = r || 0, /(^css!|\.css$)/.test(d) ? ((a = l.createElement("link")).rel = "stylesheet", a.href = p, (s = "hideFocus" in a) && a.relList && (s = 0, a.rel = "preload", a.as = "style")) : /(^img!|\.(png|gif|jpg|svg|webp)$)/.test(d) ? (a = l.createElement("img")).src = p : ((a = l.createElement("script")).src = t, a.async = void 0 === u || u), a.onload = a.onerror = a.onbeforeload = function (e) {
                                var l = e.type[0];
                                if (s) try {
                                    a.sheet.cssText.length || (l = "e")
                                } catch (e) {
                                    18 != e.code && (l = "e")
                                }
                                if ("e" == l) {
                                    if ((r += 1) < c) return o(t, i, n, r)
                                } else if ("preload" == a.rel && "style" == a.as) return a.rel = "stylesheet";
                                i(t, l, e.defaultPrevented)
                            }, !1 !== h(t, a) && l.head.appendChild(a)
                        }

                        function a(e, i, n) {
                            var a, l;
                            if (i && i.trim && (a = i), l = (a ? n : i) || {}, a) {
                                if (a in t) throw"LoadJS";
                                t[a] = !0
                            }

                            function u(t, i) {
                                !function (e, t, i) {
                                    var n, r, s = (e = e.push ? e : [e]).length, a = s, l = [];
                                    for (n = function (e, i, n) {
                                        if ("e" == i && l.push(e), "b" == i) {
                                            if (!n) return;
                                            l.push(e)
                                        }
                                        --s || t(l)
                                    }, r = 0; r < a; r++) o(e[r], n, i)
                                }(e, function (e) {
                                    s(l, e), t && s({success: t, error: i}, e), r(a, e)
                                }, l)
                            }

                            if (l.returnPromise) return new Promise(u);
                            u()
                        }

                        return a.ready = function (e, t) {
                            return function (e, t) {
                                e = e.push ? e : [e];
                                var r, s, o, a = [], l = e.length, u = l;
                                for (r = function (e, i) {
                                    i.length && a.push(e), --u || t(a)
                                }; l--;) s = e[l], (o = i[s]) ? r(s, o) : (n[s] = n[s] || []).push(r)
                            }(e, function (e) {
                                s(t, e)
                            }), a
                        }, a.done = function (e) {
                            r(e, [])
                        }, a.reset = function () {
                            t = {}, i = {}, n = {}
                        }, a.isDefined = function (e) {
                            return e in t
                        }, a
                    }()
                }(t = {exports: {}}), t.exports
            }();

            function ut(e) {
                return new Promise(function (t, i) {
                    lt(e, {success: t, error: i})
                })
            }

            function ct(e) {
                e && !this.embed.hasPlayed && (this.embed.hasPlayed = !0), this.media.paused === e && (this.media.paused = !e, Se.call(this, this.media, e ? "play" : "pause"))
            }

            var ht = {
                setup: function () {
                    var e = this;
                    fe(e.elements.wrapper, e.config.classNames.embed, !0), e.options.speed = e.config.speed.options, Le.call(e), H(window.Vimeo) ? ht.ready.call(e) : ut(e.config.urls.vimeo.sdk).then(function () {
                        ht.ready.call(e)
                    }).catch(function (t) {
                        e.debug.warn("Vimeo SDK (player.js) failed to load", t)
                    })
                }, ready: function () {
                    var e = this, t = this, i = t.config.vimeo, n = i.premium, r = i.referrerPolicy,
                        s = function (e, t) {
                            if (null == e) return {};
                            var i, n, r = function (e, t) {
                                if (null == e) return {};
                                var i, n, r = {}, s = Object.keys(e);
                                for (n = 0; n < s.length; n++) i = s[n], t.indexOf(i) >= 0 || (r[i] = e[i]);
                                return r
                            }(e, t);
                            if (Object.getOwnPropertySymbols) {
                                var s = Object.getOwnPropertySymbols(e);
                                for (n = 0; n < s.length; n++) i = s[n], t.indexOf(i) >= 0 || Object.prototype.propertyIsEnumerable.call(e, i) && (r[i] = e[i])
                            }
                            return r
                        }(i, ["premium", "referrerPolicy"]);
                    n && Object.assign(s, {controls: !1, sidedock: !1});
                    var l = Je(o({
                        loop: t.config.loop.active,
                        autoplay: t.autoplay,
                        muted: t.muted,
                        gesture: "media",
                        playsinline: !this.config.fullscreen.iosNative
                    }, s)), u = t.media.getAttribute("src");
                    Z(u) && (u = t.media.getAttribute(t.config.attributes.embed.id));
                    var c,
                        h = Z(c = u) ? null : V(Number(c)) ? c : c.match(/^.*(vimeo.com\/|video\/)(\d+).*/) ? RegExp.$2 : c,
                        d = ae("iframe"), p = Be(t.config.urls.vimeo.iframe, h, l);
                    d.setAttribute("src", p), d.setAttribute("allowfullscreen", ""), d.setAttribute("allow", "autoplay,fullscreen,picture-in-picture"), Z(r) || d.setAttribute("referrerPolicy", r);
                    var f = t.poster;
                    if (n) d.setAttribute("data-poster", f), t.media = he(d, t.media); else {
                        var m = ae("div", {class: t.config.classNames.embedContainer, "data-poster": f});
                        m.appendChild(d), t.media = he(m, t.media)
                    }
                    qe(Be(t.config.urls.vimeo.api, h), "json").then(function (e) {
                        if (!Z(e)) {
                            var i = new URL(e[0].thumbnail_large);
                            i.pathname = "".concat(i.pathname.split("_")[0], ".jpg"), ot.setPoster.call(t, i.href).catch(function () {
                            })
                        }
                    }), t.embed = new window.Vimeo.Player(d, {
                        autopause: t.config.autopause,
                        muted: t.muted
                    }), t.media.paused = !0, t.media.currentTime = 0, t.supported.ui && t.embed.disableTextTrack(), t.media.play = function () {
                        return ct.call(t, !0), t.embed.play()
                    }, t.media.pause = function () {
                        return ct.call(t, !1), t.embed.pause()
                    }, t.media.stop = function () {
                        t.pause(), t.currentTime = 0
                    };
                    var g = t.media.currentTime;
                    Object.defineProperty(t.media, "currentTime", {
                        get: function () {
                            return g
                        }, set: function (e) {
                            var i = t.embed, n = t.media, r = t.paused, s = t.volume, o = r && !i.hasPlayed;
                            n.seeking = !0, Se.call(t, n, "seeking"), Promise.resolve(o && i.setVolume(0)).then(function () {
                                return i.setCurrentTime(e)
                            }).then(function () {
                                return o && i.pause()
                            }).then(function () {
                                return o && i.setVolume(s)
                            }).catch(function () {
                            })
                        }
                    });
                    var v = t.config.speed.selected;
                    Object.defineProperty(t.media, "playbackRate", {
                        get: function () {
                            return v
                        }, set: function (e) {
                            t.embed.setPlaybackRate(e).then(function () {
                                v = e, Se.call(t, t.media, "ratechange")
                            }).catch(function () {
                                t.options.speed = [1]
                            })
                        }
                    });
                    var y = t.config.volume;
                    Object.defineProperty(t.media, "volume", {
                        get: function () {
                            return y
                        }, set: function (e) {
                            t.embed.setVolume(e).then(function () {
                                y = e, Se.call(t, t.media, "volumechange")
                            })
                        }
                    });
                    var b = t.config.muted;
                    Object.defineProperty(t.media, "muted", {
                        get: function () {
                            return b
                        }, set: function (e) {
                            var i = !!W(e) && e;
                            t.embed.setVolume(i ? 0 : t.config.volume).then(function () {
                                b = i, Se.call(t, t.media, "volumechange")
                            })
                        }
                    });
                    var w, D = t.config.loop;
                    Object.defineProperty(t.media, "loop", {
                        get: function () {
                            return D
                        }, set: function (e) {
                            var i = W(e) ? e : t.config.loop.active;
                            t.embed.setLoop(i).then(function () {
                                D = i
                            })
                        }
                    }), t.embed.getVideoUrl().then(function (e) {
                        w = e, $e.setDownloadUrl.call(t)
                    }).catch(function (t) {
                        e.debug.warn(t)
                    }), Object.defineProperty(t.media, "currentSrc", {
                        get: function () {
                            return w
                        }
                    }), Object.defineProperty(t.media, "ended", {
                        get: function () {
                            return t.currentTime === t.duration
                        }
                    }), Promise.all([t.embed.getVideoWidth(), t.embed.getVideoHeight()]).then(function (i) {
                        var n = a(i, 2), r = n[0], s = n[1];
                        t.embed.ratio = [r, s], Le.call(e)
                    }), t.embed.setAutopause(t.config.autopause).then(function (e) {
                        t.config.autopause = e
                    }), t.embed.getVideoTitle().then(function (i) {
                        t.config.title = i, ot.setTitle.call(e)
                    }), t.embed.getCurrentTime().then(function (e) {
                        g = e, Se.call(t, t.media, "timeupdate")
                    }), t.embed.getDuration().then(function (e) {
                        t.media.duration = e, Se.call(t, t.media, "durationchange")
                    }), t.embed.getTextTracks().then(function (e) {
                        t.media.textTracks = e, Qe.setup.call(t)
                    }), t.embed.on("cuechange", function (e) {
                        var i = e.cues, n = (void 0 === i ? [] : i).map(function (e) {
                            return function (e) {
                                var t = document.createDocumentFragment(), i = document.createElement("div");
                                return t.appendChild(i), i.innerHTML = e, t.firstChild.innerText
                            }(e.text)
                        });
                        Qe.updateCues.call(t, n)
                    }), t.embed.on("loaded", function () {
                        t.embed.getPaused().then(function (e) {
                            ct.call(t, !e), e || Se.call(t, t.media, "playing")
                        }), G(t.embed.element) && t.supported.ui && t.embed.element.setAttribute("tabindex", -1)
                    }), t.embed.on("bufferstart", function () {
                        Se.call(t, t.media, "waiting")
                    }), t.embed.on("bufferend", function () {
                        Se.call(t, t.media, "playing")
                    }), t.embed.on("play", function () {
                        ct.call(t, !0), Se.call(t, t.media, "playing")
                    }), t.embed.on("pause", function () {
                        ct.call(t, !1)
                    }), t.embed.on("timeupdate", function (e) {
                        t.media.seeking = !1, g = e.seconds, Se.call(t, t.media, "timeupdate")
                    }), t.embed.on("progress", function (e) {
                        t.media.buffered = e.percent, Se.call(t, t.media, "progress"), 1 === parseInt(e.percent, 10) && Se.call(t, t.media, "canplaythrough"), t.embed.getDuration().then(function (e) {
                            e !== t.media.duration && (t.media.duration = e, Se.call(t, t.media, "durationchange"))
                        })
                    }), t.embed.on("seeked", function () {
                        t.media.seeking = !1, Se.call(t, t.media, "seeked")
                    }), t.embed.on("ended", function () {
                        t.media.paused = !0, Se.call(t, t.media, "ended")
                    }), t.embed.on("error", function (e) {
                        t.media.error = e, Se.call(t, t.media, "error")
                    }), setTimeout(function () {
                        return ot.build.call(t)
                    }, 0)
                }
            };

            function dt(e) {
                e && !this.embed.hasPlayed && (this.embed.hasPlayed = !0), this.media.paused === e && (this.media.paused = !e, Se.call(this, this.media, e ? "play" : "pause"))
            }

            function pt(e) {
                return e.noCookie ? "https://www.youtube-nocookie.com" : "http:" === window.location.protocol ? "http://www.youtube.com" : void 0
            }

            var ft, mt = {
                setup: function () {
                    var e = this;
                    if (fe(this.elements.wrapper, this.config.classNames.embed, !0), H(window.YT) && X(window.YT.Player)) mt.ready.call(this); else {
                        var t = window.onYouTubeIframeAPIReady;
                        window.onYouTubeIframeAPIReady = function () {
                            X(t) && t(), mt.ready.call(e)
                        }, ut(this.config.urls.youtube.sdk).catch(function (t) {
                            e.debug.warn("YouTube API failed to load", t)
                        })
                    }
                }, getTitle: function (e) {
                    var t = this;
                    qe(Be(this.config.urls.youtube.api, e)).then(function (e) {
                        if (H(e)) {
                            var i = e.title, n = e.height, r = e.width;
                            t.config.title = i, ot.setTitle.call(t), t.embed.ratio = [r, n]
                        }
                        Le.call(t)
                    }).catch(function () {
                        Le.call(t)
                    })
                }, ready: function () {
                    var e = this, t = e.media && e.media.getAttribute("id");
                    if (Z(t) || !t.startsWith("youtube-")) {
                        var i = e.media.getAttribute("src");
                        Z(i) && (i = e.media.getAttribute(this.config.attributes.embed.id));
                        var n, r,
                            s = Z(n = i) ? null : n.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/) ? RegExp.$2 : n,
                            o = (r = e.provider, "".concat(r, "-").concat(Math.floor(1e4 * Math.random()))),
                            a = ae("div", {id: o, "data-poster": e.poster});
                        e.media = he(a, e.media);
                        var l = function (e) {
                            return "https://i.ytimg.com/vi/".concat(s, "/").concat(e, "default.jpg")
                        };
                        st(l("maxres"), 121).catch(function () {
                            return st(l("sd"), 121)
                        }).catch(function () {
                            return st(l("hq"))
                        }).then(function (t) {
                            return ot.setPoster.call(e, t.src)
                        }).then(function (t) {
                            t.includes("maxres") || (e.elements.poster.style.backgroundSize = "cover")
                        }).catch(function () {
                        });
                        var u = e.config.youtube;
                        e.embed = new window.YT.Player(o, {
                            videoId: s,
                            host: pt(u),
                            playerVars: re({}, {
                                autoplay: e.config.autoplay ? 1 : 0,
                                hl: e.config.hl,
                                controls: e.supported.ui ? 0 : 1,
                                disablekb: 1,
                                playsinline: e.config.fullscreen.iosNative ? 0 : 1,
                                cc_load_policy: e.captions.active ? 1 : 0,
                                cc_lang_pref: e.config.captions.language,
                                widget_referrer: window ? window.location.href : null
                            }, u),
                            events: {
                                onError: function (t) {
                                    if (!e.media.error) {
                                        var i = t.data, n = {
                                            2: "The request contains an invalid parameter value. For example, this error occurs if you specify a video ID that does not have 11 characters, or if the video ID contains invalid characters, such as exclamation points or asterisks.",
                                            5: "The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.",
                                            100: "The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private.",
                                            101: "The owner of the requested video does not allow it to be played in embedded players.",
                                            150: "The owner of the requested video does not allow it to be played in embedded players."
                                        }[i] || "An unknown error occured";
                                        e.media.error = {code: i, message: n}, Se.call(e, e.media, "error")
                                    }
                                }, onPlaybackRateChange: function (t) {
                                    var i = t.target;
                                    e.media.playbackRate = i.getPlaybackRate(), Se.call(e, e.media, "ratechange")
                                }, onReady: function (t) {
                                    if (!X(e.media.play)) {
                                        var i = t.target;
                                        mt.getTitle.call(e, s), e.media.play = function () {
                                            dt.call(e, !0), i.playVideo()
                                        }, e.media.pause = function () {
                                            dt.call(e, !1), i.pauseVideo()
                                        }, e.media.stop = function () {
                                            i.stopVideo()
                                        }, e.media.duration = i.getDuration(), e.media.paused = !0, e.media.currentTime = 0, Object.defineProperty(e.media, "currentTime", {
                                            get: function () {
                                                return Number(i.getCurrentTime())
                                            }, set: function (t) {
                                                e.paused && !e.embed.hasPlayed && e.embed.mute(), e.media.seeking = !0, Se.call(e, e.media, "seeking"), i.seekTo(t)
                                            }
                                        }), Object.defineProperty(e.media, "playbackRate", {
                                            get: function () {
                                                return i.getPlaybackRate()
                                            }, set: function (e) {
                                                i.setPlaybackRate(e)
                                            }
                                        });
                                        var n = e.config.volume;
                                        Object.defineProperty(e.media, "volume", {
                                            get: function () {
                                                return n
                                            }, set: function (t) {
                                                n = t, i.setVolume(100 * n), Se.call(e, e.media, "volumechange")
                                            }
                                        });
                                        var r = e.config.muted;
                                        Object.defineProperty(e.media, "muted", {
                                            get: function () {
                                                return r
                                            }, set: function (t) {
                                                var n = W(t) ? t : r;
                                                r = n, i[n ? "mute" : "unMute"](), Se.call(e, e.media, "volumechange")
                                            }
                                        }), Object.defineProperty(e.media, "currentSrc", {
                                            get: function () {
                                                return i.getVideoUrl()
                                            }
                                        }), Object.defineProperty(e.media, "ended", {
                                            get: function () {
                                                return e.currentTime === e.duration
                                            }
                                        });
                                        var o = i.getAvailablePlaybackRates();
                                        e.options.speed = o.filter(function (t) {
                                            return e.config.speed.options.includes(t)
                                        }), e.supported.ui && e.media.setAttribute("tabindex", -1), Se.call(e, e.media, "timeupdate"), Se.call(e, e.media, "durationchange"), clearInterval(e.timers.buffering), e.timers.buffering = setInterval(function () {
                                            e.media.buffered = i.getVideoLoadedFraction(), (null === e.media.lastBuffered || e.media.lastBuffered < e.media.buffered) && Se.call(e, e.media, "progress"), e.media.lastBuffered = e.media.buffered, 1 === e.media.buffered && (clearInterval(e.timers.buffering), Se.call(e, e.media, "canplaythrough"))
                                        }, 200), setTimeout(function () {
                                            return ot.build.call(e)
                                        }, 50)
                                    }
                                }, onStateChange: function (t) {
                                    var i = t.target;
                                    switch (clearInterval(e.timers.playing), e.media.seeking && [1, 2].includes(t.data) && (e.media.seeking = !1, Se.call(e, e.media, "seeked")), t.data) {
                                        case-1:
                                            Se.call(e, e.media, "timeupdate"), e.media.buffered = i.getVideoLoadedFraction(), Se.call(e, e.media, "progress");
                                            break;
                                        case 0:
                                            dt.call(e, !1), e.media.loop ? (i.stopVideo(), i.playVideo()) : Se.call(e, e.media, "ended");
                                            break;
                                        case 1:
                                            e.config.autoplay || !e.media.paused || e.embed.hasPlayed ? (dt.call(e, !0), Se.call(e, e.media, "playing"), e.timers.playing = setInterval(function () {
                                                Se.call(e, e.media, "timeupdate")
                                            }, 50), e.media.duration !== i.getDuration() && (e.media.duration = i.getDuration(), Se.call(e, e.media, "durationchange"))) : e.media.pause();
                                            break;
                                        case 2:
                                            e.muted || e.embed.unMute(), dt.call(e, !1);
                                            break;
                                        case 3:
                                            Se.call(e, e.media, "waiting")
                                    }
                                    Se.call(e, e.elements.container, "statechange", !1, {code: t.data})
                                }
                            }
                        })
                    }
                }
            }, gt = {
                setup: function () {
                    this.media ? (fe(this.elements.container, this.config.classNames.type.replace("{0}", this.type), !0), fe(this.elements.container, this.config.classNames.provider.replace("{0}", this.provider), !0), this.isEmbed && fe(this.elements.container, this.config.classNames.type.replace("{0}", "video"), !0), this.isVideo && (this.elements.wrapper = ae("div", {class: this.config.classNames.video}), se(this.media, this.elements.wrapper), this.elements.poster = ae("div", {class: this.config.classNames.poster}), this.elements.wrapper.appendChild(this.elements.poster)), this.isHTML5 ? Fe.setup.call(this) : this.isYouTube ? mt.setup.call(this) : this.isVimeo && ht.setup.call(this)) : this.debug.warn("No media element found!")
                }
            }, vt = function () {
                function t(i) {
                    var n = this;
                    e(this, t), this.player = i, this.config = i.config.ads, this.playing = !1, this.initialized = !1, this.elements = {
                        container: null,
                        displayContainer: null
                    }, this.manager = null, this.loader = null, this.cuePoints = null, this.events = {}, this.safetyTimer = null, this.countdownTimer = null, this.managerPromise = new Promise(function (e, t) {
                        n.on("loaded", e), n.on("error", t)
                    }), this.load()
                }

                return n(t, [{
                    key: "load", value: function () {
                        var e = this;
                        this.enabled && (H(window.google) && H(window.google.ima) ? this.ready() : ut(this.player.config.urls.googleIMA.sdk).then(function () {
                            e.ready()
                        }).catch(function () {
                            e.trigger("error", new Error("Google IMA SDK failed to load"))
                        }))
                    }
                }, {
                    key: "ready", value: function () {
                        var e, t = this;
                        this.enabled || ((e = this).manager && e.manager.destroy(), e.elements.displayContainer && e.elements.displayContainer.destroy(), e.elements.container.remove()), this.startSafetyTimer(12e3, "ready()"), this.managerPromise.then(function () {
                            t.clearSafetyTimer("onAdsManagerLoaded()")
                        }), this.listeners(), this.setupIMA()
                    }
                }, {
                    key: "setupIMA", value: function () {
                        var e = this;
                        this.elements.container = ae("div", {class: this.player.config.classNames.ads}), this.player.elements.container.appendChild(this.elements.container), google.ima.settings.setVpaidMode(google.ima.ImaSdkSettings.VpaidMode.ENABLED), google.ima.settings.setLocale(this.player.config.ads.language), google.ima.settings.setDisableCustomPlaybackForIOS10Plus(this.player.config.playsinline), this.elements.displayContainer = new google.ima.AdDisplayContainer(this.elements.container, this.player.media), this.loader = new google.ima.AdsLoader(this.elements.displayContainer), this.loader.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, function (t) {
                            return e.onAdsManagerLoaded(t)
                        }, !1), this.loader.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, function (t) {
                            return e.onAdError(t)
                        }, !1), this.requestAds()
                    }
                }, {
                    key: "requestAds", value: function () {
                        var e = this.player.elements.container;
                        try {
                            var t = new google.ima.AdsRequest;
                            t.adTagUrl = this.tagUrl, t.linearAdSlotWidth = e.offsetWidth, t.linearAdSlotHeight = e.offsetHeight, t.nonLinearAdSlotWidth = e.offsetWidth, t.nonLinearAdSlotHeight = e.offsetHeight, t.forceNonLinearFullSlot = !1, t.setAdWillPlayMuted(!this.player.muted), this.loader.requestAds(t)
                        } catch (e) {
                            this.onAdError(e)
                        }
                    }
                }, {
                    key: "pollCountdown", value: function () {
                        var e = this;
                        if (!(arguments.length > 0 && void 0 !== arguments[0] && arguments[0])) return clearInterval(this.countdownTimer), void this.elements.container.removeAttribute("data-badge-text");
                        this.countdownTimer = setInterval(function () {
                            var t = Ge(Math.max(e.manager.getRemainingTime(), 0)),
                                i = "".concat(He("advertisement", e.player.config), " - ").concat(t);
                            e.elements.container.setAttribute("data-badge-text", i)
                        }, 100)
                    }
                }, {
                    key: "onAdsManagerLoaded", value: function (e) {
                        var t = this;
                        if (this.enabled) {
                            var i = new google.ima.AdsRenderingSettings;
                            i.restoreCustomPlaybackStateOnAdBreakComplete = !0, i.enablePreloading = !0, this.manager = e.getAdsManager(this.player, i), this.cuePoints = this.manager.getCuePoints(), this.manager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, function (e) {
                                return t.onAdError(e)
                            }), Object.keys(google.ima.AdEvent.Type).forEach(function (e) {
                                t.manager.addEventListener(google.ima.AdEvent.Type[e], function (e) {
                                    return t.onAdEvent(e)
                                })
                            }), this.trigger("loaded")
                        }
                    }
                }, {
                    key: "addCuePoints", value: function () {
                        var e = this;
                        Z(this.cuePoints) || this.cuePoints.forEach(function (t) {
                            if (0 !== t && -1 !== t && t < e.player.duration) {
                                var i = e.player.elements.progress;
                                if (G(i)) {
                                    var n = 100 / e.player.duration * t,
                                        r = ae("span", {class: e.player.config.classNames.cues});
                                    r.style.left = "".concat(n.toString(), "%"), i.appendChild(r)
                                }
                            }
                        })
                    }
                }, {
                    key: "onAdEvent", value: function (e) {
                        var t = this, i = this.player.elements.container, n = e.getAd(), r = e.getAdData();
                        switch (function (e) {
                            Se.call(t.player, t.player.media, "ads".concat(e.replace(/_/g, "").toLowerCase()))
                        }(e.type), e.type) {
                            case google.ima.AdEvent.Type.LOADED:
                                this.trigger("loaded"), this.pollCountdown(!0), n.isLinear() || (n.width = i.offsetWidth, n.height = i.offsetHeight);
                                break;
                            case google.ima.AdEvent.Type.STARTED:
                                this.manager.setVolume(this.player.volume);
                                break;
                            case google.ima.AdEvent.Type.ALL_ADS_COMPLETED:
                                this.player.ended ? this.loadAds() : this.loader.contentComplete();
                                break;
                            case google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED:
                                this.pauseContent();
                                break;
                            case google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED:
                                this.pollCountdown(), this.resumeContent();
                                break;
                            case google.ima.AdEvent.Type.LOG:
                                r.adError && this.player.debug.warn("Non-fatal ad error: ".concat(r.adError.getMessage()))
                        }
                    }
                }, {
                    key: "onAdError", value: function (e) {
                        this.cancel(), this.player.debug.warn("Ads error", e)
                    }
                }, {
                    key: "listeners", value: function () {
                        var e, t = this, i = this.player.elements.container;
                        this.player.on("canplay", function () {
                            t.addCuePoints()
                        }), this.player.on("ended", function () {
                            t.loader.contentComplete()
                        }), this.player.on("timeupdate", function () {
                            e = t.player.currentTime
                        }), this.player.on("seeked", function () {
                            var i = t.player.currentTime;
                            Z(t.cuePoints) || t.cuePoints.forEach(function (n, r) {
                                e < n && n < i && (t.manager.discardAdBreak(), t.cuePoints.splice(r, 1))
                            })
                        }), window.addEventListener("resize", function () {
                            t.manager && t.manager.resize(i.offsetWidth, i.offsetHeight, google.ima.ViewMode.NORMAL)
                        })
                    }
                }, {
                    key: "play", value: function () {
                        var e = this, t = this.player.elements.container;
                        this.managerPromise || this.resumeContent(), this.managerPromise.then(function () {
                            e.manager.setVolume(e.player.volume), e.elements.displayContainer.initialize();
                            try {
                                e.initialized || (e.manager.init(t.offsetWidth, t.offsetHeight, google.ima.ViewMode.NORMAL), e.manager.start()), e.initialized = !0
                            } catch (t) {
                                e.onAdError(t)
                            }
                        }).catch(function () {
                        })
                    }
                }, {
                    key: "resumeContent", value: function () {
                        this.elements.container.style.zIndex = "", this.playing = !1, Pe(this.player.media.play())
                    }
                }, {
                    key: "pauseContent", value: function () {
                        this.elements.container.style.zIndex = 3, this.playing = !0, this.player.media.pause()
                    }
                }, {
                    key: "cancel", value: function () {
                        this.initialized && this.resumeContent(), this.trigger("error"), this.loadAds()
                    }
                }, {
                    key: "loadAds", value: function () {
                        var e = this;
                        this.managerPromise.then(function () {
                            e.manager && e.manager.destroy(), e.managerPromise = new Promise(function (t) {
                                e.on("loaded", t), e.player.debug.log(e.manager)
                            }), e.initialized = !1, e.requestAds()
                        }).catch(function () {
                        })
                    }
                }, {
                    key: "trigger", value: function (e) {
                        for (var t = this, i = arguments.length, n = new Array(i > 1 ? i - 1 : 0), r = 1; r < i; r++) n[r - 1] = arguments[r];
                        var s = this.events[e];
                        Y(s) && s.forEach(function (e) {
                            X(e) && e.apply(t, n)
                        })
                    }
                }, {
                    key: "on", value: function (e, t) {
                        return Y(this.events[e]) || (this.events[e] = []), this.events[e].push(t), this
                    }
                }, {
                    key: "startSafetyTimer", value: function (e, t) {
                        var i = this;
                        this.player.debug.log("Safety timer invoked from: ".concat(t)), this.safetyTimer = setTimeout(function () {
                            i.cancel(), i.clearSafetyTimer("startSafetyTimer()")
                        }, e)
                    }
                }, {
                    key: "clearSafetyTimer", value: function (e) {
                        z(this.safetyTimer) || (this.player.debug.log("Safety timer cleared from: ".concat(e)), clearTimeout(this.safetyTimer), this.safetyTimer = null)
                    }
                }, {
                    key: "enabled", get: function () {
                        var e = this.config;
                        return this.player.isHTML5 && this.player.isVideo && e.enabled && (!Z(e.publisherId) || Q(e.tagUrl))
                    }
                }, {
                    key: "tagUrl", get: function () {
                        var e = this.config;
                        if (Q(e.tagUrl)) return e.tagUrl;
                        var t = {
                            AV_PUBLISHERID: "58c25bb0073ef448b1087ad6",
                            AV_CHANNELID: "5a0458dc28a06145e4519d21",
                            AV_URL: window.location.hostname,
                            cb: Date.now(),
                            AV_WIDTH: 640,
                            AV_HEIGHT: 480,
                            AV_CDIM2: e.publisherId
                        };
                        return "".concat("https://go.aniview.com/api/adserver6/vast/", "?").concat(Je(t))
                    }
                }]), t
            }(), yt = function (e, t) {
                var i = {};
                return e > t.width / t.height ? (i.width = t.width, i.height = 1 / e * t.width) : (i.height = t.height, i.width = e * t.height), i
            }, bt = function () {
                function t(i) {
                    e(this, t), this.player = i, this.thumbnails = [], this.loaded = !1, this.lastMouseMoveTime = Date.now(), this.mouseDown = !1, this.loadedImages = [], this.elements = {
                        thumb: {},
                        scrubbing: {}
                    }, this.load()
                }

                return n(t, [{
                    key: "load", value: function () {
                        var e = this;
                        this.player.elements.display.seekTooltip && (this.player.elements.display.seekTooltip.hidden = this.enabled), this.enabled && this.getThumbnails().then(function () {
                            e.enabled && (e.render(), e.determineContainerAutoSizing(), e.loaded = !0)
                        })
                    }
                }, {
                    key: "getThumbnails", value: function () {
                        var e = this;
                        return new Promise(function (t) {
                            var i = e.player.config.previewThumbnails.src;
                            if (Z(i)) throw new Error("Missing previewThumbnails.src config attribute");
                            var n = function () {
                                e.thumbnails.sort(function (e, t) {
                                    return e.height - t.height
                                }), e.player.debug.log("Preview thumbnails", e.thumbnails), t()
                            };
                            if (X(i)) i(function (t) {
                                e.thumbnails = t, n()
                            }); else {
                                var r = (q(i) ? [i] : i).map(function (t) {
                                    return e.getThumbnail(t)
                                });
                                Promise.all(r).then(n)
                            }
                        })
                    }
                }, {
                    key: "getThumbnail", value: function (e) {
                        var t = this;
                        return new Promise(function (i) {
                            qe(e).then(function (n) {
                                var r, s, o = {
                                    frames: (r = n, s = [], r.split(/\r\n\r\n|\n\n|\r\r/).forEach(function (e) {
                                        var t = {};
                                        e.split(/\r\n|\n|\r/).forEach(function (e) {
                                            if (V(t.startTime)) {
                                                if (!Z(e.trim()) && Z(t.text)) {
                                                    var i = e.trim().split("#xywh="), n = a(i, 1);
                                                    if (t.text = n[0], i[1]) {
                                                        var r = a(i[1].split(","), 4);
                                                        t.x = r[0], t.y = r[1], t.w = r[2], t.h = r[3]
                                                    }
                                                }
                                            } else {
                                                var s = e.match(/([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})( ?--> ?)([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})/);
                                                s && (t.startTime = 60 * Number(s[1] || 0) * 60 + 60 * Number(s[2]) + Number(s[3]) + Number("0.".concat(s[4])), t.endTime = 60 * Number(s[6] || 0) * 60 + 60 * Number(s[7]) + Number(s[8]) + Number("0.".concat(s[9])))
                                            }
                                        }), t.text && s.push(t)
                                    }), s), height: null, urlPrefix: ""
                                };
                                o.frames[0].text.startsWith("/") || o.frames[0].text.startsWith("http://") || o.frames[0].text.startsWith("https://") || (o.urlPrefix = e.substring(0, e.lastIndexOf("/") + 1));
                                var l = new Image;
                                l.onload = function () {
                                    o.height = l.naturalHeight, o.width = l.naturalWidth, t.thumbnails.push(o), i()
                                }, l.src = o.urlPrefix + o.frames[0].text
                            })
                        })
                    }
                }, {
                    key: "startMove", value: function (e) {
                        if (this.loaded && $(e) && ["touchmove", "mousemove"].includes(e.type) && this.player.media.duration) {
                            if ("touchmove" === e.type) this.seekTime = this.player.media.duration * (this.player.elements.inputs.seek.value / 100); else {
                                var t = this.player.elements.progress.getBoundingClientRect(),
                                    i = 100 / t.width * (e.pageX - t.left);
                                this.seekTime = this.player.media.duration * (i / 100), this.seekTime < 0 && (this.seekTime = 0), this.seekTime > this.player.media.duration - 1 && (this.seekTime = this.player.media.duration - 1), this.mousePosX = e.pageX, this.elements.thumb.time.innerText = Ge(this.seekTime)
                            }
                            this.showImageAtCurrentTime()
                        }
                    }
                }, {
                    key: "endMove", value: function () {
                        this.toggleThumbContainer(!1, !0)
                    }
                }, {
                    key: "startScrubbing", value: function (e) {
                        (z(e.button) || !1 === e.button || 0 === e.button) && (this.mouseDown = !0, this.player.media.duration && (this.toggleScrubbingContainer(!0), this.toggleThumbContainer(!1, !0), this.showImageAtCurrentTime()))
                    }
                }, {
                    key: "endScrubbing", value: function () {
                        var e = this;
                        this.mouseDown = !1, Math.ceil(this.lastTime) === Math.ceil(this.player.media.currentTime) ? this.toggleScrubbingContainer(!1) : xe.call(this.player, this.player.media, "timeupdate", function () {
                            e.mouseDown || e.toggleScrubbingContainer(!1)
                        })
                    }
                }, {
                    key: "listeners", value: function () {
                        var e = this;
                        this.player.on("play", function () {
                            e.toggleThumbContainer(!1, !0)
                        }), this.player.on("seeked", function () {
                            e.toggleThumbContainer(!1)
                        }), this.player.on("timeupdate", function () {
                            e.lastTime = e.player.media.currentTime
                        })
                    }
                }, {
                    key: "render", value: function () {
                        this.elements.thumb.container = ae("div", {class: this.player.config.classNames.previewThumbnails.thumbContainer}), this.elements.thumb.imageContainer = ae("div", {class: this.player.config.classNames.previewThumbnails.imageContainer}), this.elements.thumb.container.appendChild(this.elements.thumb.imageContainer);
                        var e = ae("div", {class: this.player.config.classNames.previewThumbnails.timeContainer});
                        this.elements.thumb.time = ae("span", {}, "00:00"), e.appendChild(this.elements.thumb.time), this.elements.thumb.container.appendChild(e), G(this.player.elements.progress) && this.player.elements.progress.appendChild(this.elements.thumb.container), this.elements.scrubbing.container = ae("div", {class: this.player.config.classNames.previewThumbnails.scrubbingContainer}), this.player.elements.wrapper.appendChild(this.elements.scrubbing.container)
                    }
                }, {
                    key: "destroy", value: function () {
                        this.elements.thumb.container && this.elements.thumb.container.remove(), this.elements.scrubbing.container && this.elements.scrubbing.container.remove()
                    }
                }, {
                    key: "showImageAtCurrentTime", value: function () {
                        var e = this;
                        this.mouseDown ? this.setScrubbingContainerSize() : this.setThumbContainerSizeAndPos();
                        var t = this.thumbnails[0].frames.findIndex(function (t) {
                            return e.seekTime >= t.startTime && e.seekTime <= t.endTime
                        }), i = t >= 0, n = 0;
                        this.mouseDown || this.toggleThumbContainer(i), i && (this.thumbnails.forEach(function (i, r) {
                            e.loadedImages.includes(i.frames[t].text) && (n = r)
                        }), t !== this.showingThumb && (this.showingThumb = t, this.loadImage(n)))
                    }
                }, {
                    key: "loadImage", value: function () {
                        var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                            i = this.showingThumb, n = this.thumbnails[t], r = n.urlPrefix, s = n.frames[i],
                            o = n.frames[i].text, a = r + o;
                        if (this.currentImageElement && this.currentImageElement.dataset.filename === o) this.showImage(this.currentImageElement, s, t, i, o, !1), this.currentImageElement.dataset.index = i, this.removeOldImages(this.currentImageElement); else {
                            this.loadingImage && this.usingSprites && (this.loadingImage.onload = null);
                            var l = new Image;
                            l.src = a, l.dataset.index = i, l.dataset.filename = o, this.showingThumbFilename = o, this.player.debug.log("Loading image: ".concat(a)), l.onload = function () {
                                return e.showImage(l, s, t, i, o, !0)
                            }, this.loadingImage = l, this.removeOldImages(l)
                        }
                    }
                }, {
                    key: "showImage", value: function (e, t, i, n, r) {
                        var s = !(arguments.length > 5 && void 0 !== arguments[5]) || arguments[5];
                        this.player.debug.log("Showing thumb: ".concat(r, ". num: ").concat(n, ". qual: ").concat(i, ". newimg: ").concat(s)), this.setImageSizeAndOffset(e, t), s && (this.currentImageContainer.appendChild(e), this.currentImageElement = e, this.loadedImages.includes(r) || this.loadedImages.push(r)), this.preloadNearby(n, !0).then(this.preloadNearby(n, !1)).then(this.getHigherQuality(i, e, t, r))
                    }
                }, {
                    key: "removeOldImages", value: function (e) {
                        var t = this;
                        Array.from(this.currentImageContainer.children).forEach(function (i) {
                            if ("img" === i.tagName.toLowerCase()) {
                                var n = t.usingSprites ? 500 : 1e3;
                                if (i.dataset.index !== e.dataset.index && !i.dataset.deleting) {
                                    i.dataset.deleting = !0;
                                    var r = t.currentImageContainer;
                                    setTimeout(function () {
                                        r.removeChild(i), t.player.debug.log("Removing thumb: ".concat(i.dataset.filename))
                                    }, n)
                                }
                            }
                        })
                    }
                }, {
                    key: "preloadNearby", value: function (e) {
                        var t = this, i = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                        return new Promise(function (n) {
                            setTimeout(function () {
                                var r = t.thumbnails[0].frames[e].text;
                                if (t.showingThumbFilename === r) {
                                    var s;
                                    s = i ? t.thumbnails[0].frames.slice(e) : t.thumbnails[0].frames.slice(0, e).reverse();
                                    var o = !1;
                                    s.forEach(function (e) {
                                        var i = e.text;
                                        if (i !== r && !t.loadedImages.includes(i)) {
                                            o = !0, t.player.debug.log("Preloading thumb filename: ".concat(i));
                                            var s = t.thumbnails[0].urlPrefix + i, a = new Image;
                                            a.src = s, a.onload = function () {
                                                t.player.debug.log("Preloaded thumb filename: ".concat(i)), t.loadedImages.includes(i) || t.loadedImages.push(i), n()
                                            }
                                        }
                                    }), o || n()
                                }
                            }, 300)
                        })
                    }
                }, {
                    key: "getHigherQuality", value: function (e, t, i, n) {
                        var r = this;
                        if (e < this.thumbnails.length - 1) {
                            var s = t.naturalHeight;
                            this.usingSprites && (s = i.h), s < this.thumbContainerHeight && setTimeout(function () {
                                r.showingThumbFilename === n && (r.player.debug.log("Showing higher quality thumb for: ".concat(n)), r.loadImage(e + 1))
                            }, 300)
                        }
                    }
                }, {
                    key: "toggleThumbContainer", value: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                            t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                            i = this.player.config.classNames.previewThumbnails.thumbContainerShown;
                        this.elements.thumb.container.classList.toggle(i, e), !e && t && (this.showingThumb = null, this.showingThumbFilename = null)
                    }
                }, {
                    key: "toggleScrubbingContainer", value: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                            t = this.player.config.classNames.previewThumbnails.scrubbingContainerShown;
                        this.elements.scrubbing.container.classList.toggle(t, e), e || (this.showingThumb = null, this.showingThumbFilename = null)
                    }
                }, {
                    key: "determineContainerAutoSizing", value: function () {
                        (this.elements.thumb.imageContainer.clientHeight > 20 || this.elements.thumb.imageContainer.clientWidth > 20) && (this.sizeSpecifiedInCSS = !0)
                    }
                }, {
                    key: "setThumbContainerSizeAndPos", value: function () {
                        if (this.sizeSpecifiedInCSS) {
                            if (this.elements.thumb.imageContainer.clientHeight > 20 && this.elements.thumb.imageContainer.clientWidth < 20) {
                                var e = Math.floor(this.elements.thumb.imageContainer.clientHeight * this.thumbAspectRatio);
                                this.elements.thumb.imageContainer.style.width = "".concat(e, "px")
                            } else if (this.elements.thumb.imageContainer.clientHeight < 20 && this.elements.thumb.imageContainer.clientWidth > 20) {
                                var t = Math.floor(this.elements.thumb.imageContainer.clientWidth / this.thumbAspectRatio);
                                this.elements.thumb.imageContainer.style.height = "".concat(t, "px")
                            }
                        } else {
                            var i = Math.floor(this.thumbContainerHeight * this.thumbAspectRatio);
                            this.elements.thumb.imageContainer.style.height = "".concat(this.thumbContainerHeight, "px"), this.elements.thumb.imageContainer.style.width = "".concat(i, "px")
                        }
                        this.setThumbContainerPos()
                    }
                }, {
                    key: "setThumbContainerPos", value: function () {
                        var e = this.player.elements.progress.getBoundingClientRect(),
                            t = this.player.elements.container.getBoundingClientRect(),
                            i = this.elements.thumb.container, n = t.left - e.left + 10,
                            r = t.right - e.left - i.clientWidth - 10, s = this.mousePosX - e.left - i.clientWidth / 2;
                        s < n && (s = n), s > r && (s = r), i.style.left = "".concat(s, "px")
                    }
                }, {
                    key: "setScrubbingContainerSize", value: function () {
                        var e = yt(this.thumbAspectRatio, {
                            width: this.player.media.clientWidth,
                            height: this.player.media.clientHeight
                        }), t = e.width, i = e.height;
                        this.elements.scrubbing.container.style.width = "".concat(t, "px"), this.elements.scrubbing.container.style.height = "".concat(i, "px")
                    }
                }, {
                    key: "setImageSizeAndOffset", value: function (e, t) {
                        if (this.usingSprites) {
                            var i = this.thumbContainerHeight / t.h;
                            e.style.height = "".concat(e.naturalHeight * i, "px"), e.style.width = "".concat(e.naturalWidth * i, "px"), e.style.left = "-".concat(t.x * i, "px"), e.style.top = "-".concat(t.y * i, "px")
                        }
                    }
                }, {
                    key: "enabled", get: function () {
                        return this.player.isHTML5 && this.player.isVideo && this.player.config.previewThumbnails.enabled
                    }
                }, {
                    key: "currentImageContainer", get: function () {
                        return this.mouseDown ? this.elements.scrubbing.container : this.elements.thumb.imageContainer
                    }
                }, {
                    key: "usingSprites", get: function () {
                        return Object.keys(this.thumbnails[0].frames[0]).includes("w")
                    }
                }, {
                    key: "thumbAspectRatio", get: function () {
                        return this.usingSprites ? this.thumbnails[0].frames[0].w / this.thumbnails[0].frames[0].h : this.thumbnails[0].width / this.thumbnails[0].height
                    }
                }, {
                    key: "thumbContainerHeight", get: function () {
                        return this.mouseDown ? yt(this.thumbAspectRatio, {
                            width: this.player.media.clientWidth,
                            height: this.player.media.clientHeight
                        }).height : this.sizeSpecifiedInCSS ? this.elements.thumb.imageContainer.clientHeight : Math.floor(this.player.media.clientWidth / this.thumbAspectRatio / 4)
                    }
                }, {
                    key: "currentImageElement", get: function () {
                        return this.mouseDown ? this.currentScrubbingImageElement : this.currentThumbnailImageElement
                    }, set: function (e) {
                        this.mouseDown ? this.currentScrubbingImageElement = e : this.currentThumbnailImageElement = e
                    }
                }]), t
            }(), wt = {
                insertElements: function (e, t) {
                    var i = this;
                    q(t) ? le(e, this.media, {src: t}) : Y(t) && t.forEach(function (t) {
                        le(e, i.media, t)
                    })
                }, change: function (e) {
                    var t = this;
                    ne(e, "sources.length") ? (Fe.cancelRequests.call(this), this.destroy.call(this, function () {
                        t.options.quality = [], ue(t.media), t.media = null, G(t.elements.container) && t.elements.container.removeAttribute("class");
                        var i = e.sources, n = e.type, r = a(i, 1)[0], s = r.provider, o = void 0 === s ? tt.html5 : s,
                            l = r.src, u = "html5" === o ? n : "div", c = "html5" === o ? {} : {src: l};
                        Object.assign(t, {
                            provider: o,
                            type: n,
                            supported: Te.check(n, o, t.config.playsinline),
                            media: ae(u, c)
                        }), t.elements.container.appendChild(t.media), W(e.autoplay) && (t.config.autoplay = e.autoplay), t.isHTML5 && (t.config.crossorigin && t.media.setAttribute("crossorigin", ""), t.config.autoplay && t.media.setAttribute("autoplay", ""), Z(e.poster) || (t.poster = e.poster), t.config.loop.active && t.media.setAttribute("loop", ""), t.config.muted && t.media.setAttribute("muted", ""), t.config.playsinline && t.media.setAttribute("playsinline", "")), ot.addStyleHook.call(t), t.isHTML5 && wt.insertElements.call(t, "source", i), t.config.title = e.title, gt.setup.call(t), t.isHTML5 && Object.keys(e).includes("tracks") && wt.insertElements.call(t, "track", e.tracks), (t.isHTML5 || t.isEmbed && !t.supported.ui) && ot.build.call(t), t.isHTML5 && t.media.load(), Z(e.previewThumbnails) || (Object.assign(t.config.previewThumbnails, e.previewThumbnails), t.previewThumbnails && t.previewThumbnails.loaded && (t.previewThumbnails.destroy(), t.previewThumbnails = null), t.config.previewThumbnails.enabled && (t.previewThumbnails = new bt(t))), t.fullscreen.update()
                    }, !0)) : this.debug.warn("Invalid source format")
                }
            }, Dt = function () {
                function t(i, n) {
                    var r = this;
                    if (e(this, t), this.timers = {}, this.ready = !1, this.loading = !1, this.failed = !1, this.touch = Te.touch, this.media = i, q(this.media) && (this.media = document.querySelectorAll(this.media)), (window.jQuery && this.media instanceof jQuery || U(this.media) || Y(this.media)) && (this.media = this.media[0]), this.config = re({}, Ze, t.defaults, n || {}, function () {
                        try {
                            return JSON.parse(r.media.getAttribute("data-plyr-config"))
                        } catch (e) {
                            return {}
                        }
                    }()), this.elements = {
                        container: null,
                        fullscreen: null,
                        captions: null,
                        buttons: {},
                        display: {},
                        progress: {},
                        inputs: {},
                        settings: {popup: null, menu: null, panels: {}, buttons: {}}
                    }, this.captions = {
                        active: null,
                        currentTrack: -1,
                        meta: new WeakMap
                    }, this.fullscreen = {active: !1}, this.options = {
                        speed: [],
                        quality: []
                    }, this.debug = new nt(this.config.debug), this.debug.log("Config", this.config), this.debug.log("Support", Te), !z(this.media) && G(this.media)) if (this.media.plyr) this.debug.warn("Target already setup"); else if (this.config.enabled) if (Te.check().api) {
                        var s = this.media.cloneNode(!0);
                        s.autoplay = !1, this.elements.original = s;
                        var o = this.media.tagName.toLowerCase(), a = null, l = null;
                        switch (o) {
                            case"div":
                                if (a = this.media.querySelector("iframe"), G(a)) {
                                    if (l = Ke(a.getAttribute("src")), this.provider = function (e) {
                                        return /^(https?:\/\/)?(www\.)?(youtube\.com|youtube-nocookie\.com|youtu\.?be)\/.+$/.test(e) ? tt.youtube : /^https?:\/\/player.vimeo.com\/video\/\d{0,9}(?=\b|\/)/.test(e) ? tt.vimeo : null
                                    }(l.toString()), this.elements.container = this.media, this.media = a, this.elements.container.className = "", l.search.length) {
                                        var u = ["1", "true"];
                                        u.includes(l.searchParams.get("autoplay")) && (this.config.autoplay = !0), u.includes(l.searchParams.get("loop")) && (this.config.loop.active = !0), this.isYouTube ? (this.config.playsinline = u.includes(l.searchParams.get("playsinline")), this.config.youtube.hl = l.searchParams.get("hl")) : this.config.playsinline = !0
                                    }
                                } else this.provider = this.media.getAttribute(this.config.attributes.embed.provider), this.media.removeAttribute(this.config.attributes.embed.provider);
                                if (Z(this.provider) || !Object.keys(tt).includes(this.provider)) return void this.debug.error("Setup failed: Invalid provider");
                                this.type = "video";
                                break;
                            case"video":
                            case"audio":
                                this.type = o, this.provider = tt.html5, this.media.hasAttribute("crossorigin") && (this.config.crossorigin = !0), this.media.hasAttribute("autoplay") && (this.config.autoplay = !0), (this.media.hasAttribute("playsinline") || this.media.hasAttribute("webkit-playsinline")) && (this.config.playsinline = !0), this.media.hasAttribute("muted") && (this.config.muted = !0), this.media.hasAttribute("loop") && (this.config.loop.active = !0);
                                break;
                            default:
                                return void this.debug.error("Setup failed: unsupported type")
                        }
                        this.supported = Te.check(this.type, this.provider, this.config.playsinline), this.supported.api ? (this.eventListeners = [], this.listeners = new at(this), this.storage = new Ve(this), this.media.plyr = this, G(this.elements.container) || (this.elements.container = ae("div", {tabindex: 0}), se(this.media, this.elements.container)), ot.migrateStyles.call(this), ot.addStyleHook.call(this), gt.setup.call(this), this.config.debug && Ee.call(this, this.elements.container, this.config.events.join(" "), function (e) {
                            r.debug.log("event: ".concat(e.type))
                        }), this.fullscreen = new rt(this), (this.isHTML5 || this.isEmbed && !this.supported.ui) && ot.build.call(this), this.listeners.container(), this.listeners.global(), this.config.ads.enabled && (this.ads = new vt(this)), this.isHTML5 && this.config.autoplay && setTimeout(function () {
                            return Pe(r.play())
                        }, 10), this.lastSeekTime = 0, this.config.previewThumbnails.enabled && (this.previewThumbnails = new bt(this))) : this.debug.error("Setup failed: no support")
                    } else this.debug.error("Setup failed: no support"); else this.debug.error("Setup failed: disabled by config"); else this.debug.error("Setup failed: no suitable element passed")
                }

                return n(t, [{
                    key: "play", value: function () {
                        var e = this;
                        return X(this.media.play) ? (this.ads && this.ads.enabled && this.ads.managerPromise.then(function () {
                            return e.ads.play()
                        }).catch(function () {
                            return Pe(e.media.play())
                        }), this.media.play()) : null
                    }
                }, {
                    key: "pause", value: function () {
                        return this.playing && X(this.media.pause) ? this.media.pause() : null
                    }
                }, {
                    key: "togglePlay", value: function (e) {
                        return (W(e) ? e : !this.playing) ? this.play() : this.pause()
                    }
                }, {
                    key: "stop", value: function () {
                        this.isHTML5 ? (this.pause(), this.restart()) : X(this.media.stop) && this.media.stop()
                    }
                }, {
                    key: "restart", value: function () {
                        this.currentTime = 0
                    }
                }, {
                    key: "rewind", value: function (e) {
                        this.currentTime -= V(e) ? e : this.config.seekTime
                    }
                }, {
                    key: "forward", value: function (e) {
                        this.currentTime += V(e) ? e : this.config.seekTime
                    }
                }, {
                    key: "increaseVolume", value: function (e) {
                        var t = this.media.muted ? 0 : this.volume;
                        this.volume = t + (V(e) ? e : 0)
                    }
                }, {
                    key: "decreaseVolume", value: function (e) {
                        this.increaseVolume(-e)
                    }
                }, {
                    key: "toggleCaptions", value: function (e) {
                        Qe.toggle.call(this, e, !1)
                    }
                }, {
                    key: "airplay", value: function () {
                        Te.airplay && this.media.webkitShowPlaybackTargetPicker()
                    }
                }, {
                    key: "toggleControls", value: function (e) {
                        if (this.supported.ui && !this.isAudio) {
                            var t = me(this.elements.container, this.config.classNames.hideControls),
                                i = void 0 === e ? void 0 : !e,
                                n = fe(this.elements.container, this.config.classNames.hideControls, i);
                            if (n && Y(this.config.controls) && this.config.controls.includes("settings") && !Z(this.config.settings) && $e.toggleMenu.call(this, !1), n !== t) {
                                var r = n ? "controlshidden" : "controlsshown";
                                Se.call(this, this.media, r)
                            }
                            return !n
                        }
                        return !1
                    }
                }, {
                    key: "on", value: function (e, t) {
                        Ee.call(this, this.elements.container, e, t)
                    }
                }, {
                    key: "once", value: function (e, t) {
                        xe.call(this, this.elements.container, e, t)
                    }
                }, {
                    key: "off", value: function (e, t) {
                        _e(this.elements.container, e, t)
                    }
                }, {
                    key: "destroy", value: function (e) {
                        var t = this, i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                        if (this.ready) {
                            var n = function () {
                                document.body.style.overflow = "", t.embed = null, i ? (Object.keys(t.elements).length && (ue(t.elements.buttons.play), ue(t.elements.captions), ue(t.elements.controls), ue(t.elements.wrapper), t.elements.buttons.play = null, t.elements.captions = null, t.elements.controls = null, t.elements.wrapper = null), X(e) && e()) : (function () {
                                    this && this.eventListeners && (this.eventListeners.forEach(function (e) {
                                        var t = e.element, i = e.type, n = e.callback, r = e.options;
                                        t.removeEventListener(i, n, r)
                                    }), this.eventListeners = [])
                                }.call(t), he(t.elements.original, t.elements.container), Se.call(t, t.elements.original, "destroyed", !0), X(e) && e.call(t.elements.original), t.ready = !1, setTimeout(function () {
                                    t.elements = null, t.media = null
                                }, 200))
                            };
                            this.stop(), clearTimeout(this.timers.loading), clearTimeout(this.timers.controls), clearTimeout(this.timers.resized), this.isHTML5 ? (ot.toggleNativeControls.call(this, !0), n()) : this.isYouTube ? (clearInterval(this.timers.buffering), clearInterval(this.timers.playing), null !== this.embed && X(this.embed.destroy) && this.embed.destroy(), n()) : this.isVimeo && (null !== this.embed && this.embed.unload().then(n), setTimeout(n, 200))
                        }
                    }
                }, {
                    key: "supports", value: function (e) {
                        return Te.mime.call(this, e)
                    }
                }, {
                    key: "isHTML5", get: function () {
                        return this.provider === tt.html5
                    }
                }, {
                    key: "isEmbed", get: function () {
                        return this.isYouTube || this.isVimeo
                    }
                }, {
                    key: "isYouTube", get: function () {
                        return this.provider === tt.youtube
                    }
                }, {
                    key: "isVimeo", get: function () {
                        return this.provider === tt.vimeo
                    }
                }, {
                    key: "isVideo", get: function () {
                        return "video" === this.type
                    }
                }, {
                    key: "isAudio", get: function () {
                        return "audio" === this.type
                    }
                }, {
                    key: "playing", get: function () {
                        return Boolean(this.ready && !this.paused && !this.ended)
                    }
                }, {
                    key: "paused", get: function () {
                        return Boolean(this.media.paused)
                    }
                }, {
                    key: "stopped", get: function () {
                        return Boolean(this.paused && 0 === this.currentTime)
                    }
                }, {
                    key: "ended", get: function () {
                        return Boolean(this.media.ended)
                    }
                }, {
                    key: "currentTime", set: function (e) {
                        if (this.duration) {
                            var t = V(e) && e > 0;
                            this.media.currentTime = t ? Math.min(e, this.duration) : 0, this.debug.log("Seeking to ".concat(this.currentTime, " seconds"))
                        }
                    }, get: function () {
                        return Number(this.media.currentTime)
                    }
                }, {
                    key: "buffered", get: function () {
                        var e = this.media.buffered;
                        return V(e) ? e : e && e.length && this.duration > 0 ? e.end(0) / this.duration : 0
                    }
                }, {
                    key: "seeking", get: function () {
                        return Boolean(this.media.seeking)
                    }
                }, {
                    key: "duration", get: function () {
                        var e = parseFloat(this.config.duration), t = (this.media || {}).duration,
                            i = V(t) && t !== 1 / 0 ? t : 0;
                        return e || i
                    }
                }, {
                    key: "volume", set: function (e) {
                        var t = e;
                        q(t) && (t = Number(t)), V(t) || (t = this.storage.get("volume")), V(t) || (t = this.config.volume), t > 1 && (t = 1), t < 0 && (t = 0), this.config.volume = t, this.media.volume = t, !Z(e) && this.muted && t > 0 && (this.muted = !1)
                    }, get: function () {
                        return Number(this.media.volume)
                    }
                }, {
                    key: "muted", set: function (e) {
                        var t = e;
                        W(t) || (t = this.storage.get("muted")), W(t) || (t = this.config.muted), this.config.muted = t, this.media.muted = t
                    }, get: function () {
                        return Boolean(this.media.muted)
                    }
                }, {
                    key: "hasAudio", get: function () {
                        return !this.isHTML5 || !!this.isAudio || Boolean(this.media.mozHasAudio) || Boolean(this.media.webkitAudioDecodedByteCount) || Boolean(this.media.audioTracks && this.media.audioTracks.length)
                    }
                }, {
                    key: "speed", set: function (e) {
                        var t = this, i = null;
                        V(e) && (i = e), V(i) || (i = this.storage.get("speed")), V(i) || (i = this.config.speed.selected);
                        var n = this.minimumSpeed, r = this.maximumSpeed;
                        i = function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                                t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
                                i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 255;
                            return Math.min(Math.max(e, t), i)
                        }(i, n, r), this.config.speed.selected = i, setTimeout(function () {
                            t.media.playbackRate = i
                        }, 0)
                    }, get: function () {
                        return Number(this.media.playbackRate)
                    }
                }, {
                    key: "minimumSpeed", get: function () {
                        return this.isYouTube ? Math.min.apply(Math, l(this.options.speed)) : this.isVimeo ? .5 : .0625
                    }
                }, {
                    key: "maximumSpeed", get: function () {
                        return this.isYouTube ? Math.max.apply(Math, l(this.options.speed)) : this.isVimeo ? 2 : 16
                    }
                }, {
                    key: "quality", set: function (e) {
                        var t = this.config.quality, i = this.options.quality;
                        if (i.length) {
                            var n = [!Z(e) && Number(e), this.storage.get("quality"), t.selected, t.default].find(V),
                                r = !0;
                            if (!i.includes(n)) {
                                var s = function (e, t) {
                                    return Y(e) && e.length ? e.reduce(function (e, i) {
                                        return Math.abs(i - t) < Math.abs(e - t) ? i : e
                                    }) : null
                                }(i, n);
                                this.debug.warn("Unsupported quality option: ".concat(n, ", using ").concat(s, " instead")), n = s, r = !1
                            }
                            t.selected = n, this.media.quality = n, r && this.storage.set({quality: n})
                        }
                    }, get: function () {
                        return this.media.quality
                    }
                }, {
                    key: "loop", set: function (e) {
                        var t = W(e) ? e : this.config.loop.active;
                        this.config.loop.active = t, this.media.loop = t
                    }, get: function () {
                        return Boolean(this.media.loop)
                    }
                }, {
                    key: "source", set: function (e) {
                        wt.change.call(this, e)
                    }, get: function () {
                        return this.media.currentSrc
                    }
                }, {
                    key: "download", get: function () {
                        var e = this.config.urls.download;
                        return Q(e) ? e : this.source
                    }, set: function (e) {
                        Q(e) && (this.config.urls.download = e, $e.setDownloadUrl.call(this))
                    }
                }, {
                    key: "poster", set: function (e) {
                        this.isVideo ? ot.setPoster.call(this, e, !1).catch(function () {
                        }) : this.debug.warn("Poster can only be set for video")
                    }, get: function () {
                        return this.isVideo ? this.media.getAttribute("poster") || this.media.getAttribute("data-poster") : null
                    }
                }, {
                    key: "ratio", get: function () {
                        if (!this.isVideo) return null;
                        var e = Ae(Me.call(this));
                        return Y(e) ? e.join(":") : e
                    }, set: function (e) {
                        this.isVideo ? q(e) && Oe(e) ? (this.config.ratio = e, Le.call(this)) : this.debug.error("Invalid aspect ratio specified (".concat(e, ")")) : this.debug.warn("Aspect ratio can only be set for video")
                    }
                }, {
                    key: "autoplay", set: function (e) {
                        var t = W(e) ? e : this.config.autoplay;
                        this.config.autoplay = t
                    }, get: function () {
                        return Boolean(this.config.autoplay)
                    }
                }, {
                    key: "currentTrack", set: function (e) {
                        Qe.set.call(this, e, !1)
                    }, get: function () {
                        var e = this.captions, t = e.toggled, i = e.currentTrack;
                        return t ? i : -1
                    }
                }, {
                    key: "language", set: function (e) {
                        Qe.setLanguage.call(this, e, !1)
                    }, get: function () {
                        return (Qe.getCurrentTrack.call(this) || {}).language
                    }
                }, {
                    key: "pip", set: function (e) {
                        if (Te.pip) {
                            var t = W(e) ? e : !this.pip;
                            X(this.media.webkitSetPresentationMode) && this.media.webkitSetPresentationMode(t ? et : "inline"), X(this.media.requestPictureInPicture) && (!this.pip && t ? this.media.requestPictureInPicture() : this.pip && !t && document.exitPictureInPicture())
                        }
                    }, get: function () {
                        return Te.pip ? Z(this.media.webkitPresentationMode) ? this.media === document.pictureInPictureElement : this.media.webkitPresentationMode === et : null
                    }
                }], [{
                    key: "supported", value: function (e, t, i) {
                        return Te.check(e, t, i)
                    }
                }, {
                    key: "loadSprite", value: function (e, t) {
                        return We(e, t)
                    }
                }, {
                    key: "setup", value: function (e) {
                        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, n = null;
                        return q(e) ? n = Array.from(document.querySelectorAll(e)) : U(e) ? n = Array.from(e) : Y(e) && (n = e.filter(G)), Z(n) ? null : n.map(function (e) {
                            return new t(e, i)
                        })
                    }
                }]), t
            }();
            return Dt.defaults = (ft = Ze, JSON.parse(JSON.stringify(ft))), Dt
        }())
    }).call(this, i(5))
}, function (e, t) {
    var i;
    i = function () {
        return this
    }();
    try {
        i = i || new Function("return this")()
    } catch (e) {
        "object" == typeof window && (i = window)
    }
    e.exports = i
}, function (e, t, i) {
    "use strict";
    (function (e) {
        function i(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function n(e, t) {
            for (var i = 0; i < t.length; i++) {
                var n = t[i];
                n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
            }
        }

        function r(e, t, i) {
            return t && n(e.prototype, t), i && n(e, i), e
        }

        function s(e, t, i) {
            return t in e ? Object.defineProperty(e, t, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = i, e
        }

        function o(e, t) {
            var i = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var n = Object.getOwnPropertySymbols(e);
                t && (n = n.filter(function (t) {
                    return Object.getOwnPropertyDescriptor(e, t).enumerable
                })), i.push.apply(i, n)
            }
            return i
        }

        function a(e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = null != arguments[t] ? arguments[t] : {};
                t % 2 ? o(Object(i), !0).forEach(function (t) {
                    s(e, t, i[t])
                }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : o(Object(i)).forEach(function (t) {
                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(i, t))
                })
            }
            return e
        }

        function l(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && function (e, t) {
                (Object.setPrototypeOf || function (e, t) {
                    return e.__proto__ = t, e
                })(e, t)
            }(e, t)
        }

        function u(e) {
            return (u = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
                return e.__proto__ || Object.getPrototypeOf(e)
            })(e)
        }

        function c(e) {
            if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return e
        }

        function h(e, t) {
            return !t || "object" != typeof t && "function" != typeof t ? c(e) : t
        }

        function d(e, t, i) {
            return (d = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
                var n = function (e, t) {
                    for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = u(e));) ;
                    return e
                }(e, t);
                if (n) {
                    var r = Object.getOwnPropertyDescriptor(n, t);
                    return r.get ? r.get.call(i) : r.value
                }
            })(e, t, i || e)
        }

        var p = {
                el: document,
                elMobile: document,
                name: "scroll",
                offset: [0, 0],
                repeat: !1,
                smooth: !1,
                smoothMobile: !1,
                direction: "vertical",
                lerp: .1,
                class: "is-inview",
                scrollbarClass: "c-scrollbar",
                scrollingClass: "has-scroll-scrolling",
                draggingClass: "has-scroll-dragging",
                smoothClass: "has-scroll-smooth",
                initClass: "has-scroll-init",
                getSpeed: !1,
                getDirection: !1,
                multiplier: 1,
                firefoxMultiplier: 50,
                touchMultiplier: 2,
                scrollFromAnywhere: !1
            }, f = function () {
                function e() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    i(this, e), Object.assign(this, p, t), this.namespace = "locomotive", this.html = document.documentElement, this.windowHeight = window.innerHeight, this.windowMiddle = this.windowHeight / 2, this.els = [], this.listeners = {}, this.hasScrollTicking = !1, this.hasCallEventSet = !1, this.checkScroll = this.checkScroll.bind(this), this.checkResize = this.checkResize.bind(this), this.checkEvent = this.checkEvent.bind(this), this.instance = {
                        scroll: {
                            x: 0,
                            y: 0
                        }, limit: this.html.offsetHeight
                    }, this.getDirection && (this.instance.direction = null), this.getDirection && (this.instance.speed = 0), this.html.classList.add(this.initClass), window.addEventListener("resize", this.checkResize, !1)
                }

                return r(e, [{
                    key: "init", value: function () {
                        this.initEvents()
                    }
                }, {
                    key: "checkScroll", value: function () {
                        this.dispatchScroll()
                    }
                }, {
                    key: "checkResize", value: function () {
                        var e = this;
                        this.resizeTick || (this.resizeTick = !0, requestAnimationFrame(function () {
                            e.resize(), e.resizeTick = !1
                        }))
                    }
                }, {
                    key: "resize", value: function () {
                    }
                }, {
                    key: "initEvents", value: function () {
                        var e = this;
                        this.scrollToEls = this.el.querySelectorAll("[data-".concat(this.name, "-to]")), this.setScrollTo = this.setScrollTo.bind(this), this.scrollToEls.forEach(function (t) {
                            t.addEventListener("click", e.setScrollTo, !1)
                        })
                    }
                }, {
                    key: "setScrollTo", value: function (e) {
                        e.preventDefault(), this.scrollTo(e.currentTarget.getAttribute("data-".concat(this.name, "-href")) || e.currentTarget.getAttribute("href"), e.currentTarget.getAttribute("data-".concat(this.name, "-offset")))
                    }
                }, {
                    key: "addElements", value: function () {
                    }
                }, {
                    key: "detectElements", value: function (e) {
                        var t = this, i = this.instance.scroll.y, n = i + this.windowHeight;
                        this.els.forEach(function (r, s) {
                            !r || r.inView && !e || n >= r.top && i < r.bottom && t.setInView(r, s), r && r.inView && (n < r.top || i > r.bottom) && t.setOutOfView(r, s)
                        }), this.els = this.els.filter(function (e, t) {
                            return null !== e
                        }), this.hasScrollTicking = !1
                    }
                }, {
                    key: "setInView", value: function (e, t) {
                        this.els[t].inView = !0, e.el.classList.add(e.class), e.call && this.hasCallEventSet && (this.dispatchCall(e, "enter"), e.repeat || (this.els[t].call = !1)), e.repeat || e.speed || e.sticky || (!e.call || e.call && this.hasCallEventSet) && (this.els[t] = null)
                    }
                }, {
                    key: "setOutOfView", value: function (e, t) {
                        (e.repeat || void 0 !== e.speed) && (this.els[t].inView = !1), e.call && this.hasCallEventSet && this.dispatchCall(e, "exit"), e.repeat && e.el.classList.remove(e.class)
                    }
                }, {
                    key: "dispatchCall", value: function (e, t) {
                        this.callWay = t, this.callValue = e.call.split(",").map(function (e) {
                            return e.trim()
                        }), this.callObj = e, 1 == this.callValue.length && (this.callValue = this.callValue[0]);
                        var i = new Event(this.namespace + "call");
                        this.el.dispatchEvent(i)
                    }
                }, {
                    key: "dispatchScroll", value: function () {
                        var e = new Event(this.namespace + "scroll");
                        this.el.dispatchEvent(e)
                    }
                }, {
                    key: "setEvents", value: function (e, t) {
                        this.listeners[e] || (this.listeners[e] = []);
                        var i = this.listeners[e];
                        i.push(t), 1 === i.length && this.el.addEventListener(this.namespace + e, this.checkEvent, !1), "call" === e && (this.hasCallEventSet = !0, this.detectElements(!0))
                    }
                }, {
                    key: "unsetEvents", value: function (e, t) {
                        if (this.listeners[e]) {
                            var i = this.listeners[e], n = i.indexOf(t);
                            n < 0 || (i.splice(n, 1), 0 === i.index && this.el.removeEventListener(this.namespace + e, this.checkEvent, !1))
                        }
                    }
                }, {
                    key: "checkEvent", value: function (e) {
                        var t = this, i = e.type.replace(this.namespace, ""), n = this.listeners[i];
                        n && 0 !== n.length && n.forEach(function (e) {
                            switch (i) {
                                case"scroll":
                                    return e(t.instance);
                                case"call":
                                    return e(t.callValue, t.callWay, t.callObj);
                                default:
                                    return e()
                            }
                        })
                    }
                }, {
                    key: "startScroll", value: function () {
                    }
                }, {
                    key: "stopScroll", value: function () {
                    }
                }, {
                    key: "setScroll", value: function (e, t) {
                        this.instance.scroll = {x: 0, y: 0}
                    }
                }, {
                    key: "destroy", value: function () {
                        var e = this;
                        window.removeEventListener("resize", this.checkResize, !1), Object.keys(this.listeners).forEach(function (t) {
                            e.el.removeEventListener(e.namespace + t, e.checkEvent, !1)
                        }), this.listeners = {}, this.scrollToEls.forEach(function (t) {
                            t.removeEventListener("click", e.setScrollTo, !1)
                        })
                    }
                }]), e
            }(),
            m = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : void 0 !== e ? e : "undefined" != typeof self ? self : {};

        function g(e, t) {
            return e(t = {exports: {}}, t.exports), t.exports
        }

        var v = g(function (e, t) {
                e.exports = {
                    polyfill: function () {
                        var e = window, t = document;
                        if (!("scrollBehavior" in t.documentElement.style) || !0 === e.__forceSmoothScrollPolyfill__) {
                            var i, n = e.HTMLElement || e.Element, r = {
                                    scroll: e.scroll || e.scrollTo,
                                    scrollBy: e.scrollBy,
                                    elementScroll: n.prototype.scroll || a,
                                    scrollIntoView: n.prototype.scrollIntoView
                                }, s = e.performance && e.performance.now ? e.performance.now.bind(e.performance) : Date.now,
                                o = (i = e.navigator.userAgent, new RegExp(["MSIE ", "Trident/", "Edge/"].join("|")).test(i) ? 1 : 0);
                            e.scroll = e.scrollTo = function () {
                                void 0 !== arguments[0] && (!0 !== l(arguments[0]) ? d.call(e, t.body, void 0 !== arguments[0].left ? ~~arguments[0].left : e.scrollX || e.pageXOffset, void 0 !== arguments[0].top ? ~~arguments[0].top : e.scrollY || e.pageYOffset) : r.scroll.call(e, void 0 !== arguments[0].left ? arguments[0].left : "object" != typeof arguments[0] ? arguments[0] : e.scrollX || e.pageXOffset, void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : e.scrollY || e.pageYOffset))
                            }, e.scrollBy = function () {
                                void 0 !== arguments[0] && (l(arguments[0]) ? r.scrollBy.call(e, void 0 !== arguments[0].left ? arguments[0].left : "object" != typeof arguments[0] ? arguments[0] : 0, void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : 0) : d.call(e, t.body, ~~arguments[0].left + (e.scrollX || e.pageXOffset), ~~arguments[0].top + (e.scrollY || e.pageYOffset)))
                            }, n.prototype.scroll = n.prototype.scrollTo = function () {
                                if (void 0 !== arguments[0]) if (!0 !== l(arguments[0])) {
                                    var e = arguments[0].left, t = arguments[0].top;
                                    d.call(this, this, void 0 === e ? this.scrollLeft : ~~e, void 0 === t ? this.scrollTop : ~~t)
                                } else {
                                    if ("number" == typeof arguments[0] && void 0 === arguments[1]) throw new SyntaxError("Value could not be converted");
                                    r.elementScroll.call(this, void 0 !== arguments[0].left ? ~~arguments[0].left : "object" != typeof arguments[0] ? ~~arguments[0] : this.scrollLeft, void 0 !== arguments[0].top ? ~~arguments[0].top : void 0 !== arguments[1] ? ~~arguments[1] : this.scrollTop)
                                }
                            }, n.prototype.scrollBy = function () {
                                void 0 !== arguments[0] && (!0 !== l(arguments[0]) ? this.scroll({
                                    left: ~~arguments[0].left + this.scrollLeft,
                                    top: ~~arguments[0].top + this.scrollTop,
                                    behavior: arguments[0].behavior
                                }) : r.elementScroll.call(this, void 0 !== arguments[0].left ? ~~arguments[0].left + this.scrollLeft : ~~arguments[0] + this.scrollLeft, void 0 !== arguments[0].top ? ~~arguments[0].top + this.scrollTop : ~~arguments[1] + this.scrollTop))
                            }, n.prototype.scrollIntoView = function () {
                                if (!0 !== l(arguments[0])) {
                                    var i = function (e) {
                                        for (; e !== t.body && !1 === h(e);) e = e.parentNode || e.host;
                                        return e
                                    }(this), n = i.getBoundingClientRect(), s = this.getBoundingClientRect();
                                    i !== t.body ? (d.call(this, i, i.scrollLeft + s.left - n.left, i.scrollTop + s.top - n.top), "fixed" !== e.getComputedStyle(i).position && e.scrollBy({
                                        left: n.left,
                                        top: n.top,
                                        behavior: "smooth"
                                    })) : e.scrollBy({left: s.left, top: s.top, behavior: "smooth"})
                                } else r.scrollIntoView.call(this, void 0 === arguments[0] || arguments[0])
                            }
                        }

                        function a(e, t) {
                            this.scrollLeft = e, this.scrollTop = t
                        }

                        function l(e) {
                            if (null === e || "object" != typeof e || void 0 === e.behavior || "auto" === e.behavior || "instant" === e.behavior) return !0;
                            if ("object" == typeof e && "smooth" === e.behavior) return !1;
                            throw new TypeError("behavior member of ScrollOptions " + e.behavior + " is not a valid value for enumeration ScrollBehavior.")
                        }

                        function u(e, t) {
                            return "Y" === t ? e.clientHeight + o < e.scrollHeight : "X" === t ? e.clientWidth + o < e.scrollWidth : void 0
                        }

                        function c(t, i) {
                            var n = e.getComputedStyle(t, null)["overflow" + i];
                            return "auto" === n || "scroll" === n
                        }

                        function h(e) {
                            var t = u(e, "Y") && c(e, "Y"), i = u(e, "X") && c(e, "X");
                            return t || i
                        }

                        function d(i, n, o) {
                            var l, u, c, h, d = s();
                            i === t.body ? (l = e, u = e.scrollX || e.pageXOffset, c = e.scrollY || e.pageYOffset, h = r.scroll) : (l = i, u = i.scrollLeft, c = i.scrollTop, h = a), function t(i) {
                                var n, r, o, a, l = (s() - i.startTime) / 468;
                                a = l = l > 1 ? 1 : l, n = .5 * (1 - Math.cos(Math.PI * a)), r = i.startX + (i.x - i.startX) * n, o = i.startY + (i.y - i.startY) * n, i.method.call(i.scrollable, r, o), r === i.x && o === i.y || e.requestAnimationFrame(t.bind(e, i))
                            }({scrollable: l, method: h, startTime: d, startX: u, startY: c, x: n, y: o})
                        }
                    }
                }
            }), y = (v.polyfill, function (e) {
                function t() {
                    var e, n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    return i(this, t), e = h(this, u(t).call(this, n)), window.addEventListener("scroll", e.checkScroll, !1), v.polyfill(), e
                }

                return l(t, f), r(t, [{
                    key: "init", value: function () {
                        this.instance.scroll.y = window.pageYOffset, this.addElements(), this.detectElements(), d(u(t.prototype), "init", this).call(this)
                    }
                }, {
                    key: "checkScroll", value: function () {
                        var e = this;
                        d(u(t.prototype), "checkScroll", this).call(this), this.getDirection && this.addDirection(), this.getSpeed && (this.addSpeed(), this.timestamp = Date.now()), this.instance.scroll.y = window.pageYOffset, this.els.length && (this.hasScrollTicking || (requestAnimationFrame(function () {
                            e.detectElements()
                        }), this.hasScrollTicking = !0))
                    }
                }, {
                    key: "addDirection", value: function () {
                        window.pageYOffset > this.instance.scroll.y ? "down" !== this.instance.direction && (this.instance.direction = "down") : window.pageYOffset < this.instance.scroll.y && "up" !== this.instance.direction && (this.instance.direction = "up")
                    }
                }, {
                    key: "addSpeed", value: function () {
                        window.pageYOffset != this.instance.scroll.y ? this.instance.speed = (window.pageYOffset - this.instance.scroll.y) / (Date.now() - this.timestamp) : this.instance.speed = 0
                    }
                }, {
                    key: "resize", value: function () {
                        this.els.length && (this.windowHeight = window.innerHeight, this.updateElements())
                    }
                }, {
                    key: "addElements", value: function () {
                        var e = this;
                        this.els = [], this.el.querySelectorAll("[data-" + this.name + "]").forEach(function (t, i) {
                            var n = t.dataset[e.name + "Class"] || e.class,
                                r = t.getBoundingClientRect().top + e.instance.scroll.y, s = r + t.offsetHeight,
                                o = "string" == typeof t.dataset[e.name + "Offset"] ? t.dataset[e.name + "Offset"].split(",") : e.offset,
                                a = t.dataset[e.name + "Repeat"], l = t.dataset[e.name + "Call"];
                            a = "false" != a && (null != a || e.repeat);
                            var u = e.getRelativeOffset(o), c = {
                                el: t,
                                id: i,
                                class: n,
                                top: r + u[0],
                                bottom: s - u[1],
                                offset: o,
                                repeat: a,
                                inView: !!t.classList.contains(n),
                                call: l
                            };
                            e.els.push(c)
                        })
                    }
                }, {
                    key: "updateElements", value: function () {
                        var e = this;
                        this.els.forEach(function (t, i) {
                            var n = t.el.getBoundingClientRect().top + e.instance.scroll.y, r = n + t.el.offsetHeight,
                                s = e.getRelativeOffset(t.offset);
                            e.els[i].top = n + s[0], e.els[i].bottom = r - s[1]
                        }), this.hasScrollTicking = !1
                    }
                }, {
                    key: "getRelativeOffset", value: function (e) {
                        var t = [0, 0];
                        if (e) for (var i = 0; i < e.length; i++) "string" == typeof e[i] ? e[i].includes("%") ? t[i] = parseInt(e[i].replace("%", "") * this.windowHeight / 100) : t[i] = parseInt(e[i]) : t[i] = e[i];
                        return t
                    }
                }, {
                    key: "scrollTo", value: function (e, t, i, n, r, s) {
                        var o, a = t ? parseInt(t) : 0;
                        if ("string" == typeof e) {
                            if ("top" === e) o = this.html; else if ("bottom" === e) o = this.html.offsetHeight - window.innerHeight; else if (!(o = document.querySelector(e))) return
                        } else if ("number" == typeof e) o = parseInt(e); else {
                            if (!e || !e.tagName) return void console.warn("`targetOption` parameter is not valid");
                            o = e
                        }
                        a = "number" != typeof o ? o.getBoundingClientRect().top + a + this.instance.scroll.y : o + a, s && (a = a.toFixed(), window.addEventListener("scroll", function e() {
                            window.pageYOffset.toFixed() === a && (window.removeEventListener("scroll", e), s())
                        })), window.scrollTo({top: a, behavior: "smooth"})
                    }
                }, {
                    key: "update", value: function () {
                        this.addElements(), this.detectElements()
                    }
                }, {
                    key: "destroy", value: function () {
                        d(u(t.prototype), "destroy", this).call(this), window.removeEventListener("scroll", this.checkScroll, !1)
                    }
                }]), t
            }()), b = Object.getOwnPropertySymbols, w = Object.prototype.hasOwnProperty,
            D = Object.prototype.propertyIsEnumerable;
        var T = function () {
            try {
                if (!Object.assign) return !1;
                var e = new String("abc");
                if (e[5] = "de", "5" === Object.getOwnPropertyNames(e)[0]) return !1;
                for (var t = {}, i = 0; i < 10; i++) t["_" + String.fromCharCode(i)] = i;
                if ("0123456789" !== Object.getOwnPropertyNames(t).map(function (e) {
                    return t[e]
                }).join("")) return !1;
                var n = {};
                return "abcdefghijklmnopqrst".split("").forEach(function (e) {
                    n[e] = e
                }), "abcdefghijklmnopqrst" === Object.keys(Object.assign({}, n)).join("")
            } catch (e) {
                return !1
            }
        }() ? Object.assign : function (e, t) {
            for (var i, n, r = function (e) {
                if (null == e) throw new TypeError("Object.assign cannot be called with null or undefined");
                return Object(e)
            }(e), s = 1; s < arguments.length; s++) {
                for (var o in i = Object(arguments[s])) w.call(i, o) && (r[o] = i[o]);
                if (b) {
                    n = b(i);
                    for (var a = 0; a < n.length; a++) D.call(i, n[a]) && (r[n[a]] = i[n[a]])
                }
            }
            return r
        };

        function C() {
        }

        C.prototype = {
            on: function (e, t, i) {
                var n = this.e || (this.e = {});
                return (n[e] || (n[e] = [])).push({fn: t, ctx: i}), this
            }, once: function (e, t, i) {
                var n = this;

                function r() {
                    n.off(e, r), t.apply(i, arguments)
                }

                return r._ = t, this.on(e, r, i)
            }, emit: function (e) {
                for (var t = [].slice.call(arguments, 1), i = ((this.e || (this.e = {}))[e] || []).slice(), n = 0, r = i.length; n < r; n++) i[n].fn.apply(i[n].ctx, t);
                return this
            }, off: function (e, t) {
                var i = this.e || (this.e = {}), n = i[e], r = [];
                if (n && t) for (var s = 0, o = n.length; s < o; s++) n[s].fn !== t && n[s].fn._ !== t && r.push(n[s]);
                return r.length ? i[e] = r : delete i[e], this
            }
        };
        var k = C, E = g(function (e, t) {
                (function () {
                    (null !== t ? t : this).Lethargy = function () {
                        function e(e, t, i, n) {
                            this.stability = null != e ? Math.abs(e) : 8, this.sensitivity = null != t ? 1 + Math.abs(t) : 100, this.tolerance = null != i ? 1 + Math.abs(i) : 1.1, this.delay = null != n ? n : 150, this.lastUpDeltas = function () {
                                var e, t, i;
                                for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                                return i
                            }.call(this), this.lastDownDeltas = function () {
                                var e, t, i;
                                for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                                return i
                            }.call(this), this.deltasTimestamp = function () {
                                var e, t, i;
                                for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                                return i
                            }.call(this)
                        }

                        return e.prototype.check = function (e) {
                            var t;
                            return null != (e = e.originalEvent || e).wheelDelta ? t = e.wheelDelta : null != e.deltaY ? t = -40 * e.deltaY : null == e.detail && 0 !== e.detail || (t = -40 * e.detail), this.deltasTimestamp.push(Date.now()), this.deltasTimestamp.shift(), t > 0 ? (this.lastUpDeltas.push(t), this.lastUpDeltas.shift(), this.isInertia(1)) : (this.lastDownDeltas.push(t), this.lastDownDeltas.shift(), this.isInertia(-1))
                        }, e.prototype.isInertia = function (e) {
                            var t, i, n, r, s, o, a;
                            return null === (t = -1 === e ? this.lastDownDeltas : this.lastUpDeltas)[0] ? e : !(this.deltasTimestamp[2 * this.stability - 2] + this.delay > Date.now() && t[0] === t[2 * this.stability - 1]) && (n = t.slice(0, this.stability), i = t.slice(this.stability, 2 * this.stability), a = n.reduce(function (e, t) {
                                return e + t
                            }), s = i.reduce(function (e, t) {
                                return e + t
                            }), o = a / n.length, r = s / i.length, Math.abs(o) < Math.abs(r * this.tolerance) && this.sensitivity < Math.abs(r) && e)
                        }, e.prototype.showLastUpDeltas = function () {
                            return this.lastUpDeltas
                        }, e.prototype.showLastDownDeltas = function () {
                            return this.lastDownDeltas
                        }, e
                    }()
                }).call(m)
            }), _ = "onwheel" in document, x = "onmousewheel" in document,
            S = "ontouchstart" in window || window.TouchEvent || window.DocumentTouch && document instanceof DocumentTouch,
            P = navigator.msMaxTouchPoints && navigator.msMaxTouchPoints > 1, O = !!window.navigator.msPointerEnabled,
            A = "onkeydown" in document, M = navigator.userAgent.indexOf("Firefox") > -1, L = Object.prototype.toString,
            F = Object.prototype.hasOwnProperty;

        function j(e, t) {
            return function () {
                return e.apply(t, arguments)
            }
        }

        var B = E.Lethargy, I = "virtualscroll", R = N;

        function N(e) {
            !function (e) {
                if (!e) return console.warn("bindAll requires at least one argument.");
                var t = Array.prototype.slice.call(arguments, 1);
                if (0 === t.length) for (var i in e) F.call(e, i) && "function" == typeof e[i] && "[object Function]" == L.call(e[i]) && t.push(i);
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    e[r] = j(e[r], e)
                }
            }(this, "_onWheel", "_onMouseWheel", "_onTouchStart", "_onTouchMove", "_onKeyDown"), this.el = window, e && e.el && (this.el = e.el, delete e.el), this.options = T({
                mouseMultiplier: 1,
                touchMultiplier: 2,
                firefoxMultiplier: 15,
                keyStep: 120,
                preventTouch: !1,
                unpreventTouchClass: "vs-touchmove-allowed",
                limitInertia: !1,
                useKeyboard: !0,
                useTouch: !0
            }, e), this.options.limitInertia && (this._lethargy = new B), this._emitter = new k, this._event = {
                y: 0,
                x: 0,
                deltaX: 0,
                deltaY: 0
            }, this.touchStartX = null, this.touchStartY = null, this.bodyTouchAction = null, void 0 !== this.options.passive && (this.listenerOptions = {passive: this.options.passive})
        }

        function z(e, t, i) {
            return (1 - i) * e + i * t
        }

        function H(e) {
            var t = {};
            if (window.getComputedStyle) {
                var i = getComputedStyle(e), n = i.transform || i.webkitTransform || i.mozTransform,
                    r = n.match(/^matrix3d\((.+)\)$/);
                return r ? (t.x = r ? parseFloat(r[1].split(", ")[12]) : 0, t.y = r ? parseFloat(r[1].split(", ")[13]) : 0) : (r = n.match(/^matrix\((.+)\)$/), t.x = r ? parseFloat(r[1].split(", ")[4]) : 0, t.y = r ? parseFloat(r[1].split(", ")[5]) : 0), t
            }
        }

        function V(e) {
            for (var t = []; e && e !== document; e = e.parentNode) t.push(e);
            return t
        }

        N.prototype._notify = function (e) {
            var t = this._event;
            t.x += t.deltaX, t.y += t.deltaY, this._emitter.emit(I, {
                x: t.x,
                y: t.y,
                deltaX: t.deltaX,
                deltaY: t.deltaY,
                originalEvent: e
            })
        }, N.prototype._onWheel = function (e) {
            var t = this.options;
            if (!this._lethargy || !1 !== this._lethargy.check(e)) {
                var i = this._event;
                i.deltaX = e.wheelDeltaX || -1 * e.deltaX, i.deltaY = e.wheelDeltaY || -1 * e.deltaY, M && 1 == e.deltaMode && (i.deltaX *= t.firefoxMultiplier, i.deltaY *= t.firefoxMultiplier), i.deltaX *= t.mouseMultiplier, i.deltaY *= t.mouseMultiplier, this._notify(e)
            }
        }, N.prototype._onMouseWheel = function (e) {
            if (!this.options.limitInertia || !1 !== this._lethargy.check(e)) {
                var t = this._event;
                t.deltaX = e.wheelDeltaX ? e.wheelDeltaX : 0, t.deltaY = e.wheelDeltaY ? e.wheelDeltaY : e.wheelDelta, this._notify(e)
            }
        }, N.prototype._onTouchStart = function (e) {
            var t = e.targetTouches ? e.targetTouches[0] : e;
            this.touchStartX = t.pageX, this.touchStartY = t.pageY
        }, N.prototype._onTouchMove = function (e) {
            var t = this.options;
            t.preventTouch && !e.target.classList.contains(t.unpreventTouchClass) && e.preventDefault();
            var i = this._event, n = e.targetTouches ? e.targetTouches[0] : e;
            i.deltaX = (n.pageX - this.touchStartX) * t.touchMultiplier, i.deltaY = (n.pageY - this.touchStartY) * t.touchMultiplier, this.touchStartX = n.pageX, this.touchStartY = n.pageY, this._notify(e)
        }, N.prototype._onKeyDown = function (e) {
            var t = this._event;
            t.deltaX = t.deltaY = 0;
            var i = window.innerHeight - 40;
            switch (e.keyCode) {
                case 37:
                case 38:
                    t.deltaY = this.options.keyStep;
                    break;
                case 39:
                case 40:
                    t.deltaY = -this.options.keyStep;
                    break;
                case e.shiftKey:
                    t.deltaY = i;
                    break;
                case 32:
                    t.deltaY = -i;
                    break;
                default:
                    return
            }
            this._notify(e)
        }, N.prototype._bind = function () {
            _ && this.el.addEventListener("wheel", this._onWheel, this.listenerOptions), x && this.el.addEventListener("mousewheel", this._onMouseWheel, this.listenerOptions), S && this.options.useTouch && (this.el.addEventListener("touchstart", this._onTouchStart, this.listenerOptions), this.el.addEventListener("touchmove", this._onTouchMove, this.listenerOptions)), O && P && (this.bodyTouchAction = document.body.style.msTouchAction, document.body.style.msTouchAction = "none", this.el.addEventListener("MSPointerDown", this._onTouchStart, !0), this.el.addEventListener("MSPointerMove", this._onTouchMove, !0)), A && this.options.useKeyboard && document.addEventListener("keydown", this._onKeyDown)
        }, N.prototype._unbind = function () {
            _ && this.el.removeEventListener("wheel", this._onWheel), x && this.el.removeEventListener("mousewheel", this._onMouseWheel), S && (this.el.removeEventListener("touchstart", this._onTouchStart), this.el.removeEventListener("touchmove", this._onTouchMove)), O && P && (document.body.style.msTouchAction = this.bodyTouchAction, this.el.removeEventListener("MSPointerDown", this._onTouchStart, !0), this.el.removeEventListener("MSPointerMove", this._onTouchMove, !0)), A && this.options.useKeyboard && document.removeEventListener("keydown", this._onKeyDown)
        }, N.prototype.on = function (e, t) {
            this._emitter.on(I, e, t);
            var i = this._emitter.e;
            i && i[I] && 1 === i[I].length && this._bind()
        }, N.prototype.off = function (e, t) {
            this._emitter.off(I, e, t);
            var i = this._emitter.e;
            (!i[I] || i[I].length <= 0) && this._unbind()
        }, N.prototype.reset = function () {
            var e = this._event;
            e.x = 0, e.y = 0
        }, N.prototype.destroy = function () {
            this._emitter.off(), this._unbind()
        };
        var q = "function" == typeof Float32Array;

        function W(e, t) {
            return 1 - 3 * t + 3 * e
        }

        function X(e, t) {
            return 3 * t - 6 * e
        }

        function Y(e) {
            return 3 * e
        }

        function U(e, t, i) {
            return ((W(t, i) * e + X(t, i)) * e + Y(t)) * e
        }

        function G(e, t, i) {
            return 3 * W(t, i) * e * e + 2 * X(t, i) * e + Y(t)
        }

        function $(e) {
            return e
        }

        var K = function (e) {
            function t() {
                var e, n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return i(this, t), window.scrollTo(0, 0), history.scrollRestoration = "manual", (e = h(this, u(t).call(this, n))).inertia && (e.lerp = .1 * e.inertia), e.isScrolling = !1, e.isDraggingScrollbar = !1, e.isTicking = !1, e.hasScrollTicking = !1, e.parallaxElements = [], e.stop = !1, e.checkKey = e.checkKey.bind(c(e)), window.addEventListener("keydown", e.checkKey, !1), e
            }

            return l(t, f), r(t, [{
                key: "init", value: function () {
                    var e = this;
                    this.html.classList.add(this.smoothClass), this.instance = a({
                        delta: {
                            x: 0,
                            y: 0
                        }
                    }, this.instance), this.vs = new R({
                        el: this.scrollFromAnywhere ? document : this.el,
                        mouseMultiplier: navigator.platform.indexOf("Win") > -1 ? 1 : .4,
                        firefoxMultiplier: this.firefoxMultiplier,
                        touchMultiplier: this.touchMultiplier,
                        useKeyboard: !1,
                        passive: !0
                    }), this.vs.on(function (t) {
                        e.stop || (e.isTicking || e.isDraggingScrollbar || (requestAnimationFrame(function () {
                            e.updateDelta(t), e.isScrolling || e.startScrolling()
                        }), e.isTicking = !0), e.isTicking = !1)
                    }), this.setScrollLimit(), this.initScrollBar(), this.addSections(), this.addElements(), this.detectElements(), this.transformElements(!0, !0), this.checkScroll(!0), d(u(t.prototype), "init", this).call(this)
                }
            }, {
                key: "setScrollLimit", value: function () {
                    this.instance.limit = this.el.offsetHeight - this.windowHeight
                }
            }, {
                key: "startScrolling", value: function () {
                    this.isScrolling = !0, this.checkScroll(), this.html.classList.add(this.scrollingClass)
                }
            }, {
                key: "stopScrolling", value: function () {
                    this.scrollToRaf && (cancelAnimationFrame(this.scrollToRaf), this.scrollToRaf = null), this.isScrolling = !1, this.instance.scroll.y = Math.round(this.instance.scroll.y), this.html.classList.remove(this.scrollingClass)
                }
            }, {
                key: "checkKey", value: function (e) {
                    var t = this;
                    if (this.stop) 9 == e.keyCode && requestAnimationFrame(function () {
                        t.html.scrollTop = 0, document.body.scrollTop = 0
                    }); else {
                        switch (e.keyCode) {
                            case 9:
                                requestAnimationFrame(function () {
                                    t.html.scrollTop = 0, document.body.scrollTop = 0, t.scrollTo(document.activeElement, -window.innerHeight / 2)
                                });
                                break;
                            case 38:
                                this.instance.delta.y -= 240;
                                break;
                            case 40:
                                this.instance.delta.y += 240;
                                break;
                            case 33:
                                this.instance.delta.y -= window.innerHeight;
                                break;
                            case 34:
                                this.instance.delta.y += window.innerHeight;
                                break;
                            case 36:
                                this.instance.delta.y -= this.instance.limit;
                                break;
                            case 35:
                                this.instance.delta.y += this.instance.limit;
                                break;
                            case 32:
                                document.activeElement instanceof HTMLInputElement || document.activeElement instanceof HTMLTextAreaElement || (e.shiftKey ? this.instance.delta.y -= window.innerHeight : this.instance.delta.y += window.innerHeight);
                                break;
                            default:
                                return
                        }
                        this.instance.delta.y < 0 && (this.instance.delta.y = 0), this.instance.delta.y > this.instance.limit && (this.instance.delta.y = this.instance.limit), this.isScrolling = !0, this.checkScroll(), this.html.classList.add(this.scrollingClass)
                    }
                }
            }, {
                key: "checkScroll", value: function () {
                    var e = this;
                    if (arguments.length > 0 && void 0 !== arguments[0] && arguments[0] || this.isScrolling || this.isDraggingScrollbar) {
                        this.hasScrollTicking || (requestAnimationFrame(function () {
                            return e.checkScroll()
                        }), this.hasScrollTicking = !0), this.updateScroll();
                        var i = Math.abs(this.instance.delta.y - this.instance.scroll.y);
                        !this.animatingScroll && (i < .5 && 0 != this.instance.delta.y || i < .5 && 0 == this.instance.delta.y) && this.stopScrolling();
                        for (var n = this.sections.length - 1; n >= 0; n--) this.sections[n].persistent || this.instance.scroll.y > this.sections[n].offset && this.instance.scroll.y < this.sections[n].limit ? (this.transform(this.sections[n].el, 0, -this.instance.scroll.y), this.sections[n].inView || (this.sections[n].inView = !0, this.sections[n].el.style.opacity = 1, this.sections[n].el.style.pointerEvents = "all", this.sections[n].el.setAttribute("data-".concat(this.name, "-section-inview"), ""))) : (this.sections[n].inView && (this.sections[n].inView = !1, this.sections[n].el.style.opacity = 0, this.sections[n].el.style.pointerEvents = "none", this.sections[n].el.removeAttribute("data-".concat(this.name, "-section-inview"))), this.transform(this.sections[n].el, 0, 0));
                        this.getDirection && this.addDirection(), this.getSpeed && (this.addSpeed(), this.timestamp = Date.now()), this.detectElements(), this.transformElements();
                        var r = this.instance.scroll.y / this.instance.limit * this.scrollBarLimit;
                        this.transform(this.scrollbarThumb, 0, r), d(u(t.prototype), "checkScroll", this).call(this), this.hasScrollTicking = !1
                    }
                }
            }, {
                key: "resize", value: function () {
                    this.windowHeight = window.innerHeight, this.windowMiddle = this.windowHeight / 2, this.update()
                }
            }, {
                key: "updateDelta", value: function (e) {
                    this.instance.delta.y -= e.deltaY * this.multiplier, this.instance.delta.y < 0 && (this.instance.delta.y = 0), this.instance.delta.y > this.instance.limit && (this.instance.delta.y = this.instance.limit)
                }
            }, {
                key: "updateScroll", value: function (e) {
                    this.isScrolling || this.isDraggingScrollbar ? this.instance.scroll.y = z(this.instance.scroll.y, this.instance.delta.y, this.lerp) : this.instance.scroll.y > this.instance.limit ? this.setScroll(this.instance.scroll.x, this.instance.limit) : this.instance.scroll.y < 0 ? this.setScroll(this.instance.scroll.x, 0) : this.setScroll(this.instance.scroll.x, this.instance.delta.y)
                }
            }, {
                key: "addDirection", value: function () {
                    this.instance.delta.y > this.instance.scroll.y ? "down" !== this.instance.direction && (this.instance.direction = "down") : this.instance.delta.y < this.instance.scroll.y && "up" !== this.instance.direction && (this.instance.direction = "up")
                }
            }, {
                key: "addSpeed", value: function () {
                    this.instance.delta.y != this.instance.scroll.y ? this.instance.speed = (this.instance.delta.y - this.instance.scroll.y) / Math.max(1, Date.now() - this.timestamp) : this.instance.speed = 0
                }
            }, {
                key: "initScrollBar", value: function () {
                    this.scrollbar = document.createElement("span"), this.scrollbarThumb = document.createElement("span"), this.scrollbar.classList.add("".concat(this.scrollbarClass)), this.scrollbarThumb.classList.add("".concat(this.scrollbarClass, "_thumb")), this.scrollbar.append(this.scrollbarThumb), document.body.append(this.scrollbar), this.getScrollBar = this.getScrollBar.bind(this), this.releaseScrollBar = this.releaseScrollBar.bind(this), this.moveScrollBar = this.moveScrollBar.bind(this), this.scrollbarThumb.addEventListener("mousedown", this.getScrollBar), window.addEventListener("mouseup", this.releaseScrollBar), window.addEventListener("mousemove", this.moveScrollBar), this.instance.limit + this.windowHeight <= this.windowHeight || (this.scrollbarHeight = this.scrollbar.getBoundingClientRect().height, this.scrollbarThumb.style.height = "".concat(this.scrollbarHeight * this.scrollbarHeight / (this.instance.limit + this.scrollbarHeight), "px"), this.scrollBarLimit = this.scrollbarHeight - this.scrollbarThumb.getBoundingClientRect().height)
                }
            }, {
                key: "reinitScrollBar", value: function () {
                    this.instance.limit + this.windowHeight <= this.windowHeight || (this.scrollbarHeight = this.scrollbar.getBoundingClientRect().height, this.scrollbarThumb.style.height = "".concat(this.scrollbarHeight * this.scrollbarHeight / (this.instance.limit + this.scrollbarHeight), "px"), this.scrollBarLimit = this.scrollbarHeight - this.scrollbarThumb.getBoundingClientRect().height)
                }
            }, {
                key: "destroyScrollBar", value: function () {
                    this.scrollbarThumb.removeEventListener("mousedown", this.getScrollBar), window.removeEventListener("mouseup", this.releaseScrollBar), window.removeEventListener("mousemove", this.moveScrollBar), this.scrollbar.remove()
                }
            }, {
                key: "getScrollBar", value: function (e) {
                    this.isDraggingScrollbar = !0, this.checkScroll(), this.html.classList.remove(this.scrollingClass), this.html.classList.add(this.draggingClass)
                }
            }, {
                key: "releaseScrollBar", value: function (e) {
                    this.isDraggingScrollbar = !1, this.html.classList.add(this.scrollingClass), this.html.classList.remove(this.draggingClass)
                }
            }, {
                key: "moveScrollBar", value: function (e) {
                    var t = this;
                    !this.isTicking && this.isDraggingScrollbar && (requestAnimationFrame(function () {
                        var i = 100 * e.clientY / t.scrollbarHeight * t.instance.limit / 100;
                        i > 0 && i < t.instance.limit && (t.instance.delta.y = i)
                    }), this.isTicking = !0), this.isTicking = !1
                }
            }, {
                key: "addElements", value: function () {
                    var e = this;
                    this.els = [], this.parallaxElements = [], this.sections.forEach(function (t, i) {
                        e.sections[i].el.querySelectorAll("[data-".concat(e.name, "]")).forEach(function (t, n) {
                            var r, s, o = t.dataset[e.name + "Class"] || e.class, a = t.dataset[e.name + "Repeat"],
                                l = t.dataset[e.name + "Call"], u = t.dataset[e.name + "Position"],
                                c = t.dataset[e.name + "Delay"], h = t.dataset[e.name + "Direction"],
                                d = "string" == typeof t.dataset[e.name + "Sticky"],
                                p = !!t.dataset[e.name + "Speed"] && parseFloat(t.dataset[e.name + "Speed"]) / 10,
                                f = "string" == typeof t.dataset[e.name + "Offset"] ? t.dataset[e.name + "Offset"].split(",") : e.offset,
                                m = t.dataset[e.name + "Target"];
                            s = void 0 !== m ? document.querySelector("".concat(m)) : t;
                            var g = (r = e.sections[i].inView ? s.getBoundingClientRect().top + e.instance.scroll.y - H(s).y : s.getBoundingClientRect().top - H(e.sections[i].el).y - H(s).y) + s.offsetHeight,
                                v = (g - r) / 2 + r;
                            if (d) {
                                var y = t.getBoundingClientRect().top, b = y - r;
                                r += window.innerHeight, v = ((g = y + s.offsetHeight - t.offsetHeight - b) - r) / 2 + r
                            }
                            a = "false" != a && (null != a || e.repeat);
                            var w = [0, 0];
                            if (f) for (var D = 0; D < f.length; D++) "string" == typeof f[D] ? f[D].includes("%") ? w[D] = parseInt(f[D].replace("%", "") * e.windowHeight / 100) : w[D] = parseInt(f[D]) : w[D] = f[D];
                            var T = {
                                el: t,
                                id: n,
                                class: o,
                                top: r + w[0],
                                middle: v,
                                bottom: g - w[1],
                                offset: f,
                                repeat: a,
                                inView: !!t.classList.contains(o),
                                call: l,
                                speed: p,
                                delay: c,
                                position: u,
                                target: s,
                                direction: h,
                                sticky: d
                            };
                            e.els.push(T), (!1 !== p || d) && e.parallaxElements.push(T)
                        })
                    })
                }
            }, {
                key: "addSections", value: function () {
                    var e = this;
                    this.sections = [];
                    var t = this.el.querySelectorAll("[data-".concat(this.name, "-section]"));
                    0 === t.length && (t = [this.el]), t.forEach(function (t, i) {
                        var n = t.getBoundingClientRect().top - 1.5 * window.innerHeight - H(t).y,
                            r = n + t.getBoundingClientRect().height + 2 * window.innerHeight, s = {
                                el: t,
                                offset: n,
                                limit: r,
                                inView: !1,
                                persistent: "string" == typeof t.dataset[e.name + "Persistent"]
                            };
                        e.sections[i] = s
                    })
                }
            }, {
                key: "transform", value: function (e, t, i, n) {
                    var r;
                    if (n) {
                        var s = H(e), o = z(s.x, t, n), a = z(s.y, i, n);
                        r = "matrix3d(1,0,0.00,0,0.00,1,0.00,0,0,0,1,0,".concat(o, ",").concat(a, ",0,1)")
                    } else r = "matrix3d(1,0,0.00,0,0.00,1,0.00,0,0,0,1,0,".concat(t, ",").concat(i, ",0,1)");
                    e.style.webkitTransform = r, e.style.msTransform = r, e.style.transform = r
                }
            }, {
                key: "transformElements", value: function (e) {
                    var t = this, i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                        n = this.instance.scroll.y + this.windowHeight, r = this.instance.scroll.y + this.windowMiddle;
                    this.parallaxElements.forEach(function (s, o) {
                        var a = !1;
                        if (e && (a = 0), s.inView || i) switch (s.position) {
                            case"top":
                                a = t.instance.scroll.y * -s.speed;
                                break;
                            case"elementTop":
                                a = (n - s.top) * -s.speed;
                                break;
                            case"bottom":
                                a = (t.instance.limit - n + t.windowHeight) * s.speed;
                                break;
                            default:
                                a = (r - s.middle) * -s.speed
                        }
                        s.sticky && (a = s.inView ? t.instance.scroll.y - s.top + window.innerHeight : t.instance.scroll.y < s.top - window.innerHeight && t.instance.scroll.y < s.top - window.innerHeight / 2 ? 0 : t.instance.scroll.y > s.bottom && t.instance.scroll.y > s.bottom + 100 && s.bottom - s.top + window.innerHeight), !1 !== a && ("horizontal" === s.direction ? t.transform(s.el, a, 0, !e && s.delay) : t.transform(s.el, 0, a, !e && s.delay))
                    })
                }
            }, {
                key: "scrollTo", value: function (e, t) {
                    var i, n = this, r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 1e3,
                        s = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : [.25, 0, .35, 1],
                        o = arguments.length > 4 && void 0 !== arguments[4] && arguments[4],
                        a = arguments.length > 5 ? arguments[5] : void 0, l = t ? parseInt(t) : 0;
                    if (s = function (e, t, i, n) {
                        if (!(0 <= e && e <= 1 && 0 <= i && i <= 1)) throw new Error("bezier x values must be in [0, 1] range");
                        if (e === t && i === n) return $;
                        for (var r = q ? new Float32Array(11) : new Array(11), s = 0; s < 11; ++s) r[s] = U(.1 * s, e, i);

                        function o(t) {
                            for (var n = 0, s = 1; 10 !== s && r[s] <= t; ++s) n += .1;
                            var o = n + (t - r[--s]) / (r[s + 1] - r[s]) * .1, a = G(o, e, i);
                            return a >= .001 ? function (e, t, i, n) {
                                for (var r = 0; r < 4; ++r) {
                                    var s = G(t, i, n);
                                    if (0 === s) return t;
                                    t -= (U(t, i, n) - e) / s
                                }
                                return t
                            }(t, o, e, i) : 0 === a ? o : function (e, t, i, n, r) {
                                var s, o, a = 0;
                                do {
                                    (s = U(o = t + (i - t) / 2, n, r) - e) > 0 ? i = o : t = o
                                } while (Math.abs(s) > 1e-7 && ++a < 10);
                                return o
                            }(t, n, n + .1, e, i)
                        }

                        return function (e) {
                            return 0 === e ? 0 : 1 === e ? 1 : U(o(e), t, n)
                        }
                    }.apply(void 0, function (e) {
                        return function (e) {
                            if (Array.isArray(e)) {
                                for (var t = 0, i = new Array(e.length); t < e.length; t++) i[t] = e[t];
                                return i
                            }
                        }(e) || function (e) {
                            if (Symbol.iterator in Object(e) || "[object Arguments]" === Object.prototype.toString.call(e)) return Array.from(e)
                        }(e) || function () {
                            throw new TypeError("Invalid attempt to spread non-iterable instance")
                        }()
                    }(s)), "string" == typeof e) {
                        if ("top" === e) i = 0; else if ("bottom" === e) i = this.instance.limit; else if (!(i = document.querySelector(e))) return
                    } else if ("number" == typeof e) i = parseInt(e); else {
                        if (!e || !e.tagName) return void console.warn("`targetOption` parameter is not valid");
                        i = e
                    }
                    if ("number" != typeof i) {
                        if (!V(i).includes(this.el)) return;
                        var u = i.getBoundingClientRect().top, c = V(i).find(function (e) {
                            return n.sections.find(function (t) {
                                return t.el == e
                            })
                        }), h = 0;
                        c && (h = H(c).y), l = u + l - h
                    } else l = i + l;
                    var d = parseFloat(this.instance.delta.y), p = Math.max(0, Math.min(l, this.instance.limit)) - d,
                        f = function (e) {
                            o ? n.setScroll(n.instance.delta.x, d + p * e) : n.instance.delta.y = d + p * e
                        };
                    this.animatingScroll = !0, this.stopScrolling(), this.startScrolling();
                    var m = Date.now();
                    !function e() {
                        var t = (Date.now() - m) / r;
                        t > 1 ? (f(1), n.animatingScroll = !1, 0 == r && n.update(), a && a()) : (n.scrollToRaf = requestAnimationFrame(e), f(s(t)))
                    }()
                }
            }, {
                key: "update", value: function () {
                    this.setScrollLimit(), this.addSections(), this.addElements(), this.detectElements(), this.updateScroll(), this.transformElements(!0), this.reinitScrollBar(), this.checkScroll(!0)
                }
            }, {
                key: "startScroll", value: function () {
                    this.stop = !1
                }
            }, {
                key: "stopScroll", value: function () {
                    this.stop = !0
                }
            }, {
                key: "setScroll", value: function (e, t) {
                    this.instance = a({}, this.instance, {scroll: {x: e, y: t}, delta: {x: e, y: t}, speed: 0})
                }
            }, {
                key: "destroy", value: function () {
                    d(u(t.prototype), "destroy", this).call(this), this.stopScrolling(), this.html.classList.remove(this.smoothClass), this.vs.destroy(), this.destroyScrollBar(), window.removeEventListener("keydown", this.checkKey, !1)
                }
            }]), t
        }(), J = function () {
            function e() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                i(this, e), this.options = t, Object.assign(this, p, t), this.init()
            }

            return r(e, [{
                key: "init", value: function () {
                    if (this.smoothMobile || (this.isMobile = /Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || "MacIntel" === navigator.platform && navigator.maxTouchPoints > 1), !0 !== this.smooth || this.isMobile ? this.scroll = new y(this.options) : this.scroll = new K(this.options), this.scroll.init(), window.location.hash) {
                        var e = window.location.hash.slice(1, window.location.hash.length),
                            t = document.getElementById(e);
                        t && this.scroll.scrollTo(t)
                    }
                }
            }, {
                key: "update", value: function () {
                    this.scroll.update()
                }
            }, {
                key: "start", value: function () {
                    this.scroll.startScroll()
                }
            }, {
                key: "stop", value: function () {
                    this.scroll.stopScroll()
                }
            }, {
                key: "scrollTo", value: function (e, t, i, n, r, s) {
                    this.scroll.scrollTo(e, t, i, n, r, s)
                }
            }, {
                key: "setScroll", value: function (e, t) {
                    this.scroll.setScroll(e, t)
                }
            }, {
                key: "on", value: function (e, t) {
                    this.scroll.setEvents(e, t)
                }
            }, {
                key: "off", value: function (e, t) {
                    this.scroll.unsetEvents(e, t)
                }
            }, {
                key: "destroy", value: function () {
                    this.scroll.destroy()
                }
            }]), e
        }();
        t.a = J
    }).call(this, i(5))
}, function (e, t, i) {
    "use strict";
    e.exports = function (e, t) {
        return function () {
            for (var i = new Array(arguments.length), n = 0; n < i.length; n++) i[n] = arguments[n];
            return e.apply(t, i)
        }
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);

    function r(e) {
        return encodeURIComponent(e).replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
    }

    e.exports = function (e, t, i) {
        if (!t) return e;
        var s;
        if (i) s = i(t); else if (n.isURLSearchParams(t)) s = t.toString(); else {
            var o = [];
            n.forEach(t, function (e, t) {
                null != e && (n.isArray(e) ? t += "[]" : e = [e], n.forEach(e, function (e) {
                    n.isDate(e) ? e = e.toISOString() : n.isObject(e) && (e = JSON.stringify(e)), o.push(r(t) + "=" + r(e))
                }))
            }), s = o.join("&")
        }
        if (s) {
            var a = e.indexOf("#");
            -1 !== a && (e = e.slice(0, a)), e += (-1 === e.indexOf("?") ? "?" : "&") + s
        }
        return e
    }
}, function (e, t, i) {
    "use strict";
    e.exports = function (e) {
        return !(!e || !e.__CANCEL__)
    }
}, function (e, t, i) {
    "use strict";
    (function (t) {
        var n = i(0), r = i(47), s = {"Content-Type": "application/x-www-form-urlencoded"};

        function o(e, t) {
            !n.isUndefined(e) && n.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
        }

        var a, l = {
            adapter: (("undefined" != typeof XMLHttpRequest || void 0 !== t && "[object process]" === Object.prototype.toString.call(t)) && (a = i(11)), a),
            transformRequest: [function (e, t) {
                return r(t, "Accept"), r(t, "Content-Type"), n.isFormData(e) || n.isArrayBuffer(e) || n.isBuffer(e) || n.isStream(e) || n.isFile(e) || n.isBlob(e) ? e : n.isArrayBufferView(e) ? e.buffer : n.isURLSearchParams(e) ? (o(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : n.isObject(e) ? (o(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
            }],
            transformResponse: [function (e) {
                if ("string" == typeof e) try {
                    e = JSON.parse(e)
                } catch (e) {
                }
                return e
            }],
            timeout: 0,
            xsrfCookieName: "XSRF-TOKEN",
            xsrfHeaderName: "X-XSRF-TOKEN",
            maxContentLength: -1,
            maxBodyLength: -1,
            validateStatus: function (e) {
                return e >= 200 && e < 300
            },
            headers: {common: {Accept: "application/json, text/plain, */*"}}
        };
        n.forEach(["delete", "get", "head"], function (e) {
            l.headers[e] = {}
        }), n.forEach(["post", "put", "patch"], function (e) {
            l.headers[e] = n.merge(s)
        }), e.exports = l
    }).call(this, i(46))
}, function (e, t, i) {
    "use strict";
    var n = i(0), r = i(48), s = i(50), o = i(8), a = i(51), l = i(54), u = i(55), c = i(12);
    e.exports = function (e) {
        return new Promise(function (t, i) {
            var h = e.data, d = e.headers;
            n.isFormData(h) && delete d["Content-Type"], (n.isBlob(h) || n.isFile(h)) && h.type && delete d["Content-Type"];
            var p = new XMLHttpRequest;
            if (e.auth) {
                var f = e.auth.username || "", m = unescape(encodeURIComponent(e.auth.password)) || "";
                d.Authorization = "Basic " + btoa(f + ":" + m)
            }
            var g = a(e.baseURL, e.url);
            if (p.open(e.method.toUpperCase(), o(g, e.params, e.paramsSerializer), !0), p.timeout = e.timeout, p.onreadystatechange = function () {
                if (p && 4 === p.readyState && (0 !== p.status || p.responseURL && 0 === p.responseURL.indexOf("file:"))) {
                    var n = "getAllResponseHeaders" in p ? l(p.getAllResponseHeaders()) : null, s = {
                        data: e.responseType && "text" !== e.responseType ? p.response : p.responseText,
                        status: p.status,
                        statusText: p.statusText,
                        headers: n,
                        config: e,
                        request: p
                    };
                    r(t, i, s), p = null
                }
            }, p.onabort = function () {
                p && (i(c("Request aborted", e, "ECONNABORTED", p)), p = null)
            }, p.onerror = function () {
                i(c("Network Error", e, null, p)), p = null
            }, p.ontimeout = function () {
                var t = "timeout of " + e.timeout + "ms exceeded";
                e.timeoutErrorMessage && (t = e.timeoutErrorMessage), i(c(t, e, "ECONNABORTED", p)), p = null
            }, n.isStandardBrowserEnv()) {
                var v = (e.withCredentials || u(g)) && e.xsrfCookieName ? s.read(e.xsrfCookieName) : void 0;
                v && (d[e.xsrfHeaderName] = v)
            }
            if ("setRequestHeader" in p && n.forEach(d, function (e, t) {
                void 0 === h && "content-type" === t.toLowerCase() ? delete d[t] : p.setRequestHeader(t, e)
            }), n.isUndefined(e.withCredentials) || (p.withCredentials = !!e.withCredentials), e.responseType) try {
                p.responseType = e.responseType
            } catch (t) {
                if ("json" !== e.responseType) throw t
            }
            "function" == typeof e.onDownloadProgress && p.addEventListener("progress", e.onDownloadProgress), "function" == typeof e.onUploadProgress && p.upload && p.upload.addEventListener("progress", e.onUploadProgress), e.cancelToken && e.cancelToken.promise.then(function (e) {
                p && (p.abort(), i(e), p = null)
            }), h || (h = null), p.send(h)
        })
    }
}, function (e, t, i) {
    "use strict";
    var n = i(49);
    e.exports = function (e, t, i, r, s) {
        var o = new Error(e);
        return n(o, t, i, r, s)
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);
    e.exports = function (e, t) {
        t = t || {};
        var i = {}, r = ["url", "method", "data"], s = ["headers", "auth", "proxy", "params"],
            o = ["baseURL", "transformRequest", "transformResponse", "paramsSerializer", "timeout", "timeoutMessage", "withCredentials", "adapter", "responseType", "xsrfCookieName", "xsrfHeaderName", "onUploadProgress", "onDownloadProgress", "decompress", "maxContentLength", "maxBodyLength", "maxRedirects", "transport", "httpAgent", "httpsAgent", "cancelToken", "socketPath", "responseEncoding"],
            a = ["validateStatus"];

        function l(e, t) {
            return n.isPlainObject(e) && n.isPlainObject(t) ? n.merge(e, t) : n.isPlainObject(t) ? n.merge({}, t) : n.isArray(t) ? t.slice() : t
        }

        function u(r) {
            n.isUndefined(t[r]) ? n.isUndefined(e[r]) || (i[r] = l(void 0, e[r])) : i[r] = l(e[r], t[r])
        }

        n.forEach(r, function (e) {
            n.isUndefined(t[e]) || (i[e] = l(void 0, t[e]))
        }), n.forEach(s, u), n.forEach(o, function (r) {
            n.isUndefined(t[r]) ? n.isUndefined(e[r]) || (i[r] = l(void 0, e[r])) : i[r] = l(void 0, t[r])
        }), n.forEach(a, function (n) {
            n in t ? i[n] = l(e[n], t[n]) : n in e && (i[n] = l(void 0, e[n]))
        });
        var c = r.concat(s).concat(o).concat(a), h = Object.keys(e).concat(Object.keys(t)).filter(function (e) {
            return -1 === c.indexOf(e)
        });
        return n.forEach(h, u), i
    }
}, function (e, t, i) {
    "use strict";

    function n(e) {
        this.message = e
    }

    n.prototype.toString = function () {
        return "Cancel" + (this.message ? ": " + this.message : "")
    }, n.prototype.__CANCEL__ = !0, e.exports = n
}, function (e, t, i) {
    "use strict";
    var n = i(35), r = i(36), s = i(37).Lethargy, o = i(38), a = (i(39), i(40)), l = "virtualscroll";
    e.exports = u;

    function u(e) {
        a(this, "_onWheel", "_onMouseWheel", "_onTouchStart", "_onTouchMove", "_onKeyDown"), this.el = window, e && e.el && (this.el = e.el, delete e.el), this.options = n({
            mouseMultiplier: 1,
            touchMultiplier: 2,
            firefoxMultiplier: 15,
            keyStep: 120,
            preventTouch: !1,
            unpreventTouchClass: "vs-touchmove-allowed",
            limitInertia: !1,
            useKeyboard: !0,
            useTouch: !0
        }, e), this.options.limitInertia && (this._lethargy = new s), this._emitter = new r, this._event = {
            y: 0,
            x: 0,
            deltaX: 0,
            deltaY: 0
        }, this.touchStartX = null, this.touchStartY = null, this.bodyTouchAction = null, void 0 !== this.options.passive && (this.listenerOptions = {passive: this.options.passive})
    }

    u.prototype._notify = function (e) {
        var t = this._event;
        t.x += t.deltaX, t.y += t.deltaY, this._emitter.emit(l, {
            x: t.x,
            y: t.y,
            deltaX: t.deltaX,
            deltaY: t.deltaY,
            originalEvent: e
        })
    }, u.prototype._onWheel = function (e) {
        var t = this.options;
        if (!this._lethargy || !1 !== this._lethargy.check(e)) {
            var i = this._event;
            i.deltaX = e.wheelDeltaX || -1 * e.deltaX, i.deltaY = e.wheelDeltaY || -1 * e.deltaY, o.isFirefox && 1 == e.deltaMode && (i.deltaX *= t.firefoxMultiplier, i.deltaY *= t.firefoxMultiplier), i.deltaX *= t.mouseMultiplier, i.deltaY *= t.mouseMultiplier, this._notify(e)
        }
    }, u.prototype._onMouseWheel = function (e) {
        if (!this.options.limitInertia || !1 !== this._lethargy.check(e)) {
            var t = this._event;
            t.deltaX = e.wheelDeltaX ? e.wheelDeltaX : 0, t.deltaY = e.wheelDeltaY ? e.wheelDeltaY : e.wheelDelta, this._notify(e)
        }
    }, u.prototype._onTouchStart = function (e) {
        var t = e.targetTouches ? e.targetTouches[0] : e;
        this.touchStartX = t.pageX, this.touchStartY = t.pageY
    }, u.prototype._onTouchMove = function (e) {
        var t = this.options;
        t.preventTouch && !e.target.classList.contains(t.unpreventTouchClass) && e.preventDefault();
        var i = this._event, n = e.targetTouches ? e.targetTouches[0] : e;
        i.deltaX = (n.pageX - this.touchStartX) * t.touchMultiplier, i.deltaY = (n.pageY - this.touchStartY) * t.touchMultiplier, this.touchStartX = n.pageX, this.touchStartY = n.pageY, this._notify(e)
    }, u.prototype._onKeyDown = function (e) {
        var t = this._event;
        t.deltaX = t.deltaY = 0;
        var i = window.innerHeight - 40;
        switch (e.keyCode) {
            case 37:
            case 38:
                t.deltaY = this.options.keyStep;
                break;
            case 39:
            case 40:
                t.deltaY = -this.options.keyStep;
                break;
            case e.shiftKey:
                t.deltaY = i;
                break;
            case 32:
                t.deltaY = -i;
                break;
            default:
                return
        }
        this._notify(e)
    }, u.prototype._bind = function () {
        o.hasWheelEvent && this.el.addEventListener("wheel", this._onWheel, this.listenerOptions), o.hasMouseWheelEvent && this.el.addEventListener("mousewheel", this._onMouseWheel, this.listenerOptions), o.hasTouch && this.options.useTouch && (this.el.addEventListener("touchstart", this._onTouchStart, this.listenerOptions), this.el.addEventListener("touchmove", this._onTouchMove, this.listenerOptions)), o.hasPointer && o.hasTouchWin && (this.bodyTouchAction = document.body.style.msTouchAction, document.body.style.msTouchAction = "none", this.el.addEventListener("MSPointerDown", this._onTouchStart, !0), this.el.addEventListener("MSPointerMove", this._onTouchMove, !0)), o.hasKeyDown && this.options.useKeyboard && document.addEventListener("keydown", this._onKeyDown)
    }, u.prototype._unbind = function () {
        o.hasWheelEvent && this.el.removeEventListener("wheel", this._onWheel), o.hasMouseWheelEvent && this.el.removeEventListener("mousewheel", this._onMouseWheel), o.hasTouch && (this.el.removeEventListener("touchstart", this._onTouchStart), this.el.removeEventListener("touchmove", this._onTouchMove)), o.hasPointer && o.hasTouchWin && (document.body.style.msTouchAction = this.bodyTouchAction, this.el.removeEventListener("MSPointerDown", this._onTouchStart, !0), this.el.removeEventListener("MSPointerMove", this._onTouchMove, !0)), o.hasKeyDown && this.options.useKeyboard && document.removeEventListener("keydown", this._onKeyDown)
    }, u.prototype.on = function (e, t) {
        this._emitter.on(l, e, t);
        var i = this._emitter.e;
        i && i[l] && 1 === i[l].length && this._bind()
    }, u.prototype.off = function (e, t) {
        this._emitter.off(l, e, t);
        var i = this._emitter.e;
        (!i[l] || i[l].length <= 0) && this._unbind()
    }, u.prototype.reset = function () {
        var e = this._event;
        e.x = 0, e.y = 0
    }, u.prototype.destroy = function () {
        this._emitter.off(), this._unbind()
    }
}, function (e, t, i) {
    (function (t) {
        var i = /^\s+|\s+$/g, n = /^[-+]0x[0-9a-f]+$/i, r = /^0b[01]+$/i, s = /^0o[0-7]+$/i, o = parseInt,
            a = "object" == typeof t && t && t.Object === Object && t,
            l = "object" == typeof self && self && self.Object === Object && self,
            u = a || l || Function("return this")(), c = Object.prototype.toString, h = Math.max, d = Math.min,
            p = function () {
                return u.Date.now()
            };

        function f(e) {
            var t = typeof e;
            return !!e && ("object" == t || "function" == t)
        }

        function m(e) {
            if ("number" == typeof e) return e;
            if (function (e) {
                return "symbol" == typeof e || function (e) {
                    return !!e && "object" == typeof e
                }(e) && "[object Symbol]" == c.call(e)
            }(e)) return NaN;
            if (f(e)) {
                var t = "function" == typeof e.valueOf ? e.valueOf() : e;
                e = f(t) ? t + "" : t
            }
            if ("string" != typeof e) return 0 === e ? e : +e;
            e = e.replace(i, "");
            var a = r.test(e);
            return a || s.test(e) ? o(e.slice(2), a ? 2 : 8) : n.test(e) ? NaN : +e
        }

        e.exports = function (e, t, i) {
            var n, r, s, o, a, l, u = 0, c = !1, g = !1, v = !0;
            if ("function" != typeof e) throw new TypeError("Expected a function");

            function y(t) {
                var i = n, s = r;
                return n = r = void 0, u = t, o = e.apply(s, i)
            }

            function b(e) {
                var i = e - l;
                return void 0 === l || i >= t || i < 0 || g && e - u >= s
            }

            function w() {
                var e = p();
                if (b(e)) return D(e);
                a = setTimeout(w, function (e) {
                    var i = t - (e - l);
                    return g ? d(i, s - (e - u)) : i
                }(e))
            }

            function D(e) {
                return a = void 0, v && n ? y(e) : (n = r = void 0, o)
            }

            function T() {
                var e = p(), i = b(e);
                if (n = arguments, r = this, l = e, i) {
                    if (void 0 === a) return function (e) {
                        return u = e, a = setTimeout(w, t), c ? y(e) : o
                    }(l);
                    if (g) return a = setTimeout(w, t), y(l)
                }
                return void 0 === a && (a = setTimeout(w, t)), o
            }

            return t = m(t) || 0, f(i) && (c = !!i.leading, s = (g = "maxWait" in i) ? h(m(i.maxWait) || 0, t) : s, v = "trailing" in i ? !!i.trailing : v), T.cancel = function () {
                void 0 !== a && clearTimeout(a), u = 0, n = l = r = a = void 0
            }, T.flush = function () {
                return void 0 === a ? o : D(p())
            }, T
        }
    }).call(this, i(5))
}, , , , , , , , , , , , , , , , , function (e, t, i) {
}, function (e, t, i) {
    "use strict";
    e.exports = function (e) {
        if ("string" != typeof e) throw new TypeError("expected a string");
        return (e = (e = (e = e.replace(/([a-z])([A-Z])/g, "$1-$2")).replace(/[ \t\W]/g, "-")).replace(/^-+|-+$/g, "")).toLowerCase()
    }
}, function (e, t, i) {
    "use strict";
    var n = Object.getOwnPropertySymbols, r = Object.prototype.hasOwnProperty,
        s = Object.prototype.propertyIsEnumerable;
    e.exports = function () {
        try {
            if (!Object.assign) return !1;
            var e = new String("abc");
            if (e[5] = "de", "5" === Object.getOwnPropertyNames(e)[0]) return !1;
            for (var t = {}, i = 0; i < 10; i++) t["_" + String.fromCharCode(i)] = i;
            if ("0123456789" !== Object.getOwnPropertyNames(t).map(function (e) {
                return t[e]
            }).join("")) return !1;
            var n = {};
            return "abcdefghijklmnopqrst".split("").forEach(function (e) {
                n[e] = e
            }), "abcdefghijklmnopqrst" === Object.keys(Object.assign({}, n)).join("")
        } catch (e) {
            return !1
        }
    }() ? Object.assign : function (e, t) {
        for (var i, o, a = function (e) {
            if (null == e) throw new TypeError("Object.assign cannot be called with null or undefined");
            return Object(e)
        }(e), l = 1; l < arguments.length; l++) {
            for (var u in i = Object(arguments[l])) r.call(i, u) && (a[u] = i[u]);
            if (n) {
                o = n(i);
                for (var c = 0; c < o.length; c++) s.call(i, o[c]) && (a[o[c]] = i[o[c]])
            }
        }
        return a
    }
}, function (e, t) {
    function i() {
    }

    i.prototype = {
        on: function (e, t, i) {
            var n = this.e || (this.e = {});
            return (n[e] || (n[e] = [])).push({fn: t, ctx: i}), this
        }, once: function (e, t, i) {
            var n = this;

            function r() {
                n.off(e, r), t.apply(i, arguments)
            }

            return r._ = t, this.on(e, r, i)
        }, emit: function (e) {
            for (var t = [].slice.call(arguments, 1), i = ((this.e || (this.e = {}))[e] || []).slice(), n = 0, r = i.length; n < r; n++) i[n].fn.apply(i[n].ctx, t);
            return this
        }, off: function (e, t) {
            var i = this.e || (this.e = {}), n = i[e], r = [];
            if (n && t) for (var s = 0, o = n.length; s < o; s++) n[s].fn !== t && n[s].fn._ !== t && r.push(n[s]);
            return r.length ? i[e] = r : delete i[e], this
        }
    }, e.exports = i
}, function (e, t, i) {
    (function () {
        (null !== t ? t : this).Lethargy = function () {
            function e(e, t, i, n) {
                this.stability = null != e ? Math.abs(e) : 8, this.sensitivity = null != t ? 1 + Math.abs(t) : 100, this.tolerance = null != i ? 1 + Math.abs(i) : 1.1, this.delay = null != n ? n : 150, this.lastUpDeltas = function () {
                    var e, t, i;
                    for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                    return i
                }.call(this), this.lastDownDeltas = function () {
                    var e, t, i;
                    for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                    return i
                }.call(this), this.deltasTimestamp = function () {
                    var e, t, i;
                    for (i = [], e = 1, t = 2 * this.stability; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) i.push(null);
                    return i
                }.call(this)
            }

            return e.prototype.check = function (e) {
                var t;
                return null != (e = e.originalEvent || e).wheelDelta ? t = e.wheelDelta : null != e.deltaY ? t = -40 * e.deltaY : null == e.detail && 0 !== e.detail || (t = -40 * e.detail), this.deltasTimestamp.push(Date.now()), this.deltasTimestamp.shift(), t > 0 ? (this.lastUpDeltas.push(t), this.lastUpDeltas.shift(), this.isInertia(1)) : (this.lastDownDeltas.push(t), this.lastDownDeltas.shift(), this.isInertia(-1))
            }, e.prototype.isInertia = function (e) {
                var t, i, n, r, s, o, a;
                return null === (t = -1 === e ? this.lastDownDeltas : this.lastUpDeltas)[0] ? e : !(this.deltasTimestamp[2 * this.stability - 2] + this.delay > Date.now() && t[0] === t[2 * this.stability - 1]) && (n = t.slice(0, this.stability), i = t.slice(this.stability, 2 * this.stability), a = n.reduce(function (e, t) {
                    return e + t
                }), s = i.reduce(function (e, t) {
                    return e + t
                }), o = a / n.length, r = s / i.length, Math.abs(o) < Math.abs(r * this.tolerance) && this.sensitivity < Math.abs(r) && e)
            }, e.prototype.showLastUpDeltas = function () {
                return this.lastUpDeltas
            }, e.prototype.showLastDownDeltas = function () {
                return this.lastDownDeltas
            }, e
        }()
    }).call(this)
}, function (e, t, i) {
    "use strict";
    e.exports = {
        hasWheelEvent: "onwheel" in document,
        hasMouseWheelEvent: "onmousewheel" in document,
        hasTouch: "ontouchstart" in window || window.TouchEvent || window.DocumentTouch && document instanceof DocumentTouch,
        hasTouchWin: navigator.msMaxTouchPoints && navigator.msMaxTouchPoints > 1,
        hasPointer: !!window.navigator.msPointerEnabled,
        hasKeyDown: "onkeydown" in document,
        isFirefox: navigator.userAgent.indexOf("Firefox") > -1
    }
}, function (e, t, i) {
    "use strict";
    e.exports = function (e) {
        return JSON.parse(JSON.stringify(e))
    }
}, function (e, t, i) {
    "use strict";
    var n = Object.prototype.toString, r = Object.prototype.hasOwnProperty;

    function s(e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }

    e.exports = function (e) {
        if (!e) return console.warn("bindAll requires at least one argument.");
        var t = Array.prototype.slice.call(arguments, 1);
        if (0 === t.length) for (var i in e) r.call(e, i) && "function" == typeof e[i] && "[object Function]" == n.call(e[i]) && t.push(i);
        for (var o = 0; o < t.length; o++) {
            var a = t[o];
            e[a] = s(e[a], e)
        }
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0), r = i(7), s = i(42), o = i(13);

    function a(e) {
        var t = new s(e), i = r(s.prototype.request, t);
        return n.extend(i, s.prototype, t), n.extend(i, t), i
    }

    var l = a(i(10));
    l.Axios = s, l.create = function (e) {
        return a(o(l.defaults, e))
    }, l.Cancel = i(14), l.CancelToken = i(56), l.isCancel = i(9), l.all = function (e) {
        return Promise.all(e)
    }, l.spread = i(57), e.exports = l, e.exports.default = l
}, function (e, t, i) {
    "use strict";
    var n = i(0), r = i(8), s = i(43), o = i(44), a = i(13);

    function l(e) {
        this.defaults = e, this.interceptors = {request: new s, response: new s}
    }

    l.prototype.request = function (e) {
        "string" == typeof e ? (e = arguments[1] || {}).url = arguments[0] : e = e || {}, (e = a(this.defaults, e)).method ? e.method = e.method.toLowerCase() : this.defaults.method ? e.method = this.defaults.method.toLowerCase() : e.method = "get";
        var t = [o, void 0], i = Promise.resolve(e);
        for (this.interceptors.request.forEach(function (e) {
            t.unshift(e.fulfilled, e.rejected)
        }), this.interceptors.response.forEach(function (e) {
            t.push(e.fulfilled, e.rejected)
        }); t.length;) i = i.then(t.shift(), t.shift());
        return i
    }, l.prototype.getUri = function (e) {
        return e = a(this.defaults, e), r(e.url, e.params, e.paramsSerializer).replace(/^\?/, "")
    }, n.forEach(["delete", "get", "head", "options"], function (e) {
        l.prototype[e] = function (t, i) {
            return this.request(a(i || {}, {method: e, url: t}))
        }
    }), n.forEach(["post", "put", "patch"], function (e) {
        l.prototype[e] = function (t, i, n) {
            return this.request(a(n || {}, {method: e, url: t, data: i}))
        }
    }), e.exports = l
}, function (e, t, i) {
    "use strict";
    var n = i(0);

    function r() {
        this.handlers = []
    }

    r.prototype.use = function (e, t) {
        return this.handlers.push({fulfilled: e, rejected: t}), this.handlers.length - 1
    }, r.prototype.eject = function (e) {
        this.handlers[e] && (this.handlers[e] = null)
    }, r.prototype.forEach = function (e) {
        n.forEach(this.handlers, function (t) {
            null !== t && e(t)
        })
    }, e.exports = r
}, function (e, t, i) {
    "use strict";
    var n = i(0), r = i(45), s = i(9), o = i(10);

    function a(e) {
        e.cancelToken && e.cancelToken.throwIfRequested()
    }

    e.exports = function (e) {
        return a(e), e.headers = e.headers || {}, e.data = r(e.data, e.headers, e.transformRequest), e.headers = n.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers), n.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function (t) {
            delete e.headers[t]
        }), (e.adapter || o.adapter)(e).then(function (t) {
            return a(e), t.data = r(t.data, t.headers, e.transformResponse), t
        }, function (t) {
            return s(t) || (a(e), t && t.response && (t.response.data = r(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t)
        })
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);
    e.exports = function (e, t, i) {
        return n.forEach(i, function (i) {
            e = i(e, t)
        }), e
    }
}, function (e, t) {
    var i, n, r = e.exports = {};

    function s() {
        throw new Error("setTimeout has not been defined")
    }

    function o() {
        throw new Error("clearTimeout has not been defined")
    }

    function a(e) {
        if (i === setTimeout) return setTimeout(e, 0);
        if ((i === s || !i) && setTimeout) return i = setTimeout, setTimeout(e, 0);
        try {
            return i(e, 0)
        } catch (t) {
            try {
                return i.call(null, e, 0)
            } catch (t) {
                return i.call(this, e, 0)
            }
        }
    }

    !function () {
        try {
            i = "function" == typeof setTimeout ? setTimeout : s
        } catch (e) {
            i = s
        }
        try {
            n = "function" == typeof clearTimeout ? clearTimeout : o
        } catch (e) {
            n = o
        }
    }();
    var l, u = [], c = !1, h = -1;

    function d() {
        c && l && (c = !1, l.length ? u = l.concat(u) : h = -1, u.length && p())
    }

    function p() {
        if (!c) {
            var e = a(d);
            c = !0;
            for (var t = u.length; t;) {
                for (l = u, u = []; ++h < t;) l && l[h].run();
                h = -1, t = u.length
            }
            l = null, c = !1, function (e) {
                if (n === clearTimeout) return clearTimeout(e);
                if ((n === o || !n) && clearTimeout) return n = clearTimeout, clearTimeout(e);
                try {
                    n(e)
                } catch (t) {
                    try {
                        return n.call(null, e)
                    } catch (t) {
                        return n.call(this, e)
                    }
                }
            }(e)
        }
    }

    function f(e, t) {
        this.fun = e, this.array = t
    }

    function m() {
    }

    r.nextTick = function (e) {
        var t = new Array(arguments.length - 1);
        if (arguments.length > 1) for (var i = 1; i < arguments.length; i++) t[i - 1] = arguments[i];
        u.push(new f(e, t)), 1 !== u.length || c || a(p)
    }, f.prototype.run = function () {
        this.fun.apply(null, this.array)
    }, r.title = "browser", r.browser = !0, r.env = {}, r.argv = [], r.version = "", r.versions = {}, r.on = m, r.addListener = m, r.once = m, r.off = m, r.removeListener = m, r.removeAllListeners = m, r.emit = m, r.prependListener = m, r.prependOnceListener = m, r.listeners = function (e) {
        return []
    }, r.binding = function (e) {
        throw new Error("process.binding is not supported")
    }, r.cwd = function () {
        return "/"
    }, r.chdir = function (e) {
        throw new Error("process.chdir is not supported")
    }, r.umask = function () {
        return 0
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);
    e.exports = function (e, t) {
        n.forEach(e, function (i, n) {
            n !== t && n.toUpperCase() === t.toUpperCase() && (e[t] = i, delete e[n])
        })
    }
}, function (e, t, i) {
    "use strict";
    var n = i(12);
    e.exports = function (e, t, i) {
        var r = i.config.validateStatus;
        i.status && r && !r(i.status) ? t(n("Request failed with status code " + i.status, i.config, null, i.request, i)) : e(i)
    }
}, function (e, t, i) {
    "use strict";
    e.exports = function (e, t, i, n, r) {
        return e.config = t, i && (e.code = i), e.request = n, e.response = r, e.isAxiosError = !0, e.toJSON = function () {
            return {
                message: this.message,
                name: this.name,
                description: this.description,
                number: this.number,
                fileName: this.fileName,
                lineNumber: this.lineNumber,
                columnNumber: this.columnNumber,
                stack: this.stack,
                config: this.config,
                code: this.code
            }
        }, e
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);
    e.exports = n.isStandardBrowserEnv() ? {
        write: function (e, t, i, r, s, o) {
            var a = [];
            a.push(e + "=" + encodeURIComponent(t)), n.isNumber(i) && a.push("expires=" + new Date(i).toGMTString()), n.isString(r) && a.push("path=" + r), n.isString(s) && a.push("domain=" + s), !0 === o && a.push("secure"), document.cookie = a.join("; ")
        }, read: function (e) {
            var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
            return t ? decodeURIComponent(t[3]) : null
        }, remove: function (e) {
            this.write(e, "", Date.now() - 864e5)
        }
    } : {
        write: function () {
        }, read: function () {
            return null
        }, remove: function () {
        }
    }
}, function (e, t, i) {
    "use strict";
    var n = i(52), r = i(53);
    e.exports = function (e, t) {
        return e && !n(t) ? r(e, t) : t
    }
}, function (e, t, i) {
    "use strict";
    e.exports = function (e) {
        return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
    }
}, function (e, t, i) {
    "use strict";
    e.exports = function (e, t) {
        return t ? e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "") : e
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0),
        r = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
    e.exports = function (e) {
        var t, i, s, o = {};
        return e ? (n.forEach(e.split("\n"), function (e) {
            if (s = e.indexOf(":"), t = n.trim(e.substr(0, s)).toLowerCase(), i = n.trim(e.substr(s + 1)), t) {
                if (o[t] && r.indexOf(t) >= 0) return;
                o[t] = "set-cookie" === t ? (o[t] ? o[t] : []).concat([i]) : o[t] ? o[t] + ", " + i : i
            }
        }), o) : o
    }
}, function (e, t, i) {
    "use strict";
    var n = i(0);
    e.exports = n.isStandardBrowserEnv() ? function () {
        var e, t = /(msie|trident)/i.test(navigator.userAgent), i = document.createElement("a");

        function r(e) {
            var n = e;
            return t && (i.setAttribute("href", n), n = i.href), i.setAttribute("href", n), {
                href: i.href,
                protocol: i.protocol ? i.protocol.replace(/:$/, "") : "",
                host: i.host,
                search: i.search ? i.search.replace(/^\?/, "") : "",
                hash: i.hash ? i.hash.replace(/^#/, "") : "",
                hostname: i.hostname,
                port: i.port,
                pathname: "/" === i.pathname.charAt(0) ? i.pathname : "/" + i.pathname
            }
        }

        return e = r(window.location.href), function (t) {
            var i = n.isString(t) ? r(t) : t;
            return i.protocol === e.protocol && i.host === e.host
        }
    }() : function () {
        return !0
    }
}, function (e, t, i) {
    "use strict";
    var n = i(14);

    function r(e) {
        if ("function" != typeof e) throw new TypeError("executor must be a function.");
        var t;
        this.promise = new Promise(function (e) {
            t = e
        });
        var i = this;
        e(function (e) {
            i.reason || (i.reason = new n(e), t(i.reason))
        })
    }

    r.prototype.throwIfRequested = function () {
        if (this.reason) throw this.reason
    }, r.source = function () {
        var e;
        return {
            token: new r(function (t) {
                e = t
            }), cancel: e
        }
    }, e.exports = r
}, function (e, t, i) {
    "use strict";
    e.exports = function (e) {
        return function (t) {
            return e.apply(null, t)
        }
    }
}, function (e, t, i) {
    var n, r;
    "undefined" != typeof window && window, void 0 === (r = "function" == typeof (n = function () {
        "use strict";

        function e() {
        }

        var t = e.prototype;
        return t.on = function (e, t) {
            if (e && t) {
                var i = this._events = this._events || {}, n = i[e] = i[e] || [];
                return -1 == n.indexOf(t) && n.push(t), this
            }
        }, t.once = function (e, t) {
            if (e && t) {
                this.on(e, t);
                var i = this._onceEvents = this._onceEvents || {};
                return (i[e] = i[e] || {})[t] = !0, this
            }
        }, t.off = function (e, t) {
            var i = this._events && this._events[e];
            if (i && i.length) {
                var n = i.indexOf(t);
                return -1 != n && i.splice(n, 1), this
            }
        }, t.emitEvent = function (e, t) {
            var i = this._events && this._events[e];
            if (i && i.length) {
                i = i.slice(0), t = t || [];
                for (var n = this._onceEvents && this._onceEvents[e], r = 0; r < i.length; r++) {
                    var s = i[r];
                    n && n[s] && (this.off(e, s), delete n[s]), s.apply(this, t)
                }
                return this
            }
        }, t.allOff = function () {
            delete this._events, delete this._onceEvents
        }, e
    }) ? n.call(t, i, t, e) : n) || (e.exports = r)
}, function (e, t, i) {
    "use strict";
    i.r(t), i(33);
    var n = i(2), r = i.n(n), s = {
        dom: {doc: document.documentElement, body: document.body, scroll: document.querySelector("[data-smooth]")},
        bounds: {ww: window.innerWidth, wh: window.innerHeight, scroll: 0},
        flags: {
            home: !1,
            locked: !0,
            dropdown: !1,
            dropdownFilterCateogry: !1,
            workModal: !1,
            newsModal: !1,
            showVideo: !1,
            wheelVideo: !0,
            sectionView: "home",
            mouseWheelHome: !0,
            clickToSkape: !0,
            sendingSubscribe: !1,
            resize: !1,
            active: !0,
            first: !1,
            windows: -1 !== ["Win32", "Win64", "Windows", "WinCE"].indexOf(window.navigator.platform)
        }
    };
    Object.assign(s, r.a.getInfos()), s.flags.smooth = s.isDesktop;
    var o = s;

    function a(e) {
        if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return e
    }

    function l(e, t) {
        e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e.__proto__ = t
    }

    var u, c, h, d, p, f, m, g, v = {autoSleep: 120, force3D: "auto", nullTargetWarn: 1, units: {lineHeight: ""}},
        y = {duration: .5, overwrite: !1, delay: 0}, b = 1e8, w = 2 * Math.PI, D = w / 4, T = 0, C = Math.sqrt,
        k = Math.cos, E = Math.sin, _ = function (e) {
            return "string" == typeof e
        }, x = function (e) {
            return "function" == typeof e
        }, S = function (e) {
            return "number" == typeof e
        }, P = function (e) {
            return void 0 === e
        }, O = function (e) {
            return "object" == typeof e
        }, A = function (e) {
            return !1 !== e
        }, M = function () {
            return "undefined" != typeof window
        }, L = function (e) {
            return x(e) || _(e)
        }, F = Array.isArray, j = /(?:-?\.?\d|\.)+/gi, B = /[-+=.]*\d+[.e\-+]*\d*[e\-\+]*\d*/g,
        I = /[-+=.]*\d+[.e-]*\d*[a-z%]*/g, R = /[-+=.]*\d+(?:\.|e-|e)*\d*/gi, N = /\(([^()]+)\)/i, z = /[+-]=-?[\.\d]+/,
        H = /[#\-+.]*\b[a-z\d-=+%.]+/gi, V = {}, q = {}, W = function (e) {
            return (q = ge(e, V)) && Gt
        }, X = function (e, t) {
            return console.warn("Invalid property", e, "set to", t, "Missing plugin? gsap.registerPlugin()")
        }, Y = function (e, t) {
            return !t && console.warn(e)
        }, U = function (e, t) {
            return e && (V[e] = t) && q && (q[e] = t) || V
        }, G = function () {
            return 0
        }, $ = {}, K = [], J = {}, Q = {}, Z = {}, ee = 30, te = [], ie = "", ne = function (e) {
            var t, i, n = e[0];
            if (O(n) || x(n) || (e = [e]), !(t = (n._gsap || {}).harness)) {
                for (i = te.length; i-- && !te[i].targetTest(n);) ;
                t = te[i]
            }
            for (i = e.length; i--;) e[i] && (e[i]._gsap || (e[i]._gsap = new wt(e[i], t))) || e.splice(i, 1);
            return e
        }, re = function (e) {
            return e._gsap || ne(He(e))[0]._gsap
        }, se = function (e, t) {
            var i = e[t];
            return x(i) ? e[t]() : P(i) && e.getAttribute(t) || i
        }, oe = function (e, t) {
            return (e = e.split(",")).forEach(t) || e
        }, ae = function (e) {
            return Math.round(1e5 * e) / 1e5 || 0
        }, le = function (e, t) {
            for (var i = t.length, n = 0; e.indexOf(t[n]) < 0 && ++n < i;) ;
            return n < i
        }, ue = function (e, t, i) {
            var n, r = S(e[1]), s = (r ? 2 : 1) + (t < 2 ? 0 : 1), o = e[s];
            if (r && (o.duration = e[1]), o.parent = i, t) {
                for (n = o; i && !("immediateRender" in n);) n = i.vars.defaults || {}, i = A(i.vars.inherit) && i.parent;
                o.immediateRender = A(n.immediateRender), t < 2 ? o.runBackwards = 1 : o.startAt = e[s - 1]
            }
            return o
        }, ce = function () {
            var e, t, i = K.length, n = K.slice(0);
            for (J = {}, K.length = 0, e = 0; e < i; e++) (t = n[e]) && t._lazy && (t.render(t._lazy[0], t._lazy[1], !0)._lazy = 0)
        }, he = function (e, t, i, n) {
            K.length && ce(), e.render(t, i, n), K.length && ce()
        }, de = function (e) {
            var t = parseFloat(e);
            return (t || 0 === t) && (e + "").match(H).length < 2 ? t : e
        }, pe = function (e) {
            return e
        }, fe = function (e, t) {
            for (var i in t) i in e || (e[i] = t[i]);
            return e
        }, me = function (e, t) {
            for (var i in t) i in e || "duration" === i || "ease" === i || (e[i] = t[i])
        }, ge = function (e, t) {
            for (var i in t) e[i] = t[i];
            return e
        }, ve = function e(t, i) {
            for (var n in i) t[n] = O(i[n]) ? e(t[n] || (t[n] = {}), i[n]) : i[n];
            return t
        }, ye = function (e, t) {
            var i, n = {};
            for (i in e) i in t || (n[i] = e[i]);
            return n
        }, be = function (e) {
            var t = e.parent || u, i = e.keyframes ? me : fe;
            if (A(e.inherit)) for (; t;) i(e, t.vars.defaults), t = t.parent || t._dp;
            return e
        }, we = function (e, t, i, n) {
            void 0 === i && (i = "_first"), void 0 === n && (n = "_last");
            var r = t._prev, s = t._next;
            r ? r._next = s : e[i] === t && (e[i] = s), s ? s._prev = r : e[n] === t && (e[n] = r), t._next = t._prev = t.parent = null
        }, De = function (e, t) {
            e.parent && (!t || e.parent.autoRemoveChildren) && e.parent.remove(e), e._act = 0
        }, Te = function (e) {
            for (var t = e; t;) t._dirty = 1, t = t.parent;
            return e
        }, Ce = function (e) {
            return e._repeat ? ke(e._tTime, e = e.duration() + e._rDelay) * e : 0
        }, ke = function (e, t) {
            return (e /= t) && ~~e === e ? ~~e - 1 : ~~e
        }, Ee = function (e, t) {
            return (e - t._start) * t._ts + (t._ts >= 0 ? 0 : t._dirty ? t.totalDuration() : t._tDur)
        }, _e = function (e) {
            return e._end = ae(e._start + (e._tDur / Math.abs(e._ts || e._rts || 1e-8) || 0))
        }, xe = function (e, t) {
            var i = e._dp;
            return i && i.smoothChildTiming && e._ts && (e._start = ae(e._dp._time - (e._ts > 0 ? t / e._ts : ((e._dirty ? e.totalDuration() : e._tDur) - t) / -e._ts)), _e(e), i._dirty || Te(i)), e
        }, Se = function (e, t) {
            var i;
            if ((t._time || t._initted && !t._dur) && (i = Ee(e.rawTime(), t), (!t._dur || Ie(0, t.totalDuration(), i) - t._tTime > 1e-8) && t.render(i, !0)), Te(e)._dp && e._initted && e._time >= e._dur && e._ts) {
                if (e._dur < e.duration()) for (i = e; i._dp;) i.rawTime() >= 0 && i.totalTime(i._tTime), i = i._dp;
                e._zTime = -1e-8
            }
        }, Pe = function (e, t, i, n) {
            return t.parent && De(t), t._start = ae(i + t._delay), t._end = ae(t._start + (t.totalDuration() / Math.abs(t.timeScale()) || 0)), function (e, t, i, n, r) {
                void 0 === i && (i = "_first"), void 0 === n && (n = "_last");
                var s, o = e[n];
                if (r) for (s = t[r]; o && o[r] > s;) o = o._prev;
                o ? (t._next = o._next, o._next = t) : (t._next = e[i], e[i] = t), t._next ? t._next._prev = t : e[n] = t, t._prev = o, t.parent = t._dp = e
            }(e, t, "_first", "_last", e._sort ? "_start" : 0), e._recent = t, n || Se(e, t), e
        }, Oe = function (e, t) {
            return (V.ScrollTrigger || X("scrollTrigger", t)) && V.ScrollTrigger.create(t, e)
        }, Ae = function (e, t, i, n) {
            return _t(e, t), e._initted ? !i && e._pt && (e._dur && !1 !== e.vars.lazy || !e._dur && e.vars.lazy) && f !== at.frame ? (K.push(e), e._lazy = [t, n], 1) : void 0 : 1
        }, Me = function (e, t, i) {
            var n = e._repeat, r = ae(t) || 0;
            return e._dur = r, e._tDur = n ? n < 0 ? 1e10 : ae(r * (n + 1) + e._rDelay * n) : r, e._time > r && (e._time = r, e._tTime = Math.min(e._tTime, e._tDur)), !i && Te(e.parent), e.parent && _e(e), e
        }, Le = function (e) {
            return e instanceof Tt ? Te(e) : Me(e, e._dur)
        }, Fe = {_start: 0, endTime: G}, je = function e(t, i) {
            var n, r, s = t.labels, o = t._recent || Fe, a = t.duration() >= b ? o.endTime(!1) : t._dur;
            return _(i) && (isNaN(i) || i in s) ? "<" === (n = i.charAt(0)) || ">" === n ? ("<" === n ? o._start : o.endTime(o._repeat >= 0)) + (parseFloat(i.substr(1)) || 0) : (n = i.indexOf("=")) < 0 ? (i in s || (s[i] = a), s[i]) : (r = +(i.charAt(n - 1) + i.substr(n + 1)), n > 1 ? e(t, i.substr(0, n - 1)) + r : a + r) : null == i ? a : +i
        }, Be = function (e, t) {
            return e || 0 === e ? t(e) : t
        }, Ie = function (e, t, i) {
            return i < e ? e : i > t ? t : i
        }, Re = function (e) {
            return (e + "").substr((parseFloat(e) + "").length)
        }, Ne = [].slice, ze = function (e, t) {
            return e && O(e) && "length" in e && (!t && !e.length || e.length - 1 in e && O(e[0])) && !e.nodeType && e !== c
        }, He = function (e, t) {
            return !_(e) || t || !h && lt() ? F(e) ? function (e, t, i) {
                return void 0 === i && (i = []), e.forEach(function (e) {
                    var n;
                    return _(e) && !t || ze(e, 1) ? (n = i).push.apply(n, He(e)) : i.push(e)
                }) || i
            }(e, t) : ze(e) ? Ne.call(e, 0) : e ? [e] : [] : Ne.call(d.querySelectorAll(e), 0)
        }, Ve = function (e) {
            return e.sort(function () {
                return .5 - Math.random()
            })
        }, qe = function (e) {
            if (x(e)) return e;
            var t = O(e) ? e : {each: e}, i = mt(t.ease), n = t.from || 0, r = parseFloat(t.base) || 0, s = {},
                o = n > 0 && n < 1, a = isNaN(n) || o, l = t.axis, u = n, c = n;
            return _(n) ? u = c = {
                center: .5,
                edges: .5,
                end: 1
            }[n] || 0 : !o && a && (u = n[0], c = n[1]), function (e, o, h) {
                var d, p, f, m, g, v, y, w, D, T = (h || t).length, k = s[T];
                if (!k) {
                    if (!(D = "auto" === t.grid ? 0 : (t.grid || [1, b])[1])) {
                        for (y = -b; y < (y = h[D++].getBoundingClientRect().left) && D < T;) ;
                        D--
                    }
                    for (k = s[T] = [], d = a ? Math.min(D, T) * u - .5 : n % D, p = a ? T * c / D - .5 : n / D | 0, y = 0, w = b, v = 0; v < T; v++) f = v % D - d, m = p - (v / D | 0), k[v] = g = l ? Math.abs("y" === l ? m : f) : C(f * f + m * m), g > y && (y = g), g < w && (w = g);
                    "random" === n && Ve(k), k.max = y - w, k.min = w, k.v = T = (parseFloat(t.amount) || parseFloat(t.each) * (D > T ? T - 1 : l ? "y" === l ? T / D : D : Math.max(D, T / D)) || 0) * ("edges" === n ? -1 : 1), k.b = T < 0 ? r - T : r, k.u = Re(t.amount || t.each) || 0, i = i && T < 0 ? pt(i) : i
                }
                return T = (k[e] - k.min) / k.max || 0, ae(k.b + (i ? i(T) : T) * k.v) + k.u
            }
        }, We = function (e) {
            var t = e < 1 ? Math.pow(10, (e + "").length - 2) : 1;
            return function (i) {
                return Math.floor(Math.round(parseFloat(i) / e) * e * t) / t + (S(i) ? 0 : Re(i))
            }
        }, Xe = function (e, t) {
            var i, n, r = F(e);
            return !r && O(e) && (i = r = e.radius || b, e.values ? (e = He(e.values), (n = !S(e[0])) && (i *= i)) : e = We(e.increment)), Be(t, r ? x(e) ? function (t) {
                return n = e(t), Math.abs(n - t) <= i ? n : t
            } : function (t) {
                for (var r, s, o = parseFloat(n ? t.x : t), a = parseFloat(n ? t.y : 0), l = b, u = 0, c = e.length; c--;) (r = n ? (r = e[c].x - o) * r + (s = e[c].y - a) * s : Math.abs(e[c] - o)) < l && (l = r, u = c);
                return u = !i || l <= i ? e[u] : t, n || u === t || S(t) ? u : u + Re(t)
            } : We(e))
        }, Ye = function (e, t, i, n) {
            return Be(F(e) ? !t : !0 === i ? !!(i = 0) : !n, function () {
                return F(e) ? e[~~(Math.random() * e.length)] : (i = i || 1e-5) && (n = i < 1 ? Math.pow(10, (i + "").length - 2) : 1) && Math.floor(Math.round((e + Math.random() * (t - e)) / i) * i * n) / n
            })
        }, Ue = function (e, t, i) {
            return Be(i, function (i) {
                return e[~~t(i)]
            })
        }, Ge = function (e) {
            for (var t, i, n, r, s = 0, o = ""; ~(t = e.indexOf("random(", s));) n = e.indexOf(")", t), r = "[" === e.charAt(t + 7), i = e.substr(t + 7, n - t - 7).match(r ? H : j), o += e.substr(s, t - s) + Ye(r ? i : +i[0], +i[1], +i[2] || 1e-5), s = n + 1;
            return o + e.substr(s, e.length - s)
        }, $e = function (e, t, i, n, r) {
            var s = t - e, o = n - i;
            return Be(r, function (t) {
                return i + ((t - e) / s * o || 0)
            })
        }, Ke = function (e, t, i) {
            var n, r, s, o = e.labels, a = b;
            for (n in o) (r = o[n] - t) < 0 == !!i && r && a > (r = Math.abs(r)) && (s = n, a = r);
            return s
        }, Je = function (e, t, i) {
            var n, r, s = e.vars, o = s[t];
            if (o) return n = s[t + "Params"], r = s.callbackScope || e, i && K.length && ce(), n ? o.apply(r, n) : o.call(r)
        }, Qe = function (e) {
            return De(e), e.progress() < 1 && Je(e, "onInterrupt"), e
        }, Ze = {
            aqua: [0, 255, 255],
            lime: [0, 255, 0],
            silver: [192, 192, 192],
            black: [0, 0, 0],
            maroon: [128, 0, 0],
            teal: [0, 128, 128],
            blue: [0, 0, 255],
            navy: [0, 0, 128],
            white: [255, 255, 255],
            olive: [128, 128, 0],
            yellow: [255, 255, 0],
            orange: [255, 165, 0],
            gray: [128, 128, 128],
            purple: [128, 0, 128],
            green: [0, 128, 0],
            red: [255, 0, 0],
            pink: [255, 192, 203],
            cyan: [0, 255, 255],
            transparent: [255, 255, 255, 0]
        }, et = function (e, t, i) {
            return 255 * (6 * (e = e < 0 ? e + 1 : e > 1 ? e - 1 : e) < 1 ? t + (i - t) * e * 6 : e < .5 ? i : 3 * e < 2 ? t + (i - t) * (2 / 3 - e) * 6 : t) + .5 | 0
        }, tt = function (e, t, i) {
            var n, r, s, o, a, l, u, c, h, d, p = e ? S(e) ? [e >> 16, e >> 8 & 255, 255 & e] : 0 : Ze.black;
            if (!p) {
                if ("," === e.substr(-1) && (e = e.substr(0, e.length - 1)), Ze[e]) p = Ze[e]; else if ("#" === e.charAt(0)) 4 === e.length && (n = e.charAt(1), r = e.charAt(2), s = e.charAt(3), e = "#" + n + n + r + r + s + s), p = [(e = parseInt(e.substr(1), 16)) >> 16, e >> 8 & 255, 255 & e]; else if ("hsl" === e.substr(0, 3)) if (p = d = e.match(j), t) {
                    if (~e.indexOf("=")) return p = e.match(B), i && p.length < 4 && (p[3] = 1), p
                } else o = +p[0] % 360 / 360, a = +p[1] / 100, n = 2 * (l = +p[2] / 100) - (r = l <= .5 ? l * (a + 1) : l + a - l * a), p.length > 3 && (p[3] *= 1), p[0] = et(o + 1 / 3, n, r), p[1] = et(o, n, r), p[2] = et(o - 1 / 3, n, r); else p = e.match(j) || Ze.transparent;
                p = p.map(Number)
            }
            return t && !d && (n = p[0] / 255, r = p[1] / 255, s = p[2] / 255, l = ((u = Math.max(n, r, s)) + (c = Math.min(n, r, s))) / 2, u === c ? o = a = 0 : (h = u - c, a = l > .5 ? h / (2 - u - c) : h / (u + c), o = u === n ? (r - s) / h + (r < s ? 6 : 0) : u === r ? (s - n) / h + 2 : (n - r) / h + 4, o *= 60), p[0] = ~~(o + .5), p[1] = ~~(100 * a + .5), p[2] = ~~(100 * l + .5)), i && p.length < 4 && (p[3] = 1), p
        }, it = function (e) {
            var t = [], i = [], n = -1;
            return e.split(rt).forEach(function (e) {
                var r = e.match(I) || [];
                t.push.apply(t, r), i.push(n += r.length + 1)
            }), t.c = i, t
        }, nt = function (e, t, i) {
            var n, r, s, o, a = "", l = (e + a).match(rt), u = t ? "hsla(" : "rgba(", c = 0;
            if (!l) return e;
            if (l = l.map(function (e) {
                return (e = tt(e, t, 1)) && u + (t ? e[0] + "," + e[1] + "%," + e[2] + "%," + e[3] : e.join(",")) + ")"
            }), i && (s = it(e), (n = i.c).join(a) !== s.c.join(a))) for (o = (r = e.replace(rt, "1").split(I)).length - 1; c < o; c++) a += r[c] + (~n.indexOf(c) ? l.shift() || u + "0,0,0,0)" : (s.length ? s : l.length ? l : i).shift());
            if (!r) for (o = (r = e.split(rt)).length - 1; c < o; c++) a += r[c] + l[c];
            return a + r[o]
        }, rt = function () {
            var e, t = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";
            for (e in Ze) t += "|" + e + "\\b";
            return new RegExp(t + ")", "gi")
        }(), st = /hsl[a]?\(/, ot = function (e) {
            var t, i = e.join(" ");
            if (rt.lastIndex = 0, rt.test(i)) return t = st.test(i), e[1] = nt(e[1], t), e[0] = nt(e[0], t, it(e[1])), !0
        }, at = function () {
            var e, t, i, n, r = Date.now, s = 500, o = 33, a = r(), l = a, u = 1 / 240, f = u, m = [], v = function i(c) {
                var h, d, p = r() - l, g = !0 === c;
                p > s && (a += p - o), l += p, n.time = (l - a) / 1e3, ((h = n.time - f) > 0 || g) && (n.frame++, f += h + (h >= u ? .004 : u - h), d = 1), g || (e = t(i)), d && m.forEach(function (e) {
                    return e(n.time, p, n.frame, c)
                })
            };
            return n = {
                time: 0, frame: 0, tick: function () {
                    v(!0)
                }, wake: function () {
                    p && (!h && M() && (c = h = window, d = c.document || {}, V.gsap = Gt, (c.gsapVersions || (c.gsapVersions = [])).push(Gt.version), W(q || c.GreenSockGlobals || !c.gsap && c || {}), i = c.requestAnimationFrame), e && n.sleep(), t = i || function (e) {
                        return setTimeout(e, 1e3 * (f - n.time) + 1 | 0)
                    }, g = 1, v(2))
                }, sleep: function () {
                    (i ? c.cancelAnimationFrame : clearTimeout)(e), g = 0, t = G
                }, lagSmoothing: function (e, t) {
                    s = e || 1e8, o = Math.min(t, s, 0)
                }, fps: function (e) {
                    u = 1 / (e || 240), f = n.time + u
                }, add: function (e) {
                    m.indexOf(e) < 0 && m.push(e), lt()
                }, remove: function (e) {
                    var t;
                    ~(t = m.indexOf(e)) && m.splice(t, 1)
                }, _listeners: m
            }
        }(), lt = function () {
            return !g && at.wake()
        }, ut = {}, ct = /^[\d.\-M][\d.\-,\s]/, ht = /["']/g, dt = function (e) {
            for (var t, i, n, r = {}, s = e.substr(1, e.length - 3).split(":"), o = s[0], a = 1, l = s.length; a < l; a++) i = s[a], t = a !== l - 1 ? i.lastIndexOf(",") : i.length, n = i.substr(0, t), r[o] = isNaN(n) ? n.replace(ht, "").trim() : +n, o = i.substr(t + 1).trim();
            return r
        }, pt = function (e) {
            return function (t) {
                return 1 - e(1 - t)
            }
        }, ft = function e(t, i) {
            for (var n, r = t._first; r;) r instanceof Tt ? e(r, i) : !r.vars.yoyoEase || r._yoyo && r._repeat || r._yoyo === i || (r.timeline ? e(r.timeline, i) : (n = r._ease, r._ease = r._yEase, r._yEase = n, r._yoyo = i)), r = r._next
        }, mt = function (e, t) {
            return e && (x(e) ? e : ut[e] || function (e) {
                var t = (e + "").split("("), i = ut[t[0]];
                return i && t.length > 1 && i.config ? i.config.apply(null, ~e.indexOf("{") ? [dt(t[1])] : N.exec(e)[1].split(",").map(de)) : ut._CE && ct.test(e) ? ut._CE("", e) : i
            }(e)) || t
        }, gt = function (e, t, i, n) {
            void 0 === i && (i = function (e) {
                return 1 - t(1 - e)
            }), void 0 === n && (n = function (e) {
                return e < .5 ? t(2 * e) / 2 : 1 - t(2 * (1 - e)) / 2
            });
            var r, s = {easeIn: t, easeOut: i, easeInOut: n};
            return oe(e, function (e) {
                for (var t in ut[e] = V[e] = s, ut[r = e.toLowerCase()] = i, s) ut[r + ("easeIn" === t ? ".in" : "easeOut" === t ? ".out" : ".inOut")] = ut[e + "." + t] = s[t]
            }), s
        }, vt = function (e) {
            return function (t) {
                return t < .5 ? (1 - e(1 - 2 * t)) / 2 : .5 + e(2 * (t - .5)) / 2
            }
        }, yt = function e(t, i, n) {
            var r = i >= 1 ? i : 1, s = (n || (t ? .3 : .45)) / (i < 1 ? i : 1), o = s / w * (Math.asin(1 / r) || 0),
                a = function (e) {
                    return 1 === e ? 1 : r * Math.pow(2, -10 * e) * E((e - o) * s) + 1
                }, l = "out" === t ? a : "in" === t ? function (e) {
                    return 1 - a(1 - e)
                } : vt(a);
            return s = w / s, l.config = function (i, n) {
                return e(t, i, n)
            }, l
        }, bt = function e(t, i) {
            void 0 === i && (i = 1.70158);
            var n = function (e) {
                return e ? --e * e * ((i + 1) * e + i) + 1 : 0
            }, r = "out" === t ? n : "in" === t ? function (e) {
                return 1 - n(1 - e)
            } : vt(n);
            return r.config = function (i) {
                return e(t, i)
            }, r
        };
    oe("Linear,Quad,Cubic,Quart,Quint,Strong", function (e, t) {
        var i = t < 5 ? t + 1 : t;
        gt(e + ",Power" + (i - 1), t ? function (e) {
            return Math.pow(e, i)
        } : function (e) {
            return e
        }, function (e) {
            return 1 - Math.pow(1 - e, i)
        }, function (e) {
            return e < .5 ? Math.pow(2 * e, i) / 2 : 1 - Math.pow(2 * (1 - e), i) / 2
        })
    }), ut.Linear.easeNone = ut.none = ut.Linear.easeIn, gt("Elastic", yt("in"), yt("out"), yt()), function (e, t) {
        var i = function (i) {
            return i < .36363636363636365 ? e * i * i : i < .7272727272727273 ? e * Math.pow(i - 1.5 / t, 2) + .75 : i < .9090909090909092 ? e * (i -= 2.25 / t) * i + .9375 : e * Math.pow(i - 2.625 / t, 2) + .984375
        };
        gt("Bounce", function (e) {
            return 1 - i(1 - e)
        }, i)
    }(7.5625, 2.75), gt("Expo", function (e) {
        return e ? Math.pow(2, 10 * (e - 1)) : 0
    }), gt("Circ", function (e) {
        return -(C(1 - e * e) - 1)
    }), gt("Sine", function (e) {
        return 1 === e ? 1 : 1 - k(e * D)
    }), gt("Back", bt("in"), bt("out"), bt()), ut.SteppedEase = ut.steps = V.SteppedEase = {
        config: function (e, t) {
            void 0 === e && (e = 1);
            var i = 1 / e, n = e + (t ? 0 : 1), r = t ? 1 : 0;
            return function (e) {
                return ((n * Ie(0, 1 - 1e-8, e) | 0) + r) * i
            }
        }
    }, y.ease = ut["quad.out"], oe("onComplete,onUpdate,onStart,onRepeat,onReverseComplete,onInterrupt", function (e) {
        return ie += e + "," + e + "Params,"
    });
    var wt = function (e, t) {
        this.id = T++, e._gsap = this, this.target = e, this.harness = t, this.get = t ? t.get : se, this.set = t ? t.getSetter : jt
    }, Dt = function () {
        function e(e, t) {
            var i = e.parent || u;
            this.vars = e, this._delay = +e.delay || 0, (this._repeat = e.repeat || 0) && (this._rDelay = e.repeatDelay || 0, this._yoyo = !!e.yoyo || !!e.yoyoEase), this._ts = 1, Me(this, +e.duration, 1), this.data = e.data, g || at.wake(), i && Pe(i, this, t || 0 === t ? t : i._time, 1), e.reversed && this.reverse(), e.paused && this.paused(!0)
        }

        var t = e.prototype;
        return t.delay = function (e) {
            return e || 0 === e ? (this.parent && this.parent.smoothChildTiming && this.startTime(this._start + e - this._delay), this._delay = e, this) : this._delay
        }, t.duration = function (e) {
            return arguments.length ? this.totalDuration(this._repeat > 0 ? e + (e + this._rDelay) * this._repeat : e) : this.totalDuration() && this._dur
        }, t.totalDuration = function (e) {
            if (!arguments.length) return this._tDur;
            this._dirty = 0;
            var t = this._time / this._dur || 0;
            return Me(this, this._repeat < 0 ? e : (e - this._repeat * this._rDelay) / (this._repeat + 1)), this._tTime ? xe(this, t * e + Ce(this)) : this
        }, t.totalTime = function (e, t) {
            if (lt(), !arguments.length) return this._tTime;
            var i = this._dp;
            if (i && i.smoothChildTiming && this._ts) {
                for (xe(this, e); i.parent;) i.parent._time !== i._start + (i._ts >= 0 ? i._tTime / i._ts : (i.totalDuration() - i._tTime) / -i._ts) && i.totalTime(i._tTime, !0), i = i.parent;
                !this.parent && this._dp.autoRemoveChildren && (this._ts > 0 && e < this._tDur || this._ts < 0 && e > 0 || !this._tDur && !e) && Pe(this._dp, this, this._start - this._delay)
            }
            return (this._tTime !== e || !this._dur && !t || this._initted && 1e-8 === Math.abs(this._zTime) || !e && !this._initted) && (this._ts || (this._pTime = e), he(this, e, t)), this
        }, t.time = function (e, t) {
            return arguments.length ? this.totalTime(Math.min(this.totalDuration(), e + Ce(this)) % this._dur || (e ? this._dur : 0), t) : this._time
        }, t.totalProgress = function (e, t) {
            return arguments.length ? this.totalTime(this.totalDuration() * e, t) : this.totalDuration() ? Math.min(1, this._tTime / this._tDur) : this.ratio
        }, t.progress = function (e, t) {
            return arguments.length ? this.totalTime(this.duration() * (!this._yoyo || 1 & this.iteration() ? e : 1 - e) + Ce(this), t) : this.duration() ? Math.min(1, this._time / this._dur) : this.ratio
        }, t.iteration = function (e, t) {
            var i = this.duration() + this._rDelay;
            return arguments.length ? this.totalTime(this._time + (e - 1) * i, t) : this._repeat ? ke(this._tTime, i) + 1 : 1
        }, t.timeScale = function (e) {
            if (!arguments.length) return -1e-8 === this._rts ? 0 : this._rts;
            if (this._rts === e) return this;
            var t = this.parent && this._ts ? Ee(this.parent._time, this) : this._tTime;
            return this._rts = +e || 0, this._ts = this._ps || -1e-8 === e ? 0 : this._rts, function (e) {
                for (var t = e.parent; t && t.parent;) t._dirty = 1, t.totalDuration(), t = t.parent;
                return e
            }(this.totalTime(Ie(-this._delay, this._tDur, t), !0))
        }, t.paused = function (e) {
            return arguments.length ? (this._ps !== e && (this._ps = e, e ? (this._pTime = this._tTime || Math.max(-this._delay, this.rawTime()), this._ts = this._act = 0) : (lt(), this._ts = this._rts, this.totalTime(this.parent && !this.parent.smoothChildTiming ? this.rawTime() : this._tTime || this._pTime, 1 === this.progress() && (this._tTime -= 1e-8) && 1e-8 !== Math.abs(this._zTime)))), this) : this._ps
        }, t.startTime = function (e) {
            if (arguments.length) {
                this._start = e;
                var t = this.parent || this._dp;
                return t && (t._sort || !this.parent) && Pe(t, this, e - this._delay), this
            }
            return this._start
        }, t.endTime = function (e) {
            return this._start + (A(e) ? this.totalDuration() : this.duration()) / Math.abs(this._ts)
        }, t.rawTime = function (e) {
            var t = this.parent || this._dp;
            return t ? e && (!this._ts || this._repeat && this._time && this.totalProgress() < 1) ? this._tTime % (this._dur + this._rDelay) : this._ts ? Ee(t.rawTime(e), this) : this._tTime : this._tTime
        }, t.globalTime = function (e) {
            for (var t = this, i = arguments.length ? e : t.rawTime(); t;) i = t._start + i / (t._ts || 1), t = t._dp;
            return i
        }, t.repeat = function (e) {
            return arguments.length ? (this._repeat = e, Le(this)) : this._repeat
        }, t.repeatDelay = function (e) {
            return arguments.length ? (this._rDelay = e, Le(this)) : this._rDelay
        }, t.yoyo = function (e) {
            return arguments.length ? (this._yoyo = e, this) : this._yoyo
        }, t.seek = function (e, t) {
            return this.totalTime(je(this, e), A(t))
        }, t.restart = function (e, t) {
            return this.play().totalTime(e ? -this._delay : 0, A(t))
        }, t.play = function (e, t) {
            return null != e && this.seek(e, t), this.reversed(!1).paused(!1)
        }, t.reverse = function (e, t) {
            return null != e && this.seek(e || this.totalDuration(), t), this.reversed(!0).paused(!1)
        }, t.pause = function (e, t) {
            return null != e && this.seek(e, t), this.paused(!0)
        }, t.resume = function () {
            return this.paused(!1)
        }, t.reversed = function (e) {
            return arguments.length ? (!!e !== this.reversed() && this.timeScale(-this._rts || (e ? -1e-8 : 0)), this) : this._rts < 0
        }, t.invalidate = function () {
            return this._initted = 0, this._zTime = -1e-8, this
        }, t.isActive = function () {
            var e, t = this.parent || this._dp, i = this._start;
            return !(t && !(this._ts && this._initted && t.isActive() && (e = t.rawTime(!0)) >= i && e < this.endTime(!0) - 1e-8))
        }, t.eventCallback = function (e, t, i) {
            var n = this.vars;
            return arguments.length > 1 ? (t ? (n[e] = t, i && (n[e + "Params"] = i), "onUpdate" === e && (this._onUpdate = t)) : delete n[e], this) : n[e]
        }, t.then = function (e) {
            var t = this;
            return new Promise(function (i) {
                var n = x(e) ? e : pe, r = function () {
                    var e = t.then;
                    t.then = null, x(n) && (n = n(t)) && (n.then || n === t) && (t.then = e), i(n), t.then = e
                };
                t._initted && 1 === t.totalProgress() && t._ts >= 0 || !t._tTime && t._ts < 0 ? r() : t._prom = r
            })
        }, t.kill = function () {
            Qe(this)
        }, e
    }();
    fe(Dt.prototype, {
        _time: 0,
        _start: 0,
        _end: 0,
        _tTime: 0,
        _tDur: 0,
        _dirty: 0,
        _repeat: 0,
        _yoyo: !1,
        parent: null,
        _initted: !1,
        _rDelay: 0,
        _ts: 1,
        _dp: 0,
        ratio: 0,
        _zTime: -1e-8,
        _prom: 0,
        _ps: !1,
        _rts: 1
    });
    var Tt = function (e) {
        function t(t, i) {
            var n;
            return void 0 === t && (t = {}), (n = e.call(this, t, i) || this).labels = {}, n.smoothChildTiming = !!t.smoothChildTiming, n.autoRemoveChildren = !!t.autoRemoveChildren, n._sort = A(t.sortChildren), n.parent && Se(n.parent, a(n)), t.scrollTrigger && Oe(a(n), t.scrollTrigger), n
        }

        l(t, e);
        var i = t.prototype;
        return i.to = function (e, t, i) {
            return new Ot(e, ue(arguments, 0, this), je(this, S(t) ? arguments[3] : i)), this
        }, i.from = function (e, t, i) {
            return new Ot(e, ue(arguments, 1, this), je(this, S(t) ? arguments[3] : i)), this
        }, i.fromTo = function (e, t, i, n) {
            return new Ot(e, ue(arguments, 2, this), je(this, S(t) ? arguments[4] : n)), this
        }, i.set = function (e, t, i) {
            return t.duration = 0, t.parent = this, be(t).repeatDelay || (t.repeat = 0), t.immediateRender = !!t.immediateRender, new Ot(e, t, je(this, i), 1), this
        }, i.call = function (e, t, i) {
            return Pe(this, Ot.delayedCall(0, e, t), je(this, i))
        }, i.staggerTo = function (e, t, i, n, r, s, o) {
            return i.duration = t, i.stagger = i.stagger || n, i.onComplete = s, i.onCompleteParams = o, i.parent = this, new Ot(e, i, je(this, r)), this
        }, i.staggerFrom = function (e, t, i, n, r, s, o) {
            return i.runBackwards = 1, be(i).immediateRender = A(i.immediateRender), this.staggerTo(e, t, i, n, r, s, o)
        }, i.staggerFromTo = function (e, t, i, n, r, s, o, a) {
            return n.startAt = i, be(n).immediateRender = A(n.immediateRender), this.staggerTo(e, t, n, r, s, o, a)
        }, i.render = function (e, t, i) {
            var n, r, s, o, a, l, c, h, d, p, f, m, g = this._time, v = this._dirty ? this.totalDuration() : this._tDur,
                y = this._dur, b = this !== u && e > v - 1e-8 && e >= 0 ? v : e < 1e-8 ? 0 : e,
                w = this._zTime < 0 != e < 0 && (this._initted || !y);
            if (b !== this._tTime || i || w) {
                if (g !== this._time && y && (b += this._time - g, e += this._time - g), n = b, d = this._start, l = !(h = this._ts), w && (y || (g = this._zTime), (e || !t) && (this._zTime = e)), this._repeat && (f = this._yoyo, a = y + this._rDelay, ((n = ae(b % a)) > y || v === b) && (n = y), (o = ~~(b / a)) && o === b / a && (n = y, o--), p = ke(this._tTime, a), !g && this._tTime && p !== o && (p = o), f && 1 & o && (n = y - n, m = 1), o !== p && !this._lock)) {
                    var D = f && 1 & p, T = D === (f && 1 & o);
                    if (o < p && (D = !D), g = D ? 0 : y, this._lock = 1, this.render(g || (m ? 0 : ae(o * a)), t, !y)._lock = 0, !t && this.parent && Je(this, "onRepeat"), this.vars.repeatRefresh && !m && (this.invalidate()._lock = 1), g !== this._time || l !== !this._ts) return this;
                    if (T && (this._lock = 2, g = D ? y + 1e-4 : -1e-4, this.render(g, !0), this.vars.repeatRefresh && !m && this.invalidate()), this._lock = 0, !this._ts && !l) return this;
                    ft(this, m)
                }
                if (this._hasPause && !this._forcing && this._lock < 2 && (c = function (e, t, i) {
                    var n;
                    if (i > t) for (n = e._first; n && n._start <= i;) {
                        if (!n._dur && "isPause" === n.data && n._start > t) return n;
                        n = n._next
                    } else for (n = e._last; n && n._start >= i;) {
                        if (!n._dur && "isPause" === n.data && n._start < t) return n;
                        n = n._prev
                    }
                }(this, ae(g), ae(n))) && (b -= n - (n = c._start)), this._tTime = b, this._time = n, this._act = !h, this._initted || (this._onUpdate = this.vars.onUpdate, this._initted = 1, this._zTime = e), g || !n || t || Je(this, "onStart"), n >= g && e >= 0) for (r = this._first; r;) {
                    if (s = r._next, (r._act || n >= r._start) && r._ts && c !== r) {
                        if (r.parent !== this) return this.render(e, t, i);
                        if (r.render(r._ts > 0 ? (n - r._start) * r._ts : (r._dirty ? r.totalDuration() : r._tDur) + (n - r._start) * r._ts, t, i), n !== this._time || !this._ts && !l) {
                            c = 0, s && (b += this._zTime = -1e-8);
                            break
                        }
                    }
                    r = s
                } else {
                    r = this._last;
                    for (var C = e < 0 ? e : n; r;) {
                        if (s = r._prev, (r._act || C <= r._end) && r._ts && c !== r) {
                            if (r.parent !== this) return this.render(e, t, i);
                            if (r.render(r._ts > 0 ? (C - r._start) * r._ts : (r._dirty ? r.totalDuration() : r._tDur) + (C - r._start) * r._ts, t, i), n !== this._time || !this._ts && !l) {
                                c = 0, s && (b += this._zTime = C ? -1e-8 : 1e-8);
                                break
                            }
                        }
                        r = s
                    }
                }
                if (c && !t && (this.pause(), c.render(n >= g ? 0 : -1e-8)._zTime = n >= g ? 1 : -1, this._ts)) return this._start = d, _e(this), this.render(e, t, i);
                this._onUpdate && !t && Je(this, "onUpdate", !0), (b === v && v >= this.totalDuration() || !b && g) && (d !== this._start && Math.abs(h) === Math.abs(this._ts) || this._lock || ((e || !y) && (b === v && this._ts > 0 || !b && this._ts < 0) && De(this, 1), t || e < 0 && !g || !b && !g || (Je(this, b === v ? "onComplete" : "onReverseComplete", !0), this._prom && !(b < v && this.timeScale() > 0) && this._prom())))
            }
            return this
        }, i.add = function (e, t) {
            var i = this;
            if (S(t) || (t = je(this, t)), !(e instanceof Dt)) {
                if (F(e)) return e.forEach(function (e) {
                    return i.add(e, t)
                }), Te(this);
                if (_(e)) return this.addLabel(e, t);
                if (!x(e)) return this;
                e = Ot.delayedCall(0, e)
            }
            return this !== e ? Pe(this, e, t) : this
        }, i.getChildren = function (e, t, i, n) {
            void 0 === e && (e = !0), void 0 === t && (t = !0), void 0 === i && (i = !0), void 0 === n && (n = -b);
            for (var r = [], s = this._first; s;) s._start >= n && (s instanceof Ot ? t && r.push(s) : (i && r.push(s), e && r.push.apply(r, s.getChildren(!0, t, i)))), s = s._next;
            return r
        }, i.getById = function (e) {
            for (var t = this.getChildren(1, 1, 1), i = t.length; i--;) if (t[i].vars.id === e) return t[i]
        }, i.remove = function (e) {
            return _(e) ? this.removeLabel(e) : x(e) ? this.killTweensOf(e) : (we(this, e), e === this._recent && (this._recent = this._last), Te(this))
        }, i.totalTime = function (t, i) {
            return arguments.length ? (this._forcing = 1, !this._dp && this._ts && (this._start = ae(at.time - (this._ts > 0 ? t / this._ts : (this.totalDuration() - t) / -this._ts))), e.prototype.totalTime.call(this, t, i), this._forcing = 0, this) : this._tTime
        }, i.addLabel = function (e, t) {
            return this.labels[e] = je(this, t), this
        }, i.removeLabel = function (e) {
            return delete this.labels[e], this
        }, i.addPause = function (e, t, i) {
            var n = Ot.delayedCall(0, t || G, i);
            return n.data = "isPause", this._hasPause = 1, Pe(this, n, je(this, e))
        }, i.removePause = function (e) {
            var t = this._first;
            for (e = je(this, e); t;) t._start === e && "isPause" === t.data && De(t), t = t._next
        }, i.killTweensOf = function (e, t, i) {
            for (var n = this.getTweensOf(e, i), r = n.length; r--;) Ct !== n[r] && n[r].kill(e, t);
            return this
        }, i.getTweensOf = function (e, t) {
            for (var i, n = [], r = He(e), s = this._first, o = S(t); s;) s instanceof Ot ? le(s._targets, r) && (o ? (!Ct || s._initted && s._ts) && s.globalTime(0) <= t && s.globalTime(s.totalDuration()) > t : !t || s.isActive()) && n.push(s) : (i = s.getTweensOf(r, t)).length && n.push.apply(n, i), s = s._next;
            return n
        }, i.tweenTo = function (e, t) {
            t = t || {};
            var i = this, n = je(i, e), r = t, s = r.startAt, o = r.onStart, a = r.onStartParams, l = Ot.to(i, fe(t, {
                ease: "none",
                lazy: !1,
                time: n,
                duration: t.duration || Math.abs((n - (s && "time" in s ? s.time : i._time)) / i.timeScale()) || 1e-8,
                onStart: function () {
                    i.pause();
                    var e = t.duration || Math.abs((n - i._time) / i.timeScale());
                    l._dur !== e && Me(l, e).render(l._time, !0, !0), o && o.apply(l, a || [])
                }
            }));
            return l
        }, i.tweenFromTo = function (e, t, i) {
            return this.tweenTo(t, fe({startAt: {time: je(this, e)}}, i))
        }, i.recent = function () {
            return this._recent
        }, i.nextLabel = function (e) {
            return void 0 === e && (e = this._time), Ke(this, je(this, e))
        }, i.previousLabel = function (e) {
            return void 0 === e && (e = this._time), Ke(this, je(this, e), 1)
        }, i.currentLabel = function (e) {
            return arguments.length ? this.seek(e, !0) : this.previousLabel(this._time + 1e-8)
        }, i.shiftChildren = function (e, t, i) {
            void 0 === i && (i = 0);
            for (var n, r = this._first, s = this.labels; r;) r._start >= i && (r._start += e), r = r._next;
            if (t) for (n in s) s[n] >= i && (s[n] += e);
            return Te(this)
        }, i.invalidate = function () {
            var t = this._first;
            for (this._lock = 0; t;) t.invalidate(), t = t._next;
            return e.prototype.invalidate.call(this)
        }, i.clear = function (e) {
            void 0 === e && (e = !0);
            for (var t, i = this._first; i;) t = i._next, this.remove(i), i = t;
            return this._time = this._tTime = this._pTime = 0, e && (this.labels = {}), Te(this)
        }, i.totalDuration = function (e) {
            var t, i, n, r, s = 0, o = this, a = o._last, l = b;
            if (arguments.length) return o.timeScale((o._repeat < 0 ? o.duration() : o.totalDuration()) / (o.reversed() ? -e : e));
            if (o._dirty) {
                for (r = o.parent; a;) t = a._prev, a._dirty && a.totalDuration(), (n = a._start) > l && o._sort && a._ts && !o._lock ? (o._lock = 1, Pe(o, a, n - a._delay, 1)._lock = 0) : l = n, n < 0 && a._ts && (s -= n, (!r && !o._dp || r && r.smoothChildTiming) && (o._start += n / o._ts, o._time -= n, o._tTime -= n), o.shiftChildren(-n, !1, -1 / 0), l = 0), (i = _e(a)) > s && a._ts && (s = i), a = t;
                Me(o, o === u && o._time > s ? o._time : s, 1), o._dirty = 0
            }
            return o._tDur
        }, t.updateRoot = function (e) {
            if (u._ts && (he(u, Ee(e, u)), f = at.frame), at.frame >= ee) {
                ee += v.autoSleep || 120;
                var t = u._first;
                if ((!t || !t._ts) && v.autoSleep && at._listeners.length < 2) {
                    for (; t && !t._ts;) t = t._next;
                    t || at.sleep()
                }
            }
        }, t
    }(Dt);
    fe(Tt.prototype, {_lock: 0, _hasPause: 0, _forcing: 0});
    var Ct, kt = function (e, t, i, n, r, s, o, a, l) {
            x(n) && (n = n(r || 0, e, s));
            var u, c = e[t],
                h = "get" !== i ? i : x(c) ? l ? e[t.indexOf("set") || !x(e["get" + t.substr(3)]) ? t : "get" + t.substr(3)](l) : e[t]() : c,
                d = x(c) ? l ? Lt : Mt : At;
            if (_(n) && (~n.indexOf("random(") && (n = Ge(n)), "=" === n.charAt(1) && (n = parseFloat(h) + parseFloat(n.substr(2)) * ("-" === n.charAt(0) ? -1 : 1) + (Re(h) || 0))), h !== n) return isNaN(h * n) ? (!c && !(t in e) && X(t, n), function (e, t, i, n, r, s, o) {
                var a, l, u, c, h, d, p, f, m = new Wt(this._pt, e, t, 0, 1, Rt, null, r), g = 0, v = 0;
                for (m.b = i, m.e = n, i += "", (p = ~(n += "").indexOf("random(")) && (n = Ge(n)), s && (s(f = [i, n], e, t), i = f[0], n = f[1]), l = i.match(R) || []; a = R.exec(n);) c = a[0], h = n.substring(g, a.index), u ? u = (u + 1) % 5 : "rgba(" === h.substr(-5) && (u = 1), c !== l[v++] && (d = parseFloat(l[v - 1]) || 0, m._pt = {
                    _next: m._pt,
                    p: h || 1 === v ? h : ",",
                    s: d,
                    c: "=" === c.charAt(1) ? parseFloat(c.substr(2)) * ("-" === c.charAt(0) ? -1 : 1) : parseFloat(c) - d,
                    m: u && u < 4 ? Math.round : 0
                }, g = R.lastIndex);
                return m.c = g < n.length ? n.substring(g, n.length) : "", m.fp = o, (z.test(n) || p) && (m.e = 0), this._pt = m, m
            }.call(this, e, t, h, n, d, a || v.stringFilter, l)) : (u = new Wt(this._pt, e, t, +h || 0, n - (h || 0), "boolean" == typeof c ? It : Bt, 0, d), l && (u.fp = l), o && u.modifier(o, this, e), this._pt = u)
        }, Et = function (e, t, i, n, r, s) {
            var o, a, l, u;
            if (Q[e] && !1 !== (o = new Q[e]).init(r, o.rawVars ? t[e] : function (e, t, i, n, r) {
                if (x(e) && (e = xt(e, r, t, i, n)), !O(e) || e.style && e.nodeType || F(e)) return _(e) ? xt(e, r, t, i, n) : e;
                var s, o = {};
                for (s in e) o[s] = xt(e[s], r, t, i, n);
                return o
            }(t[e], n, r, s, i), i, n, s) && (i._pt = a = new Wt(i._pt, r, e, 0, 1, o.render, o, 0, o.priority), i !== m)) for (l = i._ptLookup[i._targets.indexOf(r)], u = o._props.length; u--;) l[o._props[u]] = a;
            return o
        }, _t = function e(t, i) {
            var n, r, s, o, a, l, c, h, d, p, f, m, g, v = t.vars, b = v.ease, w = v.startAt, D = v.immediateRender,
                T = v.lazy, C = v.onUpdate, k = v.onUpdateParams, E = v.callbackScope, _ = v.runBackwards, x = v.yoyoEase,
                S = v.keyframes, P = v.autoRevert, O = t._dur, M = t._startAt, L = t._targets, F = t.parent,
                j = F && "nested" === F.data ? F.parent._targets : L, B = "auto" === t._overwrite, I = t.timeline;
            if (I && (!S || !b) && (b = "none"), t._ease = mt(b, y.ease), t._yEase = x ? pt(mt(!0 === x ? b : x, y.ease)) : 0, x && t._yoyo && !t._repeat && (x = t._yEase, t._yEase = t._ease, t._ease = x), !I) {
                if (m = (h = L[0] ? re(L[0]).harness : 0) && v[h.prop], n = ye(v, $), M && M.render(-1, !0).kill(), w) {
                    if (De(t._startAt = Ot.set(L, fe({
                        data: "isStart",
                        overwrite: !1,
                        parent: F,
                        immediateRender: !0,
                        lazy: A(T),
                        startAt: null,
                        delay: 0,
                        onUpdate: C,
                        onUpdateParams: k,
                        callbackScope: E,
                        stagger: 0
                    }, w))), D) if (i > 0) !P && (t._startAt = 0); else if (O && !(i < 0 && M)) return void (t._zTime = i)
                } else if (_ && O) if (M) !P && (t._startAt = 0); else if (i && (D = !1), s = fe({
                    overwrite: !1,
                    data: "isFromStart",
                    lazy: D && A(T),
                    immediateRender: D,
                    stagger: 0,
                    parent: F
                }, n), m && (s[h.prop] = m), De(t._startAt = Ot.set(L, s)), D) {
                    if (!i) return
                } else e(t._startAt, 1e-8);
                for (t._pt = 0, T = O && A(T) || T && !O, r = 0; r < L.length; r++) {
                    if (c = (a = L[r])._gsap || ne(L)[r]._gsap, t._ptLookup[r] = p = {}, J[c.id] && ce(), f = j === L ? r : j.indexOf(a), h && !1 !== (d = new h).init(a, m || n, t, f, j) && (t._pt = o = new Wt(t._pt, a, d.name, 0, 1, d.render, d, 0, d.priority), d._props.forEach(function (e) {
                        p[e] = o
                    }), d.priority && (l = 1)), !h || m) for (s in n) Q[s] && (d = Et(s, n, t, f, a, j)) ? d.priority && (l = 1) : p[s] = o = kt.call(t, a, s, "get", n[s], f, j, 0, v.stringFilter);
                    t._op && t._op[r] && t.kill(a, t._op[r]), B && t._pt && (Ct = t, u.killTweensOf(a, p, t.globalTime(0)), g = !t.parent, Ct = 0), t._pt && T && (J[c.id] = 1)
                }
                l && qt(t), t._onInit && t._onInit(t)
            }
            t._from = !I && !!v.runBackwards, t._onUpdate = C, t._initted = (!t._op || t._pt) && !g
        }, xt = function (e, t, i, n, r) {
            return x(e) ? e.call(t, i, n, r) : _(e) && ~e.indexOf("random(") ? Ge(e) : e
        }, St = ie + "repeat,repeatDelay,yoyo,repeatRefresh,yoyoEase",
        Pt = (St + ",id,stagger,delay,duration,paused,scrollTrigger").split(","), Ot = function (e) {
            function t(t, i, n, r) {
                var s;
                "number" == typeof i && (n.duration = i, i = n, n = null);
                var o, l, c, h, d, p, f, m, g = (s = e.call(this, r ? i : be(i), n) || this).vars, y = g.duration,
                    b = g.delay, w = g.immediateRender, D = g.stagger, T = g.overwrite, C = g.keyframes, k = g.defaults,
                    E = g.scrollTrigger, _ = g.yoyoEase, x = s.parent, P = (F(t) ? S(t[0]) : "length" in i) ? [t] : He(t);
                if (s._targets = P.length ? ne(P) : Y("GSAP target " + t + " not found. https://greensock.com", !v.nullTargetWarn) || [], s._ptLookup = [], s._overwrite = T, C || D || L(y) || L(b)) {
                    if (i = s.vars, (o = s.timeline = new Tt({
                        data: "nested",
                        defaults: k || {}
                    })).kill(), o.parent = a(s), C) fe(o.vars.defaults, {ease: "none"}), C.forEach(function (e) {
                        return o.to(P, e, ">")
                    }); else {
                        if (h = P.length, f = D ? qe(D) : G, O(D)) for (d in D) ~St.indexOf(d) && (m || (m = {}), m[d] = D[d]);
                        for (l = 0; l < h; l++) {
                            for (d in c = {}, i) Pt.indexOf(d) < 0 && (c[d] = i[d]);
                            c.stagger = 0, _ && (c.yoyoEase = _), m && ge(c, m), p = P[l], c.duration = +xt(y, a(s), l, p, P), c.delay = (+xt(b, a(s), l, p, P) || 0) - s._delay, !D && 1 === h && c.delay && (s._delay = b = c.delay, s._start += b, c.delay = 0), o.to(p, c, f(l, p, P))
                        }
                        o.duration() ? y = b = 0 : s.timeline = 0
                    }
                    y || s.duration(y = o.duration())
                } else s.timeline = 0;
                return !0 === T && (Ct = a(s), u.killTweensOf(P), Ct = 0), x && Se(x, a(s)), (w || !y && !C && s._start === ae(x._time) && A(w) && function e(t) {
                    return !t || t._ts && e(t.parent)
                }(a(s)) && "nested" !== x.data) && (s._tTime = -1e-8, s.render(Math.max(0, -b))), E && Oe(a(s), E), s
            }

            l(t, e);
            var i = t.prototype;
            return i.render = function (e, t, i) {
                var n, r, s, o, a, l, u, c, h, d = this._time, p = this._tDur, f = this._dur,
                    m = e > p - 1e-8 && e >= 0 ? p : e < 1e-8 ? 0 : e;
                if (f) {
                    if (m !== this._tTime || !e || i || this._startAt && this._zTime < 0 != e < 0) {
                        if (n = m, c = this.timeline, this._repeat) {
                            if (o = f + this._rDelay, ((n = ae(m % o)) > f || p === m) && (n = f), (s = ~~(m / o)) && s === m / o && (n = f, s--), (l = this._yoyo && 1 & s) && (h = this._yEase, n = f - n), a = ke(this._tTime, o), n === d && !i && this._initted) return this;
                            s !== a && (c && this._yEase && ft(c, l), !this.vars.repeatRefresh || l || this._lock || (this._lock = i = 1, this.render(ae(o * s), !0).invalidate()._lock = 0))
                        }
                        if (!this._initted) {
                            if (Ae(this, e < 0 ? e : n, i, t)) return this._tTime = 0, this;
                            if (f !== this._dur) return this.render(e, t, i)
                        }
                        for (this._tTime = m, this._time = n, !this._act && this._ts && (this._act = 1, this._lazy = 0), this.ratio = u = (h || this._ease)(n / f), this._from && (this.ratio = u = 1 - u), n && !d && !t && Je(this, "onStart"), r = this._pt; r;) r.r(u, r.d), r = r._next;
                        c && c.render(e < 0 ? e : !n && l ? -1e-8 : c._dur * u, t, i) || this._startAt && (this._zTime = e), this._onUpdate && !t && (e < 0 && this._startAt && this._startAt.render(e, !0, i), Je(this, "onUpdate")), this._repeat && s !== a && this.vars.onRepeat && !t && this.parent && Je(this, "onRepeat"), m !== this._tDur && m || this._tTime !== m || (e < 0 && this._startAt && !this._onUpdate && this._startAt.render(e, !0, !0), (e || !f) && (m === this._tDur && this._ts > 0 || !m && this._ts < 0) && De(this, 1), t || e < 0 && !d || !m && !d || (Je(this, m === p ? "onComplete" : "onReverseComplete", !0), this._prom && !(m < p && this.timeScale() > 0) && this._prom()))
                    }
                } else !function (e, t, i, n) {
                    var r, s, o = e.ratio,
                        a = t < 0 || !t && o && !e._start && e._zTime > 1e-8 && !e._dp._lock || e._ts < 0 || e._dp._ts < 0 ? 0 : 1,
                        l = e._rDelay, u = 0;
                    if (l && e._repeat && (u = Ie(0, e._tDur, t), ke(u, l) !== (s = ke(e._tTime, l)) && (o = 1 - a, e.vars.repeatRefresh && e._initted && e.invalidate())), e._initted || !Ae(e, t, n, i)) if (a !== o || n || 1e-8 === e._zTime || !t && e._zTime) {
                        for (s = e._zTime, e._zTime = t || (i ? 1e-8 : 0), i || (i = t && !s), e.ratio = a, e._from && (a = 1 - a), e._time = 0, e._tTime = u, i || Je(e, "onStart"), r = e._pt; r;) r.r(a, r.d), r = r._next;
                        e._startAt && t < 0 && e._startAt.render(t, !0, !0), e._onUpdate && !i && Je(e, "onUpdate"), u && e._repeat && !i && e.parent && Je(e, "onRepeat"), (t >= e._tDur || t < 0) && e.ratio === a && (a && De(e, 1), i || (Je(e, a ? "onComplete" : "onReverseComplete", !0), e._prom && e._prom()))
                    } else e._zTime || (e._zTime = t)
                }(this, e, t, i);
                return this
            }, i.targets = function () {
                return this._targets
            }, i.invalidate = function () {
                return this._pt = this._op = this._startAt = this._onUpdate = this._act = this._lazy = 0, this._ptLookup = [], this.timeline && this.timeline.invalidate(), e.prototype.invalidate.call(this)
            }, i.kill = function (e, t) {
                if (void 0 === t && (t = "all"), !(e || t && "all" !== t) && (this._lazy = 0, this.parent)) return Qe(this);
                if (this.timeline) {
                    var i = this.timeline.totalDuration();
                    return this.timeline.killTweensOf(e, t, Ct && !0 !== Ct.vars.overwrite)._first || Qe(this), this.parent && i !== this.timeline.totalDuration() && Me(this, this._dur * this.timeline._tDur / i), this
                }
                var n, r, s, o, a, l, u, c = this._targets, h = e ? He(e) : c, d = this._ptLookup, p = this._pt;
                if ((!t || "all" === t) && function (e, t) {
                    for (var i = e.length, n = i === t.length; n && i-- && e[i] === t[i];) ;
                    return i < 0
                }(c, h)) return "all" === t && (this._pt = 0), Qe(this);
                for (n = this._op = this._op || [], "all" !== t && (_(t) && (a = {}, oe(t, function (e) {
                    return a[e] = 1
                }), t = a), t = function (e, t) {
                    var i, n, r, s, o = e[0] ? re(e[0]).harness : 0, a = o && o.aliases;
                    if (!a) return t;
                    for (n in i = ge({}, t), a) if (n in i) for (r = (s = a[n].split(",")).length; r--;) i[s[r]] = i[n];
                    return i
                }(c, t)), u = c.length; u--;) if (~h.indexOf(c[u])) for (a in r = d[u], "all" === t ? (n[u] = t, o = r, s = {}) : (s = n[u] = n[u] || {}, o = t), o) (l = r && r[a]) && ("kill" in l.d && !0 !== l.d.kill(a) || we(this, l, "_pt"), delete r[a]), "all" !== s && (s[a] = 1);
                return this._initted && !this._pt && p && Qe(this), this
            }, t.to = function (e, i) {
                return new t(e, i, arguments[2])
            }, t.from = function (e, i) {
                return new t(e, ue(arguments, 1))
            }, t.delayedCall = function (e, i, n, r) {
                return new t(i, 0, {
                    immediateRender: !1,
                    lazy: !1,
                    overwrite: !1,
                    delay: e,
                    onComplete: i,
                    onReverseComplete: i,
                    onCompleteParams: n,
                    onReverseCompleteParams: n,
                    callbackScope: r
                })
            }, t.fromTo = function (e, i, n) {
                return new t(e, ue(arguments, 2))
            }, t.set = function (e, i) {
                return i.duration = 0, i.repeatDelay || (i.repeat = 0), new t(e, i)
            }, t.killTweensOf = function (e, t, i) {
                return u.killTweensOf(e, t, i)
            }, t
        }(Dt);
    fe(Ot.prototype, {
        _targets: [],
        _lazy: 0,
        _startAt: 0,
        _op: 0,
        _onInit: 0
    }), oe("staggerTo,staggerFrom,staggerFromTo", function (e) {
        Ot[e] = function () {
            var t = new Tt, i = Ne.call(arguments, 0);
            return i.splice("staggerFromTo" === e ? 5 : 4, 0, 0), t[e].apply(t, i)
        }
    });
    var At = function (e, t, i) {
        return e[t] = i
    }, Mt = function (e, t, i) {
        return e[t](i)
    }, Lt = function (e, t, i, n) {
        return e[t](n.fp, i)
    }, Ft = function (e, t, i) {
        return e.setAttribute(t, i)
    }, jt = function (e, t) {
        return x(e[t]) ? Mt : P(e[t]) && e.setAttribute ? Ft : At
    }, Bt = function (e, t) {
        return t.set(t.t, t.p, Math.round(1e4 * (t.s + t.c * e)) / 1e4, t)
    }, It = function (e, t) {
        return t.set(t.t, t.p, !!(t.s + t.c * e), t)
    }, Rt = function (e, t) {
        var i = t._pt, n = "";
        if (!e && t.b) n = t.b; else if (1 === e && t.e) n = t.e; else {
            for (; i;) n = i.p + (i.m ? i.m(i.s + i.c * e) : Math.round(1e4 * (i.s + i.c * e)) / 1e4) + n, i = i._next;
            n += t.c
        }
        t.set(t.t, t.p, n, t)
    }, Nt = function (e, t) {
        for (var i = t._pt; i;) i.r(e, i.d), i = i._next
    }, zt = function (e, t, i, n) {
        for (var r, s = this._pt; s;) r = s._next, s.p === n && s.modifier(e, t, i), s = r
    }, Ht = function (e) {
        for (var t, i, n = this._pt; n;) i = n._next, n.p === e && !n.op || n.op === e ? we(this, n, "_pt") : n.dep || (t = 1), n = i;
        return !t
    }, Vt = function (e, t, i, n) {
        n.mSet(e, t, n.m.call(n.tween, i, n.mt), n)
    }, qt = function (e) {
        for (var t, i, n, r, s = e._pt; s;) {
            for (t = s._next, i = n; i && i.pr > s.pr;) i = i._next;
            (s._prev = i ? i._prev : r) ? s._prev._next = s : n = s, (s._next = i) ? i._prev = s : r = s, s = t
        }
        e._pt = n
    }, Wt = function () {
        function e(e, t, i, n, r, s, o, a, l) {
            this.t = t, this.s = n, this.c = r, this.p = i, this.r = s || Bt, this.d = o || this, this.set = a || At, this.pr = l || 0, this._next = e, e && (e._prev = this)
        }

        return e.prototype.modifier = function (e, t, i) {
            this.mSet = this.mSet || this.set, this.set = Vt, this.m = e, this.mt = i, this.tween = t
        }, e
    }();
    oe(ie + "parent,duration,ease,delay,overwrite,runBackwards,startAt,yoyo,immediateRender,repeat,repeatDelay,data,paused,reversed,lazy,callbackScope,stringFilter,id,yoyoEase,stagger,inherit,repeatRefresh,keyframes,autoRevert,scrollTrigger", function (e) {
        return $[e] = 1
    }), V.TweenMax = V.TweenLite = Ot, V.TimelineLite = V.TimelineMax = Tt, u = new Tt({
        sortChildren: !1,
        defaults: y,
        autoRemoveChildren: !0,
        id: "root",
        smoothChildTiming: !0
    }), v.stringFilter = ot;
    var Xt = {
        registerPlugin: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            t.forEach(function (e) {
                return function (e) {
                    var t = (e = !e.name && e.default || e).name, i = x(e), n = t && !i && e.init ? function () {
                            this._props = []
                        } : e, r = {init: G, render: Nt, add: kt, kill: Ht, modifier: zt, rawVars: 0},
                        s = {targetTest: 0, get: 0, getSetter: jt, aliases: {}, register: 0};
                    if (lt(), e !== n) {
                        if (Q[t]) return;
                        fe(n, fe(ye(e, r), s)), ge(n.prototype, ge(r, ye(e, s))), Q[n.prop = t] = n, e.targetTest && (te.push(n), $[t] = 1), t = ("css" === t ? "CSS" : t.charAt(0).toUpperCase() + t.substr(1)) + "Plugin"
                    }
                    U(t, n), e.register && e.register(Gt, n, Wt)
                }(e)
            })
        },
        timeline: function (e) {
            return new Tt(e)
        },
        getTweensOf: function (e, t) {
            return u.getTweensOf(e, t)
        },
        getProperty: function (e, t, i, n) {
            _(e) && (e = He(e)[0]);
            var r = re(e || {}).get, s = i ? pe : de;
            return "native" === i && (i = ""), e ? t ? s((Q[t] && Q[t].get || r)(e, t, i, n)) : function (t, i, n) {
                return s((Q[t] && Q[t].get || r)(e, t, i, n))
            } : e
        },
        quickSetter: function (e, t, i) {
            if ((e = He(e)).length > 1) {
                var n = e.map(function (e) {
                    return Gt.quickSetter(e, t, i)
                }), r = n.length;
                return function (e) {
                    for (var t = r; t--;) n[t](e)
                }
            }
            e = e[0] || {};
            var s = Q[t], o = re(e), a = o.harness && (o.harness.aliases || {})[t] || t, l = s ? function (t) {
                var n = new s;
                m._pt = 0, n.init(e, i ? t + i : t, m, 0, [e]), n.render(1, n), m._pt && Nt(1, m)
            } : o.set(e, a);
            return s ? l : function (t) {
                return l(e, a, i ? t + i : t, o, 1)
            }
        },
        isTweening: function (e) {
            return u.getTweensOf(e, !0).length > 0
        },
        defaults: function (e) {
            return e && e.ease && (e.ease = mt(e.ease, y.ease)), ve(y, e || {})
        },
        config: function (e) {
            return ve(v, e || {})
        },
        registerEffect: function (e) {
            var t = e.name, i = e.effect, n = e.plugins, r = e.defaults, s = e.extendTimeline;
            (n || "").split(",").forEach(function (e) {
                return e && !Q[e] && !V[e] && Y(t + " effect requires " + e + " plugin.")
            }), Z[t] = function (e, t, n) {
                return i(He(e), fe(t || {}, r), n)
            }, s && (Tt.prototype[t] = function (e, i, n) {
                return this.add(Z[t](e, O(i) ? i : (n = i) && {}, this), n)
            })
        },
        registerEase: function (e, t) {
            ut[e] = mt(t)
        },
        parseEase: function (e, t) {
            return arguments.length ? mt(e, t) : ut
        },
        getById: function (e) {
            return u.getById(e)
        },
        exportRoot: function (e, t) {
            void 0 === e && (e = {});
            var i, n, r = new Tt(e);
            for (r.smoothChildTiming = A(e.smoothChildTiming), u.remove(r), r._dp = 0, r._time = r._tTime = u._time, i = u._first; i;) n = i._next, !t && !i._dur && i instanceof Ot && i.vars.onComplete === i._targets[0] || Pe(r, i, i._start - i._delay), i = n;
            return Pe(u, r, 0), r
        },
        utils: {
            wrap: function e(t, i, n) {
                var r = i - t;
                return F(t) ? Ue(t, e(0, t.length), i) : Be(n, function (e) {
                    return (r + (e - t) % r) % r + t
                })
            }, wrapYoyo: function e(t, i, n) {
                var r = i - t, s = 2 * r;
                return F(t) ? Ue(t, e(0, t.length - 1), i) : Be(n, function (e) {
                    return t + ((e = (s + (e - t) % s) % s || 0) > r ? s - e : e)
                })
            }, distribute: qe, random: Ye, snap: Xe, normalize: function (e, t, i) {
                return $e(e, t, 0, 1, i)
            }, getUnit: Re, clamp: function (e, t, i) {
                return Be(i, function (i) {
                    return Ie(e, t, i)
                })
            }, splitColor: tt, toArray: He, mapRange: $e, pipe: function () {
                for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
                return function (e) {
                    return t.reduce(function (e, t) {
                        return t(e)
                    }, e)
                }
            }, unitize: function (e, t) {
                return function (i) {
                    return e(parseFloat(i)) + (t || Re(i))
                }
            }, interpolate: function e(t, i, n, r) {
                var s = isNaN(t + i) ? 0 : function (e) {
                    return (1 - e) * t + e * i
                };
                if (!s) {
                    var o, a, l, u, c, h = _(t), d = {};
                    if (!0 === n && (r = 1) && (n = null), h) t = {p: t}, i = {p: i}; else if (F(t) && !F(i)) {
                        for (l = [], u = t.length, c = u - 2, a = 1; a < u; a++) l.push(e(t[a - 1], t[a]));
                        u--, s = function (e) {
                            e *= u;
                            var t = Math.min(c, ~~e);
                            return l[t](e - t)
                        }, n = i
                    } else r || (t = ge(F(t) ? [] : {}, t));
                    if (!l) {
                        for (o in i) kt.call(d, t, o, "get", i[o]);
                        s = function (e) {
                            return Nt(e, d) || (h ? t.p : t)
                        }
                    }
                }
                return Be(n, s)
            }, shuffle: Ve
        },
        install: W,
        effects: Z,
        ticker: at,
        updateRoot: Tt.updateRoot,
        plugins: Q,
        globalTimeline: u,
        core: {
            PropTween: Wt,
            globals: U,
            Tween: Ot,
            Timeline: Tt,
            Animation: Dt,
            getCache: re,
            _removeLinkedListItem: we
        }
    };
    oe("to,from,fromTo,delayedCall,set,killTweensOf", function (e) {
        return Xt[e] = Ot[e]
    }), at.add(Tt.updateRoot), m = Xt.to({}, {duration: 0});
    var Yt = function (e, t) {
        for (var i = e._pt; i && i.p !== t && i.op !== t && i.fp !== t;) i = i._next;
        return i
    }, Ut = function (e, t) {
        return {
            name: e, rawVars: 1, init: function (e, i, n) {
                n._onInit = function (e) {
                    var n, r;
                    if (_(i) && (n = {}, oe(i, function (e) {
                        return n[e] = 1
                    }), i = n), t) {
                        for (r in n = {}, i) n[r] = t(i[r]);
                        i = n
                    }
                    !function (e, t) {
                        var i, n, r, s = e._targets;
                        for (i in t) for (n = s.length; n--;) (r = e._ptLookup[n][i]) && (r = r.d) && (r._pt && (r = Yt(r, i)), r && r.modifier && r.modifier(t[i], e, s[n], i))
                    }(e, i)
                }
            }
        }
    }, Gt = Xt.registerPlugin({
        name: "attr", init: function (e, t, i, n, r) {
            var s, o;
            for (s in t) (o = this.add(e, "setAttribute", (e.getAttribute(s) || 0) + "", t[s], n, r, 0, 0, s)) && (o.op = s), this._props.push(s)
        }
    }, {
        name: "endArray", init: function (e, t) {
            for (var i = t.length; i--;) this.add(e, i, e[i] || 0, t[i])
        }
    }, Ut("roundProps", We), Ut("modifiers"), Ut("snap", Xe)) || Xt;
    Ot.version = Tt.version = Gt.version = "3.4.2", p = 1, M() && lt(), ut.Power0, ut.Power1, ut.Power2, ut.Power3, ut.Power4, ut.Linear, ut.Quad, ut.Cubic, ut.Quart, ut.Quint, ut.Strong, ut.Elastic, ut.Back, ut.SteppedEase, ut.Bounce, ut.Sine, ut.Expo, ut.Circ;
    var $t, Kt, Jt, Qt, Zt, ei, ti, ii, ni = {}, ri = 180 / Math.PI, si = Math.PI / 180, oi = Math.atan2,
        ai = /([A-Z])/g, li = /(?:left|right|width|margin|padding|x)/i, ui = /[\s,\(]\S/,
        ci = {autoAlpha: "opacity,visibility", scale: "scaleX,scaleY", alpha: "opacity"}, hi = function (e, t) {
            return t.set(t.t, t.p, Math.round(1e4 * (t.s + t.c * e)) / 1e4 + t.u, t)
        }, di = function (e, t) {
            return t.set(t.t, t.p, 1 === e ? t.e : Math.round(1e4 * (t.s + t.c * e)) / 1e4 + t.u, t)
        }, pi = function (e, t) {
            return t.set(t.t, t.p, e ? Math.round(1e4 * (t.s + t.c * e)) / 1e4 + t.u : t.b, t)
        }, fi = function (e, t) {
            var i = t.s + t.c * e;
            t.set(t.t, t.p, ~~(i + (i < 0 ? -.5 : .5)) + t.u, t)
        }, mi = function (e, t) {
            return t.set(t.t, t.p, e ? t.e : t.b, t)
        }, gi = function (e, t) {
            return t.set(t.t, t.p, 1 !== e ? t.b : t.e, t)
        }, vi = function (e, t, i) {
            return e.style[t] = i
        }, yi = function (e, t, i) {
            return e.style.setProperty(t, i)
        }, bi = function (e, t, i) {
            return e._gsap[t] = i
        }, wi = function (e, t, i) {
            return e._gsap.scaleX = e._gsap.scaleY = i
        }, Di = function (e, t, i, n, r) {
            var s = e._gsap;
            s.scaleX = s.scaleY = i, s.renderTransform(r, s)
        }, Ti = function (e, t, i, n, r) {
            var s = e._gsap;
            s[t] = i, s.renderTransform(r, s)
        }, Ci = "transform", ki = Ci + "Origin", Ei = function (e, t) {
            var i = Kt.createElementNS ? Kt.createElementNS((t || "http://www.w3.org/1999/xhtml").replace(/^https/, "http"), e) : Kt.createElement(e);
            return i.style ? i : Kt.createElement(e)
        }, _i = function e(t, i, n) {
            var r = getComputedStyle(t);
            return r[i] || r.getPropertyValue(i.replace(ai, "-$1").toLowerCase()) || r.getPropertyValue(i) || !n && e(t, Si(i) || i, 1) || ""
        }, xi = "O,Moz,ms,Ms,Webkit".split(","), Si = function (e, t, i) {
            var n = (t || Zt).style, r = 5;
            if (e in n && !i) return e;
            for (e = e.charAt(0).toUpperCase() + e.substr(1); r-- && !(xi[r] + e in n);) ;
            return r < 0 ? null : (3 === r ? "ms" : r >= 0 ? xi[r] : "") + e
        }, Pi = function () {
            "undefined" != typeof window && window.document && ($t = window, Kt = $t.document, Jt = Kt.documentElement, Zt = Ei("div") || {style: {}}, ei = Ei("div"), Ci = Si(Ci), ki = Ci + "Origin", Zt.style.cssText = "border-width:0;line-height:0;position:absolute;padding:0", ii = !!Si("perspective"), Qt = 1)
        }, Oi = function e(t) {
            var i,
                n = Ei("svg", this.ownerSVGElement && this.ownerSVGElement.getAttribute("xmlns") || "http://www.w3.org/2000/svg"),
                r = this.parentNode, s = this.nextSibling, o = this.style.cssText;
            if (Jt.appendChild(n), n.appendChild(this), this.style.display = "block", t) try {
                i = this.getBBox(), this._gsapBBox = this.getBBox, this.getBBox = e
            } catch (e) {
            } else this._gsapBBox && (i = this._gsapBBox());
            return r && (s ? r.insertBefore(this, s) : r.appendChild(this)), Jt.removeChild(n), this.style.cssText = o, i
        }, Ai = function (e, t) {
            for (var i = t.length; i--;) if (e.hasAttribute(t[i])) return e.getAttribute(t[i])
        }, Mi = function (e) {
            var t;
            try {
                t = e.getBBox()
            } catch (i) {
                t = Oi.call(e, !0)
            }
            return t && (t.width || t.height) || e.getBBox === Oi || (t = Oi.call(e, !0)), !t || t.width || t.x || t.y ? t : {
                x: +Ai(e, ["x", "cx", "x1"]) || 0,
                y: +Ai(e, ["y", "cy", "y1"]) || 0,
                width: 0,
                height: 0
            }
        }, Li = function (e) {
            return !(!e.getCTM || e.parentNode && !e.ownerSVGElement || !Mi(e))
        }, Fi = function (e, t) {
            if (t) {
                var i = e.style;
                t in ni && t !== ki && (t = Ci), i.removeProperty ? ("ms" !== t.substr(0, 2) && "webkit" !== t.substr(0, 6) || (t = "-" + t), i.removeProperty(t.replace(ai, "-$1").toLowerCase())) : i.removeAttribute(t)
            }
        }, ji = function (e, t, i, n, r, s) {
            var o = new Wt(e._pt, t, i, 0, 1, s ? gi : mi);
            return e._pt = o, o.b = n, o.e = r, e._props.push(i), o
        }, Bi = {deg: 1, rad: 1, turn: 1}, Ii = function e(t, i, n, r) {
            var s, o, a, l, u = parseFloat(n) || 0, c = (n + "").trim().substr((u + "").length) || "px", h = Zt.style,
                d = li.test(i), p = "svg" === t.tagName.toLowerCase(),
                f = (p ? "client" : "offset") + (d ? "Width" : "Height"), m = "px" === r, g = "%" === r;
            return r === c || !u || Bi[r] || Bi[c] ? u : ("px" !== c && !m && (u = e(t, i, n, "px")), l = t.getCTM && Li(t), g && (ni[i] || ~i.indexOf("adius")) ? ae(u / (l ? t.getBBox()[d ? "width" : "height"] : t[f]) * 100) : (h[d ? "width" : "height"] = 100 + (m ? c : r), o = ~i.indexOf("adius") || "em" === r && t.appendChild && !p ? t : t.parentNode, l && (o = (t.ownerSVGElement || {}).parentNode), o && o !== Kt && o.appendChild || (o = Kt.body), (a = o._gsap) && g && a.width && d && a.time === at.time ? ae(u / a.width * 100) : ((g || "%" === c) && (h.position = _i(t, "position")), o === t && (h.position = "static"), o.appendChild(Zt), s = Zt[f], o.removeChild(Zt), h.position = "absolute", d && g && ((a = re(o)).time = at.time, a.width = o[f]), ae(m ? s * u / 100 : s && u ? 100 / s * u : 0))))
        }, Ri = function (e, t, i, n) {
            var r;
            return Qt || Pi(), t in ci && "transform" !== t && ~(t = ci[t]).indexOf(",") && (t = t.split(",")[0]), ni[t] && "transform" !== t ? (r = $i(e, n), r = "transformOrigin" !== t ? r[t] : Ki(_i(e, ki)) + " " + r.zOrigin + "px") : (!(r = e.style[t]) || "auto" === r || n || ~(r + "").indexOf("calc(")) && (r = Vi[t] && Vi[t](e, t, i) || _i(e, t) || se(e, t) || ("opacity" === t ? 1 : 0)), i && !~(r + "").indexOf(" ") ? Ii(e, t, r, i) + i : r
        }, Ni = function (e, t, i, n) {
            if (!i || "none" === i) {
                var r = Si(t, e, 1), s = r && _i(e, r, 1);
                s && s !== i ? (t = r, i = s) : "borderColor" === t && (i = _i(e, "borderTopColor"))
            }
            var o, a, l, u, c, h, d, p, f, m, g, y, b = new Wt(this._pt, e.style, t, 0, 1, Rt), w = 0, D = 0;
            if (b.b = i, b.e = n, i += "", "auto" == (n += "") && (e.style[t] = n, n = _i(e, t) || n, e.style[t] = i), ot(o = [i, n]), n = o[1], l = (i = o[0]).match(I) || [], (n.match(I) || []).length) {
                for (; a = I.exec(n);) d = a[0], f = n.substring(w, a.index), c ? c = (c + 1) % 5 : "rgba(" !== f.substr(-5) && "hsla(" !== f.substr(-5) || (c = 1), d !== (h = l[D++] || "") && (u = parseFloat(h) || 0, g = h.substr((u + "").length), (y = "=" === d.charAt(1) ? +(d.charAt(0) + "1") : 0) && (d = d.substr(2)), p = parseFloat(d), m = d.substr((p + "").length), w = I.lastIndex - m.length, m || (m = m || v.units[t] || g, w === n.length && (n += m, b.e += m)), g !== m && (u = Ii(e, t, h, m) || 0), b._pt = {
                    _next: b._pt,
                    p: f || 1 === D ? f : ",",
                    s: u,
                    c: y ? y * p : p - u,
                    m: c && c < 4 ? Math.round : 0
                });
                b.c = w < n.length ? n.substring(w, n.length) : ""
            } else b.r = "display" === t && "none" === n ? gi : mi;
            return z.test(n) && (b.e = 0), this._pt = b, b
        }, zi = {top: "0%", bottom: "100%", left: "0%", right: "100%", center: "50%"}, Hi = function (e, t) {
            if (t.tween && t.tween._time === t.tween._dur) {
                var i, n, r, s = t.t, o = s.style, a = t.u, l = s._gsap;
                if ("all" === a || !0 === a) o.cssText = "", n = 1; else for (r = (a = a.split(",")).length; --r > -1;) i = a[r], ni[i] && (n = 1, i = "transformOrigin" === i ? ki : Ci), Fi(s, i);
                n && (Fi(s, Ci), l && (l.svg && s.removeAttribute("transform"), $i(s, 1), l.uncache = 1))
            }
        }, Vi = {
            clearProps: function (e, t, i, n, r) {
                if ("isFromStart" !== r.data) {
                    var s = e._pt = new Wt(e._pt, t, i, 0, 0, Hi);
                    return s.u = n, s.pr = -10, s.tween = r, e._props.push(i), 1
                }
            }
        }, qi = [1, 0, 0, 1, 0, 0], Wi = {}, Xi = function (e) {
            return "matrix(1, 0, 0, 1, 0, 0)" === e || "none" === e || !e
        }, Yi = function (e) {
            var t = _i(e, Ci);
            return Xi(t) ? qi : t.substr(7).match(B).map(ae)
        }, Ui = function (e, t) {
            var i, n, r, s, o = e._gsap || re(e), a = e.style, l = Yi(e);
            return o.svg && e.getAttribute("transform") ? "1,0,0,1,0,0" === (l = [(r = e.transform.baseVal.consolidate().matrix).a, r.b, r.c, r.d, r.e, r.f]).join(",") ? qi : l : (l !== qi || e.offsetParent || e === Jt || o.svg || (r = a.display, a.display = "block", (i = e.parentNode) && e.offsetParent || (s = 1, n = e.nextSibling, Jt.appendChild(e)), l = Yi(e), r ? a.display = r : Fi(e, "display"), s && (n ? i.insertBefore(e, n) : i ? i.appendChild(e) : Jt.removeChild(e))), t && l.length > 6 ? [l[0], l[1], l[4], l[5], l[12], l[13]] : l)
        }, Gi = function (e, t, i, n, r, s) {
            var o, a, l, u = e._gsap, c = r || Ui(e, !0), h = u.xOrigin || 0, d = u.yOrigin || 0, p = u.xOffset || 0,
                f = u.yOffset || 0, m = c[0], g = c[1], v = c[2], y = c[3], b = c[4], w = c[5], D = t.split(" "),
                T = parseFloat(D[0]) || 0, C = parseFloat(D[1]) || 0;
            i ? c !== qi && (a = m * y - g * v) && (l = T * (-g / a) + C * (m / a) - (m * w - g * b) / a, T = T * (y / a) + C * (-v / a) + (v * w - y * b) / a, C = l) : (T = (o = Mi(e)).x + (~D[0].indexOf("%") ? T / 100 * o.width : T), C = o.y + (~(D[1] || D[0]).indexOf("%") ? C / 100 * o.height : C)), n || !1 !== n && u.smooth ? (b = T - h, w = C - d, u.xOffset = p + (b * m + w * v) - b, u.yOffset = f + (b * g + w * y) - w) : u.xOffset = u.yOffset = 0, u.xOrigin = T, u.yOrigin = C, u.smooth = !!n, u.origin = t, u.originIsAbsolute = !!i, e.style[ki] = "0px 0px", s && (ji(s, u, "xOrigin", h, T), ji(s, u, "yOrigin", d, C), ji(s, u, "xOffset", p, u.xOffset), ji(s, u, "yOffset", f, u.yOffset)), e.setAttribute("data-svg-origin", T + " " + C)
        }, $i = function (e, t) {
            var i = e._gsap || new wt(e);
            if ("x" in i && !t && !i.uncache) return i;
            var n, r, s, o, a, l, u, c, h, d, p, f, m, g, y, b, w, D, T, C, k, E, _, x, S, P, O, A, M, L, F, j, B = e.style,
                I = i.scaleX < 0, R = _i(e, ki) || "0";
            return n = r = s = l = u = c = h = d = p = 0, o = a = 1, i.svg = !(!e.getCTM || !Li(e)), g = Ui(e, i.svg), i.svg && (x = !i.uncache && e.getAttribute("data-svg-origin"), Gi(e, x || R, !!x || i.originIsAbsolute, !1 !== i.smooth, g)), f = i.xOrigin || 0, m = i.yOrigin || 0, g !== qi && (D = g[0], T = g[1], C = g[2], k = g[3], n = E = g[4], r = _ = g[5], 6 === g.length ? (o = Math.sqrt(D * D + T * T), a = Math.sqrt(k * k + C * C), l = D || T ? oi(T, D) * ri : 0, (h = C || k ? oi(C, k) * ri + l : 0) && (a *= Math.cos(h * si)), i.svg && (n -= f - (f * D + m * C), r -= m - (f * T + m * k))) : (j = g[6], L = g[7], O = g[8], A = g[9], M = g[10], F = g[11], n = g[12], r = g[13], s = g[14], u = (y = oi(j, M)) * ri, y && (x = E * (b = Math.cos(-y)) + O * (w = Math.sin(-y)), S = _ * b + A * w, P = j * b + M * w, O = E * -w + O * b, A = _ * -w + A * b, M = j * -w + M * b, F = L * -w + F * b, E = x, _ = S, j = P), c = (y = oi(-C, M)) * ri, y && (b = Math.cos(-y), F = k * (w = Math.sin(-y)) + F * b, D = x = D * b - O * w, T = S = T * b - A * w, C = P = C * b - M * w), l = (y = oi(T, D)) * ri, y && (x = D * (b = Math.cos(y)) + T * (w = Math.sin(y)), S = E * b + _ * w, T = T * b - D * w, _ = _ * b - E * w, D = x, E = S), u && Math.abs(u) + Math.abs(l) > 359.9 && (u = l = 0, c = 180 - c), o = ae(Math.sqrt(D * D + T * T + C * C)), a = ae(Math.sqrt(_ * _ + j * j)), y = oi(E, _), h = Math.abs(y) > 2e-4 ? y * ri : 0, p = F ? 1 / (F < 0 ? -F : F) : 0), i.svg && (x = e.getAttribute("transform"), i.forceCSS = e.setAttribute("transform", "") || !Xi(_i(e, Ci)), x && e.setAttribute("transform", x))), Math.abs(h) > 90 && Math.abs(h) < 270 && (I ? (o *= -1, h += l <= 0 ? 180 : -180, l += l <= 0 ? 180 : -180) : (a *= -1, h += h <= 0 ? 180 : -180)), i.x = ((i.xPercent = n && Math.round(e.offsetWidth / 2) === Math.round(-n) ? -50 : 0) ? 0 : n) + "px", i.y = ((i.yPercent = r && Math.round(e.offsetHeight / 2) === Math.round(-r) ? -50 : 0) ? 0 : r) + "px", i.z = s + "px", i.scaleX = ae(o), i.scaleY = ae(a), i.rotation = ae(l) + "deg", i.rotationX = ae(u) + "deg", i.rotationY = ae(c) + "deg", i.skewX = h + "deg", i.skewY = d + "deg", i.transformPerspective = p + "px", (i.zOrigin = parseFloat(R.split(" ")[2]) || 0) && (B[ki] = Ki(R)), i.xOffset = i.yOffset = 0, i.force3D = v.force3D, i.renderTransform = i.svg ? en : ii ? Zi : Qi, i.uncache = 0, i
        }, Ki = function (e) {
            return (e = e.split(" "))[0] + " " + e[1]
        }, Ji = function (e, t, i) {
            var n = Re(t);
            return ae(parseFloat(t) + parseFloat(Ii(e, "x", i + "px", n))) + n
        }, Qi = function (e, t) {
            t.z = "0px", t.rotationY = t.rotationX = "0deg", t.force3D = 0, Zi(e, t)
        }, Zi = function (e, t) {
            var i = t || this, n = i.xPercent, r = i.yPercent, s = i.x, o = i.y, a = i.z, l = i.rotation, u = i.rotationY,
                c = i.rotationX, h = i.skewX, d = i.skewY, p = i.scaleX, f = i.scaleY, m = i.transformPerspective,
                g = i.force3D, v = i.target, y = i.zOrigin, b = "", w = "auto" === g && e && 1 !== e || !0 === g;
            if (y && ("0deg" !== c || "0deg" !== u)) {
                var D, T = parseFloat(u) * si, C = Math.sin(T), k = Math.cos(T);
                T = parseFloat(c) * si, D = Math.cos(T), s = Ji(v, s, C * D * -y), o = Ji(v, o, -Math.sin(T) * -y), a = Ji(v, a, k * D * -y + y)
            }
            "0px" !== m && (b += "perspective(" + m + ") "), (n || r) && (b += "translate(" + n + "%, " + r + "%) "), (w || "0px" !== s || "0px" !== o || "0px" !== a) && (b += "0px" !== a || w ? "translate3d(" + s + ", " + o + ", " + a + ") " : "translate(" + s + ", " + o + ") "), "0deg" !== l && (b += "rotate(" + l + ") "), "0deg" !== u && (b += "rotateY(" + u + ") "), "0deg" !== c && (b += "rotateX(" + c + ") "), "0deg" === h && "0deg" === d || (b += "skew(" + h + ", " + d + ") "), 1 === p && 1 === f || (b += "scale(" + p + ", " + f + ") "), v.style[Ci] = b || "translate(0, 0)"
        }, en = function (e, t) {
            var i, n, r, s, o, a = t || this, l = a.xPercent, u = a.yPercent, c = a.x, h = a.y, d = a.rotation, p = a.skewX,
                f = a.skewY, m = a.scaleX, g = a.scaleY, v = a.target, y = a.xOrigin, b = a.yOrigin, w = a.xOffset,
                D = a.yOffset, T = a.forceCSS, C = parseFloat(c), k = parseFloat(h);
            d = parseFloat(d), p = parseFloat(p), (f = parseFloat(f)) && (p += f = parseFloat(f), d += f), d || p ? (d *= si, p *= si, i = Math.cos(d) * m, n = Math.sin(d) * m, r = Math.sin(d - p) * -g, s = Math.cos(d - p) * g, p && (f *= si, o = Math.tan(p - f), r *= o = Math.sqrt(1 + o * o), s *= o, f && (o = Math.tan(f), i *= o = Math.sqrt(1 + o * o), n *= o)), i = ae(i), n = ae(n), r = ae(r), s = ae(s)) : (i = m, s = g, n = r = 0), (C && !~(c + "").indexOf("px") || k && !~(h + "").indexOf("px")) && (C = Ii(v, "x", c, "px"), k = Ii(v, "y", h, "px")), (y || b || w || D) && (C = ae(C + y - (y * i + b * r) + w), k = ae(k + b - (y * n + b * s) + D)), (l || u) && (o = v.getBBox(), C = ae(C + l / 100 * o.width), k = ae(k + u / 100 * o.height)), o = "matrix(" + i + "," + n + "," + r + "," + s + "," + C + "," + k + ")", v.setAttribute("transform", o), T && (v.style[Ci] = o)
        }, tn = function (e, t, i, n, r, s) {
            var o, a, l = _(r), u = parseFloat(r) * (l && ~r.indexOf("rad") ? ri : 1), c = s ? u * s : u - n,
                h = n + c + "deg";
            return l && ("short" === (o = r.split("_")[1]) && (c %= 360) != c % 180 && (c += c < 0 ? 360 : -360), "cw" === o && c < 0 ? c = (c + 36e9) % 360 - 360 * ~~(c / 360) : "ccw" === o && c > 0 && (c = (c - 36e9) % 360 - 360 * ~~(c / 360))), e._pt = a = new Wt(e._pt, t, i, n, c, di), a.e = h, a.u = "deg", e._props.push(i), a
        }, nn = function (e, t, i) {
            var n, r, s, o, a, l, u, c = ei.style, h = i._gsap;
            for (r in c.cssText = getComputedStyle(i).cssText + ";position:absolute;display:block;", c[Ci] = t, Kt.body.appendChild(ei), n = $i(ei, 1), ni) (s = h[r]) !== (o = n[r]) && "perspective,force3D,transformOrigin,svgOrigin".indexOf(r) < 0 && (a = Re(s) !== (u = Re(o)) ? Ii(i, r, s, u) : parseFloat(s), l = parseFloat(o), e._pt = new Wt(e._pt, h, r, a, l - a, hi), e._pt.u = u || 0, e._props.push(r));
            Kt.body.removeChild(ei)
        };
    oe("padding,margin,Width,Radius", function (e, t) {
        var i = "Top", n = "Right", r = "Bottom", s = "Left",
            o = (t < 3 ? [i, n, r, s] : [i + s, i + n, r + n, r + s]).map(function (i) {
                return t < 2 ? e + i : "border" + i + e
            });
        Vi[t > 1 ? "border" + e : e] = function (e, t, i, n, r) {
            var s, a;
            if (arguments.length < 4) return s = o.map(function (t) {
                return Ri(e, t, i)
            }), 5 === (a = s.join(" ")).split(s[0]).length ? s[0] : a;
            s = (n + "").split(" "), a = {}, o.forEach(function (e, t) {
                return a[e] = s[t] = s[t] || s[(t - 1) / 2 | 0]
            }), e.init(t, a, r)
        }
    });
    var rn, sn, on = {
        name: "css", register: Pi, targetTest: function (e) {
            return e.style && e.nodeType
        }, init: function (e, t, i, n, r) {
            var s, o, a, l, u, c, h, d, p, f, m, g, y, b, w, D, T, C, k, E = this._props, _ = e.style;
            for (h in Qt || Pi(), t) if ("autoRound" !== h && (o = t[h], !Q[h] || !Et(h, t, i, n, e, r))) if (u = typeof o, c = Vi[h], "function" === u && (u = typeof (o = o.call(i, n, e, r))), "string" === u && ~o.indexOf("random(") && (o = Ge(o)), c) c(this, e, h, o, i) && (w = 1); else if ("--" === h.substr(0, 2)) this.add(_, "setProperty", getComputedStyle(e).getPropertyValue(h) + "", o + "", n, r, 0, 0, h); else {
                if (s = Ri(e, h), l = parseFloat(s), (f = "string" === u && "=" === o.charAt(1) ? +(o.charAt(0) + "1") : 0) && (o = o.substr(2)), a = parseFloat(o), h in ci && ("autoAlpha" === h && (1 === l && "hidden" === Ri(e, "visibility") && a && (l = 0), ji(this, _, "visibility", l ? "inherit" : "hidden", a ? "inherit" : "hidden", !a)), "scale" !== h && "transform" !== h && ~(h = ci[h]).indexOf(",") && (h = h.split(",")[0])), m = h in ni) if (g || ((y = e._gsap).renderTransform || $i(e), b = !1 !== t.smoothOrigin && y.smooth, (g = this._pt = new Wt(this._pt, _, Ci, 0, 1, y.renderTransform, y, 0, -1)).dep = 1), "scale" === h) this._pt = new Wt(this._pt, y, "scaleY", y.scaleY, f ? f * a : a - y.scaleY), E.push("scaleY", h), h += "X"; else {
                    if ("transformOrigin" === h) {
                        T = void 0, C = void 0, k = void 0, C = (T = (D = o).split(" "))[0], k = T[1] || "50%", "top" !== C && "bottom" !== C && "left" !== k && "right" !== k || (D = C, C = k, k = D), T[0] = zi[C] || C, T[1] = zi[k] || k, o = T.join(" "), y.svg ? Gi(e, o, 0, b, 0, this) : ((p = parseFloat(o.split(" ")[2]) || 0) !== y.zOrigin && ji(this, y, "zOrigin", y.zOrigin, p), ji(this, _, h, Ki(s), Ki(o)));
                        continue
                    }
                    if ("svgOrigin" === h) {
                        Gi(e, o, 1, b, 0, this);
                        continue
                    }
                    if (h in Wi) {
                        tn(this, y, h, l, o, f);
                        continue
                    }
                    if ("smoothOrigin" === h) {
                        ji(this, y, "smooth", y.smooth, o);
                        continue
                    }
                    if ("force3D" === h) {
                        y[h] = o;
                        continue
                    }
                    if ("transform" === h) {
                        nn(this, o, e);
                        continue
                    }
                } else h in _ || (h = Si(h) || h);
                if (m || (a || 0 === a) && (l || 0 === l) && !ui.test(o) && h in _) a || (a = 0), (d = (s + "").substr((l + "").length)) !== (p = (o + "").substr((a + "").length) || (h in v.units ? v.units[h] : d)) && (l = Ii(e, h, s, p)), this._pt = new Wt(this._pt, m ? y : _, h, l, f ? f * a : a - l, "px" !== p || !1 === t.autoRound || m ? hi : fi), this._pt.u = p || 0, d !== p && (this._pt.b = s, this._pt.r = pi); else if (h in _) Ni.call(this, e, h, s, o); else {
                    if (!(h in e)) {
                        X(h, o);
                        continue
                    }
                    this.add(e, h, e[h], o, n, r)
                }
                E.push(h)
            }
            w && qt(this)
        }, get: Ri, aliases: ci, getSetter: function (e, t, i) {
            var n = ci[t];
            return n && n.indexOf(",") < 0 && (t = n), t in ni && t !== ki && (e._gsap.x || Ri(e, "x")) ? i && ti === i ? "scale" === t ? wi : bi : (ti = i || {}) && ("scale" === t ? Di : Ti) : e.style && !P(e.style[t]) ? vi : ~t.indexOf("-") ? yi : jt(e, t)
        }, core: {_removeProperty: Fi, _getMatrix: Ui}
    };
    Gt.utils.checkPrefix = Si, sn = oe("x,y,z,scale,scaleX,scaleY,xPercent,yPercent," + (rn = "rotation,rotationX,rotationY,skewX,skewY") + ",transform,transformOrigin,svgOrigin,force3D,smoothOrigin,transformPerspective", function (e) {
        ni[e] = 1
    }), oe(rn, function (e) {
        v.units[e] = "deg", Wi[e] = 1
    }), ci[sn[13]] = "x,y,z,scale,scaleX,scaleY,xPercent,yPercent," + rn, oe("0:translateX,1:translateY,2:translateZ,8:rotate,8:rotationZ,8:rotateZ,9:rotateX,10:rotateY", function (e) {
        var t = e.split(":");
        ci[t[1]] = sn[t[0]]
    }), oe("x,y,z,top,right,bottom,left,width,height,fontSize,padding,margin,perspective", function (e) {
        v.units[e] = "px"
    }), Gt.registerPlugin(on);
    var an = Gt.registerPlugin(on) || Gt, ln = (an.core.Tween, function (e) {
        return (arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : document).querySelector(e)
    }), un = function (e) {
        return (arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : document).querySelectorAll(e)
    };

    function cn(e, t, i, n, r) {
        var s = !0 === r && {passive: !0};
        return e[("a" === t ? "add" : "remove") + "EventListener"](i, n, s)
    }

    var hn, dn, pn, fn, mn, gn = function () {
            return "undefined" != typeof window
        }, vn = function () {
            return hn || gn() && (hn = window.gsap) && hn.registerPlugin && hn
        }, yn = /[-+=\.]*\d+[\.e\-\+]*\d*[e\-\+]*\d*/gi,
        bn = {rect: ["width", "height"], circle: ["r", "r"], ellipse: ["rx", "ry"], line: ["x2", "y2"]},
        wn = function (e) {
            return Math.round(1e4 * e) / 1e4
        }, Dn = function (e) {
            return parseFloat(e || 0)
        }, Tn = function (e, t) {
            return Dn(e.getAttribute(t))
        }, Cn = Math.sqrt, kn = function (e, t, i, n, r, s) {
            return Cn(Math.pow((Dn(i) - Dn(e)) * r, 2) + Math.pow((Dn(n) - Dn(t)) * s, 2))
        }, En = function (e) {
            return console.warn(e)
        }, _n = function (e) {
            return "non-scaling-stroke" === e.getAttribute("vector-effect")
        }, xn = function (e) {
            if (!(e = dn(e)[0])) return 0;
            var t, i, n, r, s, o, a, l = e.tagName.toLowerCase(), u = e.style, c = 1, h = 1;
            _n(e) && (h = e.getScreenCTM(), c = Cn(h.a * h.a + h.b * h.b), h = Cn(h.d * h.d + h.c * h.c));
            try {
                i = e.getBBox()
            } catch (e) {
                En("Some browsers won't measure invisible elements (like display:none or masks inside defs).")
            }
            var d = i || {x: 0, y: 0, width: 0, height: 0}, p = d.x, f = d.y, m = d.width, g = d.height;
            if (i && (m || g) || !bn[l] || (m = Tn(e, bn[l][0]), g = Tn(e, bn[l][1]), "rect" !== l && "line" !== l && (m *= 2, g *= 2), "line" === l && (p = Tn(e, "x1"), f = Tn(e, "y1"), m = Math.abs(m - p), g = Math.abs(g - f))), "path" === l) r = u.strokeDasharray, u.strokeDasharray = "none", t = e.getTotalLength() || 0, c !== h && En("Warning: <path> length cannot be measured when vector-effect is non-scaling-stroke and the element isn't proportionally scaled."), t *= (c + h) / 2, u.strokeDasharray = r; else if ("rect" === l) t = 2 * m * c + 2 * g * h; else if ("line" === l) t = kn(p, f, p + m, f + g, c, h); else if ("polyline" === l || "polygon" === l) for (n = e.getAttribute("points").match(yn) || [], "polygon" === l && n.push(n[0], n[1]), t = 0, s = 2; s < n.length; s += 2) t += kn(n[s - 2], n[s - 1], n[s], n[s + 1], c, h) || 0; else "circle" !== l && "ellipse" !== l || (o = m / 2 * c, a = g / 2 * h, t = Math.PI * (3 * (o + a) - Cn((3 * o + a) * (o + 3 * a))));
            return t || 0
        }, Sn = function (e, t) {
            if (!(e = dn(e)[0])) return [0, 0];
            t || (t = xn(e) + 1);
            var i = pn.getComputedStyle(e), n = i.strokeDasharray || "", r = Dn(i.strokeDashoffset), s = n.indexOf(",");
            return s < 0 && (s = n.indexOf(" ")), (n = s < 0 ? t : Dn(n.substr(0, s)) || 1e-5) > t && (n = t), [Math.max(0, -r), Math.max(0, n - r)]
        }, Pn = function () {
            gn() && (document, pn = window, mn = hn = vn(), dn = hn.utils.toArray, fn = -1 !== ((pn.navigator || {}).userAgent || "").indexOf("Edge"))
        }, On = {
            version: "3.0.4", name: "drawSVG", register: function (e) {
                hn = e, Pn()
            }, init: function (e, t, i, n, r) {
                if (!e.getBBox) return !1;
                mn || Pn();
                var s, o, a, l, u = xn(e) + 1;
                return this._style = e.style, this._target = e, t + "" == "true" ? t = "0 100%" : t ? -1 === (t + "").indexOf(" ") && (t = "0 " + t) : t = "0 0", o = function (e, t, i) {
                    var n, r, s = e.indexOf(" ");
                    return s < 0 ? (n = void 0 !== i ? i + "" : e, r = e) : (n = e.substr(0, s), r = e.substr(s + 1)), (n = ~n.indexOf("%") ? Dn(n) / 100 * t : Dn(n)) > (r = ~r.indexOf("%") ? Dn(r) / 100 * t : Dn(r)) ? [r, n] : [n, r]
                }(t, u, (s = Sn(e, u))[0]), this._length = wn(u + 10), 0 === s[0] && 0 === o[0] ? (a = Math.max(1e-5, o[1] - u), this._dash = wn(u + a), this._offset = wn(u - s[1] + a), this._offsetPT = this.add(this, "_offset", this._offset, wn(u - o[1] + a))) : (this._dash = wn(s[1] - s[0]) || 1e-6, this._offset = wn(-s[0]), this._dashPT = this.add(this, "_dash", this._dash, wn(o[1] - o[0]) || 1e-5), this._offsetPT = this.add(this, "_offset", this._offset, wn(-o[0]))), fn && (l = pn.getComputedStyle(e)).strokeLinecap !== l.strokeLinejoin && (o = Dn(l.strokeMiterlimit), this.add(e.style, "strokeMiterlimit", o, o + .01)), this._live = _n(e) || ~(t + "").indexOf("live"), this._props.push("drawSVG"), 1
            }, render: function (e, t) {
                var i, n, r, s, o = t._pt, a = t._style;
                if (o) {
                    for (t._live && (i = xn(t._target) + 11) !== t._length && (n = i / t._length, t._length = i, t._offsetPT.s *= n, t._offsetPT.c *= n, t._dashPT ? (t._dashPT.s *= n, t._dashPT.c *= n) : t._dash *= n); o;) o.r(e, o.d), o = o._next;
                    r = t._dash, s = t._offset, i = t._length, a.strokeDashoffset = t._offset, 1 !== e && e ? a.strokeDasharray = r + "px," + i + "px" : (r - s < .001 && i - r <= 10 && (a.strokeDashoffset = s + 1), a.strokeDasharray = s < .001 && i - r <= 10 ? "none" : s === r ? "0px, 999999px" : r + "px," + i + "px")
                }
            }, getLength: xn, getPosition: Sn
        };
    vn() && hn.registerPlugin(On);
    var An = /([\uD800-\uDBFF][\uDC00-\uDFFF](?:[\u200D\uFE0F][\uD800-\uDBFF][\uDC00-\uDFFF]){2,}|\uD83D\uDC69(?:\u200D(?:(?:\uD83D\uDC69\u200D)?\uD83D\uDC67|(?:\uD83D\uDC69\u200D)?\uD83D\uDC66)|\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC69\u200D(?:\uD83D\uDC69\u200D)?\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC69\u200D(?:\uD83D\uDC69\u200D)?\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]\uFE0F|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC6F\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3C-\uDD3E\uDDD6-\uDDDF])\u200D[\u2640\u2642]\uFE0F|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDF4\uD83C\uDDF2|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uFE0F\u200D[\u2640\u2642]|(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642])\uFE0F|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC69\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708]))\uFE0F|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83D\uDC69\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69]))|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74|\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67)\uDB40\uDC7F|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC66\u200D\uD83D\uDC66|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]))|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC68(?:\u200D(?:(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC67|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC66)|\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDD1-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])?|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF8]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD4C\uDD50-\uDD6B\uDD80-\uDD97\uDDC0\uDDD0-\uDDE6])|(?:[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u2660\u2663\u2665\u2666\u2668\u267B\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF8]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD4C\uDD50-\uDD6B\uDD80-\uDD97\uDDC0\uDDD0-\uDDE6])\uFE0F)/;

    function Mn(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Ln(e) {
        return (Ln = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    var Fn, jn, Bn, In = /(?:\r|\n|\t\t)/g, Rn = /(?:\s\s+)/g, Nn = function (e) {
        return jn.getComputedStyle(e)
    }, zn = Array.isArray, Hn = [].slice, Vn = function (e, t) {
        var i;
        return zn(e) ? e : "string" === (i = Ln(e)) && !t && e ? Hn.call(Fn.querySelectorAll(e), 0) : e && "object" === i && "length" in e ? Hn.call(e, 0) : e ? [e] : []
    }, qn = function (e) {
        return "absolute" === e.position || !0 === e.absolute
    }, Wn = function (e, t) {
        for (var i, n = t.length; --n > -1;) if (i = t[n], e.substr(0, i.length) === i) return i.length
    }, Xn = function () {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
            t = arguments.length > 1 ? arguments[1] : void 0, i = ~e.indexOf("++"), n = 1;
        return i && (e = e.split("++").join("")), function () {
            return "<" + t + " style='position:relative;display:inline-block;'" + (e ? " class='" + e + (i ? n++ : "") + "'>" : ">")
        }
    }, Yn = function e(t, i, n) {
        var r = t.nodeType;
        if (1 === r || 9 === r || 11 === r) for (t = t.firstChild; t; t = t.nextSibling) e(t, i, n); else 3 !== r && 4 !== r || (t.nodeValue = t.nodeValue.split(i).join(n))
    }, Un = function (e, t) {
        for (var i = t.length; --i > -1;) e.push(t[i])
    }, Gn = function (e, t, i) {
        for (var n; e && e !== t;) {
            if (n = e._next || e.nextSibling) return n.textContent.charAt(0) === i;
            e = e.parentNode || e._parent
        }
    }, $n = function e(t) {
        var i, n, r = Vn(t.childNodes), s = r.length;
        for (i = 0; i < s; i++) (n = r[i])._isSplit ? e(n) : (i && 3 === n.previousSibling.nodeType ? n.previousSibling.nodeValue += 3 === n.nodeType ? n.nodeValue : n.firstChild.nodeValue : 3 !== n.nodeType && t.insertBefore(n.firstChild, n), t.removeChild(n))
    }, Kn = function (e, t) {
        return parseFloat(t[e]) || 0
    }, Jn = function (e, t, i, n, r, s, o) {
        var a, l, u, c, h, d, p, f, m, g, v, y, b = Nn(e), w = Kn("paddingLeft", b), D = -999,
            T = Kn("borderBottomWidth", b) + Kn("borderTopWidth", b),
            C = Kn("borderLeftWidth", b) + Kn("borderRightWidth", b), k = Kn("paddingTop", b) + Kn("paddingBottom", b),
            E = Kn("paddingLeft", b) + Kn("paddingRight", b), _ = .2 * Kn("fontSize", b), x = b.textAlign, S = [],
            P = [], O = [], A = t.wordDelimiter || " ", M = t.tag ? t.tag : t.span ? "span" : "div",
            L = t.type || t.split || "chars,words,lines", F = r && ~L.indexOf("lines") ? [] : null,
            j = ~L.indexOf("words"), B = ~L.indexOf("chars"), I = qn(t), R = t.linesClass, N = ~(R || "").indexOf("++"),
            z = [];
        for (N && (R = R.split("++").join("")), u = (l = e.getElementsByTagName("*")).length, h = [], a = 0; a < u; a++) h[a] = l[a];
        if (F || I) for (a = 0; a < u; a++) ((d = (c = h[a]).parentNode === e) || I || B && !j) && (y = c.offsetTop, F && d && Math.abs(y - D) > _ && ("BR" !== c.nodeName || 0 === a) && (p = [], F.push(p), D = y), I && (c._x = c.offsetLeft, c._y = y, c._w = c.offsetWidth, c._h = c.offsetHeight), F && ((c._isSplit && d || !B && d || j && d || !j && c.parentNode.parentNode === e && !c.parentNode._isSplit) && (p.push(c), c._x -= w, Gn(c, e, A) && (c._wordEnd = !0)), "BR" === c.nodeName && (c.nextSibling && "BR" === c.nextSibling.nodeName || 0 === a) && F.push([])));
        for (a = 0; a < u; a++) d = (c = h[a]).parentNode === e, "BR" !== c.nodeName ? (I && (m = c.style, j || d || (c._x += c.parentNode._x, c._y += c.parentNode._y), m.left = c._x + "px", m.top = c._y + "px", m.position = "absolute", m.display = "block", m.width = c._w + 1 + "px", m.height = c._h + "px"), !j && B ? c._isSplit ? (c._next = c.nextSibling, c.parentNode.appendChild(c)) : c.parentNode._isSplit ? (c._parent = c.parentNode, !c.previousSibling && c.firstChild && (c.firstChild._isFirst = !0), c.nextSibling && " " === c.nextSibling.textContent && !c.nextSibling.nextSibling && z.push(c.nextSibling), c._next = c.nextSibling && c.nextSibling._isFirst ? null : c.nextSibling, c.parentNode.removeChild(c), h.splice(a--, 1), u--) : d || (y = !c.nextSibling && Gn(c.parentNode, e, A), c.parentNode._parent && c.parentNode._parent.appendChild(c), y && c.parentNode.appendChild(Fn.createTextNode(" ")), "span" === M && (c.style.display = "inline"), S.push(c)) : c.parentNode._isSplit && !c._isSplit && "" !== c.innerHTML ? P.push(c) : B && !c._isSplit && ("span" === M && (c.style.display = "inline"), S.push(c))) : F || I ? (c.parentNode && c.parentNode.removeChild(c), h.splice(a--, 1), u--) : j || e.appendChild(c);
        for (a = z.length; --a > -1;) z[a].parentNode.removeChild(z[a]);
        if (F) {
            for (I && (g = Fn.createElement(M), e.appendChild(g), v = g.offsetWidth + "px", y = g.offsetParent === e ? 0 : e.offsetLeft, e.removeChild(g)), m = e.style.cssText, e.style.cssText = "display:none;"; e.firstChild;) e.removeChild(e.firstChild);
            for (f = " " === A && (!I || !j && !B), a = 0; a < F.length; a++) {
                for (p = F[a], (g = Fn.createElement(M)).style.cssText = "display:block;text-align:" + x + ";position:" + (I ? "absolute;" : "relative;"), R && (g.className = R + (N ? a + 1 : "")), O.push(g), u = p.length, l = 0; l < u; l++) "BR" !== p[l].nodeName && (c = p[l], g.appendChild(c), f && c._wordEnd && g.appendChild(Fn.createTextNode(" ")), I && (0 === l && (g.style.top = c._y + "px", g.style.left = w + y + "px"), c.style.top = "0px", y && (c.style.left = c._x - y + "px")));
                0 === u ? g.innerHTML = "&nbsp;" : j || B || ($n(g), Yn(g, String.fromCharCode(160), " ")), I && (g.style.width = v, g.style.height = c._h + "px"), e.appendChild(g)
            }
            e.style.cssText = m
        }
        I && (o > e.clientHeight && (e.style.height = o - k + "px", e.clientHeight < o && (e.style.height = o + T + "px")), s > e.clientWidth && (e.style.width = s - E + "px", e.clientWidth < s && (e.style.width = s + C + "px"))), Un(i, S), j && Un(n, P), Un(r, O)
    }, Qn = function e(t, i, n, r) {
        var s, o, a = Vn(t.childNodes), l = a.length, u = qn(i);
        if (3 !== t.nodeType || l > 1) {
            for (i.absolute = !1, s = 0; s < l; s++) (3 !== (o = a[s]).nodeType || /\S+/.test(o.nodeValue)) && (u && 3 !== o.nodeType && "inline" === Nn(o).display && (o.style.display = "inline-block", o.style.position = "relative"), o._isSplit = !0, e(o, i, n, r));
            return i.absolute = u, void (t._isSplit = !0)
        }
        !function (e, t, i, n) {
            var r, s, o, a, l, u, c, h, d = t.tag ? t.tag : t.span ? "span" : "div",
                p = ~(t.type || t.split || "chars,words,lines").indexOf("chars"), f = qn(t), m = t.wordDelimiter || " ",
                g = " " !== m ? "" : f ? "&#173; " : " ", v = "</" + d + ">", y = 1,
                b = t.specialChars ? "function" == typeof t.specialChars ? t.specialChars : Wn : null,
                w = Fn.createElement("div"), D = e.parentNode;
            for (D.insertBefore(w, e), w.textContent = e.nodeValue, D.removeChild(e), c = -1 !== (r = function e(t) {
                var i = t.nodeType, n = "";
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) n += e(t)
                } else if (3 === i || 4 === i) return t.nodeValue;
                return n
            }(e = w)).indexOf("<"), !1 !== t.reduceWhiteSpace && (r = r.replace(Rn, " ").replace(In, "")), c && (r = r.split("<").join("{{LT}}")), l = r.length, s = (" " === r.charAt(0) ? g : "") + i(), o = 0; o < l; o++) if (u = r.charAt(o), b && (h = b(r.substr(o), t.specialChars))) u = r.substr(o, h || 1), s += p && " " !== u ? n() + u + "</" + d + ">" : u, o += h - 1; else if (u === m && r.charAt(o - 1) !== m && o) {
                for (s += y ? v : "", y = 0; r.charAt(o + 1) === m;) s += g, o++;
                o === l - 1 ? s += g : ")" !== r.charAt(o + 1) && (s += g + i(), y = 1)
            } else "{" === u && "{{LT}}" === r.substr(o, 6) ? (s += p ? n() + "{{LT}}</" + d + ">" : "{{LT}}", o += 5) : u.charCodeAt(0) >= 55296 && u.charCodeAt(0) <= 56319 || r.charCodeAt(o + 1) >= 65024 && r.charCodeAt(o + 1) <= 65039 ? (a = ((r.substr(o, 12).split(An) || [])[1] || "").length || 2, s += p && " " !== u ? n() + r.substr(o, a) + "</" + d + ">" : r.substr(o, a), o += a - 1) : s += p && " " !== u ? n() + u + "</" + d + ">" : u;
            e.outerHTML = s + (y ? v : ""), c && Yn(D, "{{LT}}", "<")
        }(t, i, n, r)
    }, Zn = function () {
        function e(t, i) {
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), Bn || (Fn = document, jn = window, Bn = 1), this.elements = Vn(t), this.chars = [], this.words = [], this.lines = [], this._originals = [], this.vars = i || {}, this.split(i)
        }

        var t, i, n;
        return t = e, n = [{
            key: "create", value: function (t, i) {
                return new e(t, i)
            }
        }], (i = [{
            key: "split", value: function (e) {
                this.isSplit && this.revert(), this.vars = e = e || this.vars, this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;
                for (var t, i, n, r = this.elements.length, s = e.tag ? e.tag : e.span ? "span" : "div", o = Xn(e.wordsClass, s), a = Xn(e.charsClass, s); --r > -1;) n = this.elements[r], this._originals[r] = n.innerHTML, t = n.clientHeight, i = n.clientWidth, Qn(n, e, o, a), Jn(n, e, this.chars, this.words, this.lines, i, t);
                return this.chars.reverse(), this.words.reverse(), this.lines.reverse(), this.isSplit = !0, this
            }
        }, {
            key: "revert", value: function () {
                var e = this._originals;
                if (!e) throw"revert() call wasn't scoped properly.";
                return this.elements.forEach(function (t, i) {
                    return t.innerHTML = e[i]
                }), this.chars = [], this.words = [], this.lines = [], this.isSplit = !1, this
            }
        }]) && Mn(t.prototype, i), n && Mn(t, n), e
    }();

    function er(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function tr(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    Zn.version = "3.0.4", an.registerPlugin(On), an.registerPlugin(Zn);
    var ir = new (function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), tr(this, "animate", function () {
                t.target++, t.target = an.utils.clamp(0, t.max, t.target), t.current += .05 * (t.target - t.current), t.currentRounded = Math.round(100 * t.current) / 100, t.currentRounded <= 99.9 ? t.goRaf() : t.cancelRaf()
            }), tr(this, "reveal", function () {
            }), tr(this, "getProgress", function (e, t) {
                return e / t * 100
            }), this.promises = [], this.el = null, this.target = 0, this.current = 0, this.currentRounded = 0, this.currentNormalize = 0, this.max = 0, this.raf = void 0
        }

        var t, i;
        return t = e, (i = [{
            key: "load", value: function () {
                var e = this;
                return this.el = o.dom.body, this.addMedia(), an.set(".js-loader-content", {alpha: 1}), this.loader = new Promise(function (t) {
                    e.progressPromise(e.promises).then(function () {
                        e.promises = [], t()
                    })
                })
            }
        }, {
            key: "add", value: function (e) {
                this.promises.push(e)
            }
        }, {
            key: "addMedia", value: function () {
                for (var e = this, t = un("img", this.el), i = function (i) {
                    e.promises.push(new Promise(function (e) {
                        var n = new Image;
                        cn(n, "a", "load", e), cn(n, "a", "error", e), n.src = t[i].src
                    }))
                }, n = 0; n < t.length; n++) i(n)
            }
        }, {
            key: "goRaf", value: function () {
                this.raf = requestAnimationFrame(this.animate)
            }
        }, {
            key: "cancelRaf", value: function () {
                cancelAnimationFrame(this.raf), this.reveal()
            }
        }, {
            key: "progressPromise", value: function (e, t) {
                var i = this, n = 0;
                return this.goRaf(), Promise.all(e.map(function (t) {
                    return t.then(function () {
                        n++, i.max = i.getProgress(n, e.length)
                    }).catch(function (e) {
                        console.log(e)
                    })
                }))
            }
        }]) && er(t.prototype, i), e
    }());

    function nr(e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }

    function rr() {
    }

    rr.prototype = {
        on: function (e, t, i) {
            var n = this.e || (this.e = {});
            return (n[e] || (n[e] = [])).push({fn: t, ctx: i}), this
        }, once: function (e, t, i) {
            var n = this;

            function r() {
                n.off(e, r), t.apply(i, arguments)
            }

            return r._ = t, this.on(e, r, i)
        }, emit: function (e) {
            for (var t = [].slice.call(arguments, 1), i = ((this.e || (this.e = {}))[e] || []).slice(), n = 0, r = i.length; n < r; n++) i[n].fn.apply(i[n].ctx, t);
            return this
        }, off: function (e, t) {
            var i = this.e || (this.e = {}), n = i[e], r = [];
            if (n && t) for (var s = 0, o = n.length; s < o; s++) n[s].fn !== t && n[s].fn._ !== t && r.push(n[s]);
            return r.length ? i[e] = r : delete i[e], this
        }
    };
    var sr = rr;
    sr.TinyEmitter = rr;
    var or = function (e) {
        this.wrap = document.querySelector("[data-router-wrapper]"), this.properties = e, this.Transition = e.transition ? new e.transition.class(this.wrap, e.transition.name) : null
    };
    or.prototype.setup = function () {
        this.onEnter && this.onEnter(), this.onEnterCompleted && this.onEnterCompleted()
    }, or.prototype.add = function () {
        this.wrap.insertAdjacentHTML("beforeend", this.properties.view.outerHTML)
    }, or.prototype.update = function () {
        document.title = this.properties.page.title
    }, or.prototype.show = function (e) {
        var t = this;
        return new Promise(function (i) {
            try {
                function n(e) {
                    t.onEnterCompleted && t.onEnterCompleted(), i()
                }

                return t.update(), t.onEnter && t.onEnter(), Promise.resolve(t.Transition ? Promise.resolve(t.Transition.show(e)).then(n) : n())
            } catch (e) {
                return Promise.reject(e)
            }
        })
    }, or.prototype.hide = function (e) {
        var t = this;
        return new Promise(function (i) {
            try {
                function n(e) {
                    t.onLeaveCompleted && t.onLeaveCompleted(), i()
                }

                return t.onLeave && t.onLeave(), Promise.resolve(t.Transition ? Promise.resolve(t.Transition.hide(e)).then(n) : n())
            } catch (e) {
                return Promise.reject(e)
            }
        })
    };
    var ar = new window.DOMParser, lr = function (e, t) {
        this.renderers = e, this.transitions = t
    };
    lr.prototype.getOrigin = function (e) {
        var t = e.match(/(https?:\/\/[\w\-.]+)/);
        return t ? t[1].replace(/https?:\/\//, "") : null
    }, lr.prototype.getPathname = function (e) {
        var t = e.match(/https?:\/\/.*?(\/[\w_\-.\/]+)/);
        return t ? t[1] : "/"
    }, lr.prototype.getAnchor = function (e) {
        var t = e.match(/(#.*)$/);
        return t ? t[1] : null
    }, lr.prototype.getParams = function (e) {
        var t = e.match(/\?([\w_\-.=&]+)/);
        if (!t) return null;
        for (var i = t[1].split("&"), n = {}, r = 0; r < i.length; r++) {
            var s = i[r].split("=");
            n[s[0]] = s[1]
        }
        return n
    }, lr.prototype.getDOM = function (e) {
        return "string" == typeof e ? ar.parseFromString(e, "text/html") : e
    }, lr.prototype.getView = function (e) {
        return e.querySelector("[data-router-view]")
    }, lr.prototype.getSlug = function (e) {
        return e.getAttribute("data-router-view")
    }, lr.prototype.getRenderer = function (e) {
        if (!this.renderers) return Promise.resolve(or);
        if (e in this.renderers) {
            var t = this.renderers[e];
            return "function" != typeof t || or.isPrototypeOf(t) ? "function" == typeof t.then ? Promise.resolve(t).then(function (e) {
                return e.default
            }) : Promise.resolve(t) : Promise.resolve(t()).then(function (e) {
                return e.default
            })
        }
        return Promise.resolve(or)
    }, lr.prototype.getTransition = function (e) {
        return this.transitions ? e in this.transitions ? {
            class: this.transitions[e],
            name: e
        } : "default" in this.transitions ? {class: this.transitions.default, name: "default"} : null : null
    }, lr.prototype.getProperties = function (e) {
        var t = this.getDOM(e), i = this.getView(t), n = this.getSlug(i);
        return {
            page: t,
            view: i,
            slug: n,
            renderer: this.getRenderer(n, this.renderers),
            transition: this.getTransition(n, this.transitions)
        }
    }, lr.prototype.getLocation = function (e) {
        return {
            href: e,
            anchor: this.getAnchor(e),
            origin: this.getOrigin(e),
            params: this.getParams(e),
            pathname: this.getPathname(e)
        }
    };
    var ur = function (e) {
        function t(t) {
            var i = this;
            void 0 === t && (t = {});
            var n = t.renderers, r = t.transitions;
            e.call(this), this.Helpers = new lr(n, r), this.Transitions = r, this.Contextual = !1, this.location = this.Helpers.getLocation(window.location.href), this.properties = this.Helpers.getProperties(document.cloneNode(!0)), this.popping = !1, this.running = !1, this.trigger = null, this.cache = new Map, this.cache.set(this.location.href, this.properties), this.properties.renderer.then(function (e) {
                i.From = new e(i.properties), i.From.setup()
            }), this._navigate = this.navigate.bind(this), window.addEventListener("popstate", this.popState.bind(this)), this.links = document.querySelectorAll("a:not([target]):not([data-router-disabled])"), this.attach(this.links)
        }

        return e && (t.__proto__ = e), (t.prototype = Object.create(e && e.prototype)).constructor = t, t.prototype.attach = function (e) {
            for (var t = 0, i = e; t < i.length; t += 1) i[t].addEventListener("click", this._navigate)
        }, t.prototype.detach = function (e) {
            for (var t = 0, i = e; t < i.length; t += 1) i[t].removeEventListener("click", this._navigate)
        }, t.prototype.navigate = function (e) {
            if (!e.metaKey && !e.ctrlKey) {
                e.preventDefault();
                var t = !!e.currentTarget.hasAttribute("data-transition") && e.currentTarget.dataset.transition;
                this.redirect(e.currentTarget.href, t, e.currentTarget)
            }
        }, t.prototype.redirect = function (e, t, i) {
            if (void 0 === t && (t = !1), void 0 === i && (i = "script"), this.trigger = i, !this.running && e !== this.location.href) {
                var n = this.Helpers.getLocation(e);
                this.Contextual = !1, t && (this.Contextual = this.Transitions.contextual[t].prototype, this.Contextual.name = t), n.origin !== this.location.origin || n.anchor && n.pathname === this.location.pathname ? window.location.href = e : (this.location = n, this.beforeFetch())
            }
        }, t.prototype.popState = function () {
            this.trigger = "popstate", this.Contextual = !1;
            var e = this.Helpers.getLocation(window.location.href);
            this.location.pathname !== e.pathname || !this.location.anchor && !e.anchor ? (this.popping = !0, this.location = e, this.beforeFetch()) : this.location = e
        }, t.prototype.pushState = function () {
            this.popping || window.history.pushState(this.location, "", this.location.href)
        }, t.prototype.fetch = function () {
            try {
                var e = this;
                return Promise.resolve(fetch(e.location.href, {
                    mode: "same-origin",
                    method: "GET",
                    headers: {"X-Requested-With": "Highway"},
                    credentials: "same-origin"
                })).then(function (t) {
                    if (t.status >= 200 && t.status < 300) return t.text();
                    window.location.href = e.location.href
                })
            } catch (e) {
                return Promise.reject(e)
            }
        }, t.prototype.beforeFetch = function () {
            try {
                var e = this;

                function t() {
                    e.afterFetch()
                }

                e.pushState(), e.running = !0, e.emit("NAVIGATE_OUT", {
                    from: {
                        page: e.From.properties.page,
                        view: e.From.properties.view
                    }, trigger: e.trigger, location: e.location
                });
                var i = {trigger: e.trigger, contextual: e.Contextual},
                    n = e.cache.has(e.location.href) ? Promise.resolve(e.From.hide(i)).then(function () {
                        e.properties = e.cache.get(e.location.href)
                    }) : Promise.resolve(Promise.all([e.fetch(), e.From.hide(i)])).then(function (t) {
                        e.properties = e.Helpers.getProperties(t[0]), e.cache.set(e.location.href, e.properties)
                    });
                return Promise.resolve(n && n.then ? n.then(t) : t())
            } catch (e) {
                return Promise.reject(e)
            }
        }, t.prototype.afterFetch = function () {
            try {
                var e = this;
                return Promise.resolve(e.properties.renderer).then(function (t) {
                    return e.To = new t(e.properties), e.To.add(), e.emit("NAVIGATE_IN", {
                        to: {
                            page: e.To.properties.page,
                            view: e.To.wrap.lastElementChild
                        }, trigger: e.trigger, location: e.location
                    }), Promise.resolve(e.To.show({trigger: e.trigger, contextual: e.Contextual})).then(function () {
                        e.popping = !1, e.running = !1, e.detach(e.links), e.links = document.querySelectorAll("a:not([target]):not([data-router-disabled])"), e.attach(e.links), e.emit("NAVIGATE_END", {
                            to: {
                                page: e.To.properties.page,
                                view: e.To.wrap.lastElementChild
                            },
                            from: {page: e.From.properties.page, view: e.From.properties.view},
                            trigger: e.trigger,
                            location: e.location
                        }), e.From = e.To, e.trigger = null
                    })
                })
            } catch (e) {
                return Promise.reject(e)
            }
        }, t
    }(sr), cr = function (e, t) {
        this.wrap = e, this.name = t
    };
    cr.prototype.show = function (e) {
        var t = this, i = e.trigger, n = e.contextual, r = this.wrap.lastElementChild, s = this.wrap.firstElementChild;
        return new Promise(function (e) {
            n ? (r.setAttribute("data-transition-in", n.name), r.removeAttribute("data-transition-out", n.name), n.in && n.in({
                to: r,
                from: s,
                trigger: i,
                done: e
            })) : (r.setAttribute("data-transition-in", t.name), r.removeAttribute("data-transition-out", t.name), t.in && t.in({
                to: r,
                from: s,
                trigger: i,
                done: e
            }))
        })
    }, cr.prototype.hide = function (e) {
        var t = this, i = e.trigger, n = e.contextual, r = this.wrap.firstElementChild;
        return new Promise(function (e) {
            n ? (r.setAttribute("data-transition-out", n.name), r.removeAttribute("data-transition-in", n.name), n.out && n.out({
                from: r,
                trigger: i,
                done: e
            })) : (r.setAttribute("data-transition-out", t.name), r.removeAttribute("data-transition-in", t.name), t.out && t.out({
                from: r,
                trigger: i,
                done: e
            }))
        })
    }, console.log("Highway v2.2.0");
    var hr = {Core: ur, Helpers: lr, Renderer: or, Transition: cr}, dr = i(6);

    function pr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function fr(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    var mr = function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), fr(this, "setInitial", function () {
                var e = t.ui, i = e.modal, n = e.title, r = e.titleThanks, s = e.inputs, o = e.btnSend, a = e.close,
                    l = e.flapOne, u = e.flapTwo;
                an.set(i, {
                    autoAlpha: 0,
                    pointerEvents: "none"
                }), an.set(n, {yPercent: 100}), an.set(r, {yPercent: 100}), an.set(s, {
                    autoAlpha: 0,
                    yPercent: 100
                }), an.set(o, {autoAlpha: 0, xPercent: -50}), an.set(a, {
                    autoAlpha: 0,
                    xPercent: 50
                }), an.set(l, {autoAlpha: 0, yPercent: 50}), an.set(u, {autoAlpha: 0, yPercent: 50})
            }), fr(this, "open", function () {
                o.flags.newsModal = !0, document.body.classList.add("news-is-open"), t.tl && t.tl.kill(), t.tl = an.timeline();
                var e = t.ui, i = e.modal, n = e.title, r = e.titleThanks, s = e.inputs, a = e.btnSend, l = e.close,
                    u = e.flapOne, c = e.flapTwo;
                t.tl.to(i, {
                    autoAlpha: 1,
                    pointerEvents: "auto",
                    duration: 1,
                    ease: "expo.out"
                }, 0).to([u, c], {
                    autoAlpha: 1,
                    yPercent: 0,
                    duration: 1.2,
                    stagger: .2,
                    ease: "expo.out"
                }, .35).to(l, {
                    autoAlpha: 1,
                    xPercent: 0,
                    duration: 1.2,
                    ease: "expo.out"
                }, .75), !1 === o.flags.sendingSubscribe ? t.tl.to(n, {
                    yPercent: 0,
                    duration: 1.5,
                    stagger: .1,
                    ease: "expo.out"
                }, .45).to(s, {
                    autoAlpha: 1,
                    yPercent: 0,
                    duration: 1,
                    stagger: .1,
                    ease: "power3.out"
                }, .65).to(a, {
                    autoAlpha: 1,
                    xPercent: 0,
                    duration: 1.2,
                    ease: "expo.out"
                }, 1.05) : t.tl.to(r, {yPercent: 0, duration: 1.5, stagger: .1, ease: "expo.out"}, .45)
            }), fr(this, "close", function () {
                o.flags.newsModal = !1, document.body.classList.remove("news-is-open"), t.tl && t.tl.kill(), t.tl = an.timeline();
                var e = t.ui, i = e.modal, n = e.title, r = e.titleThanks, s = e.inputs, a = e.btnSend, l = e.close,
                    u = e.flapOne, c = e.flapTwo;
                t.tl.to(l, {autoAlpha: 0, xPercent: 50, duration: 1.2, ease: "expo.out"}, 0).to(n, {
                    yPercent: 100,
                    duration: 1.5,
                    stagger: .075,
                    ease: "expo.out"
                }, 0).to(s, {
                    autoAlpha: 0,
                    yPercent: 100,
                    duration: 1,
                    stagger: .075,
                    ease: "power3.out"
                }, .1).to(a, {
                    autoAlpha: 0,
                    xPercent: -50,
                    duration: 1.2,
                    ease: "expo.out"
                }, 0).to([u, c], {
                    autoAlpha: 0,
                    yPercent: 50,
                    duration: 1.2,
                    stagger: .15,
                    ease: "expo.out"
                }, 0).to(i, {
                    autoAlpha: 0,
                    pointerEvents: "none",
                    duration: 1,
                    ease: "expo.out"
                }, .75), o.flags.sendingSubscribe && t.tl.to(r, {
                    yPercent: 100,
                    duration: 1.5,
                    stagger: .075,
                    ease: "expo.out"
                }, 0)
            }), this.ui = {
                open: ln(".js-open-m-news"),
                close: ln(".js-close-m-news"),
                modal: ln(".js-modal-newsletter"),
                title: un(".js-m-news-title"),
                titleThanks: un(".js-m-news-thanks"),
                inputs: un(".js-m-news-input"),
                btnSend: ln(".js-m-news-send"),
                flapOne: ln(".js-m-flap-one"),
                flapTwo: ln(".js-m-flap-two")
            }, this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "init", value: function () {
                this.setInitial(), this.addEvents()
            }
        }, {
            key: "addEvents", value: function () {
                // this.ui.open.addEventListener("click", this.open), this.ui.close.addEventListener("click", this.close)
            }
        }]) && pr(t.prototype, i), e
    }();

    function gr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    var vr = function () {
        function e(t) {
            var i, n, r = this, s = t.scroll, o = t.limit;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), n = function () {
                var e = r.state, t = e.current, i = e.max;
                r.state.progress = function (e, t, i) {
                    return (e - 0) / (i - 0)
                }(t, 0, i), r.ui.progress.style.transform = "scaleY(".concat(r.state.progress, ")")
            }, (i = "transform") in this ? Object.defineProperty(this, i, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : this[i] = n, this.ui = {progress: ln(".js-progress-scroll")}, this.state = {
                current: s,
                max: o,
                progress: 0
            }
        }

        var t, i;
        return t = e, (i = [{
            key: "init", value: function () {
                this.transform()
            }
        }, {
            key: "destroy", value: function () {
                this.ui.progress.style.transform = "scaleY(".concat(0, ")")
            }
        }]) && gr(t.prototype, i), e
    }();

    function yr() {
        if (!(this instanceof yr)) return new yr;
        this.size = 0, this.uid = 0, this.selectors = [], this.selectorObjects = {}, this.indexes = Object.create(this.indexes), this.activeIndexes = []
    }

    var br = window.document.documentElement,
        wr = br.matches || br.webkitMatchesSelector || br.mozMatchesSelector || br.oMatchesSelector || br.msMatchesSelector;
    yr.prototype.matchesSelector = function (e, t) {
        return wr.call(e, t)
    }, yr.prototype.querySelectorAll = function (e, t) {
        return t.querySelectorAll(e)
    }, yr.prototype.indexes = [];
    var Dr = /^#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/g;
    yr.prototype.indexes.push({
        name: "ID", selector: function (e) {
            var t;
            if (t = e.match(Dr)) return t[0].slice(1)
        }, element: function (e) {
            if (e.id) return [e.id]
        }
    });
    var Tr = /^\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/g;
    yr.prototype.indexes.push({
        name: "CLASS", selector: function (e) {
            var t;
            if (t = e.match(Tr)) return t[0].slice(1)
        }, element: function (e) {
            var t = e.className;
            if (t) {
                if ("string" == typeof t) return t.split(/\s/);
                if ("object" == typeof t && "baseVal" in t) return t.baseVal.split(/\s/)
            }
        }
    });
    var Cr, kr = /^((?:[\w\u00c0-\uFFFF\-]|\\.)+)/g;
    yr.prototype.indexes.push({
        name: "TAG", selector: function (e) {
            var t;
            if (t = e.match(kr)) return t[0].toUpperCase()
        }, element: function (e) {
            return [e.nodeName.toUpperCase()]
        }
    }), yr.prototype.indexes.default = {
        name: "UNIVERSAL", selector: function () {
            return !0
        }, element: function () {
            return [!0]
        }
    }, Cr = "function" == typeof window.Map ? window.Map : function () {
        function e() {
            this.map = {}
        }

        return e.prototype.get = function (e) {
            return this.map[e + " "]
        }, e.prototype.set = function (e, t) {
            this.map[e + " "] = t
        }, e
    }();
    var Er = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g;

    function _r(e, t) {
        var i, n, r, s, o, a, l = (e = e.slice(0).concat(e.default)).length, u = t, c = [];
        do {
            if (Er.exec(""), (r = Er.exec(u)) && (u = r[3], r[2] || !u)) for (i = 0; i < l; i++) if (o = (a = e[i]).selector(r[1])) {
                for (n = c.length, s = !1; n--;) if (c[n].index === a && c[n].key === o) {
                    s = !0;
                    break
                }
                s || c.push({index: a, key: o});
                break
            }
        } while (r);
        return c
    }

    function xr(e, t) {
        var i, n, r;
        for (i = 0, n = e.length; i < n; i++) if (r = e[i], t.isPrototypeOf(r)) return r
    }

    function Sr(e, t) {
        return e.id - t.id
    }

    yr.prototype.logDefaultIndexUsed = function () {
    }, yr.prototype.add = function (e, t) {
        var i, n, r, s, o, a, l, u, c = this.activeIndexes, h = this.selectors, d = this.selectorObjects;
        if ("string" == typeof e) {
            for (d[(i = {
                id: this.uid++,
                selector: e,
                data: t
            }).id] = i, l = _r(this.indexes, e), n = 0; n < l.length; n++) s = (u = l[n]).key, (o = xr(c, r = u.index)) || ((o = Object.create(r)).map = new Cr, c.push(o)), r === this.indexes.default && this.logDefaultIndexUsed(i), (a = o.map.get(s)) || (a = [], o.map.set(s, a)), a.push(i);
            this.size++, h.push(e)
        }
    }, yr.prototype.remove = function (e, t) {
        if ("string" == typeof e) {
            var i, n, r, s, o, a, l, u, c = this.activeIndexes, h = this.selectors = [], d = this.selectorObjects,
                p = {}, f = 1 === arguments.length;
            for (i = _r(this.indexes, e), r = 0; r < i.length; r++) for (n = i[r], s = c.length; s--;) if (a = c[s], n.index.isPrototypeOf(a)) {
                if (l = a.map.get(n.key)) for (o = l.length; o--;) (u = l[o]).selector !== e || !f && u.data !== t || (l.splice(o, 1), p[u.id] = !0);
                break
            }
            for (r in p) delete d[r], this.size--;
            for (r in d) h.push(d[r].selector)
        }
    }, yr.prototype.queryAll = function (e) {
        if (!this.selectors.length) return [];
        var t, i, n, r, s, o, a, l, u = {}, c = [], h = this.querySelectorAll(this.selectors.join(", "), e);
        for (t = 0, n = h.length; t < n; t++) for (s = h[t], i = 0, r = (o = this.matches(s)).length; i < r; i++) u[(l = o[i]).id] ? a = u[l.id] : (a = {
            id: l.id,
            selector: l.selector,
            data: l.data,
            elements: []
        }, u[l.id] = a, c.push(a)), a.elements.push(s);
        return c.sort(Sr)
    }, yr.prototype.matches = function (e) {
        if (!e) return [];
        var t, i, n, r, s, o, a, l, u, c, h, d = this.activeIndexes, p = {}, f = [];
        for (t = 0, r = d.length; t < r; t++) if (l = (a = d[t]).element(e)) for (i = 0, s = l.length; i < s; i++) if (u = a.map.get(l[i])) for (n = 0, o = u.length; n < o; n++) !p[h = (c = u[n]).id] && this.matchesSelector(e, c.selector) && (p[h] = !0, f.push(c));
        return f.sort(Sr)
    };
    const Pr = {}, Or = {}, Ar = ["mouseenter", "mouseleave"];

    function Mr(e) {
        void 0 === Or[e] && (Or[e] = [])
    }

    function Lr(e) {
        return "string" == typeof e ? document.querySelectorAll(e) : e
    }

    function Fr(e) {
        let t = function (e, t) {
            const i = [];
            let n = t;
            do {
                if (1 !== n.nodeType) break;
                const t = e.matches(n);
                t.length && i.push({delegatedTarget: n, stack: t})
            } while (n = n.parentElement);
            return i
        }(Pr[e.type], e.target);
        if (t.length) for (let i = 0; i < t.length; i++) for (let n = 0; n < t[i].stack.length; n++) -1 !== Ar.indexOf(e.type) ? (jr(e, t[i].delegatedTarget), e.target === t[i].delegatedTarget && t[i].stack[n].data(e)) : (jr(e, t[i].delegatedTarget), t[i].stack[n].data(e))
    }

    function jr(e, t) {
        Object.defineProperty(e, "currentTarget", {configurable: !0, enumerable: !0, value: t})
    }

    function Br(e) {
        return JSON.parse(JSON.stringify(e))
    }

    var Ir = new class {
        bindAll(e, t) {
            void 0 === t && (t = Object.getOwnPropertyNames(Object.getPrototypeOf(e)));
            for (let i = 0; i < t.length; i++) e[t[i]] = e[t[i]].bind(e)
        }

        on(e, t, i, n) {
            if ("function" == typeof t && void 0 === i) return Mr(e), void Or[e].push(t);
            const r = e.split(" ");
            for (let e = 0; e < r.length; e++) if (t.nodeType && 1 === t.nodeType || t === window || t === document) t.addEventListener(r[e], i, n); else {
                t = Lr(t);
                for (let s = 0; s < t.length; s++) t[s].addEventListener(r[e], i, n)
            }
        }

        delegate(e, t, i) {
            const n = e.split(" ");
            for (let e = 0; e < n.length; e++) {
                let r = Pr[n[e]];
                void 0 === r && (r = new yr, Pr[n[e]] = r, -1 !== Ar.indexOf(n[e]) ? document.addEventListener(n[e], Fr, !0) : document.addEventListener(n[e], Fr)), r.add(t, i)
            }
        }

        off(e, t, i) {
            if (void 0 === t) return void (Or[e] = []);
            if ("function" == typeof t) {
                Mr(e);
                for (let i = 0; i < Or[e].length; i++) Or[e][i] === t && Or[e].splice(i, 1);
                return
            }
            const n = e.split(" ");
            for (let e = 0; e < n.length; e++) {
                const r = Pr[n[e]];
                if (void 0 === r || (r.remove(t, i), 0 !== r.size)) if (void 0 === t.removeEventListener) {
                    t = Lr(t);
                    for (let r = 0; r < t.length; r++) t[r].removeEventListener(n[e], i)
                } else t.removeEventListener(n[e], i); else delete Pr[n[e]], document.removeEventListener(n[e], Fr)
            }
        }

        emit(e, ...t) {
            !function (e, t) {
                if (Or[e]) for (let i = 0; i < Or[e].length; i++) Or[e][i](...t)
            }(e, t)
        }

        debugDelegated() {
            return Br(Pr)
        }

        debugBus() {
            return Br(Or)
        }
    }, Rr = i(15), Nr = i.n(Rr);

    function zr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Hr(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    new function e() {
        var t, i;
        !function (e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }(this, e), i = function (e) {
            var t = e.y, i = e.deltaY;
            o.flags.locked || "INPUT" === document.activeElement.nodeName || "TEXTAREA" === document.activeElement.nodeName || Ir.emit("scroll", {
                y: -1 * i,
                yNormal: t
            })
        }, (t = "onVS") in this ? Object.defineProperty(this, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : this[t] = i;
        var n = o.flags, r = n.windows;
        n.isDesktop, o.isDesktop && (this.vs = new Nr.a({
            mouseMultiplier: r ? 1.1 : .45,
            touchMultiplier: 3.5,
            firefoxMultiplier: r ? 40 : 90
        }).on(this.onVS))
    };
    var Vr = o.flags, qr = (Vr.isDevice, Vr.isDesktop, new (function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), Hr(this, "tick", function () {
                o.flags.locked || (t.current = function (e, t, i) {
                    return e * (1 - i) + t * i
                }(t.current, t.target, t.ease), t.rounded = Math.round(100 * t.current) / 100, t.diff = 5e-4 * (t.target - t.current), o.flags.isDesktop && t.calcMouse(), Ir.emit("tick", {
                    target: t.target,
                    current: t.rounded,
                    mouse: t.mouse
                }))
            }), Hr(this, "onScroll", function (e) {
                var i = e.y;
                t.target += i, t.clamp()
            }), Hr(this, "resize", function () {
                t.clamp(), t.rounded = t.current = t.target
            }), Hr(this, "move", function (e) {
                var i = e.y, n = e.x, r = e.target;
                t.mouse.x = n, t.mouse.y = i, t.mouse.target = r
            }), this.target = 0, this.current = 0, this.rounded = 0, this.ease = .115, this.mouse = {
                x: 0,
                y: 0,
                target: null,
                glX: 0,
                glY: 0,
                lerped: {x: 0, y: 0},
                velo: {x: 0, y: 0}
            }, this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "calcMouse", value: function () {
                var e = this.mouse;
                e.lerped.x += .1 * (e.x - e.lerped.x), e.lerped.y += .1 * (e.y - e.lerped.y), e.velo.x = e.x - e.lerped.x, e.velo.y = e.y - e.lerped.y
            }
        }, {
            key: "clamp", value: function () {
                this.target = an.utils.clamp(0, o.bounds.scroll, this.target)
            }
        }, {
            key: "reset", value: function () {
                this.target = this.current = this.rounded = 0
            }
        }, {
            key: "scrollTo", value: function (e) {
                an.to(this, {target: e, duration: 1.2, ease: "expo.in"})
            }
        }, {
            key: "init", value: function () {
                an.ticker.fps(-1), an.ticker.add(this.tick), Ir.on("scroll", this.onScroll), Ir.on("mousemove", this.move), Ir.on("resize", this.resize)
            }
        }]) && zr(t.prototype, i), e
    }()), i(16)), Wr = i.n(qr);

    function Xr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Yr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Ur(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Gr(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    function $r(e, t) {
        (null == t || t > e.length) && (t = e.length);
        for (var i = 0, n = new Array(t); i < t; i++) n[i] = e[i];
        return n
    }

    function Kr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    new (function () {
        function e() {
            var t, i, n = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), i = function () {
                var e = o.flags, t = o.bounds;
                e.resize = !0, e.small = window.matchMedia("(max-width: 639px)").matches, t.ww = window.innerWidth, t.wh = window.innerHeight, n.setAspect(), Ir.emit("resize"), e.resize = !1
            }, (t = "onResize") in this ? Object.defineProperty(this, t, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : this[t] = i, this.on(), this.setAspect()
        }

        var t, i;
        return t = e, (i = [{
            key: "setAspect", value: function () {
                var e = o.bounds, t = o.dom, i = o.flags;
                e.wh <= e.ww ? (t.body.classList.remove("is-portrait"), t.body.classList.add("is-landscape"), i.landscape = !0) : (t.body.classList.remove("is-landscape"), t.body.classList.add("is-portrait"), i.landscape = !1)
            }
        }, {
            key: "on", value: function () {
                cn(window, "a", "resize", Wr()(this.onResize, 200))
            }
        }]) && Xr(t.prototype, i), e
    }()), new (function () {
        function e() {
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), function (e) {
                for (var t = [].slice.call(arguments, 1), i = 0; i < t.length; i++) {
                    var n = t[i];
                    e[n] = nr(e[n], e)
                }
            }(this, "onOrentationChange"), this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "onOrientationChange", value: function () {
                var e = o.dom;
                e.body.classList.contains("is-orientation-changed") ? e.body.classList.remove("is-orientation-changed") : e.body.classList.add("is-orientation-changed"), Ir.emit("orientationchange")
            }
        }, {
            key: "on", value: function () {
                cn(window, "orientationchange", this.onOrientationChange)
            }
        }, {
            key: "init", value: function () {
                this.on()
            }
        }]) && Yr(t.prototype, i), e
    }()), new (function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), Gr(this, "onMove", function (e) {
                var i = t.getPos(e), n = i.x, r = i.y, s = i.target;
                Ir.emit("mousemove", {x: n, y: r, target: s, e: e})
            }), Gr(this, "onDown", function (e) {
                var i = t.getPos(e), n = i.x, r = i.y, s = i.target;
                t.state.on = n, Ir.emit("mousedown", {x: n, y: r, target: s})
            }), Gr(this, "onUp", function (e) {
                var i = t.getPos(e), n = i.x, r = i.target, s = t.state;
                s.off = n;
                var o = Math.abs(s.on - s.off) > 10;
                Ir.emit("mouseup", {x: n, target: r, isClick: o})
            }), Gr(this, "onClick", function (e) {
                Ir.emit("click", e)
            }), this.state = {
                on: 0,
                off: 0,
                coords: {x: 0, y: 0}
            }, this.events = {
                move: o.isDevice ? "touchmove" : "mousemove",
                down: o.isDevice ? "touchstart" : "mousedown",
                up: o.isDevice ? "touchend" : "mouseup"
            }, this.on()
        }

        var t, i;
        return t = e, (i = [{
            key: "on", value: function () {
                var e = this.events, t = e.move, i = e.down, n = e.up;
                cn(window, "a", t, this.onMove), cn(window, "a", i, this.onDown), cn(window, "a", n, this.onUp), cn(window, "a", "click", this.onClick)
            }
        }, {
            key: "getPos", value: function (e) {
                return {
                    x: e.changedTouches ? e.changedTouches[0].clientX : e.clientX,
                    y: e.changedTouches ? e.changedTouches[0].clientY : e.clientY,
                    target: e.target
                }
            }
        }]) && Ur(t.prototype, i), e
    }());
    var Jr = function () {
        function e() {
            var t = this, i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
            (function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            })(this, e), function (e, t, i) {
                t in e ? Object.defineProperty(e, t, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = i
            }(this, "onResize", function () {
                t.clamp(), t.setHeight();
                var e = t.slides[0].getBoundingClientRect().width;
                t.sliderWidth = t.slidesNumb * e, t.max = -(t.sliderWidth - window.innerWidth), t.slides.forEach(function (t, i) {
                    t.style.left = "".concat(i * e, "px")
                })
            }), this.bind(), this.opts = {
                el: i.el || ".js-slider",
                ease: i.ease || .1,
                speed: i.speed || 1.5,
                velocity: 15
            }, this.slider = ln(".js-slider"), this.sliderInner = ln(".js-slider__inner"), this.slides = function (e) {
                return function (e) {
                    if (Array.isArray(e)) return $r(e)
                }(e) || function (e) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) return Array.from(e)
                }(e) || function (e, t) {
                    if (e) {
                        if ("string" == typeof e) return $r(e, t);
                        var i = Object.prototype.toString.call(e).slice(8, -1);
                        return "Object" === i && e.constructor && (i = e.constructor.name), "Map" === i || "Set" === i ? Array.from(e) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? $r(e, t) : void 0
                    }
                }(e) || function () {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }()
            }(un(".js-slide")), this.slidesNumb = this.slides.length, this.sliderWidth = 0, this.onX = 0, this.offX = 0, this.currentX = 0, this.lastX = 0, this.min = 0, this.max = 0, this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "bind", value: function () {
                var e = this;
                ["setPos", "run", "on", "off", "onResize", "setInitial"].forEach(function (t) {
                    return e[t] = e[t].bind(e)
                })
            }
        }, {
            key: "setInitial", value: function () {
                this.slides.forEach(function (e, t) {
                    an.set(e, {rotation: 2.8 * Math.floor(5 * Math.random()) * t})
                })
            }
        }, {
            key: "setHeight", value: function () {
                var e = this.slides[0].getBoundingClientRect();
                ln(".js-drag-areaSlider").style.height = e.height, ln(".js-drag-areaSlider").style.height = "".concat(e.height, "px")
            }
        }, {
            key: "setBounds", value: function () {
                var e = this.slides[0].getBoundingClientRect().width;
                this.sliderWidth = this.slidesNumb * e, this.max = -(this.sliderWidth - window.innerWidth)
            }
        }, {
            key: "enter", value: function () {
                var e = this, t = an.timeline({
                    onComplete: function () {
                        setTimeout(function () {
                            e.addEvents(), an.ticker.add(function () {
                                e.run()
                            })
                        }, 1500)
                    }
                }), i = this.slides[0].getBoundingClientRect().width, n = .15;
                this.slides.forEach(function (e, r) {
                    n += .075, t.to(e, {
                        xPercent: 0,
                        yPercent: 0,
                        rotation: 0,
                        top: 0,
                        left: "".concat(r * i, "px"),
                        ease: "power3",
                        duration: 2.5
                    }, n)
                })
            }
        }, {
            key: "setPos", value: function (e) {
                if (this.isDragging) {
                    var t = e.changedTouches ? e.changedTouches[0].clientX : e.clientX;
                    this.currentX = this.offX + (t - this.onX) * this.opts.speed, this.clamp()
                }
            }
        }, {
            key: "clamp", value: function () {
                this.currentX = Math.max(Math.min(this.currentX, this.min), this.max)
            }
        }, {
            key: "run", value: function () {
                this.lastX = function (e, t, i) {
                    return (1 - i) * e + i * t
                }(this.lastX, this.currentX, this.opts.ease), this.lastX = Math.floor(100 * this.lastX) / 100;
                var e = +(this.currentX - this.lastX) / window.innerWidth;
                this.sliderInner.style.transform = "translate3d(".concat(this.lastX, "px, 0, 0) skewX(").concat(e * this.opts.velocity, "deg)"), document.querySelector(".js-planet-slider") && (document.querySelector(".js-planet-slider").style.transform = "rotate(".concat(this.lastX / 10, "deg)")), o.bounds.ww > 1600 ? document.querySelector(".js-orbit-slider") && (document.querySelector(".js-orbit-slider").style.transform = "rotate(".concat(this.lastX / 30, "deg)")) : o.bounds.ww < 850 ? document.querySelector(".js-orbit-slider") && (document.querySelector(".js-orbit-slider").style.transform = "rotate(".concat(this.lastX / 25, "deg)")) : document.querySelector(".js-orbit-slider") && (document.querySelector(".js-orbit-slider").style.transform = "rotate(".concat(this.lastX / 15, "deg)"))
            }
        }, {
            key: "on", value: function (e) {
                this.isDragging = !0, this.onX = e.changedTouches ? e.changedTouches[0].clientX : e.clientX, this.slider.classList.add("is-grabbing")
            }
        }, {
            key: "off", value: function (e) {
                this.isDragging = !1, this.offX = this.currentX, this.slider.classList.remove("is-grabbing")
            }
        }, {
            key: "addEvents", value: function () {
                Ir.on("resize", this.onResize), ln(".js-drag-areaSlider") && ln(".js-drag-areaSlider").addEventListener(o.isDevice ? "touchmove" : "mousemove", this.setPos), ln(".js-drag-areaSlider") && ln(".js-drag-areaSlider").addEventListener(o.isDevice ? "touchstart" : "mousedown", this.on), ln(".js-drag-areaSlider") && ln(".js-drag-areaSlider").addEventListener(o.isDevice ? "touchend" : "mouseup", this.off)
            }
        }, {
            key: "init", value: function () {
                this.setInitial(), this.setHeight(), this.setBounds()
            }
        }]) && Kr(t.prototype, i), e
    }();

    function Qr(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    var Zr = function () {
            function e() {
                !function (e, t) {
                    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                }(this, e), this.dom = {switch1: un(".js-project-form-btn"), switch2: un(".js-join-form-btn")}, this.init()
            }

            var t, i;
            return t = e, (i = [{
                key: "init", value: function () {
                    this.setInitital(), this.classToggle(), this.addEvents()
                }
            }, {
                key: "setInitital", value: function () {
                    an.to(".js-j-h2", {autoAlpha: 0, pointerEvents: "none"}), an.to(".js-j-form", {
                        autoAlpha: 0,
                        pointerEvents: "none"
                    })
                }
            }, {
                key: "classToggle", value: function () {
                    var e = this;
                    this.selectBtn = un(".js-select-form-btn"), this.selectBtn.forEach(function (t) {
                        t.addEventListener("click", function () {
                            e.selectBtn.forEach(function (e) {
                                e.classList.remove("select-active")
                            }), t.classList.add("select-active")
                        })
                    })
                }
            }, {
                key: "project", value: function () {
                    var e = {
                        currentTitleWrap: un(".js-j-h2"),
                        currentTitle: un(".js-j-title"),
                        currentForm: ln(".js-j-form"),
                        currentInput: un(".js-j-input"),
                        currentBtn: ln(".js-j-btn"),
                        nextTitleWrap: un(".js-p-h2"),
                        nextTitle: un(".js-p-title"),
                        nextForm: ln(".js-p-form"),
                        nextInput: un(".js-p-input"),
                        nextCheckbox: ln(".js-p-checkbox"),
                        nextBtn: ln(".js-p-btn")
                    };
                    this.projectForm && this.projectForm.kill(), this.projectForm = an.timeline(), this.projectForm.to(e.currentTitle, {
                        yPercent: 100,
                        duration: 1.5,
                        stagger: .075,
                        ease: "expo.out"
                    }, 0).to(e.currentTitleWrap, {
                        autoAlpha: 0,
                        duration: .5,
                        ease: "expo.out"
                    }, .075).to(e.currentInput, {
                        autoAlpha: 0,
                        yPercent: 100,
                        duration: 1,
                        stagger: .075,
                        ease: "power3.out"
                    }, .075).to(e.currentBtn, {
                        autoAlpha: 0,
                        xPercent: -50,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .095).to(e.currentForm, {
                        autoAlpha: 0,
                        duration: .5,
                        pointerEvents: "none",
                        ease: "expo.out"
                    }, .1).to(e.nextTitleWrap, {
                        autoAlpha: 1,
                        duration: .5,
                        ease: "expo.out"
                    }, .15).fromTo(e.nextTitle, {yPercent: 100}, {
                        yPercent: 0,
                        duration: 1.5,
                        stagger: .1,
                        ease: "expo.out"
                    }, .45).to(e.nextForm, {
                        autoAlpha: 1,
                        duration: .5,
                        pointerEvents: "auto",
                        ease: "expo.out"
                    }, .15).fromTo(e.nextInput, {yPercent: 100}, {
                        autoAlpha: 1,
                        yPercent: 0,
                        duration: 1,
                        stagger: .1,
                        ease: "power3.out"
                    }, .75).fromTo(e.nextCheckbox, {autoAlpha: 0, yPercent: 50}, {
                        autoAlpha: 1,
                        yPercent: 0,
                        duration: 1,
                        ease: "expo.out"
                    }, .85).fromTo(e.nextBtn, {autoAlpha: 0, xPercent: -50}, {
                        autoAlpha: 1,
                        xPercent: 0,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .95)
                }
            }, {
                key: "join", value: function () {
                    var e = {
                        currentTitleWrap: un(".js-p-h2"),
                        currentTitle: un(".js-p-title"),
                        currentForm: ln(".js-p-form"),
                        currentInput: un(".js-p-input"),
                        currentCheckbox: ln(".js-p-checkbox"),
                        currentBtn: ln(".js-p-btn"),
                        nextTitleWrap: un(".js-j-h2"),
                        nextTitle: un(".js-j-title"),
                        nextForm: ln(".js-j-form"),
                        nextInput: un(".js-j-input"),
                        nextBtn: ln(".js-j-btn")
                    };
                    this.joinForm && this.joinForm.kill(), this.joinForm = an.timeline(), this.joinForm.to(e.currentTitle, {
                        yPercent: 100,
                        duration: 1.5,
                        stagger: .075,
                        ease: "expo.out"
                    }, 0).to(e.currentTitleWrap, {
                        autoAlpha: 0,
                        duration: .5,
                        ease: "expo.out"
                    }, .075).to(e.currentInput, {
                        autoAlpha: 0,
                        yPercent: 100,
                        duration: 1,
                        stagger: .075,
                        ease: "power3.out"
                    }, .075).to(e.currentCheckbox, {
                        autoAlpha: 0,
                        yPercent: 50,
                        duration: 1,
                        ease: "expo.out"
                    }, .085).to(e.currentBtn, {
                        autoAlpha: 0,
                        xPercent: -50,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .095).to(e.currentForm, {
                        autoAlpha: 0,
                        duration: .5,
                        pointerEvents: "none",
                        ease: "expo.out"
                    }, .1).to(e.nextTitleWrap, {
                        autoAlpha: 1,
                        duration: .5,
                        ease: "expo.out"
                    }, .15).fromTo(e.nextTitle, {yPercent: 100}, {
                        yPercent: 0,
                        duration: 1.5,
                        stagger: .1,
                        ease: "expo.out"
                    }, .45).to(e.nextForm, {
                        autoAlpha: 1,
                        duration: .5,
                        pointerEvents: "auto",
                        ease: "expo.out"
                    }, .15).fromTo(e.nextInput, {yPercent: 100}, {
                        autoAlpha: 1,
                        yPercent: 0,
                        duration: 1,
                        stagger: .1,
                        ease: "power3.out"
                    }, .75).fromTo(e.nextBtn, {autoAlpha: 0, xPercent: -50}, {
                        autoAlpha: 1,
                        xPercent: 0,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .95)
                }
            }, {
                key: "addEvents", value: function () {
                    var e = this;
                    this.dom.switch1.forEach(function (t) {
                        t.addEventListener("click", e.project)
                    }), this.dom.switch2.forEach(function (t) {
                        t.addEventListener("click", e.join)
                    })
                }
            }]) && Qr(t.prototype, i), e
        }(), es = i(1), ts = i.n(es), is = i(3), ns = i.n(is), rs = /[achlmqstvz]|(-?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,
        ss = /(?:(-)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi, os = /[\+\-]?\d*\.?\d+e[\+\-]?\d+/gi,
        as = /(^[#\.][a-z]|[a-y][a-z])/i, ls = Math.PI / 180, us = 180 / Math.PI, cs = Math.sin, hs = Math.cos,
        ds = Math.abs, ps = Math.sqrt, fs = Math.atan2, ms = function (e) {
            return "string" == typeof e
        }, gs = function (e) {
            return "number" == typeof e
        }, vs = {}, ys = {}, bs = function (e) {
            return Math.round((e + 1e8) % 1 * 1e5) / 1e5 || (e < 0 ? 0 : 1)
        }, ws = function (e) {
            return Math.round(1e5 * e) / 1e5 || 0
        }, Ds = function (e, t, i, n) {
            var r = e[t], s = 1 === n ? 6 : As(r, i, n);
            if (s && s + i + 2 < r.length) return e.splice(t, 0, r.slice(0, i + s + 2)), r.splice(0, i + s), 1
        }, Ts = function (e, t) {
            return t.totalLength = e.totalLength, e.samples ? (t.samples = e.samples.slice(0), t.lookup = e.lookup.slice(0), t.minLength = e.minLength, t.resolution = e.resolution) : t.totalPoints = e.totalPoints, t
        }, Cs = function (e, t) {
            var i = e.length, n = e[i - 1] || [], r = n.length;
            t[0] === n[r - 2] && t[1] === n[r - 1] && (t = n.concat(t.slice(2)), i--), e[i] = t
        };

    function ks(e) {
        var t, i = (e = ms(e) && as.test(e) && document.querySelector(e) || e).getAttribute ? e : 0;
        return i && (e = e.getAttribute("d")) ? (i._gsPath || (i._gsPath = {}), (t = i._gsPath[e]) && !t._dirty ? t : i._gsPath[e] = Bs(e)) : e ? ms(e) ? Bs(e) : gs(e[0]) ? [e] : e : console.warn("Expecting a <path> element or an SVG path data string")
    }

    function Es(e) {
        var t, i = 0;
        for (e.reverse(); i < e.length; i += 2) t = e[i], e[i] = e[i + 1], e[i + 1] = t;
        e.reversed = !e.reversed
    }

    var _s = {rect: "rx,ry,x,y,width,height", circle: "r,cx,cy", ellipse: "rx,ry,cx,cy", line: "x1,x2,y1,y2"};

    function xs(e, t, i) {
        var n, r = e[t], s = e[t + 2], o = e[t + 4];
        return r += (s - r) * i, r += ((s += (o - s) * i) - r) * i, n = s + (o + (e[t + 6] - o) * i - s) * i - r, r = e[t + 1], r += ((s = e[t + 3]) - r) * i, r += ((s += ((o = e[t + 5]) - s) * i) - r) * i, ws(fs(s + (o + (e[t + 7] - o) * i - s) * i - r, n) * us)
    }

    function Ss(e, t, i) {
        void 0 === i && (i = 1);
        var n = (t = t || 0) > i, r = Math.max(0, ~~(ds(i - t) - 1e-8));
        if (n && (n = i, i = t, t = n, n = 1, r -= r ? 1 : 0), t < 0 || i < 0) {
            var s = 1 + ~~Math.min(t, i);
            t += s, i += s
        }
        var o, a, l, u, c, h, d, p = function (e) {
                for (var t = [], i = 0; i < e.length; i++) t[i] = Ts(e[i], e[i].slice(0));
                return Ts(e, t)
            }(e.totalLength ? e : Os(e)), f = i > 1, m = Ms(p, t, vs, !0), g = Ms(p, i, ys), v = g.segment, y = m.segment,
            b = g.segIndex, w = m.segIndex, D = g.i, T = m.i, C = w === b, k = D === T && C,
            E = C && T > D || k && m.t > g.t;
        if (f || r) {
            if (Ds(p, w, T, m.t) && (o = 1, w++, k ? E ? g.t /= m.t : (g.t = (g.t - m.t) / (1 - m.t), b++, D = 0) : w <= b + 1 && !E && (b++, C && (D -= T))), g.t ? Ds(p, b, D, g.t) && (E && o && w++, n && b++) : (b--, n && w--), u = [], h = 1 + (c = p.length) * r, d = w, n) for (h += (c - (b = (b || c) - 1) + w) % c, l = 0; l < h; l++) Cs(u, p[d]), d = (d || c) - 1; else for (h += (c - w + b) % c, l = 0; l < h; l++) Cs(u, p[d++ % c]);
            p = u
        } else if (a = 1 === g.t ? 6 : As(v, D, g.t), t !== i) for (o = As(y, T, k ? m.t / g.t : m.t), C && (a += o), v.splice(D + a + 2), (o || T) && y.splice(0, T + o), l = p.length; l--;) (l < w || l > b) && p.splice(l, 1); else v.angle = xs(v, D + a, 0), m = v[D += a], g = v[D + 1], v.length = v.totalLength = 0, v.totalPoints = p.totalPoints = 8, v.push(m, g, m, g, m, g, m, g);
        return n && function (e, t) {
            var i = e.length;
            for (t || e.reverse(); i--;) e[i].reversed || Es(e[i])
        }(p, f || r), p.totalLength = 0, p
    }

    function Ps(e, t, i) {
        t = t || 0, e.samples || (e.samples = [], e.lookup = []);
        var n, r, s, o, a, l, u, c, h, d, p, f, m, g, v, y, b, w = ~~e.resolution || 12, D = 1 / w,
            T = i ? t + 6 * i + 1 : e.length, C = e[t], k = e[t + 1], E = t ? t / 6 * w : 0, _ = e.samples,
            x = e.lookup, S = (t ? e.minLength : 1e8) || 1e8, P = _[E + i * w - 1], O = t ? _[E - 1] : 0;
        for (_.length = x.length = 0, r = t + 2; r < T; r += 6) {
            if (s = e[r + 4] - C, o = e[r + 2] - C, a = e[r] - C, c = e[r + 5] - k, h = e[r + 3] - k, d = e[r + 1] - k, l = u = p = f = 0, ds(s) < 1e-5 && ds(c) < 1e-5 && ds(a) + ds(d) < 1e-5) e.length > 8 && (e.splice(r, 6), r -= 6, T -= 6); else for (n = 1; n <= w; n++) l = u - (u = ((g = D * n) * g * s + 3 * (m = 1 - g) * (g * o + m * a)) * g), p = f - (f = (g * g * c + 3 * m * (g * h + m * d)) * g), (y = ps(p * p + l * l)) < S && (S = y), O += y, _[E++] = O;
            C += s, k += c
        }
        if (P) for (P -= O; E < _.length; E++) _[E] += P;
        if (_.length && S) for (e.totalLength = b = _[_.length - 1] || 0, e.minLength = S, y = v = 0, n = 0; n < b; n += S) x[y++] = _[v] < n ? ++v : v; else e.totalLength = _[0] = 0;
        return t ? O - _[t / 2 - 1] : O
    }

    function Os(e, t) {
        var i, n, r;
        for (r = i = n = 0; r < e.length; r++) e[r].resolution = ~~t || 12, n += e[r].length, i += Ps(e[r]);
        return e.totalPoints = n, e.totalLength = i, e
    }

    function As(e, t, i) {
        if (i <= 0 || i >= 1) return 0;
        var n = e[t], r = e[t + 1], s = e[t + 2], o = e[t + 3], a = e[t + 4], l = e[t + 5], u = n + (s - n) * i,
            c = s + (a - s) * i, h = r + (o - r) * i, d = o + (l - o) * i, p = u + (c - u) * i, f = h + (d - h) * i,
            m = a + (e[t + 6] - a) * i, g = l + (e[t + 7] - l) * i;
        return c += (m - c) * i, d += (g - d) * i, e.splice(t + 2, 4, ws(u), ws(h), ws(p), ws(f), ws(p + (c - p) * i), ws(f + (d - f) * i), ws(c), ws(d), ws(m), ws(g)), e.samples && e.samples.splice(t / 6 * e.resolution | 0, 0, 0, 0, 0, 0, 0, 0), 6
    }

    function Ms(e, t, i, n) {
        i = i || {}, e.totalLength || Os(e), (t < 0 || t > 1) && (t = bs(t));
        var r, s, o, a, l, u, c, h = 0, d = e[0];
        if (e.length > 1) {
            for (o = e.totalLength * t, l = u = 0; (l += e[u++].totalLength) < o;) h = u;
            t = (o - (a = l - (d = e[h]).totalLength)) / (l - a) || 0
        }
        return r = d.samples, s = d.resolution, o = d.totalLength * t, a = (u = d.lookup[~~(o / d.minLength)] || 0) ? r[u - 1] : 0, (l = r[u]) < o && (a = l, l = r[++u]), c = 1 / s * ((o - a) / (l - a) + u % s), u = 6 * ~~(u / s), n && 1 === c && (u + 6 < d.length ? (u += 6, c = 0) : h + 1 < e.length && (u = c = 0, d = e[++h])), i.t = c, i.i = u, i.path = e, i.segment = d, i.segIndex = h, i
    }

    function Ls(e, t, i, n) {
        var r, s, o, a, l, u, c, h, d, p = e[0], f = n || {};
        if ((t < 0 || t > 1) && (t = bs(t)), e.length > 1) {
            for (o = e.totalLength * t, l = u = 0; (l += e[u++].totalLength) < o;) p = e[u];
            t = (o - (a = l - p.totalLength)) / (l - a) || 0
        }
        return r = p.samples, s = p.resolution, o = p.totalLength * t, a = (u = p.lookup[~~(o / p.minLength)] || 0) ? r[u - 1] : 0, (l = r[u]) < o && (a = l, l = r[++u]), d = 1 - (c = 1 / s * ((o - a) / (l - a) + u % s) || 0), h = p[u = 6 * ~~(u / s)], f.x = ws((c * c * (p[u + 6] - h) + 3 * d * (c * (p[u + 4] - h) + d * (p[u + 2] - h))) * c + h), f.y = ws((c * c * (p[u + 7] - (h = p[u + 1])) + 3 * d * (c * (p[u + 5] - h) + d * (p[u + 3] - h))) * c + h), i && (f.angle = p.totalLength ? xs(p, u, c >= 1 ? 1 - 1e-9 : c || 1e-9) : p.angle || 0), f
    }

    function Fs(e, t, i, n, r, s, o) {
        for (var a, l, u, c, h, d = e.length; --d > -1;) for (l = (a = e[d]).length, u = 0; u < l; u += 2) c = a[u], h = a[u + 1], a[u] = c * t + h * n + s, a[u + 1] = c * i + h * r + o;
        return e._dirty = 1, e
    }

    function js(e, t, i, n, r, s, o, a, l) {
        if (e !== a || t !== l) {
            i = ds(i), n = ds(n);
            var u = r % 360 * ls, c = hs(u), h = cs(u), d = Math.PI, p = 2 * d, f = (e - a) / 2, m = (t - l) / 2,
                g = c * f + h * m, v = -h * f + c * m, y = g * g, b = v * v, w = y / (i * i) + b / (n * n);
            w > 1 && (i = ps(w) * i, n = ps(w) * n);
            var D = i * i, T = n * n, C = (D * T - D * b - T * y) / (D * b + T * y);
            C < 0 && (C = 0);
            var k = (s === o ? -1 : 1) * ps(C), E = k * (i * v / n), _ = k * (-n * g / i),
                x = (e + a) / 2 + (c * E - h * _), S = (t + l) / 2 + (h * E + c * _), P = (g - E) / i, O = (v - _) / n,
                A = (-g - E) / i, M = (-v - _) / n, L = P * P + O * O, F = (O < 0 ? -1 : 1) * Math.acos(P / ps(L)),
                j = (P * M - O * A < 0 ? -1 : 1) * Math.acos((P * A + O * M) / ps(L * (A * A + M * M)));
            isNaN(j) && (j = d), !o && j > 0 ? j -= p : o && j < 0 && (j += p), F %= p, j %= p;
            var B, I = Math.ceil(ds(j) / (p / 4)), R = [], N = j / I, z = 4 / 3 * cs(N / 2) / (1 + hs(N / 2)),
                H = c * i, V = h * i, q = h * -n, W = c * n;
            for (B = 0; B < I; B++) g = hs(r = F + B * N), v = cs(r), P = hs(r += N), O = cs(r), R.push(g - z * v, v + z * g, P + z * O, O - z * P, P, O);
            for (B = 0; B < R.length; B += 2) g = R[B], v = R[B + 1], R[B] = g * H + v * q + x, R[B + 1] = g * V + v * W + S;
            return R[B - 2] = a, R[B - 1] = l, R
        }
    }

    function Bs(e) {
        var t, i, n, r, s, o, a, l, u, c, h, d, p, f, m, g = (e + "").replace(os, function (e) {
                var t = +e;
                return t < 1e-4 && t > -1e-4 ? 0 : t
            }).match(rs) || [], v = [], y = 0, b = 0, w = g.length, D = 0, T = "ERROR: malformed path: " + e,
            C = function (e, t, i, n) {
                c = (i - e) / 3, h = (n - t) / 3, a.push(e + c, t + h, i - c, n - h, i, n)
            };
        if (!e || !isNaN(g[0]) || isNaN(g[1])) return console.log(T), v;
        for (t = 0; t < w; t++) if (p = s, isNaN(g[t]) ? o = (s = g[t].toUpperCase()) !== g[t] : t--, n = +g[t + 1], r = +g[t + 2], o && (n += y, r += b), t || (l = n, u = r), "M" === s) a && (a.length < 8 ? v.length -= 1 : D += a.length), y = l = n, b = u = r, a = [n, r], v.push(a), t += 2, s = "L"; else if ("C" === s) a || (a = [0, 0]), o || (y = b = 0), a.push(n, r, y + 1 * g[t + 3], b + 1 * g[t + 4], y += 1 * g[t + 5], b += 1 * g[t + 6]), t += 6; else if ("S" === s) c = y, h = b, "C" !== p && "S" !== p || (c += y - a[a.length - 4], h += b - a[a.length - 3]), o || (y = b = 0), a.push(c, h, n, r, y += 1 * g[t + 3], b += 1 * g[t + 4]), t += 4; else if ("Q" === s) c = y + 2 / 3 * (n - y), h = b + 2 / 3 * (r - b), o || (y = b = 0), y += 1 * g[t + 3], b += 1 * g[t + 4], a.push(c, h, y + 2 / 3 * (n - y), b + 2 / 3 * (r - b), y, b), t += 4; else if ("T" === s) c = y - a[a.length - 4], h = b - a[a.length - 3], a.push(y + c, b + h, n + 2 / 3 * (y + 1.5 * c - n), r + 2 / 3 * (b + 1.5 * h - r), y = n, b = r), t += 2; else if ("H" === s) C(y, b, y = n, b), t += 1; else if ("V" === s) C(y, b, y, b = n + (o ? b - y : 0)), t += 1; else if ("L" === s || "Z" === s) "Z" === s && (n = l, r = u, a.closed = !0), ("L" === s || ds(y - n) > .5 || ds(b - r) > .5) && (C(y, b, n, r), "L" === s && (t += 2)), y = n, b = r; else if ("A" === s) {
            if (f = g[t + 4], m = g[t + 5], c = g[t + 6], h = g[t + 7], i = 7, f.length > 1 && (f.length < 3 ? (h = c, c = m, i--) : (h = m, c = f.substr(2), i -= 2), m = f.charAt(1), f = f.charAt(0)), d = js(y, b, +g[t + 1], +g[t + 2], +g[t + 3], +f, +m, (o ? y : 0) + 1 * c, (o ? b : 0) + 1 * h), t += i, d) for (i = 0; i < d.length; i++) a.push(d[i]);
            y = a[a.length - 2], b = a[a.length - 1]
        } else console.log(T);
        return (t = a.length) < 6 ? (v.pop(), t = 0) : a[0] === a[t - 2] && a[1] === a[t - 1] && (a.closed = !0), v.totalPoints = D + t, v
    }

    function Is(e, t) {
        void 0 === t && (t = 1);
        for (var i = e[0], n = 0, r = [i, n], s = 2; s < e.length; s += 2) r.push(i, n, e[s], n = (e[s] - i) * t / 2, i = e[s], -n);
        return r
    }

    function Rs(e, t, i) {
        var n, r, s, o, a, l, u, c, h, d, p, f, m, g, v = e.length - 2, y = +e[0], b = +e[1], w = +e[2], D = +e[3],
            T = [y, b, y, b], C = w - y, k = D - b, E = Math.abs(e[v] - y) < .001 && Math.abs(e[v + 1] - b) < .001;
        for (isNaN(i) && (i = Math.PI / 10), E && (e.push(w, D), w = y, D = b, y = e[v - 2], b = e[v - 1], e.unshift(y, b), v += 4), t = t || 0 === t ? +t : 1, a = 2; a < v; a += 2) n = y, r = b, y = w, b = D, f = (l = C) * l + (c = k) * c, m = (C = (w = +e[a + 2]) - y) * C + (k = (D = +e[a + 3]) - b) * k, g = (u = w - n) * u + (h = D - r) * h, p = (s = Math.acos((f + m - g) / ps(4 * f * m))) / Math.PI * t, d = ps(f) * p, p *= ps(m), y === n && b === r || (s > i ? (o = fs(h, u), T.push(ws(y - hs(o) * d), ws(b - cs(o) * d), ws(y), ws(b), ws(y + hs(o) * p), ws(b + cs(o) * p))) : (o = fs(c, l), T.push(ws(y - hs(o) * d), ws(b - cs(o) * d)), o = fs(k, C), T.push(ws(y), ws(b), ws(y + hs(o) * p), ws(b + cs(o) * p))));
        return T.push(ws(w), ws(D), ws(w), ws(D)), E && (T.splice(0, 6), T.length = T.length - 6), T
    }

    function Ns(e) {
        gs(e[0]) && (e = [e]);
        var t, i, n, r, s = "", o = e.length;
        for (i = 0; i < o; i++) {
            for (r = e[i], s += "M" + ws(r[0]) + "," + ws(r[1]) + " C", t = r.length, n = 2; n < t; n++) s += ws(r[n++]) + "," + ws(r[n++]) + " " + ws(r[n++]) + "," + ws(r[n++]) + " " + ws(r[n++]) + "," + ws(r[n]) + " ";
            r.closed && (s += "z")
        }
        return s
    }

    var zs, Hs, Vs, qs, Ws, Xs, Ys, Us, Gs = "transform", $s = Gs + "Origin", Ks = function (e) {
        var t = e.ownerDocument || e;
        !(Gs in e.style) && "msTransform" in e.style && ($s = (Gs = "msTransform") + "Origin");
        for (; t.parentNode && (t = t.parentNode);) ;
        if (Hs = window, Ys = new no, t) {
            zs = t, Vs = t.documentElement, qs = t.body;
            var i = t.createElement("div"), n = t.createElement("div");
            qs.appendChild(i), i.appendChild(n), i.style.position = "static", i.style[Gs] = "translate3d(0,0,1px)", Us = n.offsetParent !== i, qs.removeChild(i)
        }
        return t
    }, Js = [], Qs = [], Zs = function (e) {
        return e.ownerSVGElement || ("svg" === (e.tagName + "").toLowerCase() ? e : null)
    }, eo = function e(t, i) {
        if (t.parentNode && (zs || Ks(t))) {
            var n = Zs(t),
                r = n ? n.getAttribute("xmlns") || "http://www.w3.org/2000/svg" : "http://www.w3.org/1999/xhtml",
                s = n ? i ? "rect" : "g" : "div", o = 2 !== i ? 0 : 100, a = 3 === i ? 100 : 0,
                l = "position:absolute;display:block;pointer-events:none;",
                u = zs.createElementNS ? zs.createElementNS(r.replace(/^https/, "http"), s) : zs.createElement(s);
            return i && (n ? (Xs || (Xs = e(t)), u.setAttribute("width", .01), u.setAttribute("height", .01), u.setAttribute("transform", "translate(" + o + "," + a + ")"), Xs.appendChild(u)) : (Ws || ((Ws = e(t)).style.cssText = l), u.style.cssText = l + "width:0.1px;height:0.1px;top:" + a + "px;left:" + o + "px", Ws.appendChild(u))), u
        }
        throw"Need document and parent."
    }, to = function (e, t) {
        var i, n, r, s, o, a = Zs(e), l = e === a, u = a ? Js : Qs;
        if (e === Hs) return e;
        if (u.length || u.push(eo(e, 1), eo(e, 2), eo(e, 3)), i = a ? Xs : Ws, a) r = l ? {
            x: 0,
            y: 0
        } : e.getBBox(), (n = e.transform ? e.transform.baseVal : {}).numberOfItems ? (s = (n = n.numberOfItems > 1 ? function (e) {
            for (var t = new no, i = 0; i < e.numberOfItems; i++) t.multiply(e.getItem(i).matrix);
            return t
        }(n) : n.getItem(0).matrix).a * r.x + n.c * r.y, o = n.b * r.x + n.d * r.y) : (n = Ys, s = r.x, o = r.y), t && "g" === e.tagName.toLowerCase() && (s = o = 0), i.setAttribute("transform", "matrix(" + n.a + "," + n.b + "," + n.c + "," + n.d + "," + (n.e + s) + "," + (n.f + o) + ")"), (l ? a : e.parentNode).appendChild(i); else {
            if (s = o = 0, Us) for (n = e.offsetParent, r = e; r && (r = r.parentNode) && r !== n && r.parentNode;) (Hs.getComputedStyle(r)[Gs] + "").length > 4 && (s = r.offsetLeft, o = r.offsetTop, r = 0);
            (r = i.style).top = e.offsetTop - o + "px", r.left = e.offsetLeft - s + "px", n = Hs.getComputedStyle(e), r[Gs] = n[Gs], r[$s] = n[$s], r.border = n.border, r.borderLeftStyle = n.borderLeftStyle, r.borderTopStyle = n.borderTopStyle, r.borderLeftWidth = n.borderLeftWidth, r.borderTopWidth = n.borderTopWidth, r.position = "fixed" === n.position ? "fixed" : "absolute", e.parentNode.appendChild(i)
        }
        return i
    }, io = function (e, t, i, n, r, s, o) {
        return e.a = t, e.b = i, e.c = n, e.d = r, e.e = s, e.f = o, e
    }, no = function () {
        function e(e, t, i, n, r, s) {
            void 0 === e && (e = 1), void 0 === t && (t = 0), void 0 === i && (i = 0), void 0 === n && (n = 1), void 0 === r && (r = 0), void 0 === s && (s = 0), io(this, e, t, i, n, r, s)
        }

        var t = e.prototype;
        return t.inverse = function () {
            var e = this.a, t = this.b, i = this.c, n = this.d, r = this.e, s = this.f, o = e * n - t * i || 1e-10;
            return io(this, n / o, -t / o, -i / o, e / o, (i * s - n * r) / o, -(e * s - t * r) / o)
        }, t.multiply = function (e) {
            var t = this.a, i = this.b, n = this.c, r = this.d, s = this.e, o = this.f, a = e.a, l = e.c, u = e.b,
                c = e.d, h = e.e, d = e.f;
            return io(this, a * t + u * n, a * i + u * r, l * t + c * n, l * i + c * r, s + h * t + d * n, o + h * i + d * r)
        }, t.clone = function () {
            return new e(this.a, this.b, this.c, this.d, this.e, this.f)
        }, t.equals = function (e) {
            var t = this.a, i = this.b, n = this.c, r = this.d, s = this.e, o = this.f;
            return t === e.a && i === e.b && n === e.c && r === e.d && s === e.e && o === e.f
        }, t.apply = function (e, t) {
            void 0 === t && (t = {});
            var i = e.x, n = e.y, r = this.a, s = this.b, o = this.c, a = this.d, l = this.e, u = this.f;
            return t.x = i * r + n * o + l || 0, t.y = i * s + n * a + u || 0, t
        }, e
    }();

    function ro(e, t, i) {
        if (!e || !e.parentNode || (zs || Ks(e)).documentElement === e) return new no;
        var n = function (e) {
                for (var t, i; e && e !== qs;) (i = e._gsap) && !i.scaleX && !i.scaleY && i.renderTransform && (i.scaleX = i.scaleY = 1e-4, i.renderTransform(1, i), t ? t.push(i) : t = [i]), e = e.parentNode;
                return t
            }(e.parentNode), r = Zs(e) ? Js : Qs, s = to(e, i), o = r[0].getBoundingClientRect(),
            a = r[1].getBoundingClientRect(), l = r[2].getBoundingClientRect(), u = s.parentNode, c = function e(t) {
                return "fixed" === Hs.getComputedStyle(t).position || ((t = t.parentNode) && 1 === t.nodeType ? e(t) : void 0)
            }(e),
            h = new no((a.left - o.left) / 100, (a.top - o.top) / 100, (l.left - o.left) / 100, (l.top - o.top) / 100, o.left + (c ? 0 : Hs.pageXOffset || zs.scrollLeft || Vs.scrollLeft || qs.scrollLeft || 0), o.top + (c ? 0 : Hs.pageYOffset || zs.scrollTop || Vs.scrollTop || qs.scrollTop || 0));
        if (u.removeChild(s), n) for (o = n.length; o--;) (a = n[o]).scaleX = a.scaleY = 0, a.renderTransform(1, a);
        return t ? h.inverse() : h
    }

    var so, oo, ao, lo, uo = ["x", "translateX", "left", "marginLeft"], co = ["y", "translateY", "top", "marginTop"],
        ho = Math.PI / 180, po = function (e, t, i, n) {
            for (var r = t.length, s = 2 === n ? 0 : n, o = 0; o < r; o++) e[s] = parseFloat(t[o][i]), 2 === n && (e[s + 1] = 0), s += 2;
            return e
        }, fo = function (e, t, i) {
            return parseFloat(e._gsap.get(e, t, i || "px")) || 0
        }, mo = function (e) {
            var t, i = e[0], n = e[1];
            for (t = 2; t < e.length; t += 2) i = e[t] += i, n = e[t + 1] += n
        }, go = function (e, t, i, n, r, s, o) {
            return "cubic" === o.type ? t = [t] : (t.unshift(fo(i, n, o.unitX), r ? fo(i, r, o.unitY) : 0), o.relative && mo(t), t = [(r ? Rs : Is)(t, o.curviness)]), t = s(Do(t, i, o)), To(e, i, n, t, "x", o.unitX), r && To(e, i, r, t, "y", o.unitY), Os(t, o.resolution || (0 === o.curviness ? 20 : 12))
        }, vo = function (e) {
            return e
        }, yo = /[-+\.]*\d+[\.e\-\+]*\d*[e\-\+]*\d*/g, bo = function (e, t, i) {
            var n, r, s, o = ro(e);
            return "svg" === (e.tagName + "").toLowerCase() ? (r = (n = e.viewBox.baseVal).x, s = n.y, n.width || (n = {
                width: +e.getAttribute("width"),
                height: +e.getAttribute("height")
            })) : (n = t && e.getBBox && e.getBBox(), r = s = 0), t && "auto" !== t && (r += t.push ? t[0] * (n ? n.width : e.offsetWidth || 0) : t.x, s += t.push ? t[1] * (n ? n.height : e.offsetHeight || 0) : t.y), i.apply(r || s ? o.apply({
                x: r,
                y: s
            }) : {x: o.e, y: o.f})
        }, wo = function (e, t, i, n) {
            var r, s = ro(e.parentNode, !0, !0), o = s.clone().multiply(ro(t)), a = bo(e, i, s), l = bo(t, n, s), u = l.x,
                c = l.y;
            return o.e = o.f = 0, "auto" === n && t.getTotalLength && "path" === t.tagName.toLowerCase() && (r = t.getAttribute("d").match(yo) || [], u += (r = o.apply({
                x: +r[0],
                y: +r[1]
            })).x, c += r.y), (r || t.getBBox && e.getBBox && t.ownerSVGElement === e.ownerSVGElement) && (u -= (r = o.apply(t.getBBox())).x, c -= r.y), o.e = u - a.x, o.f = c - a.y, o
        }, Do = function (e, t, i) {
            var n, r, s, o = i.align, a = i.matrix, l = i.offsetX, u = i.offsetY, c = i.alignOrigin, h = e[0][0],
                d = e[0][1], p = fo(t, "x"), f = fo(t, "y");
            return e && e.length ? (o && ("self" === o || (n = lo(o)[0] || t) === t ? Fs(e, 1, 0, 0, 1, p - h, f - d) : (c && !1 !== c[2] ? so.set(t, {transformOrigin: 100 * c[0] + "% " + 100 * c[1] + "%"}) : c = [fo(t, "xPercent") / -100, fo(t, "yPercent") / -100], s = (r = wo(t, n, c, "auto")).apply({
                x: h,
                y: d
            }), Fs(e, r.a, r.b, r.c, r.d, p + r.e - (s.x - r.e), f + r.f - (s.y - r.f)))), a ? Fs(e, a.a, a.b, a.c, a.d, a.e, a.f) : (l || u) && Fs(e, 1, 0, 0, 1, l || 0, u || 0), e) : ks("M0,0L0,0")
        }, To = function (e, t, i, n, r, s) {
            var o = t._gsap, a = o.harness, l = a && a.aliases && a.aliases[i], u = l && l.indexOf(",") < 0 ? l : i,
                c = e._pt = new oo(e._pt, t, u, 0, 0, vo, 0, o.set(t, u, e));
            c.u = ao(o.get(t, u, s)) || 0, c.path = n, c.pp = r, e._props.push(u)
        }, Co = {
            version: "3.4.2",
            name: "motionPath",
            register: function (e, t, i) {
                ao = (so = e).utils.getUnit, lo = so.utils.toArray, oo = i
            },
            init: function (e, t) {
                if (!so) return console.warn("Please gsap.registerPlugin(MotionPathPlugin)"), !1;
                "object" == typeof t && !t.style && t.path || (t = {path: t});
                var i, n, r, s, o, a, l = [], u = t.path, c = u[0], h = t.autoRotate,
                    d = (o = t.start, a = "end" in t ? t.end : 1, function (e) {
                        return o || 1 !== a ? Ss(e, o, a) : e
                    });
                if (this.rawPaths = l, this.target = e, (this.rotate = h || 0 === h) && (this.rOffset = parseFloat(h) || 0, this.radians = !!t.useRadians, this.rProp = t.rotation || "rotation", this.rSet = e._gsap.set(e, this.rProp, this), this.ru = ao(e._gsap.get(e, this.rProp)) || 0), !Array.isArray(u) || "closed" in u || "number" == typeof c) Os(i = d(Do(ks(t.path), e, t)), t.resolution), l.push(i), To(this, e, t.x || "x", i, "x", t.unitX || "px"), To(this, e, t.y || "y", i, "y", t.unitY || "px"); else {
                    for (n in c) ~uo.indexOf(n) ? r = n : ~co.indexOf(n) && (s = n);
                    for (n in r && s ? l.push(go(this, po(po([], u, r, 0), u, s, 1), e, t.x || r, t.y || s, d, t)) : r = s = 0, c) n !== r && n !== s && l.push(go(this, po([], u, n, 2), e, n, 0, d, t))
                }
            },
            render: function (e, t) {
                var i = t.rawPaths, n = i.length, r = t._pt;
                for (e > 1 ? e = 1 : e < 0 && (e = 0); n--;) Ls(i[n], e, !n && t.rotate, i[n]);
                for (; r;) r.set(r.t, r.p, r.path[r.pp] + r.u, r.d, e), r = r._next;
                t.rotate && t.rSet(t.target, t.rProp, i[0].angle * (t.radians ? ho : 1) + t.rOffset + t.ru, t, e)
            },
            getLength: function (e) {
                return Os(ks(e)).totalLength
            },
            sliceRawPath: Ss,
            getRawPath: ks,
            pointsToSegment: Rs,
            stringToRawPath: Bs,
            rawPathToString: Ns,
            transformRawPath: Fs,
            getGlobalMatrix: ro,
            getPositionOnPath: Ls,
            cacheRawPathMeasurements: Os,
            convertToPath: function (e, t) {
                return lo(e).map(function (e) {
                    return function (e, t) {
                        var i, n, r, s, o, a, l, u, c, h, d, p, f, m, g, v, y, b, w, D, T, C, k = e.tagName.toLowerCase(),
                            E = .552284749831;
                        return "path" !== k && e.getBBox ? (a = function (e, t) {
                            var i, n = document.createElementNS("http://www.w3.org/2000/svg", "path"),
                                r = [].slice.call(e.attributes), s = r.length;
                            for (t = "," + t + ","; --s > -1;) i = r[s].nodeName.toLowerCase(), t.indexOf("," + i + ",") < 0 && n.setAttributeNS(null, i, r[s].nodeValue);
                            return n
                        }(e, "x,y,width,height,cx,cy,rx,ry,r,x1,x2,y1,y2,points"), C = function (e, t) {
                            for (var i = t ? t.split(",") : [], n = {}, r = i.length; --r > -1;) n[i[r]] = +e.getAttribute(i[r]) || 0;
                            return n
                        }(e, _s[k]), "rect" === k ? (s = C.rx, o = C.ry || s, n = C.x, r = C.y, h = C.width - 2 * s, d = C.height - 2 * o, i = s || o ? "M" + (v = (m = (f = n + s) + h) + s) + "," + (b = r + o) + " V" + (w = b + d) + " C" + [v, D = w + o * E, g = m + s * E, T = w + o, m, T, m - (m - f) / 3, T, f + (m - f) / 3, T, f, T, p = n + s * (1 - E), T, n, D, n, w, n, w - (w - b) / 3, n, b + (w - b) / 3, n, b, n, y = r + o * (1 - E), p, r, f, r, f + (m - f) / 3, r, m - (m - f) / 3, r, m, r, g, r, v, y, v, b].join(",") + "z" : "M" + (n + h) + "," + r + " v" + d + " h" + -h + " v" + -d + " h" + h + "z") : "circle" === k || "ellipse" === k ? ("circle" === k ? u = (s = o = C.r) * E : (s = C.rx, u = (o = C.ry) * E), i = "M" + ((n = C.cx) + s) + "," + (r = C.cy) + " C" + [n + s, r + u, n + (l = s * E), r + o, n, r + o, n - l, r + o, n - s, r + u, n - s, r, n - s, r - u, n - l, r - o, n, r - o, n + l, r - o, n + s, r - u, n + s, r].join(",") + "z") : "line" === k ? i = "M" + C.x1 + "," + C.y1 + " L" + C.x2 + "," + C.y2 : "polyline" !== k && "polygon" !== k || (i = "M" + (n = (c = (e.getAttribute("points") + "").match(ss) || []).shift()) + "," + (r = c.shift()) + " L" + c.join(","), "polygon" === k && (i += "," + n + "," + r + "z")), a.setAttribute("d", Ns(a._gsRawPath = Bs(i))), t && e.parentNode && (e.parentNode.insertBefore(a, e), e.parentNode.removeChild(e)), a) : e
                    }(e, !1 !== t)
                })
            },
            convertCoordinates: function (e, t, i) {
                var n = ro(t, !0, !0).multiply(ro(e));
                return i ? n.apply(i) : n
            },
            getAlignMatrix: wo,
            getRelativePosition: function (e, t, i, n) {
                var r = wo(e, t, i, n);
                return {x: r.e, y: r.f}
            },
            arrayToRawPath: function (e, t) {
                var i = po(po([], e, (t = t || {}).x || "x", 0), e, t.y || "y", 1);
                return t.relative && mo(i), ["cubic" === t.type ? i : Rs(i, t.curviness)]
            }
        };
    (so || "undefined" != typeof window && (so = window.gsap) && so.registerPlugin && so) && so.registerPlugin(Co);
    var ko, Eo, _o, xo, So, Po, Oo, Ao, Mo, Lo, Fo, jo, Bo, Io, Ro, No, zo, Ho, Vo, qo, Wo, Xo, Yo, Uo, Go, $o = 1,
        Ko = [], Jo = [], Qo = Date.now, Zo = Qo(), ea = 0, ta = 1, ia = function (e) {
            return e
        }, na = function () {
            return "undefined" != typeof window
        }, ra = function () {
            return ko || na() && (ko = window.gsap) && ko.registerPlugin && ko
        }, sa = function (e) {
            return !!~Oo.indexOf(e)
        }, oa = function (e, t) {
            return ~Ko.indexOf(e) && Ko[Ko.indexOf(e) + 1][t]
        }, aa = function (e, t) {
            var i = t.s, n = t.sc, r = Jo.indexOf(e), s = ~r ? Jo[r + 1] : oa(e, i) || (sa(e) ? n : function (t) {
                return arguments.length ? e[i] = t : e[i]
            });
            return !~r && Jo.push(e, s), s
        }, la = function (e) {
            return oa(e, "getBoundingClientRect") || (sa(e) ? function () {
                return sl.width = _o.innerWidth, sl.height = _o.innerHeight, sl
            } : function () {
                return ka(e)
            })
        }, ua = function (e, t) {
            var i = t.s, n = t.d2, r = t.d, s = t.a;
            return (i = "scroll" + n) && (s = oa(e, i)) ? s() - la(e)()[r] : sa(e) ? Math.max(So[i], Po[i]) - (_o["inner" + n] || So["client" + n] || Po["client" + n]) : e[i] - e["offset" + n]
        }, ca = function (e, t) {
            for (var i = 0; i < Wo.length; i += 3) (!t || ~t.indexOf(Wo[i + 1])) && e(Wo[i], Wo[i + 1], Wo[i + 2])
        }, ha = function (e) {
            return "string" == typeof e
        }, da = function (e) {
            return "function" == typeof e
        }, pa = function (e) {
            return "number" == typeof e
        }, fa = function (e) {
            return "object" == typeof e
        }, ma = function (e) {
            return da(e) && e()
        }, ga = function (e, t) {
            return function () {
                var i = ma(e), n = ma(t);
                return function () {
                    ma(i), ma(n)
                }
            }
        }, va = Math.abs, ya = "padding", ba = "px", wa = {
            s: "scrollLeft",
            p: "left",
            p2: "Left",
            os: "right",
            os2: "Right",
            d: "width",
            d2: "Width",
            a: "x",
            sc: function (e) {
                return arguments.length ? _o.scrollTo(e, Da.sc()) : _o.pageXOffset || xo.scrollLeft || So.scrollLeft || Po.scrollLeft || 0
            }
        }, Da = {
            s: "scrollTop",
            p: "top",
            p2: "Top",
            os: "bottom",
            os2: "Bottom",
            d: "height",
            d2: "Height",
            a: "y",
            op: wa,
            sc: function (e) {
                return arguments.length ? _o.scrollTo(wa.sc(), e) : _o.pageYOffset || xo.scrollTop || So.scrollTop || Po.scrollTop || 0
            }
        }, Ta = function (e) {
            return _o.getComputedStyle(e)
        }, Ca = function (e, t) {
            for (var i in t) i in e || (e[i] = t[i]);
            return e
        }, ka = function (e, t) {
            var i = t && "matrix(1, 0, 0, 1, 0, 0)" !== Ta(e)[zo] && ko.to(e, {
                x: 0,
                y: 0,
                xPercent: 0,
                yPercent: 0,
                rotation: 0,
                rotationX: 0,
                rotationY: 0,
                scale: 1,
                skewX: 0,
                skewY: 0
            }).progress(1), n = e.getBoundingClientRect();
            return i && i.progress(0).kill(), n
        }, Ea = function (e, t) {
            var i = t.d2;
            return e["offset" + i] || e["client" + i] || 0
        }, _a = function (e, t, i, n) {
            return i.split(",").forEach(function (i) {
                return e(t, i, n)
            })
        }, xa = function (e, t, i) {
            return e.addEventListener(t, i, {passive: !0})
        }, Sa = function (e, t, i) {
            return e.removeEventListener(t, i)
        }, Pa = {startColor: "green", endColor: "red", indent: 0, fontSize: "16px", fontWeight: "normal"},
        Oa = {toggleActions: "play", anticipatePin: 0}, Aa = {top: 0, left: 0, center: .5, bottom: 1, right: 1},
        Ma = function (e, t) {
            if (ha(e)) {
                var i = e.indexOf("="), n = ~i ? +(e.charAt(i - 1) + 1) * parseFloat(e.substr(i + 1)) : 0;
                n && (e.indexOf("%") > i && (n *= t / 100), e = e.substr(0, i - 1)), e = n + (e in Aa ? Aa[e] * t : ~e.indexOf("%") ? parseFloat(e) * t / 100 : parseFloat(e) || 0)
            }
            return e
        }, La = function (e, t, i, n, r, s, o) {
            var a = r.startColor, l = r.endColor, u = r.fontSize, c = r.indent, h = r.fontWeight,
                d = xo.createElement("div"), p = sa(i) || "fixed" === oa(i, "pinType"), f = -1 !== e.indexOf("scroller"),
                m = p ? Po : i, g = -1 !== e.indexOf("start"), v = g ? a : l,
                y = "border-color:" + v + ";font-size:" + u + ";color:" + v + ";font-weight:" + h + ";pointer-events:none;white-space:nowrap;font-family:sans-serif,Arial;z-index:1000;padding:4px 8px;border-width:0;border-style:solid;";
            return y += "position:" + (f && p ? "fixed;" : "absolute;"), (f || !p) && (y += (n === Da ? "right" : "bottom") + ":" + (s + parseFloat(c)) + "px;"), o && (y += "box-sizing:border-box;text-align:left;width:" + o.offsetWidth + "px;"), d._isStart = g, d.setAttribute("class", "gsap-marker-" + e), d.style.cssText = y, d.innerText = t || 0 === t ? e + "-" + t : e, m.insertBefore(d, m.children[0]), d._offset = d["offset" + n.op.d2], Fa(d, 0, n, g), d
        }, Fa = function (e, t, i, n) {
            var r = {display: "block"}, s = i[n ? "os2" : "p2"], o = i[n ? "p2" : "os2"];
            e._isFlipped = n, r[i.a + "Percent"] = n ? -100 : 0, r[i.a] = n ? 1 : 0, r["border" + s + "Width"] = 1, r["border" + o + "Width"] = 0, r[i.p] = t, ko.set(e, r)
        }, ja = [], Ba = {}, Ia = function () {
            return Lo || (Lo = Mo(Qa))
        }, Ra = function () {
            Lo || (Lo = Mo(Qa), ea || Xa("scrollStart"), ea = Qo())
        }, Na = function () {
            return !Ro && Ao.restart(!0)
        }, za = {}, Ha = [], Va = [], qa = function (e) {
            var t = ko.ticker.frame, i = [], n = 0;
            if (Go !== t || $o) {
                for (Ga(); n < Va.length; n += 3) _o.matchMedia(Va[n]).matches ? i.push(n) : Ga(1, Va[n]) || da(Va[n + 2]) && Va[n + 2]();
                for (Ua(), n = 0; n < i.length; n++) Uo = Va[i[n]], Va[i[n] + 2] = Va[i[n] + 1](e);
                Uo = 0, $a(0, 1), Go = t
            }
        }, Wa = function e() {
            return Sa(ul, "scrollEnd", e) || $a(!0)
        }, Xa = function (e) {
            return za[e] && za[e].map(function (e) {
                return e()
            }) || Ha
        }, Ya = [], Ua = function (e) {
            for (var t = 0; t < Ya.length; t += 4) e && Ya[t + 3] !== e || (Ya[t].style.cssText = Ya[t + 1], Ya[t + 2].uncache = 1)
        }, Ga = function (e, t) {
            var i;
            for (Ho = 0; Ho < ja.length; Ho++) i = ja[Ho], t && i.media !== t || (e ? i.kill(1) : (i.scroll.rec || (i.scroll.rec = i.scroll()), i.revert()));
            Ua(t), t || Xa("revert")
        }, $a = function (e, t) {
            if (!ea || e) {
                var i = Xa("refreshInit");
                for (Xo && ul.sort(), t || Ga(), Ho = 0; Ho < ja.length; Ho++) ja[Ho].refresh();
                for (i.forEach(function (e) {
                    return e && e.render && e.render(-1)
                }), Ho = ja.length; Ho--;) ja[Ho].scroll.rec = 0;
                Xa("refresh")
            } else xa(ul, "scrollEnd", Wa)
        }, Ka = 0, Ja = 1, Qa = function () {
            var e = ja.length, t = Qo(), i = t - Zo >= 50, n = e && ja[0].scroll();
            if (Ja = Ka > n ? -1 : 1, Ka = n, i && (ea && !No && t - ea > 200 && (ea = 0, Xa("scrollEnd")), Bo = Zo, Zo = t), Ja < 0) {
                for (Ho = e; Ho--;) ja[Ho].update(0, i);
                Ja = 1
            } else for (Ho = 0; Ho < e; Ho++) ja[Ho] && ja[Ho].update(0, i);
            Lo = 0
        },
        Za = ["left", "top", "bottom", "right", "marginBottom", "marginRight", "marginTop", "marginLeft", "display", "flexShrink", "float"],
        el = Za.concat(["width", "height", "boxSizing", "maxWidth", "maxHeight", "position", "margin", ya, ya + "Top", ya + "Right", ya + "Bottom", ya + "Left"]),
        tl = function (e, t, i, n) {
            if (e.parentNode !== t) {
                for (var r, s = Za.length, o = t.style, a = e.style; s--;) o[r = Za[s]] = i[r];
                o.position = "absolute" === i.position ? "absolute" : "relative", "inline" === i.display && (o.display = "inline-block"), a.bottom = a.right = "auto", o.overflow = "visible", o.boxSizing = "border-box", o.width = Ea(e, wa) + ba, o.height = Ea(e, Da) + ba, o[ya] = a.margin = a.top = a.left = "0", nl(n), a.width = i.width, a.height = i.height, a[ya] = i[ya], e.parentNode.insertBefore(t, e), t.appendChild(e)
            }
        }, il = /([A-Z])/g, nl = function (e) {
            if (e) for (var t, i, n = e.t.style, r = e.length, s = 0; s < r; s += 2) i = e[s + 1], t = e[s], i ? n[t] = i : n[t] && n.removeProperty(t.replace(il, "-$1").toLowerCase())
        }, rl = function (e) {
            for (var t = el.length, i = e.style, n = [], r = 0; r < t; r++) n.push(el[r], i[el[r]]);
            return n.t = e, n
        }, sl = {left: 0, top: 0}, ol = function (e, t, i, n, r, s, o, a, l, u, c, h) {
            if (da(e) && (e = e(a)), ha(e) && "max" === e.substr(0, 3) && (e = h + ("=" === e.charAt(4) ? Ma("0" + e.substr(3), i) : 0)), pa(e)) o && Fa(o, i, n, !0); else {
                da(t) && (t = t(a));
                var d, p, f, m = Fo(t)[0] || Po, g = ka(m) || {}, v = e.split(" ");
                g && (g.left || g.top) || "none" !== Ta(m).display || (f = m.style.display, m.style.display = "block", g = ka(m), f ? m.style.display = f : m.style.removeProperty("display")), d = Ma(v[0], g[n.d]), p = Ma(v[1] || "0", i), e = g[n.p] - l[n.p] - u + d + r - p, o && Fa(o, p, n, i - p < 20 || o._isStart && p > 20), i -= i - p
            }
            if (s) {
                var y = e + i, b = s._isStart;
                h = "scroll" + n.d2, Fa(s, y, n, b && y > 20 || !b && (c ? Math.max(Po[h], So[h]) : s.parentNode[h]) <= y + 1), c && (l = ka(o), c && (s.style[n.op.p] = l[n.op.p] - n.op.m - s._offset + ba))
            }
            return Math.round(e)
        }, al = /(?:webkit|moz|length)/i, ll = function (e, t) {
            var i, n = aa(e, t), r = "_scroll" + t.p2;
            return e[r] = n, function t(s, o, a, l, u) {
                var c = t.tween, h = o.onComplete, d = {};
                return c && c.kill(), i = n(), o[r] = s, o.modifiers = d, d[r] = function (e) {
                    return Math.abs(n() - i) > 7 ? (c.kill(), t.tween = 0, e = n()) : l && (e = a + l * c.ratio + u * c.ratio * c.ratio), i = Math.round(e)
                }, o.onComplete = function () {
                    t.tween = 0, h && h.call(c)
                }, c = t.tween = ko.to(e, o)
            }
        };
    wa.op = Da;
    var ul = function () {
        function e(t, i) {
            Eo || e.register(ko) || console.warn("Please gsap.registerPlugin(ScrollTrigger)"), this.init(t, i)
        }

        return e.prototype.init = function (t, i) {
            if (this.progress = 0, this.vars && this.kill(1), ta) {
                var n, r, s, o, a, l, u, c, h, d, p, f, m, g, v, y, b, w, D, T, C, k, E, _, x, S, P, O, A, M, L, F, j,
                    B, I, R, N, z, H,
                    V = (t = Ca(ha(t) || pa(t) || t.nodeType ? {trigger: t} : t, Oa)).horizontal ? wa : Da, q = t,
                    W = q.onUpdate, X = q.toggleClass, Y = q.id, U = q.onToggle, G = q.onRefresh, $ = q.scrub,
                    K = q.trigger, J = q.pin, Q = q.pinSpacing, Z = q.invalidateOnRefresh, ee = q.anticipatePin,
                    te = q.onScrubComplete, ie = q.onSnapComplete, ne = q.once, re = q.snap, se = q.pinReparent,
                    oe = !$ && 0 !== $, ae = Fo(t.scroller || _o)[0], le = ko.core.getCache(ae), ue = sa(ae),
                    ce = ue || "fixed" === oa(ae, "pinType"), he = [t.onEnter, t.onLeave, t.onEnterBack, t.onLeaveBack],
                    de = oe && (ne ? "play" : t.toggleActions).split(" "), pe = "markers" in t ? t.markers : Oa.markers,
                    fe = ue ? 0 : parseFloat(Ta(ae)["border" + V.p2 + "Width"]) || 0, me = this,
                    ge = t.onRefreshInit && function () {
                        return t.onRefreshInit(me)
                    }, ve = function (e, t, i) {
                        var n = i.d, r = i.d2, s = i.a;
                        return (s = oa(e, "getBoundingClientRect")) ? function () {
                            return s()[n]
                        } : function () {
                            return (t ? _o["inner" + r] : e["client" + r]) || 0
                        }
                    }(ae, ue, V), ye = function (e, t) {
                        return !ue || ~Ko.indexOf(e) ? la(e) : function () {
                            return sl
                        }
                    }(ae);
                me.media = Uo, ee *= 45, ja.push(me), me.scroller = ae, me.scroll = aa(ae, V), a = me.scroll(), me.vars = t, i = i || t.animation, "refreshPriority" in t && (Xo = 1), le.tweenScroll = le.tweenScroll || {
                    top: ll(ae, Da),
                    left: ll(ae, wa)
                }, me.tweenTo = n = le.tweenScroll[V.p], i && (i.vars.lazy = !1, i._initted || !1 !== i.vars.immediateRender && !1 !== t.immediateRender && i.render(0, !0, !0), me.animation = i.pause(), i.scrollTrigger = me, (j = pa($) && $) && (F = ko.to(i, {
                    ease: "power3",
                    duration: j,
                    onComplete: function () {
                        return te && te(me)
                    }
                })), A = 0, Y || (Y = i.vars.id)), re && (fa(re) || (re = {snapTo: re}), ko.set(ue ? [Po, So] : ae, {scrollBehavior: "auto"}), s = da(re.snapTo) ? re.snapTo : "labels" === re.snapTo ? function (e) {
                    return function (t) {
                        var i, n = [], r = e.labels, s = e.duration();
                        for (i in r) n.push(r[i] / s);
                        return ko.utils.snap(n, t)
                    }
                }(i) : ko.utils.snap(re.snapTo), B = re.duration || {
                    min: .1,
                    max: 2
                }, B = fa(B) ? jo(B.min, B.max) : jo(B, B), I = ko.delayedCall(re.delay || j / 2 || .1, function () {
                    if (!ea || ea === L && !No) {
                        var e = i && !oe ? i.totalProgress() : me.progress, t = (e - M) / (Qo() - Bo) * 1e3 || 0,
                            r = va(t / 2) * t / .185, o = e + r, a = jo(0, 1, s(o, me)), l = a - e - r, h = me.scroll(),
                            d = Math.round(u + a * g), p = n.tween;
                        if (h <= c && h >= u) {
                            if (p && !p._initted) {
                                if (p.data <= Math.abs(d - h)) return;
                                p.kill()
                            }
                            n(d, {
                                duration: B(va(.185 * Math.max(va(o - e), va(a - e)) / t / .05 || 0)),
                                ease: re.ease || "power3",
                                data: Math.abs(d - h),
                                onComplete: function () {
                                    A = M = i && !oe ? i.totalProgress() : me.progress, ie && ie(me)
                                }
                            }, u + e * g, r * g, l * g)
                        }
                    } else I.restart(!0)
                }).pause()), Y && (Ba[Y] = me), K = me.trigger = Fo(K || J)[0], J = !0 === J ? K : Fo(J)[0], ha(X) && (X = {
                    targets: K,
                    className: X
                }), J && (!1 === Q || "margin" === Q || (Q = "flex" !== Ta(J.parentNode).display && ya), me.pin = J, !1 !== t.force3D && ko.set(J, {force3D: !0}), (r = ko.core.getCache(J)).spacer ? v = r.pinState : (r.spacer = w = xo.createElement("div"), w.setAttribute("class", "pin-spacer" + (Y ? " pin-spacer-" + Y : "")), r.pinState = v = rl(J)), me.spacer = w = r.spacer, O = Ta(J), _ = O[Q + V.os2], T = ko.getProperty(J), C = ko.quickSetter(J, V.a, ba), J.firstChild && !ua(J, V) && (J.style.overflow = "hidden"), tl(J, w, O), b = rl(J)), pe && (m = fa(pe) ? Ca(pe, Pa) : Pa, p = La("scroller-start", Y, ae, V, m, 0), f = La("scroller-end", Y, ae, V, m, 0, p), D = p["offset" + V.op.d2], h = La("start", Y, ae, V, m, D), d = La("end", Y, ae, V, m, D), ce || ((H = ae).style.position = "absolute" === Ta(H).position ? "absolute" : "relative", ko.set([p, f], {force3D: !0}), S = ko.quickSetter(p, V.a, ba), P = ko.quickSetter(f, V.a, ba))), me.revert = function (e) {
                    var t = !1 !== e || !me.enabled, n = Ro;
                    t !== o && (t && (N = Math.max(me.scroll(), me.scroll.rec || 0), R = me.progress, z = i && i.progress()), h && [h, d, p, f].forEach(function (e) {
                        return e.style.display = t ? "none" : "block"
                    }), Ro = 1, me.update(t), Ro = n, J && (t ? function (e, t, i) {
                        if (nl(v), e.parentNode === t) {
                            var n = t.parentNode;
                            n && (n.insertBefore(e, t), n.removeChild(t))
                        }
                    }(J, w) : tl(J, w, Ta(J), x)), o = t)
                }, me.refresh = function (n) {
                    if (!Ro && me.enabled) if (J && n && ea) xa(e, "scrollEnd", Wa); else {
                        Ro = 1, F && F.kill(), Z && i && i.progress(0).invalidate(), o || me.revert();
                        for (var r, s, m, D, C, _, S, P = ve(), O = ye(), A = ua(ae, V), M = 0, L = 0, j = t.end, B = t.endTrigger || K, I = t.start || (J || !K ? "0 0" : "0 100%"), H = K && Math.max(0, ja.indexOf(me)) || 0, q = H; q--;) (S = ja[q].pin) && (S === K || S === J) && ja[q].revert();
                        for (u = ol(I, K, P, V, me.scroll(), h, p, me, O, fe, ce, A) || (J ? -.001 : 0), da(j) && (j = j(me)), ha(j) && !j.indexOf("+=") && (~j.indexOf(" ") ? j = (ha(I) ? I.split(" ")[0] : "") + j : (M = Ma(j.substr(2), P), j = ha(I) ? I : u + M, B = K)), c = Math.max(u, ol(j || (B ? "100% 0" : A), B, P, V, me.scroll() + M, d, f, me, O, fe, ce, A)) || -.001, g = c - u || (u -= .01) && .001, M = 0, q = H; q--;) (S = (_ = ja[q]).pin) && _.start - _._pinPush < u && (r = _.end - _.start, S === K && (M += r), S === J && (L += r));
                        if (u += M, c += M, me._pinPush = L, h && M && ((r = {})[V.a] = "+=" + M, ko.set([h, d], r)), J) r = Ta(J), D = V === Da, m = me.scroll(), k = parseFloat(T(V.a)) + L, tl(J, w, r), b = rl(J), s = ka(J, !0), Q && ((x = [Q + V.os2, g + L + ba]).t = w, (q = Q === ya ? Ea(J, V) + g + L : 0) && x.push(V.d, q + ba), nl(x), ce && me.scroll(N)), ce && ((C = {
                            top: s.top + (D ? m - u : 0) + ba,
                            left: s.left + (D ? 0 : m - u) + ba,
                            boxSizing: "border-box",
                            position: "fixed"
                        }).width = C.maxWidth = Math.ceil(s.width) + ba, C.height = C.maxHeight = Math.ceil(s.height) + ba, C.margin = C.marginTop = C.marginRight = C.marginBottom = C.marginLeft = "0", C[ya] = r[ya], C[ya + "Top"] = r[ya + "Top"], C[ya + "Right"] = r[ya + "Right"], C[ya + "Bottom"] = r[ya + "Bottom"], C[ya + "Left"] = r[ya + "Left"], y = function (e, t, i) {
                            for (var n, r = [], s = e.length, o = i ? 8 : 0; o < s; o += 2) n = e[o], r.push(n, n in t ? t[n] : e[o + 1]);
                            return r.t = e.t, r
                        }(v, C, se)), i ? (i.progress(1, !0), E = T(V.a) - k + g + L, g !== E && y.splice(y.length - 2, 2), i.progress(0, !0)) : E = g; else if (K && me.scroll()) for (s = K.parentNode; s && s !== Po;) s._pinOffset && (u -= s._pinOffset, c -= s._pinOffset), s = s.parentNode;
                        for (q = 0; q < H; q++) (_ = ja[q].pin) && (_ === K || _ === J) && ja[q].revert(!1);
                        me.start = u, me.end = c, (a = l = me.scroll()) < N && me.scroll(N), me.revert(!1), Ro = 0, z && oe && i.progress(z, !0), R !== me.progress && (F && i.totalProgress(R, !0), me.progress = R, me.update()), J && Q && (w._pinOffset = Math.round(me.progress * E)), G && G(me)
                    }
                }, me.getVelocity = function () {
                    return (me.scroll() - l) / (Qo() - Bo) * 1e3 || 0
                }, me.update = function (e, t) {
                    var r, s, o, h, d, f = me.scroll(), m = e ? 0 : (f - u) / g, v = m < 0 ? 0 : m > 1 ? 1 : m || 0,
                        D = me.progress;
                    if (t && (l = a, a = f, re && (M = A, A = i && !oe ? i.totalProgress() : v)), ee && !v && J && !Ro && !$o && ea && u < f + (f - l) / (Qo() - Bo) * ee && (v = 1e-4), v !== D && me.enabled) {
                        if (h = (d = (r = me.isActive = !!v && v < 1) != (!!D && D < 1)) || !!v != !!D, me.direction = v > D ? 1 : -1, me.progress = v, oe || (!F || Ro || $o ? i && i.totalProgress(v, !!Ro) : (F.vars.totalProgress = v, F.invalidate().restart())), J) if (e && Q && (w.style[Q + V.os2] = _), ce) {
                            if (h) {
                                if (o = !e && v > D && c + 1 > f && f + 1 >= ua(ae, V), se) {
                                    if (!Ro && (r || o)) {
                                        var T = ka(J, !0), x = f - u;
                                        J.style.top = T.top + (V === Da ? x : 0) + ba, J.style.left = T.left + (V === Da ? 0 : x) + ba
                                    }
                                    !function (e, t) {
                                        if (e.parentNode !== t) {
                                            var i, n, r = e.style;
                                            if (t === Po) for (i in e._stOrig = r.cssText, n = Ta(e)) +i || al.test(i) || !n[i] || "string" != typeof r[i] || "0" === i || (r[i] = n[i]); else r.cssText = e._stOrig;
                                            t.appendChild(e)
                                        }
                                    }(J, Ro || !r && !o ? w : Po)
                                }
                                nl(r || o ? y : b), E !== g && v < 1 && r || C(k + (1 !== v || o ? 0 : E))
                            }
                        } else C(k + E * v);
                        !re || n.tween || Ro || $o || (L = ea, I.restart(!0)), X && d && (!ne || r) && Fo(X.targets).forEach(function (e) {
                            return e.classList[r ? "add" : "remove"](X.className)
                        }), W && !oe && !e && W(me), h && !Ro ? (s = v && !D ? 0 : 1 === v ? 1 : 1 === D ? 2 : 3, oe && (o = !d && "none" !== de[s + 1] && de[s + 1] || de[s], i && ("complete" === o || "reset" === o || o in i) && ("complete" === o ? i.pause().totalProgress(1) : "reset" === o ? i.restart(!0).pause() : i[o]()), W && W(me)), !d && Yo || (U && d && U(me), he[s] && he[s](me), ne && (1 === v ? me.kill(!1, 1) : he[s] = 0), d || he[s = 1 === v ? 1 : 3] && he[s](me))) : oe && W && !Ro && W(me)
                    }
                    P && (S(f + (p._isFlipped ? 1 : 0)), P(f))
                }, me.enable = function () {
                    me.enabled || (me.enabled = !0, xa(ae, "resize", Na), xa(ae, "scroll", Ra), ge && xa(e, "refreshInit", ge), i && i.add ? ko.delayedCall(.01, me.refresh) && (g = .01) && (u = c = 0) : me.refresh())
                }, me.disable = function (t, i) {
                    if (me.enabled && (!1 !== t && me.revert(), me.enabled = me.isActive = !1, i || F && F.pause(), N = 0, r && (r.uncache = 1), ge && Sa(e, "refreshInit", ge), I && (I.pause(), n.tween && n.tween.kill()), !ue)) {
                        for (var s = ja.length; s--;) if (ja[s].scroller === ae && ja[s] !== me) return;
                        Sa(ae, "resize", Na), Sa(ae, "scroll", Ra)
                    }
                }, me.kill = function (e, t) {
                    me.disable(e, t), Y && delete Ba[Y];
                    var n = ja.indexOf(me);
                    ja.splice(n, 1), n === Ho && Ja > 0 && Ho--, i && (i.scrollTrigger = null, e && i.render(-1), t && F || i.kill()), h && [h, d, p, f].forEach(function (e) {
                        return e.parentNode.removeChild(e)
                    }), r && (r.uncache = 1)
                }, me.enable()
            } else this.update = this.refresh = this.kill = ia
        }, e.register = function (t) {
            if (!Eo && (ko = t || ra(), na() && window.document && (_o = window, xo = document, So = xo.documentElement, Po = xo.body), ko && (Fo = ko.utils.toArray, jo = ko.utils.clamp, ko.core.globals("ScrollTrigger", e), Po))) {
                Mo = _o.requestAnimationFrame || function (e) {
                    return setTimeout(e, 16)
                }, xa(_o, "mousewheel", Ra), Oo = [_o, xo, So, Po], xa(xo, "scroll", Ra);
                var i, n = Po.style, r = n.borderTop;
                n.borderTop = "1px solid #000", i = ka(Po), Da.m = Math.round(i.top + Da.sc()) || 0, wa.m = Math.round(i.left + wa.sc()) || 0, r ? n.borderTop = r : n.removeProperty("border-top"), Io = setInterval(Ia, 200), ko.delayedCall(.5, function () {
                    return $o = 0
                }), xa(xo, "touchcancel", ia), xa(Po, "touchstart", ia), _a(xa, xo, "pointerdown,touchstart,mousedown", function () {
                    return No = 1
                }), _a(xa, xo, "pointerup,touchend,mouseup", function () {
                    return No = 0
                }), zo = ko.utils.checkPrefix("transform"), el.push(zo), Eo = Qo(), Ao = ko.delayedCall(.2, $a).pause(), Wo = [xo, "visibilitychange", function () {
                    var e = _o.innerWidth, t = _o.innerHeight;
                    xo.hidden ? (Vo = e, qo = t) : Vo === e && qo === t || Na()
                }, xo, "DOMContentLoaded", $a, _o, "load", function () {
                    return ea || $a()
                }, _o, "resize", Na], ca(xa)
            }
            return Eo
        }, e.defaults = function (e) {
            for (var t in e) Oa[t] = e[t]
        }, e.kill = function () {
            ta = 0, ja.slice(0).forEach(function (e) {
                return e.kill(1)
            })
        }, e.config = function (e) {
            "limitCallbacks" in e && (Yo = !!e.limitCallbacks);
            var t = e.syncInterval;
            t && clearInterval(Io) || (Io = t) && setInterval(Ia, t), "autoRefreshEvents" in e && (ca(Sa) || ca(xa, e.autoRefreshEvents || "none"))
        }, e.scrollerProxy = function (e, t) {
            var i = Fo(e)[0];
            sa(i) ? Ko.unshift(_o, t, Po, t, So, t) : Ko.unshift(i, t)
        }, e.matchMedia = function (e) {
            var t, i, n, r, s;
            for (i in e) n = Va.indexOf(i), r = e[i], Uo = i, "all" === i ? r() : (t = _o.matchMedia(i)) && (t.matches && (s = r()), ~n ? (Va[n + 1] = ga(Va[n + 1], r), Va[n + 2] = ga(Va[n + 2], s)) : (n = Va.length, Va.push(i, r, s), t.addListener ? t.addListener(qa) : t.addEventListener("change", qa))), Uo = 0;
            return Va
        }, e
    }();

    function cl(e) {
        return (cl = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function hl(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function dl(e, t) {
        return (dl = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function pl(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = ml(e);
            if (t) {
                var r = ml(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== cl(t) && "function" != typeof t ? fl(e) : t
            }(this, i)
        }
    }

    function fl(e) {
        if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return e
    }

    function ml(e) {
        return (ml = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    function gl(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    ul.version = "3.4.2", ul.saveStyles = function (e) {
        return e ? Fo(e).forEach(function (e) {
            var t = Ya.indexOf(e);
            t >= 0 && Ya.splice(t, 4), Ya.push(e, e.style.cssText, ko.core.getCache(e), Uo)
        }) : Ya
    }, ul.revert = function (e, t) {
        return Ga(!e, t)
    }, ul.create = function (e, t) {
        return new ul(e, t)
    }, ul.refresh = function (e) {
        return e ? Na() : $a(!0)
    }, ul.update = Qa, ul.maxScroll = function (e, t) {
        return ua(e, t ? wa : Da)
    }, ul.getScrollFunc = function (e, t) {
        return aa(Fo(e)[0], t ? wa : Da)
    }, ul.getById = function (e) {
        return Ba[e]
    }, ul.getAll = function () {
        return ja.slice(0)
    }, ul.isScrolling = function () {
        return !!ea
    }, ul.addEventListener = function (e, t) {
        var i = za[e] || (za[e] = []);
        ~i.indexOf(t) || i.push(t)
    }, ul.removeEventListener = function (e, t) {
        var i = za[e], n = i && i.indexOf(t);
        n >= 0 && i.splice(n, 1)
    }, ul.batch = function (e, t) {
        var i, n = [], r = {}, s = t.interval || .016, o = t.batchMax || 1e9, a = function (e, t) {
            var i = [], n = [], r = ko.delayedCall(s, function () {
                t(i, n), i = [], n = []
            }).pause();
            return function (e) {
                i.length || r.restart(!0), i.push(e.trigger), n.push(e), o <= i.length && r.progress(1)
            }
        };
        for (i in t) r[i] = "on" === i.substr(0, 2) && da(t[i]) && "onRefreshInit" !== i ? a(0, t[i]) : t[i];
        return da(o) && (o = o(), xa(ul, "refresh", function () {
            return o = t.batchMax()
        })), Fo(e).forEach(function (e) {
            var t = {};
            for (i in r) t[i] = r[i];
            t.trigger = e, n.push(ul.create(t))
        }), n
    }, ul.sort = function (e) {
        return ja.sort(e || function (e, t) {
            return -1e6 * (e.vars.refreshPriority || 0) + e.start - (t.start + -1e6 * (t.vars.refreshPriority || 0))
        })
    }, ra() && ko.registerPlugin(ul), an.registerPlugin(ul), an.registerPlugin(Co);
    var vl = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && dl(e, t)
        }(r, hr.Renderer);
        var t, i, n = pl(r);

        function r(e) {
            var t;
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), gl(fl(t = n.call(this, e)), "init", function () {
                if (o.flags.locked = !1, o.isDesktop) {
                    ns()(ln(".js-smooth")).on("done", function (e) {
                        setTimeout(function () {
                            t.onPageChange()
                        }, 2e3)
                    }), t.scroll = new dr.a({
                        el: ln(".js-smooth"),
                        smooth: !0,
                        getDirection: !0,
                        multiplier: 1
                    }), ul.defaults({scroller: ".js-smooth"}), ul.scrollerProxy(".js-smooth", {
                        scrollTop: function () {
                            return t.scroll.scroll.instance.scroll.y
                        }, getBoundingClientRect: function () {
                            return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight}
                        }
                    });
                    var e = !1, i = !0;
                    t.scroll.on("scroll", function (i) {
                        if (t.scroll && (t.progressScroll = new vr({
                            scroll: i.scroll.y,
                            limit: i.limit
                        }), t.progressScroll.init()), ul.update(), o.bounds.scroll > i.scroll.y ? (ln(".js-header").classList.remove("hide"), ln(".js-header").classList.add("bg-add")) : i.scroll.y > 100 && (ln(".js-header").classList.add("hide"), ln(".js-header").classList.remove("bg-add")), i.scroll.y < 100 && ln(".js-header").classList.remove("bg-add"), t.scroll && (o.bounds.scroll = i.scroll.y), ln(".teamAll") && ln(".teamAll").classList.contains("is-inview") && !e) {
                            e = !0;
                            var n = new Jr;
                            setTimeout(function () {
                                n.enter(), t.updateScrollBounds()
                            }, 500)
                        }
                        ln(".gal1").style.transform = "translate3d(0, ".concat(i.scroll.y / 30, "px, 0)"), ln(".gal2").style.transform = "translate3d(0, -".concat(i.scroll.y / 18, "px, 0)")
                    }), t.scroll.on("call", function (e) {
                        "scrollUpdate" === e && i && (console.log("ANDA ?"), i = !1, setTimeout(function () {
                            t.scroll.update()
                        }, 2e3), console.log(i))
                    }), ln(".js-scroll-down-home") && ln("#about-section") && ln(".js-scroll-down-home").addEventListener("click", function () {
                        var e = ln("#about-section");
                        t.scroll.scrollTo(e, -15)
                    }), o.flags.first && "home" === o.flags.sectionView ? (o.flags.first = !1, t.onHideVideo(), ln(".rocket__trigger") && (ln(".rocket__path").style.opacity = 1, ln(".rocket__path").style.pointerEvents = "auto", t.rocketProcess = an.timeline({
                        paused: !0,
                        scrollTrigger: {
                            trigger: ".rocket__trigger",
                            scroller: ".js-smooth",
                            scrub: !0,
                            start: "top top+=225",
                            end: "+=165%"
                        }
                    }), t.rocketProcess.to("#rocket", {
                        motionPath: {
                            path: "#rocket__path",
                            align: "#rocket__path",
                            autoRotate: !0,
                            alignOrigin: [.5, .5]
                        }, ease: "linear"
                    }), t.rocketTop = an.timeline({
                        paused: !0,
                        scrollTrigger: {
                            trigger: ".hero",
                            scroller: ".js-smooth",
                            scrub: !0,
                            start: "top top+=5",
                            end: "+=35%"
                        }
                    }), t.rocketTop.to("#ro", {
                        motionPath: {
                            path: "#ro__path",
                            align: "#ro__path",
                            autoRotate: !0,
                            alignOrigin: [.5, .5]
                        }, scale: 3.25, ease: "linear"
                    }), t.rocketTop = an.timeline({
                        paused: !0,
                        scrollTrigger: {
                            trigger: ".pre__about",
                            scroller: ".js-smooth",
                            scrub: !0,
                            start: "top top+=40%",
                            end: "+=45%"
                        }
                    }), setTimeout(function () {
                        an.to(".ero", {
                            scrollTrigger: {
                                trigger: ".pre__about",
                                scroller: ".js-smooth",
                                start: "top top+=40%",
                                end: "+=25%"
                            }, autoAlpha: 1, ease: "linear"
                        })
                    }, 200), t.rocketTop.to("#bigRo", {
                        motionPath: {
                            path: "#bigRoPath",
                            align: "#bigRoPath",
                            autoRotate: !0,
                            alignOrigin: [.5, .5]
                        }, ease: "linear"
                    }))) : (ln(".showreel").style.opacity = 0, ln(".showreel").style.pointerEvents = "none", ln(".rocket__trigger") && (ln(".rocket__trigger").style.opacity = 0, ln(".rocket__trigger").style.pointerEvents = "none", ln(".rockTopAll").style.opacity = 0, ln(".rockTopAll").style.pointerEvents = "none"), setTimeout(function () {
                        ln(".js-home-enter") && ln(".js-home-enter").classList.add("is-inview")
                    }, 1e3)), ul.addEventListener("refresh", function () {
                        t.scroll.update()
                    }), ul.refresh(), ln(".video__showreel") && ln(".video__showreel").addEventListener("ended", function () {
                        an.to(ln(".showreel"), {
                            delay: .5,
                            opacity: 0,
                            pointerEvents: "none",
                            duration: 1.2,
                            ease: "expo"
                        }), setTimeout(function () {
                            un(".js-home-enter").forEach(function (e) {
                                e.classList.add("is-inview")
                            })
                        }, 500)
                    })
                } else if (o.isDevice) {
                    t.scroll = new dr.a({el: ln(".js-smooth"), smooth: !1, smoothMobile: !0});
                    var n = !1;
                    t.scroll.on("scroll", function (e) {
                        o.bounds.scroll > e.scroll.y ? (ln(".js-header").classList.remove("hide"), ln(".js-header").classList.add("bg-add")) : e.scroll.y > 100 && (ln(".js-header").classList.add("hide"), ln(".js-header").classList.remove("bg-add")), e.scroll.y < 100 && ln(".js-header").classList.remove("bg-add"), t.scroll && (o.bounds.scroll = e.scroll.y), ln(".teamAll") && ln(".teamAll").classList.contains("is-inview") && !n && (n = !0, (new Jr).enter())
                    }), un(".js-home-enter").forEach(function (e) {
                        e.classList.add("is-inview")
                    })
                }
                var r = window.location.search.split("?").join("");
                "services" === r || "team" === r ? setTimeout(function () {
                    t.scroll && t.scroll.scrollTo("#".concat(r, "-section"), -60)
                }, 1500) : "focus" === r && setTimeout(function () {
                    t.scroll && t.scroll.scrollTo("#".concat(r, "-section"), 300)
                }, 1500), new mr, t.eventsFullVideo(), t.sendNewsletter(), t.initContactToggle(), t.formProject(), t.formCarrers(), ln(".js-toTop") && ln(".js-toTop").addEventListener("click", function () {
                    t.scroll.scrollTo(0)
                })
            }), gl(fl(t), "openFullVideoScreen", function () {
                t.stopScroll(), o.flags.showVideo = !0, o.flags.mouseWheelHome = !1, an.to(".showreel", {
                    autoAlpha: 1,
                    pointerEvents: "inherit",
                    duration: 1.5,
                    ease: "power3",
                    onComplete: function () {
                        setTimeout(function () {
                            ln(".video__showreel").play()
                        }, 1500)
                    }
                }), an.fromTo(un(".js-t-skip"), {xPercent: 100}, {
                    duration: 1.5,
                    xPercent: 0,
                    stagger: .075,
                    delay: 1.5,
                    ease: "expo.inOut"
                })
            }), gl(fl(t), "closeFullVideoScreen", function () {
                t.startScroll(), o.flags.showVideo = !1, o.flags.clickToSkape ? (o.flags.clickToSkape = !1, an.to(".showreel", {
                    autoAlpha: 0,
                    pointerEvents: "none",
                    duration: 1.5,
                    ease: "power3"
                }), setTimeout(function () {
                    ln(".video__showreel").pause(), ln(".video__showreel").currentTime = 0
                }, 500), setTimeout(function () {
                    ln(".js-home-enter") && ln(".js-home-enter").classList.add("is-inview")
                }, 1e3)) : (an.to(".showreel", {
                    autoAlpha: 0,
                    pointerEvents: "none",
                    duration: 1.5,
                    ease: "power3"
                }), an.fromTo(un(".js-t-skip"), {yPercent: 0}, {
                    duration: 1.5,
                    yPercent: 100,
                    stagger: .075,
                    delay: .5,
                    ease: "expo.inOut"
                }), setTimeout(function () {
                    ln(".video__showreel").pause(), ln(".video__showreel").currentTime = 0
                }, 500))
            }), t.scroll = null, t
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                this.el = this.wrap.lastElementChild
            }
        }, {
            key: "onLeave", value: function () {
            }
        }, {
            key: "onEnterCompleted", value: function () {
                var e = this;
                "complete" === document.readyState ? this.init() : cn(window, "a", "load", function () {
                    return e.init()
                }), hr.initial || (hr.initial = !0), ln(".rocket__path") && (ln(".rocket__path").style.opacity = 0, ln(".rocket__path").style.pointerEvents = "none")
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                this.scroll.destroy(), o.isDesktop && (this.progressScroll.destroy(), ul.getAll().forEach(function (e) {
                    return e.kill()
                }))
            }
        }, {
            key: "updateScrollBounds", value: function () {
                this.scroll && this.scroll.update()
            }
        }, {
            key: "stopScroll", value: function () {
                this.scroll && this.scroll.stop()
            }
        }, {
            key: "startScroll", value: function () {
                this.scroll && this.scroll.start()
            }
        }, {
            key: "onPageChange", value: function () {
                var e = this;
                setTimeout(function () {
                    e.updateScrollBounds()
                }, 2e3)
            }
        }, {
            key: "onHideVideo", value: function () {
                var e = this;
                ln(".showreel") && window.addEventListener("wheel", function (t) {
                    e.startScroll(), (t.deltaY > 2.5 || t.deltaY < 0) && (an.to(".showreel", {
                        autoAlpha: 0,
                        pointerEvents: "none",
                        duration: 1.5,
                        ease: "power3"
                    }), setTimeout(function () {
                        ln(".video__showreel").pause(), ln(".video__showreel").currentTime = 0
                    }, 500), setTimeout(function () {
                        un(".js-home-enter").forEach(function (e) {
                            e.classList.add("is-inview")
                        })
                    }, 1e3))
                })
            }
        }, {
            key: "eventsFullVideo", value: function () {
                !1 === o.flags.showVideo && ln(".js-full-video") && ln(".js-full-video").addEventListener("click", this.openFullVideoScreen), ln(".js-scroll-scape") && ln(".js-scroll-scape").addEventListener("click", this.closeFullVideoScreen), ln(".js-sound-wave") && ln(".js-sound-wave").addEventListener("click", function () {
                    ln(".wave__music").classList.contains("sound-active") ? (ln(".wave__music").classList.remove("sound-active"), ln(".isSound").innerHTML = "on") : (ln(".wave__music").classList.add("sound-active"), ln(".isSound").innerHTML = "off"), ln(".video__showreel").muted = !ln(".video__showreel").muted
                })
            }
        }, {
            key: "sendNewsletter", value: function () {
                ln("#mNewsletter") && ln("#mNewsletter").addEventListener("submit", function (e) {
                    e.preventDefault();
                    var t = ln("#mNewsletter"), i = t.action, n = {name: t.name.value, email: t.email.value},
                        r = new FormData;
                    for (var s in n) r.append(s, n[s]);
                    ts.a.post(i, r).then(function (e) {
                        200 === e.status && (o.flags.sendingSubscribe = !0, an.to(un(".js-m-news-title"), {
                            yPercent: 100,
                            duration: 1.5,
                            stagger: .075,
                            ease: "expo.out",
                            onComplete: function () {
                                an.to(un(".js-m-news-thanks"), {
                                    yPercent: 0,
                                    duration: 1.5,
                                    stagger: .075,
                                    ease: "expo.out"
                                })
                            }
                        }), an.to(ln("#mNewsletter"), {
                            autoAlpha: 0,
                            duration: 1.5,
                            pointerEvents: "none",
                            ease: "expo.out"
                        }), setTimeout(function () {
                            t.reset()
                        }, 1e3))
                    })
                })
            }
        }, {
            key: "initContactToggle", value: function () {
                this.contactToggle = new Zr
            }
        }, {
            key: "formProject", value: function () {
                ln("#projectForm") && ln("#projectForm").addEventListener("submit", function (e) {
                    e.preventDefault();
                    var t = ln("#projectForm"), i = t.action, n = t.dataset.url, r = {
                        type: "projects",
                        name: t.name.value,
                        email: t.email.value,
                        phone: t.phone.value,
                        company: t.company.value,
                        message: t.message.value,
                        confirm: ln(".check_news").value
                    }, s = new FormData;
                    for (var o in r) s.append(o, r[o]);
                    Gc.redirect(n), ts.a.post(i, s).then(function (e) {
                        200 === e.status && setTimeout(function () {
                            t.reset()
                        }, 1e3)
                    })
                })
            }
        }, {
            key: "formCarrers", value: function () {
                ln("#joinForm") && ln("#joinForm").addEventListener("submit", function (e) {
                    e.preventDefault();
                    var t = ln("#joinForm"), i = t.action, n = t.dataset.url, r = {
                        type: "careers",
                        name: t.name.value,
                        email: t.email.value,
                        phone: t.phone.value,
                        link: t.link.value,
                        message: t.message.value
                    }, s = new FormData;
                    for (var o in r) s.append(o, r[o]);
                    Gc.redirect(n), ts.a.post(i, s).then(function (e) {
                        200 === e.status && setTimeout(function () {
                            t.reset()
                        }, 1e3)
                    })
                })
            }
        }]) && hl(t.prototype, i), r
    }(), yl = i(4), bl = i.n(yl);

    function wl(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Dl(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    var Tl = function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), Dl(this, "setInitial", function () {
                var e = t.ui, i = e.closeBtn;
                e.videoWindow, an.set(i, {autoAlpha: 0, xPercent: 50})
            }), Dl(this, "open", function () {
                document.body.classList.add("video-is-open"), t.ui.videoToggle && t.ui.videoToggle.forEach(function (e) {
                    e.addEventListener("click", function () {
                        o.flags.workModal = !0;
                        var i = e.getAttribute("data-title"), n = e.getAttribute("data-video-id");
                        console.log(i, n), t.addVideoModal(i, n), t.tlModal && t.tlModal.kill(), t.tlModal = an.timeline(), t.tlModal.to(t.ui.modal, {
                            autoAlpha: 1,
                            pointerEvents: "auto",
                            duration: 1,
                            ease: "expo.out",
                            onComplete: function () {
                                t.tlModal.to(".js-inner-video", {
                                    autoAlpha: 1,
                                    y: 0,
                                    duration: 1.5,
                                    ease: "expo.out"
                                }), t.tlModal.to(t.ui.closeBtn, {
                                    autoAlpha: 1,
                                    xPercent: 0,
                                    duration: 1.2,
                                    ease: "expo.out"
                                }, 1.75), t.tlModal.to(".js-video-title", {
                                    autoAlpha: 1,
                                    y: 0,
                                    duration: 1.2,
                                    ease: "expo.out"
                                }, 1.95)
                            }
                        })
                    })
                })
            }), Dl(this, "close", function () {
                document.body.classList.remove("video-is-open"), t.ui.closeBtn && t.ui.closeBtn.addEventListener("click", function () {
                    o.flags.workModal = !1, (t.videoElement.active || t.videoElement.exist) && (t.player.pause(), t.tlCloseModal && t.tlCloseModal.kill(), t.tlCloseModal = an.timeline(), t.tlCloseModal.to(".js-video-title", {
                        autoAlpha: 0,
                        y: 100,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .15).to(".js-inner-video", {
                        autoAlpha: 0,
                        y: 50,
                        duration: 1.5,
                        ease: "expo.out"
                    }, .25).to(t.ui.closeBtn, {
                        autoAlpha: 0,
                        xPercent: 100,
                        duration: 1.2,
                        ease: "expo.out"
                    }, .35).to(t.ui.modal, {
                        autoAlpha: 0,
                        pointerEvents: "none",
                        duration: 1,
                        ease: "expo.out"
                    }, .45), setTimeout(function () {
                        t.videoElement.el.remove()
                    }, 500))
                })
            }), this.ui = {
                videoToggle: un(".js-toggle-video"),
                modal: ln(".js-modal-video"),
                closeBtn: ln(".js-modal-close"),
                videoWindow: ln(".js-inner-video")
            }, this.player = null, this.videoElement = {el: null, exist: null, active: !1}, this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "init", value: function () {
                this.setInitial(), this.open(), this.close()
            }
        }, {
            key: "addVideoModal", value: function (e, t) {
                var i = this;
                this.videoElement.el = document.createElement("div"), this.videoElement.el.classList.add("video-vimeo"), this.videoElement.el.innerHTML = '\n      <div class="video__inner__title">\n        <h2 class="video__title">\n          <div class="stroke-o-2 oh">\n            <span class="dib | js-video-title">'.concat(e, '</span>\n          </div>\n        </h2>\n      </div>\n      <div class="video__inner__vid | js-inner-video">\n        <div id="work_player" data-plyr-provider="vimeo" data-plyr-embed-id="').concat(t, '"></div>\n      </div>\n    '), this.videoElement && ln(".video__info").appendChild(this.videoElement.el), this.videoElement.exist = !0, this.videoElement.active = !0, this.player = new bl.a("#work_player", {
                    fullscreen: {
                        enabled: !0,
                        fallback: !0,
                        iosNative: !0,
                        container: null
                    }
                }), setTimeout(function () {
                    i.player.play()
                }, 1e3)
            }
        }]) && wl(t.prototype, i), e
    }();

    function Cl(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function kl(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    var El = function () {
        function e() {
            var t = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), kl(this, "run", function () {
                var e = t.mouse, i = e.x, n = e.y, r = e.current, s = e.ease, o = t.bounds,
                    a = i - (o.width / 2 - o.left);
                r.x += (a - r.x) * s, r.y += (n - r.y) * s, t.ui.moon.style.transform = "translate3d(".concat(.0025 * r.x, "px, ").concat(.025 * r.y, "px, 0)"), an.ticker.add(t.run)
            }), kl(this, "onMove", function (e) {
                var i = t.getPos(e), n = i.x, r = i.y;
                t.mouse.x = n, t.mouse.y = r
            }), this.ui = {moon: ln(".js-moon")}, this.mouse = {
                x: 0,
                y: 0,
                current: {x: 0, y: 0},
                ease: .5
            }, this.ui.moon.style.willChange = "transform", this.bounds = this.ui.moon.getBoundingClientRect(), this.init()
        }

        var t, i;
        return t = e, (i = [{
            key: "init", value: function () {
                this.addEvents(), this.run()
            }
        }, {
            key: "getPos", value: function (e) {
                return {x: e.x, y: e.y}
            }
        }, {
            key: "addEvents", value: function () {
                window.addEventListener("mousemove", this.onMove)
            }
        }]) && Cl(t.prototype, i), e
    }();

    function _l(e) {
        return null !== e && "object" == typeof e && "constructor" in e && e.constructor === Object
    }

    function xl(e, t) {
        void 0 === e && (e = {}), void 0 === t && (t = {}), Object.keys(t).forEach(function (i) {
            void 0 === e[i] ? e[i] = t[i] : _l(t[i]) && _l(e[i]) && Object.keys(t[i]).length > 0 && xl(e[i], t[i])
        })
    }

    var Sl = {
        body: {}, addEventListener: function () {
        }, removeEventListener: function () {
        }, activeElement: {
            blur: function () {
            }, nodeName: ""
        }, querySelector: function () {
            return null
        }, querySelectorAll: function () {
            return []
        }, getElementById: function () {
            return null
        }, createEvent: function () {
            return {
                initEvent: function () {
                }
            }
        }, createElement: function () {
            return {
                children: [], childNodes: [], style: {}, setAttribute: function () {
                }, getElementsByTagName: function () {
                    return []
                }
            }
        }, createElementNS: function () {
            return {}
        }, importNode: function () {
            return null
        }, location: {hash: "", host: "", hostname: "", href: "", origin: "", pathname: "", protocol: "", search: ""}
    };

    function Pl() {
        var e = "undefined" != typeof document ? document : {};
        return xl(e, Sl), e
    }

    var Ol = {
        document: Sl,
        navigator: {userAgent: ""},
        location: {hash: "", host: "", hostname: "", href: "", origin: "", pathname: "", protocol: "", search: ""},
        history: {
            replaceState: function () {
            }, pushState: function () {
            }, go: function () {
            }, back: function () {
            }
        },
        CustomEvent: function () {
            return this
        },
        addEventListener: function () {
        },
        removeEventListener: function () {
        },
        getComputedStyle: function () {
            return {
                getPropertyValue: function () {
                    return ""
                }
            }
        },
        Image: function () {
        },
        Date: function () {
        },
        screen: {},
        setTimeout: function () {
        },
        clearTimeout: function () {
        },
        matchMedia: function () {
            return {}
        },
        requestAnimationFrame: function (e) {
            return "undefined" == typeof setTimeout ? (e(), null) : setTimeout(e, 0)
        },
        cancelAnimationFrame: function (e) {
            "undefined" != typeof setTimeout && clearTimeout(e)
        }
    };

    function Al() {
        var e = "undefined" != typeof window ? window : {};
        return xl(e, Ol), e
    }

    function Ml(e) {
        return (Ml = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    function Ll(e, t) {
        return (Ll = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Fl(e, t, i) {
        return (Fl = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }() ? Reflect.construct : function (e, t, i) {
            var n = [null];
            n.push.apply(n, t);
            var r = new (Function.bind.apply(e, n));
            return i && Ll(r, i.prototype), r
        }).apply(null, arguments)
    }

    function jl(e) {
        var t = "function" == typeof Map ? new Map : void 0;
        return (jl = function (e) {
            if (null === e || (i = e, -1 === Function.toString.call(i).indexOf("[native code]"))) return e;
            var i;
            if ("function" != typeof e) throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== t) {
                if (t.has(e)) return t.get(e);
                t.set(e, n)
            }

            function n() {
                return Fl(e, arguments, Ml(this).constructor)
            }

            return n.prototype = Object.create(e.prototype, {
                constructor: {
                    value: n,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), Ll(n, e)
        })(e)
    }

    var Bl = function (e) {
        var t, i;

        function n(t) {
            var i, n, r;
            return n = function (e) {
                if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return e
            }(i = e.call.apply(e, [this].concat(t)) || this), r = n.__proto__, Object.defineProperty(n, "__proto__", {
                get: function () {
                    return r
                }, set: function (e) {
                    r.__proto__ = e
                }
            }), i
        }

        return i = e, (t = n).prototype = Object.create(i.prototype), t.prototype.constructor = t, t.__proto__ = i, n
    }(jl(Array));

    function Il(e) {
        void 0 === e && (e = []);
        var t = [];
        return e.forEach(function (e) {
            Array.isArray(e) ? t.push.apply(t, Il(e)) : t.push(e)
        }), t
    }

    function Rl(e, t) {
        return Array.prototype.filter.call(e, t)
    }

    function Nl(e, t) {
        var i = Al(), n = Pl(), r = [];
        if (!t && e instanceof Bl) return e;
        if (!e) return new Bl(r);
        if ("string" == typeof e) {
            var s = e.trim();
            if (s.indexOf("<") >= 0 && s.indexOf(">") >= 0) {
                var o = "div";
                0 === s.indexOf("<li") && (o = "ul"), 0 === s.indexOf("<tr") && (o = "tbody"), 0 !== s.indexOf("<td") && 0 !== s.indexOf("<th") || (o = "tr"), 0 === s.indexOf("<tbody") && (o = "table"), 0 === s.indexOf("<option") && (o = "select");
                var a = n.createElement(o);
                a.innerHTML = s;
                for (var l = 0; l < a.childNodes.length; l += 1) r.push(a.childNodes[l])
            } else r = function (e, t) {
                if ("string" != typeof e) return [e];
                for (var i = [], n = t.querySelectorAll(e), r = 0; r < n.length; r += 1) i.push(n[r]);
                return i
            }(e.trim(), t || n)
        } else if (e.nodeType || e === i || e === n) r.push(e); else if (Array.isArray(e)) {
            if (e instanceof Bl) return e;
            r = e
        }
        return new Bl(function (e) {
            for (var t = [], i = 0; i < e.length; i += 1) -1 === t.indexOf(e[i]) && t.push(e[i]);
            return t
        }(r))
    }

    Nl.fn = Bl.prototype;
    var zl = "resize scroll".split(" ");

    function Hl(e) {
        return function () {
            for (var t = arguments.length, i = new Array(t), n = 0; n < t; n++) i[n] = arguments[n];
            if (void 0 === i[0]) {
                for (var r = 0; r < this.length; r += 1) zl.indexOf(e) < 0 && (e in this[r] ? this[r][e]() : Nl(this[r]).trigger(e));
                return this
            }
            return this.on.apply(this, [e].concat(i))
        }
    }

    Hl("click"), Hl("blur"), Hl("focus"), Hl("focusin"), Hl("focusout"), Hl("keyup"), Hl("keydown"), Hl("keypress"), Hl("submit"), Hl("change"), Hl("mousedown"), Hl("mousemove"), Hl("mouseup"), Hl("mouseenter"), Hl("mouseleave"), Hl("mouseout"), Hl("mouseover"), Hl("touchstart"), Hl("touchend"), Hl("touchmove"), Hl("resize"), Hl("scroll");
    var Vl = {
        addClass: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = Il(t.map(function (e) {
                return e.split(" ")
            }));
            return this.forEach(function (e) {
                var t;
                (t = e.classList).add.apply(t, n)
            }), this
        }, removeClass: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = Il(t.map(function (e) {
                return e.split(" ")
            }));
            return this.forEach(function (e) {
                var t;
                (t = e.classList).remove.apply(t, n)
            }), this
        }, hasClass: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = Il(t.map(function (e) {
                return e.split(" ")
            }));
            return Rl(this, function (e) {
                return n.filter(function (t) {
                    return e.classList.contains(t)
                }).length > 0
            }).length > 0
        }, toggleClass: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = Il(t.map(function (e) {
                return e.split(" ")
            }));
            this.forEach(function (e) {
                n.forEach(function (t) {
                    e.classList.toggle(t)
                })
            })
        }, attr: function (e, t) {
            if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
            for (var i = 0; i < this.length; i += 1) if (2 === arguments.length) this[i].setAttribute(e, t); else for (var n in e) this[i][n] = e[n], this[i].setAttribute(n, e[n]);
            return this
        }, removeAttr: function (e) {
            for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e);
            return this
        }, transform: function (e) {
            for (var t = 0; t < this.length; t += 1) this[t].style.transform = e;
            return this
        }, transition: function (e) {
            for (var t = 0; t < this.length; t += 1) this[t].style.transition = "string" != typeof e ? e + "ms" : e;
            return this
        }, on: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = t[0], r = t[1], s = t[2], o = t[3];

            function a(e) {
                var t = e.target;
                if (t) {
                    var i = e.target.dom7EventData || [];
                    if (i.indexOf(e) < 0 && i.unshift(e), Nl(t).is(r)) s.apply(t, i); else for (var n = Nl(t).parents(), o = 0; o < n.length; o += 1) Nl(n[o]).is(r) && s.apply(n[o], i)
                }
            }

            function l(e) {
                var t = e && e.target && e.target.dom7EventData || [];
                t.indexOf(e) < 0 && t.unshift(e), s.apply(this, t)
            }

            "function" == typeof t[1] && (n = t[0], s = t[1], o = t[2], r = void 0), o || (o = !1);
            for (var u, c = n.split(" "), h = 0; h < this.length; h += 1) {
                var d = this[h];
                if (r) for (u = 0; u < c.length; u += 1) {
                    var p = c[u];
                    d.dom7LiveListeners || (d.dom7LiveListeners = {}), d.dom7LiveListeners[p] || (d.dom7LiveListeners[p] = []), d.dom7LiveListeners[p].push({
                        listener: s,
                        proxyListener: a
                    }), d.addEventListener(p, a, o)
                } else for (u = 0; u < c.length; u += 1) {
                    var f = c[u];
                    d.dom7Listeners || (d.dom7Listeners = {}), d.dom7Listeners[f] || (d.dom7Listeners[f] = []), d.dom7Listeners[f].push({
                        listener: s,
                        proxyListener: l
                    }), d.addEventListener(f, l, o)
                }
            }
            return this
        }, off: function () {
            for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
            var n = t[0], r = t[1], s = t[2], o = t[3];
            "function" == typeof t[1] && (n = t[0], s = t[1], o = t[2], r = void 0), o || (o = !1);
            for (var a = n.split(" "), l = 0; l < a.length; l += 1) for (var u = a[l], c = 0; c < this.length; c += 1) {
                var h = this[c], d = void 0;
                if (!r && h.dom7Listeners ? d = h.dom7Listeners[u] : r && h.dom7LiveListeners && (d = h.dom7LiveListeners[u]), d && d.length) for (var p = d.length - 1; p >= 0; p -= 1) {
                    var f = d[p];
                    s && f.listener === s || s && f.listener && f.listener.dom7proxy && f.listener.dom7proxy === s ? (h.removeEventListener(u, f.proxyListener, o), d.splice(p, 1)) : s || (h.removeEventListener(u, f.proxyListener, o), d.splice(p, 1))
                }
            }
            return this
        }, trigger: function () {
            for (var e = Al(), t = arguments.length, i = new Array(t), n = 0; n < t; n++) i[n] = arguments[n];
            for (var r = i[0].split(" "), s = i[1], o = 0; o < r.length; o += 1) for (var a = r[o], l = 0; l < this.length; l += 1) {
                var u = this[l];
                if (e.CustomEvent) {
                    var c = new e.CustomEvent(a, {detail: s, bubbles: !0, cancelable: !0});
                    u.dom7EventData = i.filter(function (e, t) {
                        return t > 0
                    }), u.dispatchEvent(c), u.dom7EventData = [], delete u.dom7EventData
                }
            }
            return this
        }, transitionEnd: function (e) {
            var t = this;
            return e && t.on("transitionend", function i(n) {
                n.target === this && (e.call(this, n), t.off("transitionend", i))
            }), this
        }, outerWidth: function (e) {
            if (this.length > 0) {
                if (e) {
                    var t = this.styles();
                    return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                }
                return this[0].offsetWidth
            }
            return null
        }, outerHeight: function (e) {
            if (this.length > 0) {
                if (e) {
                    var t = this.styles();
                    return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                }
                return this[0].offsetHeight
            }
            return null
        }, styles: function () {
            var e = Al();
            return this[0] ? e.getComputedStyle(this[0], null) : {}
        }, offset: function () {
            if (this.length > 0) {
                var e = Al(), t = Pl(), i = this[0], n = i.getBoundingClientRect(), r = t.body,
                    s = i.clientTop || r.clientTop || 0, o = i.clientLeft || r.clientLeft || 0,
                    a = i === e ? e.scrollY : i.scrollTop, l = i === e ? e.scrollX : i.scrollLeft;
                return {top: n.top + a - s, left: n.left + l - o}
            }
            return null
        }, css: function (e, t) {
            var i, n = Al();
            if (1 === arguments.length) {
                if ("string" != typeof e) {
                    for (i = 0; i < this.length; i += 1) for (var r in e) this[i].style[r] = e[r];
                    return this
                }
                if (this[0]) return n.getComputedStyle(this[0], null).getPropertyValue(e)
            }
            if (2 === arguments.length && "string" == typeof e) {
                for (i = 0; i < this.length; i += 1) this[i].style[e] = t;
                return this
            }
            return this
        }, each: function (e) {
            return e ? (this.forEach(function (t, i) {
                e.apply(t, [t, i])
            }), this) : this
        }, html: function (e) {
            if (void 0 === e) return this[0] ? this[0].innerHTML : null;
            for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e;
            return this
        }, text: function (e) {
            if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;
            for (var t = 0; t < this.length; t += 1) this[t].textContent = e;
            return this
        }, is: function (e) {
            var t, i, n = Al(), r = Pl(), s = this[0];
            if (!s || void 0 === e) return !1;
            if ("string" == typeof e) {
                if (s.matches) return s.matches(e);
                if (s.webkitMatchesSelector) return s.webkitMatchesSelector(e);
                if (s.msMatchesSelector) return s.msMatchesSelector(e);
                for (t = Nl(e), i = 0; i < t.length; i += 1) if (t[i] === s) return !0;
                return !1
            }
            if (e === r) return s === r;
            if (e === n) return s === n;
            if (e.nodeType || e instanceof Bl) {
                for (t = e.nodeType ? [e] : e, i = 0; i < t.length; i += 1) if (t[i] === s) return !0;
                return !1
            }
            return !1
        }, index: function () {
            var e, t = this[0];
            if (t) {
                for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1);
                return e
            }
        }, eq: function (e) {
            if (void 0 === e) return this;
            var t = this.length;
            if (e > t - 1) return Nl([]);
            if (e < 0) {
                var i = t + e;
                return Nl(i < 0 ? [] : [this[i]])
            }
            return Nl([this[e]])
        }, append: function () {
            for (var e, t = Pl(), i = 0; i < arguments.length; i += 1) {
                e = i < 0 || arguments.length <= i ? void 0 : arguments[i];
                for (var n = 0; n < this.length; n += 1) if ("string" == typeof e) {
                    var r = t.createElement("div");
                    for (r.innerHTML = e; r.firstChild;) this[n].appendChild(r.firstChild)
                } else if (e instanceof Bl) for (var s = 0; s < e.length; s += 1) this[n].appendChild(e[s]); else this[n].appendChild(e)
            }
            return this
        }, prepend: function (e) {
            var t, i, n = Pl();
            for (t = 0; t < this.length; t += 1) if ("string" == typeof e) {
                var r = n.createElement("div");
                for (r.innerHTML = e, i = r.childNodes.length - 1; i >= 0; i -= 1) this[t].insertBefore(r.childNodes[i], this[t].childNodes[0])
            } else if (e instanceof Bl) for (i = 0; i < e.length; i += 1) this[t].insertBefore(e[i], this[t].childNodes[0]); else this[t].insertBefore(e, this[t].childNodes[0]);
            return this
        }, next: function (e) {
            return this.length > 0 ? e ? this[0].nextElementSibling && Nl(this[0].nextElementSibling).is(e) ? Nl([this[0].nextElementSibling]) : Nl([]) : this[0].nextElementSibling ? Nl([this[0].nextElementSibling]) : Nl([]) : Nl([])
        }, nextAll: function (e) {
            var t = [], i = this[0];
            if (!i) return Nl([]);
            for (; i.nextElementSibling;) {
                var n = i.nextElementSibling;
                e ? Nl(n).is(e) && t.push(n) : t.push(n), i = n
            }
            return Nl(t)
        }, prev: function (e) {
            if (this.length > 0) {
                var t = this[0];
                return e ? t.previousElementSibling && Nl(t.previousElementSibling).is(e) ? Nl([t.previousElementSibling]) : Nl([]) : t.previousElementSibling ? Nl([t.previousElementSibling]) : Nl([])
            }
            return Nl([])
        }, prevAll: function (e) {
            var t = [], i = this[0];
            if (!i) return Nl([]);
            for (; i.previousElementSibling;) {
                var n = i.previousElementSibling;
                e ? Nl(n).is(e) && t.push(n) : t.push(n), i = n
            }
            return Nl(t)
        }, parent: function (e) {
            for (var t = [], i = 0; i < this.length; i += 1) null !== this[i].parentNode && (e ? Nl(this[i].parentNode).is(e) && t.push(this[i].parentNode) : t.push(this[i].parentNode));
            return Nl(t)
        }, parents: function (e) {
            for (var t = [], i = 0; i < this.length; i += 1) for (var n = this[i].parentNode; n;) e ? Nl(n).is(e) && t.push(n) : t.push(n), n = n.parentNode;
            return Nl(t)
        }, closest: function (e) {
            var t = this;
            return void 0 === e ? Nl([]) : (t.is(e) || (t = t.parents(e).eq(0)), t)
        }, find: function (e) {
            for (var t = [], i = 0; i < this.length; i += 1) for (var n = this[i].querySelectorAll(e), r = 0; r < n.length; r += 1) t.push(n[r]);
            return Nl(t)
        }, children: function (e) {
            for (var t = [], i = 0; i < this.length; i += 1) for (var n = this[i].children, r = 0; r < n.length; r += 1) e && !Nl(n[r]).is(e) || t.push(n[r]);
            return Nl(t)
        }, filter: function (e) {
            return Nl(Rl(this, e))
        }, remove: function () {
            for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]);
            return this
        }
    };
    Object.keys(Vl).forEach(function (e) {
        Nl.fn[e] = Vl[e]
    });
    var ql, Wl, Xl, Yl = Nl;

    function Ul(e, t) {
        return void 0 === t && (t = 0), setTimeout(e, t)
    }

    function Gl() {
        return Date.now()
    }

    function $l(e) {
        return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
    }

    function Kl() {
        for (var e = Object(arguments.length <= 0 ? void 0 : arguments[0]), t = 1; t < arguments.length; t += 1) {
            var i = t < 0 || arguments.length <= t ? void 0 : arguments[t];
            if (null != i) for (var n = Object.keys(Object(i)), r = 0, s = n.length; r < s; r += 1) {
                var o = n[r], a = Object.getOwnPropertyDescriptor(i, o);
                void 0 !== a && a.enumerable && ($l(e[o]) && $l(i[o]) ? Kl(e[o], i[o]) : !$l(e[o]) && $l(i[o]) ? (e[o] = {}, Kl(e[o], i[o])) : e[o] = i[o])
            }
        }
        return e
    }

    function Jl(e, t) {
        Object.keys(t).forEach(function (i) {
            $l(t[i]) && Object.keys(t[i]).forEach(function (n) {
                "function" == typeof t[i][n] && (t[i][n] = t[i][n].bind(e))
            }), e[i] = t[i]
        })
    }

    function Ql() {
        return ql || (e = Al(), t = Pl(), ql = {
            touch: !!("ontouchstart" in e || e.DocumentTouch && t instanceof e.DocumentTouch),
            pointerEvents: !!e.PointerEvent && "maxTouchPoints" in e.navigator && e.navigator.maxTouchPoints >= 0,
            observer: "MutationObserver" in e || "WebkitMutationObserver" in e,
            passiveListener: function () {
                var t = !1;
                try {
                    var i = Object.defineProperty({}, "passive", {
                        get: function () {
                            t = !0
                        }
                    });
                    e.addEventListener("testPassiveListener", null, i)
                } catch (e) {
                }
                return t
            }(),
            gestures: "ongesturestart" in e
        }), ql;
        var e, t
    }

    function Zl(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function eu() {
        return (eu = Object.assign || function (e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
            }
            return e
        }).apply(this, arguments)
    }

    function tu() {
        var e = this.params, t = this.el;
        if (!t || 0 !== t.offsetWidth) {
            e.breakpoints && this.setBreakpoint();
            var i = this.allowSlideNext, n = this.allowSlidePrev, r = this.snapGrid;
            this.allowSlideNext = !0, this.allowSlidePrev = !0, this.updateSize(), this.updateSlides(), this.updateSlidesClasses(), ("auto" === e.slidesPerView || e.slidesPerView > 1) && this.isEnd && !this.isBeginning && !this.params.centeredSlides ? this.slideTo(this.slides.length - 1, 0, !1, !0) : this.slideTo(this.activeIndex, 0, !1, !0), this.autoplay && this.autoplay.running && this.autoplay.paused && this.autoplay.run(), this.allowSlidePrev = n, this.allowSlideNext = i, this.params.watchOverflow && r !== this.snapGrid && this.checkOverflow()
        }
    }

    var iu = !1;

    function nu() {
    }

    var ru = {
        init: !0,
        direction: "horizontal",
        touchEventsTarget: "container",
        initialSlide: 0,
        speed: 300,
        cssMode: !1,
        updateOnWindowResize: !0,
        width: null,
        height: null,
        preventInteractionOnTransition: !1,
        userAgent: null,
        url: null,
        edgeSwipeDetection: !1,
        edgeSwipeThreshold: 20,
        freeMode: !1,
        freeModeMomentum: !0,
        freeModeMomentumRatio: 1,
        freeModeMomentumBounce: !0,
        freeModeMomentumBounceRatio: 1,
        freeModeMomentumVelocityRatio: 1,
        freeModeSticky: !1,
        freeModeMinimumVelocity: .02,
        autoHeight: !1,
        setWrapperSize: !1,
        virtualTranslate: !1,
        effect: "slide",
        breakpoints: void 0,
        spaceBetween: 0,
        slidesPerView: 1,
        slidesPerColumn: 1,
        slidesPerColumnFill: "column",
        slidesPerGroup: 1,
        slidesPerGroupSkip: 0,
        centeredSlides: !1,
        centeredSlidesBounds: !1,
        slidesOffsetBefore: 0,
        slidesOffsetAfter: 0,
        normalizeSlideIndex: !0,
        centerInsufficientSlides: !1,
        watchOverflow: !1,
        roundLengths: !1,
        touchRatio: 1,
        touchAngle: 45,
        simulateTouch: !0,
        shortSwipes: !0,
        longSwipes: !0,
        longSwipesRatio: .5,
        longSwipesMs: 300,
        followFinger: !0,
        allowTouchMove: !0,
        threshold: 0,
        touchMoveStopPropagation: !1,
        touchStartPreventDefault: !0,
        touchStartForcePreventDefault: !1,
        touchReleaseOnEdges: !1,
        uniqueNavElements: !0,
        resistance: !0,
        resistanceRatio: .85,
        watchSlidesProgress: !1,
        watchSlidesVisibility: !1,
        grabCursor: !1,
        preventClicks: !0,
        preventClicksPropagation: !0,
        slideToClickedSlide: !1,
        preloadImages: !0,
        updateOnImagesReady: !0,
        loop: !1,
        loopAdditionalSlides: 0,
        loopedSlides: null,
        loopFillGroupWithBlank: !1,
        loopPreventsSlide: !0,
        allowSlidePrev: !0,
        allowSlideNext: !0,
        swipeHandler: null,
        noSwiping: !0,
        noSwipingClass: "swiper-no-swiping",
        noSwipingSelector: null,
        passiveListeners: !0,
        containerModifierClass: "swiper-container-",
        slideClass: "swiper-slide",
        slideBlankClass: "swiper-slide-invisible-blank",
        slideActiveClass: "swiper-slide-active",
        slideDuplicateActiveClass: "swiper-slide-duplicate-active",
        slideVisibleClass: "swiper-slide-visible",
        slideDuplicateClass: "swiper-slide-duplicate",
        slideNextClass: "swiper-slide-next",
        slideDuplicateNextClass: "swiper-slide-duplicate-next",
        slidePrevClass: "swiper-slide-prev",
        slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
        wrapperClass: "swiper-wrapper",
        runCallbacksOnInit: !0,
        _emitClasses: !1
    }, su = {
        modular: {
            useParams: function (e) {
                var t = this;
                t.modules && Object.keys(t.modules).forEach(function (i) {
                    var n = t.modules[i];
                    n.params && Kl(e, n.params)
                })
            }, useModules: function (e) {
                void 0 === e && (e = {});
                var t = this;
                t.modules && Object.keys(t.modules).forEach(function (i) {
                    var n = t.modules[i], r = e[i] || {};
                    n.on && t.on && Object.keys(n.on).forEach(function (e) {
                        t.on(e, n.on[e])
                    }), n.create && n.create.bind(t)(r)
                })
            }
        }, eventsEmitter: {
            on: function (e, t, i) {
                var n = this;
                if ("function" != typeof t) return n;
                var r = i ? "unshift" : "push";
                return e.split(" ").forEach(function (e) {
                    n.eventsListeners[e] || (n.eventsListeners[e] = []), n.eventsListeners[e][r](t)
                }), n
            }, once: function (e, t, i) {
                var n = this;
                if ("function" != typeof t) return n;

                function r() {
                    n.off(e, r), r.__emitterProxy && delete r.__emitterProxy;
                    for (var i = arguments.length, s = new Array(i), o = 0; o < i; o++) s[o] = arguments[o];
                    t.apply(n, s)
                }

                return r.__emitterProxy = t, n.on(e, r, i)
            }, onAny: function (e, t) {
                if ("function" != typeof e) return this;
                var i = t ? "unshift" : "push";
                return this.eventsAnyListeners.indexOf(e) < 0 && this.eventsAnyListeners[i](e), this
            }, offAny: function (e) {
                if (!this.eventsAnyListeners) return this;
                var t = this.eventsAnyListeners.indexOf(e);
                return t >= 0 && this.eventsAnyListeners.splice(t, 1), this
            }, off: function (e, t) {
                var i = this;
                return i.eventsListeners ? (e.split(" ").forEach(function (e) {
                    void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].forEach(function (n, r) {
                        (n === t || n.__emitterProxy && n.__emitterProxy === t) && i.eventsListeners[e].splice(r, 1)
                    })
                }), i) : i
            }, emit: function () {
                var e, t, i, n = this;
                if (!n.eventsListeners) return n;
                for (var r = arguments.length, s = new Array(r), o = 0; o < r; o++) s[o] = arguments[o];
                return "string" == typeof s[0] || Array.isArray(s[0]) ? (e = s[0], t = s.slice(1, s.length), i = n) : (e = s[0].events, t = s[0].data, i = s[0].context || n), t.unshift(i), (Array.isArray(e) ? e : e.split(" ")).forEach(function (e) {
                    if (n.eventsListeners && n.eventsListeners[e]) {
                        var r = [];
                        n.eventsListeners[e].forEach(function (e) {
                            r.push(e)
                        }), r.forEach(function (e) {
                            e.apply(i, t)
                        })
                    }
                }), n
            }
        }, update: {
            updateSize: function () {
                var e, t, i = this.$el;
                e = void 0 !== this.params.width && null !== this.params.width ? this.params.width : i[0].clientWidth, t = void 0 !== this.params.height && null !== this.params.width ? this.params.height : i[0].clientHeight, 0 === e && this.isHorizontal() || 0 === t && this.isVertical() || (e = e - parseInt(i.css("padding-left") || 0, 10) - parseInt(i.css("padding-right") || 0, 10), t = t - parseInt(i.css("padding-top") || 0, 10) - parseInt(i.css("padding-bottom") || 0, 10), Number.isNaN(e) && (e = 0), Number.isNaN(t) && (t = 0), Kl(this, {
                    width: e,
                    height: t,
                    size: this.isHorizontal() ? e : t
                }))
            }, updateSlides: function () {
                var e = Al(), t = this.params, i = this.$wrapperEl, n = this.size, r = this.rtlTranslate,
                    s = this.wrongRTL, o = this.virtual && t.virtual.enabled,
                    a = o ? this.virtual.slides.length : this.slides.length,
                    l = i.children("." + this.params.slideClass), u = o ? this.virtual.slides.length : l.length, c = [],
                    h = [], d = [];

                function p(e, i) {
                    return !t.cssMode || i !== l.length - 1
                }

                var f = t.slidesOffsetBefore;
                "function" == typeof f && (f = t.slidesOffsetBefore.call(this));
                var m = t.slidesOffsetAfter;
                "function" == typeof m && (m = t.slidesOffsetAfter.call(this));
                var g = this.snapGrid.length, v = this.snapGrid.length, y = t.spaceBetween, b = -f, w = 0, D = 0;
                if (void 0 !== n) {
                    var T, C;
                    "string" == typeof y && y.indexOf("%") >= 0 && (y = parseFloat(y.replace("%", "")) / 100 * n), this.virtualSize = -y, r ? l.css({
                        marginLeft: "",
                        marginTop: ""
                    }) : l.css({
                        marginRight: "",
                        marginBottom: ""
                    }), t.slidesPerColumn > 1 && (T = Math.floor(u / t.slidesPerColumn) === u / this.params.slidesPerColumn ? u : Math.ceil(u / t.slidesPerColumn) * t.slidesPerColumn, "auto" !== t.slidesPerView && "row" === t.slidesPerColumnFill && (T = Math.max(T, t.slidesPerView * t.slidesPerColumn)));
                    for (var k, E = t.slidesPerColumn, _ = T / E, x = Math.floor(u / t.slidesPerColumn), S = 0; S < u; S += 1) {
                        C = 0;
                        var P = l.eq(S);
                        if (t.slidesPerColumn > 1) {
                            var O = void 0, A = void 0, M = void 0;
                            if ("row" === t.slidesPerColumnFill && t.slidesPerGroup > 1) {
                                var L = Math.floor(S / (t.slidesPerGroup * t.slidesPerColumn)),
                                    F = S - t.slidesPerColumn * t.slidesPerGroup * L,
                                    j = 0 === L ? t.slidesPerGroup : Math.min(Math.ceil((u - L * E * t.slidesPerGroup) / E), t.slidesPerGroup);
                                O = (A = F - (M = Math.floor(F / j)) * j + L * t.slidesPerGroup) + M * T / E, P.css({
                                    "-webkit-box-ordinal-group": O,
                                    "-moz-box-ordinal-group": O,
                                    "-ms-flex-order": O,
                                    "-webkit-order": O,
                                    order: O
                                })
                            } else "column" === t.slidesPerColumnFill ? (M = S - (A = Math.floor(S / E)) * E, (A > x || A === x && M === E - 1) && (M += 1) >= E && (M = 0, A += 1)) : A = S - (M = Math.floor(S / _)) * _;
                            P.css("margin-" + (this.isHorizontal() ? "top" : "left"), 0 !== M && t.spaceBetween && t.spaceBetween + "px")
                        }
                        if ("none" !== P.css("display")) {
                            if ("auto" === t.slidesPerView) {
                                var B = e.getComputedStyle(P[0], null), I = P[0].style.transform,
                                    R = P[0].style.webkitTransform;
                                if (I && (P[0].style.transform = "none"), R && (P[0].style.webkitTransform = "none"), t.roundLengths) C = this.isHorizontal() ? P.outerWidth(!0) : P.outerHeight(!0); else if (this.isHorizontal()) {
                                    var N = parseFloat(B.getPropertyValue("width") || 0),
                                        z = parseFloat(B.getPropertyValue("padding-left") || 0),
                                        H = parseFloat(B.getPropertyValue("padding-right") || 0),
                                        V = parseFloat(B.getPropertyValue("margin-left") || 0),
                                        q = parseFloat(B.getPropertyValue("margin-right") || 0),
                                        W = B.getPropertyValue("box-sizing");
                                    C = W && "border-box" === W ? N + V + q : N + z + H + V + q
                                } else {
                                    var X = parseFloat(B.getPropertyValue("height") || 0),
                                        Y = parseFloat(B.getPropertyValue("padding-top") || 0),
                                        U = parseFloat(B.getPropertyValue("padding-bottom") || 0),
                                        G = parseFloat(B.getPropertyValue("margin-top") || 0),
                                        $ = parseFloat(B.getPropertyValue("margin-bottom") || 0),
                                        K = B.getPropertyValue("box-sizing");
                                    C = K && "border-box" === K ? X + G + $ : X + Y + U + G + $
                                }
                                I && (P[0].style.transform = I), R && (P[0].style.webkitTransform = R), t.roundLengths && (C = Math.floor(C))
                            } else C = (n - (t.slidesPerView - 1) * y) / t.slidesPerView, t.roundLengths && (C = Math.floor(C)), l[S] && (this.isHorizontal() ? l[S].style.width = C + "px" : l[S].style.height = C + "px");
                            l[S] && (l[S].swiperSlideSize = C), d.push(C), t.centeredSlides ? (b = b + C / 2 + w / 2 + y, 0 === w && 0 !== S && (b = b - n / 2 - y), 0 === S && (b = b - n / 2 - y), Math.abs(b) < .001 && (b = 0), t.roundLengths && (b = Math.floor(b)), D % t.slidesPerGroup == 0 && c.push(b), h.push(b)) : (t.roundLengths && (b = Math.floor(b)), (D - Math.min(this.params.slidesPerGroupSkip, D)) % this.params.slidesPerGroup == 0 && c.push(b), h.push(b), b = b + C + y), this.virtualSize += C + y, w = C, D += 1
                        }
                    }
                    if (this.virtualSize = Math.max(this.virtualSize, n) + m, r && s && ("slide" === t.effect || "coverflow" === t.effect) && i.css({width: this.virtualSize + t.spaceBetween + "px"}), t.setWrapperSize && (this.isHorizontal() ? i.css({width: this.virtualSize + t.spaceBetween + "px"}) : i.css({height: this.virtualSize + t.spaceBetween + "px"})), t.slidesPerColumn > 1 && (this.virtualSize = (C + t.spaceBetween) * T, this.virtualSize = Math.ceil(this.virtualSize / t.slidesPerColumn) - t.spaceBetween, this.isHorizontal() ? i.css({width: this.virtualSize + t.spaceBetween + "px"}) : i.css({height: this.virtualSize + t.spaceBetween + "px"}), t.centeredSlides)) {
                        k = [];
                        for (var J = 0; J < c.length; J += 1) {
                            var Q = c[J];
                            t.roundLengths && (Q = Math.floor(Q)), c[J] < this.virtualSize + c[0] && k.push(Q)
                        }
                        c = k
                    }
                    if (!t.centeredSlides) {
                        k = [];
                        for (var Z = 0; Z < c.length; Z += 1) {
                            var ee = c[Z];
                            t.roundLengths && (ee = Math.floor(ee)), c[Z] <= this.virtualSize - n && k.push(ee)
                        }
                        c = k, Math.floor(this.virtualSize - n) - Math.floor(c[c.length - 1]) > 1 && c.push(this.virtualSize - n)
                    }
                    if (0 === c.length && (c = [0]), 0 !== t.spaceBetween && (this.isHorizontal() ? r ? l.filter(p).css({marginLeft: y + "px"}) : l.filter(p).css({marginRight: y + "px"}) : l.filter(p).css({marginBottom: y + "px"})), t.centeredSlides && t.centeredSlidesBounds) {
                        var te = 0;
                        d.forEach(function (e) {
                            te += e + (t.spaceBetween ? t.spaceBetween : 0)
                        });
                        var ie = (te -= t.spaceBetween) - n;
                        c = c.map(function (e) {
                            return e < 0 ? -f : e > ie ? ie + m : e
                        })
                    }
                    if (t.centerInsufficientSlides) {
                        var ne = 0;
                        if (d.forEach(function (e) {
                            ne += e + (t.spaceBetween ? t.spaceBetween : 0)
                        }), (ne -= t.spaceBetween) < n) {
                            var re = (n - ne) / 2;
                            c.forEach(function (e, t) {
                                c[t] = e - re
                            }), h.forEach(function (e, t) {
                                h[t] = e + re
                            })
                        }
                    }
                    Kl(this, {
                        slides: l,
                        snapGrid: c,
                        slidesGrid: h,
                        slidesSizesGrid: d
                    }), u !== a && this.emit("slidesLengthChange"), c.length !== g && (this.params.watchOverflow && this.checkOverflow(), this.emit("snapGridLengthChange")), h.length !== v && this.emit("slidesGridLengthChange"), (t.watchSlidesProgress || t.watchSlidesVisibility) && this.updateSlidesOffset()
                }
            }, updateAutoHeight: function (e) {
                var t, i = [], n = 0;
                if ("number" == typeof e ? this.setTransition(e) : !0 === e && this.setTransition(this.params.speed), "auto" !== this.params.slidesPerView && this.params.slidesPerView > 1) if (this.params.centeredSlides) this.visibleSlides.each(function (e) {
                    i.push(e)
                }); else for (t = 0; t < Math.ceil(this.params.slidesPerView); t += 1) {
                    var r = this.activeIndex + t;
                    if (r > this.slides.length) break;
                    i.push(this.slides.eq(r)[0])
                } else i.push(this.slides.eq(this.activeIndex)[0]);
                for (t = 0; t < i.length; t += 1) if (void 0 !== i[t]) {
                    var s = i[t].offsetHeight;
                    n = s > n ? s : n
                }
                n && this.$wrapperEl.css("height", n + "px")
            }, updateSlidesOffset: function () {
                for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
            }, updateSlidesProgress: function (e) {
                void 0 === e && (e = this && this.translate || 0);
                var t = this.params, i = this.slides, n = this.rtlTranslate;
                if (0 !== i.length) {
                    void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
                    var r = -e;
                    n && (r = e), i.removeClass(t.slideVisibleClass), this.visibleSlidesIndexes = [], this.visibleSlides = [];
                    for (var s = 0; s < i.length; s += 1) {
                        var o = i[s],
                            a = (r + (t.centeredSlides ? this.minTranslate() : 0) - o.swiperSlideOffset) / (o.swiperSlideSize + t.spaceBetween);
                        if (t.watchSlidesVisibility || t.centeredSlides && t.autoHeight) {
                            var l = -(r - o.swiperSlideOffset), u = l + this.slidesSizesGrid[s];
                            (l >= 0 && l < this.size - 1 || u > 1 && u <= this.size || l <= 0 && u >= this.size) && (this.visibleSlides.push(o), this.visibleSlidesIndexes.push(s), i.eq(s).addClass(t.slideVisibleClass))
                        }
                        o.progress = n ? -a : a
                    }
                    this.visibleSlides = Yl(this.visibleSlides)
                }
            }, updateProgress: function (e) {
                if (void 0 === e) {
                    var t = this.rtlTranslate ? -1 : 1;
                    e = this && this.translate && this.translate * t || 0
                }
                var i = this.params, n = this.maxTranslate() - this.minTranslate(), r = this.progress,
                    s = this.isBeginning, o = this.isEnd, a = s, l = o;
                0 === n ? (r = 0, s = !0, o = !0) : (s = (r = (e - this.minTranslate()) / n) <= 0, o = r >= 1), Kl(this, {
                    progress: r,
                    isBeginning: s,
                    isEnd: o
                }), (i.watchSlidesProgress || i.watchSlidesVisibility || i.centeredSlides && i.autoHeight) && this.updateSlidesProgress(e), s && !a && this.emit("reachBeginning toEdge"), o && !l && this.emit("reachEnd toEdge"), (a && !s || l && !o) && this.emit("fromEdge"), this.emit("progress", r)
            }, updateSlidesClasses: function () {
                var e, t = this.slides, i = this.params, n = this.$wrapperEl, r = this.activeIndex, s = this.realIndex,
                    o = this.virtual && i.virtual.enabled;
                t.removeClass(i.slideActiveClass + " " + i.slideNextClass + " " + i.slidePrevClass + " " + i.slideDuplicateActiveClass + " " + i.slideDuplicateNextClass + " " + i.slideDuplicatePrevClass), (e = o ? this.$wrapperEl.find("." + i.slideClass + '[data-swiper-slide-index="' + r + '"]') : t.eq(r)).addClass(i.slideActiveClass), i.loop && (e.hasClass(i.slideDuplicateClass) ? n.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + s + '"]').addClass(i.slideDuplicateActiveClass) : n.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + s + '"]').addClass(i.slideDuplicateActiveClass));
                var a = e.nextAll("." + i.slideClass).eq(0).addClass(i.slideNextClass);
                i.loop && 0 === a.length && (a = t.eq(0)).addClass(i.slideNextClass);
                var l = e.prevAll("." + i.slideClass).eq(0).addClass(i.slidePrevClass);
                i.loop && 0 === l.length && (l = t.eq(-1)).addClass(i.slidePrevClass), i.loop && (a.hasClass(i.slideDuplicateClass) ? n.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass) : n.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass), l.hasClass(i.slideDuplicateClass) ? n.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass) : n.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass)), this.emitSlidesClasses()
            }, updateActiveIndex: function (e) {
                var t, i = this.rtlTranslate ? this.translate : -this.translate, n = this.slidesGrid, r = this.snapGrid,
                    s = this.params, o = this.activeIndex, a = this.realIndex, l = this.snapIndex, u = e;
                if (void 0 === u) {
                    for (var c = 0; c < n.length; c += 1) void 0 !== n[c + 1] ? i >= n[c] && i < n[c + 1] - (n[c + 1] - n[c]) / 2 ? u = c : i >= n[c] && i < n[c + 1] && (u = c + 1) : i >= n[c] && (u = c);
                    s.normalizeSlideIndex && (u < 0 || void 0 === u) && (u = 0)
                }
                if (r.indexOf(i) >= 0) t = r.indexOf(i); else {
                    var h = Math.min(s.slidesPerGroupSkip, u);
                    t = h + Math.floor((u - h) / s.slidesPerGroup)
                }
                if (t >= r.length && (t = r.length - 1), u !== o) {
                    var d = parseInt(this.slides.eq(u).attr("data-swiper-slide-index") || u, 10);
                    Kl(this, {
                        snapIndex: t,
                        realIndex: d,
                        previousIndex: o,
                        activeIndex: u
                    }), this.emit("activeIndexChange"), this.emit("snapIndexChange"), a !== d && this.emit("realIndexChange"), (this.initialized || this.params.runCallbacksOnInit) && this.emit("slideChange")
                } else t !== l && (this.snapIndex = t, this.emit("snapIndexChange"))
            }, updateClickedSlide: function (e) {
                var t = this.params, i = Yl(e.target).closest("." + t.slideClass)[0], n = !1;
                if (i) for (var r = 0; r < this.slides.length; r += 1) this.slides[r] === i && (n = !0);
                if (!i || !n) return this.clickedSlide = void 0, void (this.clickedIndex = void 0);
                this.clickedSlide = i, this.virtual && this.params.virtual.enabled ? this.clickedIndex = parseInt(Yl(i).attr("data-swiper-slide-index"), 10) : this.clickedIndex = Yl(i).index(), t.slideToClickedSlide && void 0 !== this.clickedIndex && this.clickedIndex !== this.activeIndex && this.slideToClickedSlide()
            }
        }, translate: {
            getTranslate: function (e) {
                void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                var t = this.params, i = this.rtlTranslate, n = this.translate, r = this.$wrapperEl;
                if (t.virtualTranslate) return i ? -n : n;
                if (t.cssMode) return n;
                var s = function (e, t) {
                    void 0 === t && (t = "x");
                    var i, n, r, s = Al(), o = s.getComputedStyle(e, null);
                    return s.WebKitCSSMatrix ? ((n = o.transform || o.webkitTransform).split(",").length > 6 && (n = n.split(", ").map(function (e) {
                        return e.replace(",", ".")
                    }).join(", ")), r = new s.WebKitCSSMatrix("none" === n ? "" : n)) : i = (r = o.MozTransform || o.OTransform || o.MsTransform || o.msTransform || o.transform || o.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (n = s.WebKitCSSMatrix ? r.m41 : 16 === i.length ? parseFloat(i[12]) : parseFloat(i[4])), "y" === t && (n = s.WebKitCSSMatrix ? r.m42 : 16 === i.length ? parseFloat(i[13]) : parseFloat(i[5])), n || 0
                }(r[0], e);
                return i && (s = -s), s || 0
            }, setTranslate: function (e, t) {
                var i = this.rtlTranslate, n = this.params, r = this.$wrapperEl, s = this.wrapperEl, o = this.progress,
                    a = 0, l = 0;
                this.isHorizontal() ? a = i ? -e : e : l = e, n.roundLengths && (a = Math.floor(a), l = Math.floor(l)), n.cssMode ? s[this.isHorizontal() ? "scrollLeft" : "scrollTop"] = this.isHorizontal() ? -a : -l : n.virtualTranslate || r.transform("translate3d(" + a + "px, " + l + "px, 0px)"), this.previousTranslate = this.translate, this.translate = this.isHorizontal() ? a : l;
                var u = this.maxTranslate() - this.minTranslate();
                (0 === u ? 0 : (e - this.minTranslate()) / u) !== o && this.updateProgress(e), this.emit("setTranslate", this.translate, t)
            }, minTranslate: function () {
                return -this.snapGrid[0]
            }, maxTranslate: function () {
                return -this.snapGrid[this.snapGrid.length - 1]
            }, translateTo: function (e, t, i, n, r) {
                void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0), void 0 === n && (n = !0);
                var s = this, o = s.params, a = s.wrapperEl;
                if (s.animating && o.preventInteractionOnTransition) return !1;
                var l, u = s.minTranslate(), c = s.maxTranslate();
                if (l = n && e > u ? u : n && e < c ? c : e, s.updateProgress(l), o.cssMode) {
                    var h, d = s.isHorizontal();
                    return 0 === t ? a[d ? "scrollLeft" : "scrollTop"] = -l : a.scrollTo ? a.scrollTo(((h = {})[d ? "left" : "top"] = -l, h.behavior = "smooth", h)) : a[d ? "scrollLeft" : "scrollTop"] = -l, !0
                }
                return 0 === t ? (s.setTransition(0), s.setTranslate(l), i && (s.emit("beforeTransitionStart", t, r), s.emit("transitionEnd"))) : (s.setTransition(t), s.setTranslate(l), i && (s.emit("beforeTransitionStart", t, r), s.emit("transitionStart")), s.animating || (s.animating = !0, s.onTranslateToWrapperTransitionEnd || (s.onTranslateToWrapperTransitionEnd = function (e) {
                    s && !s.destroyed && e.target === this && (s.$wrapperEl[0].removeEventListener("transitionend", s.onTranslateToWrapperTransitionEnd), s.$wrapperEl[0].removeEventListener("webkitTransitionEnd", s.onTranslateToWrapperTransitionEnd), s.onTranslateToWrapperTransitionEnd = null, delete s.onTranslateToWrapperTransitionEnd, i && s.emit("transitionEnd"))
                }), s.$wrapperEl[0].addEventListener("transitionend", s.onTranslateToWrapperTransitionEnd), s.$wrapperEl[0].addEventListener("webkitTransitionEnd", s.onTranslateToWrapperTransitionEnd))), !0
            }
        }, transition: {
            setTransition: function (e, t) {
                this.params.cssMode || this.$wrapperEl.transition(e), this.emit("setTransition", e, t)
            }, transitionStart: function (e, t) {
                void 0 === e && (e = !0);
                var i = this.activeIndex, n = this.params, r = this.previousIndex;
                if (!n.cssMode) {
                    n.autoHeight && this.updateAutoHeight();
                    var s = t;
                    if (s || (s = i > r ? "next" : i < r ? "prev" : "reset"), this.emit("transitionStart"), e && i !== r) {
                        if ("reset" === s) return void this.emit("slideResetTransitionStart");
                        this.emit("slideChangeTransitionStart"), "next" === s ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart")
                    }
                }
            }, transitionEnd: function (e, t) {
                void 0 === e && (e = !0);
                var i = this.activeIndex, n = this.previousIndex, r = this.params;
                if (this.animating = !1, !r.cssMode) {
                    this.setTransition(0);
                    var s = t;
                    if (s || (s = i > n ? "next" : i < n ? "prev" : "reset"), this.emit("transitionEnd"), e && i !== n) {
                        if ("reset" === s) return void this.emit("slideResetTransitionEnd");
                        this.emit("slideChangeTransitionEnd"), "next" === s ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd")
                    }
                }
            }
        }, slide: {
            slideTo: function (e, t, i, n) {
                void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                var r = this, s = e;
                s < 0 && (s = 0);
                var o = r.params, a = r.snapGrid, l = r.slidesGrid, u = r.previousIndex, c = r.activeIndex,
                    h = r.rtlTranslate, d = r.wrapperEl;
                if (r.animating && o.preventInteractionOnTransition) return !1;
                var p = Math.min(r.params.slidesPerGroupSkip, s), f = p + Math.floor((s - p) / r.params.slidesPerGroup);
                f >= a.length && (f = a.length - 1), (c || o.initialSlide || 0) === (u || 0) && i && r.emit("beforeSlideChangeStart");
                var m, g = -a[f];
                if (r.updateProgress(g), o.normalizeSlideIndex) for (var v = 0; v < l.length; v += 1) -Math.floor(100 * g) >= Math.floor(100 * l[v]) && (s = v);
                if (r.initialized && s !== c) {
                    if (!r.allowSlideNext && g < r.translate && g < r.minTranslate()) return !1;
                    if (!r.allowSlidePrev && g > r.translate && g > r.maxTranslate() && (c || 0) !== s) return !1
                }
                if (m = s > c ? "next" : s < c ? "prev" : "reset", h && -g === r.translate || !h && g === r.translate) return r.updateActiveIndex(s), o.autoHeight && r.updateAutoHeight(), r.updateSlidesClasses(), "slide" !== o.effect && r.setTranslate(g), "reset" !== m && (r.transitionStart(i, m), r.transitionEnd(i, m)), !1;
                if (o.cssMode) {
                    var y, b = r.isHorizontal(), w = -g;
                    return h && (w = d.scrollWidth - d.offsetWidth - w), 0 === t ? d[b ? "scrollLeft" : "scrollTop"] = w : d.scrollTo ? d.scrollTo(((y = {})[b ? "left" : "top"] = w, y.behavior = "smooth", y)) : d[b ? "scrollLeft" : "scrollTop"] = w, !0
                }
                return 0 === t ? (r.setTransition(0), r.setTranslate(g), r.updateActiveIndex(s), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, n), r.transitionStart(i, m), r.transitionEnd(i, m)) : (r.setTransition(t), r.setTranslate(g), r.updateActiveIndex(s), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, n), r.transitionStart(i, m), r.animating || (r.animating = !0, r.onSlideToWrapperTransitionEnd || (r.onSlideToWrapperTransitionEnd = function (e) {
                    r && !r.destroyed && e.target === this && (r.$wrapperEl[0].removeEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].removeEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd), r.onSlideToWrapperTransitionEnd = null, delete r.onSlideToWrapperTransitionEnd, r.transitionEnd(i, m))
                }), r.$wrapperEl[0].addEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].addEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd))), !0
            }, slideToLoop: function (e, t, i, n) {
                void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                var r = e;
                return this.params.loop && (r += this.loopedSlides), this.slideTo(r, t, i, n)
            }, slideNext: function (e, t, i) {
                void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                var n = this.params, r = this.animating,
                    s = this.activeIndex < n.slidesPerGroupSkip ? 1 : n.slidesPerGroup;
                if (n.loop) {
                    if (r && n.loopPreventsSlide) return !1;
                    this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft
                }
                return this.slideTo(this.activeIndex + s, e, t, i)
            }, slidePrev: function (e, t, i) {
                void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                var n = this.params, r = this.animating, s = this.snapGrid, o = this.slidesGrid, a = this.rtlTranslate;
                if (n.loop) {
                    if (r && n.loopPreventsSlide) return !1;
                    this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft
                }

                function l(e) {
                    return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                }

                var u, c = l(a ? this.translate : -this.translate), h = s.map(function (e) {
                    return l(e)
                }), d = (s[h.indexOf(c)], s[h.indexOf(c) - 1]);
                return void 0 === d && n.cssMode && s.forEach(function (e) {
                    !d && c >= e && (d = e)
                }), void 0 !== d && (u = o.indexOf(d)) < 0 && (u = this.activeIndex - 1), this.slideTo(u, e, t, i)
            }, slideReset: function (e, t, i) {
                return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, i)
            }, slideToClosest: function (e, t, i, n) {
                void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), void 0 === n && (n = .5);
                var r = this.activeIndex, s = Math.min(this.params.slidesPerGroupSkip, r),
                    o = s + Math.floor((r - s) / this.params.slidesPerGroup),
                    a = this.rtlTranslate ? this.translate : -this.translate;
                if (a >= this.snapGrid[o]) {
                    var l = this.snapGrid[o];
                    a - l > (this.snapGrid[o + 1] - l) * n && (r += this.params.slidesPerGroup)
                } else {
                    var u = this.snapGrid[o - 1];
                    a - u <= (this.snapGrid[o] - u) * n && (r -= this.params.slidesPerGroup)
                }
                return r = Math.max(r, 0), r = Math.min(r, this.slidesGrid.length - 1), this.slideTo(r, e, t, i)
            }, slideToClickedSlide: function () {
                var e, t = this, i = t.params, n = t.$wrapperEl,
                    r = "auto" === i.slidesPerView ? t.slidesPerViewDynamic() : i.slidesPerView, s = t.clickedIndex;
                if (i.loop) {
                    if (t.animating) return;
                    e = parseInt(Yl(t.clickedSlide).attr("data-swiper-slide-index"), 10), i.centeredSlides ? s < t.loopedSlides - r / 2 || s > t.slides.length - t.loopedSlides + r / 2 ? (t.loopFix(), s = n.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), Ul(function () {
                        t.slideTo(s)
                    })) : t.slideTo(s) : s > t.slides.length - r ? (t.loopFix(), s = n.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), Ul(function () {
                        t.slideTo(s)
                    })) : t.slideTo(s)
                } else t.slideTo(s)
            }
        }, loop: {
            loopCreate: function () {
                var e = this, t = Pl(), i = e.params, n = e.$wrapperEl;
                n.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                var r = n.children("." + i.slideClass);
                if (i.loopFillGroupWithBlank) {
                    var s = i.slidesPerGroup - r.length % i.slidesPerGroup;
                    if (s !== i.slidesPerGroup) {
                        for (var o = 0; o < s; o += 1) {
                            var a = Yl(t.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                            n.append(a)
                        }
                        r = n.children("." + i.slideClass)
                    }
                }
                "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = r.length), e.loopedSlides = Math.ceil(parseFloat(i.loopedSlides || i.slidesPerView, 10)), e.loopedSlides += i.loopAdditionalSlides, e.loopedSlides > r.length && (e.loopedSlides = r.length);
                var l = [], u = [];
                r.each(function (t, i) {
                    var n = Yl(t);
                    i < e.loopedSlides && u.push(t), i < r.length && i >= r.length - e.loopedSlides && l.push(t), n.attr("data-swiper-slide-index", i)
                });
                for (var c = 0; c < u.length; c += 1) n.append(Yl(u[c].cloneNode(!0)).addClass(i.slideDuplicateClass));
                for (var h = l.length - 1; h >= 0; h -= 1) n.prepend(Yl(l[h].cloneNode(!0)).addClass(i.slideDuplicateClass))
            }, loopFix: function () {
                this.emit("beforeLoopFix");
                var e, t = this.activeIndex, i = this.slides, n = this.loopedSlides, r = this.allowSlidePrev,
                    s = this.allowSlideNext, o = this.snapGrid, a = this.rtlTranslate;
                this.allowSlidePrev = !0, this.allowSlideNext = !0;
                var l = -o[t] - this.getTranslate();
                t < n ? (e = i.length - 3 * n + t, e += n, this.slideTo(e, 0, !1, !0) && 0 !== l && this.setTranslate((a ? -this.translate : this.translate) - l)) : t >= i.length - n && (e = -i.length + t + n, e += n, this.slideTo(e, 0, !1, !0) && 0 !== l && this.setTranslate((a ? -this.translate : this.translate) - l)), this.allowSlidePrev = r, this.allowSlideNext = s, this.emit("loopFix")
            }, loopDestroy: function () {
                var e = this.$wrapperEl, t = this.params, i = this.slides;
                e.children("." + t.slideClass + "." + t.slideDuplicateClass + ",." + t.slideClass + "." + t.slideBlankClass).remove(), i.removeAttr("data-swiper-slide-index")
            }
        }, grabCursor: {
            setGrabCursor: function (e) {
                if (!(this.support.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked || this.params.cssMode)) {
                    var t = this.el;
                    t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                }
            }, unsetGrabCursor: function () {
                this.support.touch || this.params.watchOverflow && this.isLocked || this.params.cssMode || (this.el.style.cursor = "")
            }
        }, manipulation: {
            appendSlide: function (e) {
                var t = this.$wrapperEl, i = this.params;
                if (i.loop && this.loopDestroy(), "object" == typeof e && "length" in e) for (var n = 0; n < e.length; n += 1) e[n] && t.append(e[n]); else t.append(e);
                i.loop && this.loopCreate(), i.observer && this.support.observer || this.update()
            }, prependSlide: function (e) {
                var t = this.params, i = this.$wrapperEl, n = this.activeIndex;
                t.loop && this.loopDestroy();
                var r = n + 1;
                if ("object" == typeof e && "length" in e) {
                    for (var s = 0; s < e.length; s += 1) e[s] && i.prepend(e[s]);
                    r = n + e.length
                } else i.prepend(e);
                t.loop && this.loopCreate(), t.observer && this.support.observer || this.update(), this.slideTo(r, 0, !1)
            }, addSlide: function (e, t) {
                var i = this.$wrapperEl, n = this.params, r = this.activeIndex;
                n.loop && (r -= this.loopedSlides, this.loopDestroy(), this.slides = i.children("." + n.slideClass));
                var s = this.slides.length;
                if (e <= 0) this.prependSlide(t); else if (e >= s) this.appendSlide(t); else {
                    for (var o = r > e ? r + 1 : r, a = [], l = s - 1; l >= e; l -= 1) {
                        var u = this.slides.eq(l);
                        u.remove(), a.unshift(u)
                    }
                    if ("object" == typeof t && "length" in t) {
                        for (var c = 0; c < t.length; c += 1) t[c] && i.append(t[c]);
                        o = r > e ? r + t.length : r
                    } else i.append(t);
                    for (var h = 0; h < a.length; h += 1) i.append(a[h]);
                    n.loop && this.loopCreate(), n.observer && this.support.observer || this.update(), n.loop ? this.slideTo(o + this.loopedSlides, 0, !1) : this.slideTo(o, 0, !1)
                }
            }, removeSlide: function (e) {
                var t = this.params, i = this.$wrapperEl, n = this.activeIndex;
                t.loop && (n -= this.loopedSlides, this.loopDestroy(), this.slides = i.children("." + t.slideClass));
                var r, s = n;
                if ("object" == typeof e && "length" in e) {
                    for (var o = 0; o < e.length; o += 1) r = e[o], this.slides[r] && this.slides.eq(r).remove(), r < s && (s -= 1);
                    s = Math.max(s, 0)
                } else r = e, this.slides[r] && this.slides.eq(r).remove(), r < s && (s -= 1), s = Math.max(s, 0);
                t.loop && this.loopCreate(), t.observer && this.support.observer || this.update(), t.loop ? this.slideTo(s + this.loopedSlides, 0, !1) : this.slideTo(s, 0, !1)
            }, removeAllSlides: function () {
                for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                this.removeSlide(e)
            }
        }, events: {
            attachEvents: function () {
                var e = Pl(), t = this.params, i = this.touchEvents, n = this.el, r = this.wrapperEl, s = this.device,
                    o = this.support;
                this.onTouchStart = function (e) {
                    var t = Pl(), i = Al(), n = this.touchEventsData, r = this.params, s = this.touches;
                    if (!this.animating || !r.preventInteractionOnTransition) {
                        var o = e;
                        o.originalEvent && (o = o.originalEvent);
                        var a = Yl(o.target);
                        if (("wrapper" !== r.touchEventsTarget || a.closest(this.wrapperEl).length) && (n.isTouchEvent = "touchstart" === o.type, (n.isTouchEvent || !("which" in o) || 3 !== o.which) && !(!n.isTouchEvent && "button" in o && o.button > 0 || n.isTouched && n.isMoved))) if (r.noSwiping && a.closest(r.noSwipingSelector ? r.noSwipingSelector : "." + r.noSwipingClass)[0]) this.allowClick = !0; else if (!r.swipeHandler || a.closest(r.swipeHandler)[0]) {
                            s.currentX = "touchstart" === o.type ? o.targetTouches[0].pageX : o.pageX, s.currentY = "touchstart" === o.type ? o.targetTouches[0].pageY : o.pageY;
                            var l = s.currentX, u = s.currentY, c = r.edgeSwipeDetection || r.iOSEdgeSwipeDetection,
                                h = r.edgeSwipeThreshold || r.iOSEdgeSwipeThreshold;
                            if (!c || !(l <= h || l >= i.screen.width - h)) {
                                if (Kl(n, {
                                    isTouched: !0,
                                    isMoved: !1,
                                    allowTouchCallbacks: !0,
                                    isScrolling: void 0,
                                    startMoving: void 0
                                }), s.startX = l, s.startY = u, n.touchStartTime = Gl(), this.allowClick = !0, this.updateSize(), this.swipeDirection = void 0, r.threshold > 0 && (n.allowThresholdMove = !1), "touchstart" !== o.type) {
                                    var d = !0;
                                    a.is(n.formElements) && (d = !1), t.activeElement && Yl(t.activeElement).is(n.formElements) && t.activeElement !== a[0] && t.activeElement.blur();
                                    var p = d && this.allowTouchMove && r.touchStartPreventDefault;
                                    (r.touchStartForcePreventDefault || p) && o.preventDefault()
                                }
                                this.emit("touchStart", o)
                            }
                        }
                    }
                }.bind(this), this.onTouchMove = function (e) {
                    var t = Pl(), i = this.touchEventsData, n = this.params, r = this.touches, s = this.rtlTranslate,
                        o = e;
                    if (o.originalEvent && (o = o.originalEvent), i.isTouched) {
                        if (!i.isTouchEvent || "touchmove" === o.type) {
                            var a = "touchmove" === o.type && o.targetTouches && (o.targetTouches[0] || o.changedTouches[0]),
                                l = "touchmove" === o.type ? a.pageX : o.pageX,
                                u = "touchmove" === o.type ? a.pageY : o.pageY;
                            if (o.preventedByNestedSwiper) return r.startX = l, void (r.startY = u);
                            if (!this.allowTouchMove) return this.allowClick = !1, void (i.isTouched && (Kl(r, {
                                startX: l,
                                startY: u,
                                currentX: l,
                                currentY: u
                            }), i.touchStartTime = Gl()));
                            if (i.isTouchEvent && n.touchReleaseOnEdges && !n.loop) if (this.isVertical()) {
                                if (u < r.startY && this.translate <= this.maxTranslate() || u > r.startY && this.translate >= this.minTranslate()) return i.isTouched = !1, void (i.isMoved = !1)
                            } else if (l < r.startX && this.translate <= this.maxTranslate() || l > r.startX && this.translate >= this.minTranslate()) return;
                            if (i.isTouchEvent && t.activeElement && o.target === t.activeElement && Yl(o.target).is(i.formElements)) return i.isMoved = !0, void (this.allowClick = !1);
                            if (i.allowTouchCallbacks && this.emit("touchMove", o), !(o.targetTouches && o.targetTouches.length > 1)) {
                                r.currentX = l, r.currentY = u;
                                var c, h = r.currentX - r.startX, d = r.currentY - r.startY;
                                if (!(this.params.threshold && Math.sqrt(Math.pow(h, 2) + Math.pow(d, 2)) < this.params.threshold)) if (void 0 === i.isScrolling && (this.isHorizontal() && r.currentY === r.startY || this.isVertical() && r.currentX === r.startX ? i.isScrolling = !1 : h * h + d * d >= 25 && (c = 180 * Math.atan2(Math.abs(d), Math.abs(h)) / Math.PI, i.isScrolling = this.isHorizontal() ? c > n.touchAngle : 90 - c > n.touchAngle)), i.isScrolling && this.emit("touchMoveOpposite", o), void 0 === i.startMoving && (r.currentX === r.startX && r.currentY === r.startY || (i.startMoving = !0)), i.isScrolling) i.isTouched = !1; else if (i.startMoving) {
                                    this.allowClick = !1, !n.cssMode && o.cancelable && o.preventDefault(), n.touchMoveStopPropagation && !n.nested && o.stopPropagation(), i.isMoved || (n.loop && this.loopFix(), i.startTranslate = this.getTranslate(), this.setTransition(0), this.animating && this.$wrapperEl.trigger("webkitTransitionEnd transitionend"), i.allowMomentumBounce = !1, !n.grabCursor || !0 !== this.allowSlideNext && !0 !== this.allowSlidePrev || this.setGrabCursor(!0), this.emit("sliderFirstMove", o)), this.emit("sliderMove", o), i.isMoved = !0;
                                    var p = this.isHorizontal() ? h : d;
                                    r.diff = p, p *= n.touchRatio, s && (p = -p), this.swipeDirection = p > 0 ? "prev" : "next", i.currentTranslate = p + i.startTranslate;
                                    var f = !0, m = n.resistanceRatio;
                                    if (n.touchReleaseOnEdges && (m = 0), p > 0 && i.currentTranslate > this.minTranslate() ? (f = !1, n.resistance && (i.currentTranslate = this.minTranslate() - 1 + Math.pow(-this.minTranslate() + i.startTranslate + p, m))) : p < 0 && i.currentTranslate < this.maxTranslate() && (f = !1, n.resistance && (i.currentTranslate = this.maxTranslate() + 1 - Math.pow(this.maxTranslate() - i.startTranslate - p, m))), f && (o.preventedByNestedSwiper = !0), !this.allowSlideNext && "next" === this.swipeDirection && i.currentTranslate < i.startTranslate && (i.currentTranslate = i.startTranslate), !this.allowSlidePrev && "prev" === this.swipeDirection && i.currentTranslate > i.startTranslate && (i.currentTranslate = i.startTranslate), n.threshold > 0) {
                                        if (!(Math.abs(p) > n.threshold || i.allowThresholdMove)) return void (i.currentTranslate = i.startTranslate);
                                        if (!i.allowThresholdMove) return i.allowThresholdMove = !0, r.startX = r.currentX, r.startY = r.currentY, i.currentTranslate = i.startTranslate, void (r.diff = this.isHorizontal() ? r.currentX - r.startX : r.currentY - r.startY)
                                    }
                                    n.followFinger && !n.cssMode && ((n.freeMode || n.watchSlidesProgress || n.watchSlidesVisibility) && (this.updateActiveIndex(), this.updateSlidesClasses()), n.freeMode && (0 === i.velocities.length && i.velocities.push({
                                        position: r[this.isHorizontal() ? "startX" : "startY"],
                                        time: i.touchStartTime
                                    }), i.velocities.push({
                                        position: r[this.isHorizontal() ? "currentX" : "currentY"],
                                        time: Gl()
                                    })), this.updateProgress(i.currentTranslate), this.setTranslate(i.currentTranslate))
                                }
                            }
                        }
                    } else i.startMoving && i.isScrolling && this.emit("touchMoveOpposite", o)
                }.bind(this), this.onTouchEnd = function (e) {
                    var t = this, i = t.touchEventsData, n = t.params, r = t.touches, s = t.rtlTranslate,
                        o = t.$wrapperEl, a = t.slidesGrid, l = t.snapGrid, u = e;
                    if (u.originalEvent && (u = u.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", u), i.allowTouchCallbacks = !1, !i.isTouched) return i.isMoved && n.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void (i.startMoving = !1);
                    n.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                    var c, h = Gl(), d = h - i.touchStartTime;
                    if (t.allowClick && (t.updateClickedSlide(u), t.emit("tap click", u), d < 300 && h - i.lastClickTime < 300 && t.emit("doubleTap doubleClick", u)), i.lastClickTime = Gl(), Ul(function () {
                        t.destroyed || (t.allowClick = !0)
                    }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === r.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, i.isMoved = !1, void (i.startMoving = !1);
                    if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, c = n.followFinger ? s ? t.translate : -t.translate : -i.currentTranslate, !n.cssMode) if (n.freeMode) {
                        if (c < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                        if (c > -t.maxTranslate()) return void (t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));
                        if (n.freeModeMomentum) {
                            if (i.velocities.length > 1) {
                                var p = i.velocities.pop(), f = i.velocities.pop(), m = p.position - f.position,
                                    g = p.time - f.time;
                                t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < n.freeModeMinimumVelocity && (t.velocity = 0), (g > 150 || Gl() - p.time > 300) && (t.velocity = 0)
                            } else t.velocity = 0;
                            t.velocity *= n.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                            var v = 1e3 * n.freeModeMomentumRatio, y = t.velocity * v, b = t.translate + y;
                            s && (b = -b);
                            var w, D, T = !1, C = 20 * Math.abs(t.velocity) * n.freeModeMomentumBounceRatio;
                            if (b < t.maxTranslate()) n.freeModeMomentumBounce ? (b + t.maxTranslate() < -C && (b = t.maxTranslate() - C), w = t.maxTranslate(), T = !0, i.allowMomentumBounce = !0) : b = t.maxTranslate(), n.loop && n.centeredSlides && (D = !0); else if (b > t.minTranslate()) n.freeModeMomentumBounce ? (b - t.minTranslate() > C && (b = t.minTranslate() + C), w = t.minTranslate(), T = !0, i.allowMomentumBounce = !0) : b = t.minTranslate(), n.loop && n.centeredSlides && (D = !0); else if (n.freeModeSticky) {
                                for (var k, E = 0; E < l.length; E += 1) if (l[E] > -b) {
                                    k = E;
                                    break
                                }
                                b = -(b = Math.abs(l[k] - b) < Math.abs(l[k - 1] - b) || "next" === t.swipeDirection ? l[k] : l[k - 1])
                            }
                            if (D && t.once("transitionEnd", function () {
                                t.loopFix()
                            }), 0 !== t.velocity) {
                                if (v = s ? Math.abs((-b - t.translate) / t.velocity) : Math.abs((b - t.translate) / t.velocity), n.freeModeSticky) {
                                    var _ = Math.abs((s ? -b : b) - t.translate), x = t.slidesSizesGrid[t.activeIndex];
                                    v = _ < x ? n.speed : _ < 2 * x ? 1.5 * n.speed : 2.5 * n.speed
                                }
                            } else if (n.freeModeSticky) return void t.slideToClosest();
                            n.freeModeMomentumBounce && T ? (t.updateProgress(w), t.setTransition(v), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating = !0, o.transitionEnd(function () {
                                t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(n.speed), setTimeout(function () {
                                    t.setTranslate(w), o.transitionEnd(function () {
                                        t && !t.destroyed && t.transitionEnd()
                                    })
                                }, 0))
                            })) : t.velocity ? (t.updateProgress(b), t.setTransition(v), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, o.transitionEnd(function () {
                                t && !t.destroyed && t.transitionEnd()
                            }))) : t.updateProgress(b), t.updateActiveIndex(), t.updateSlidesClasses()
                        } else if (n.freeModeSticky) return void t.slideToClosest();
                        (!n.freeModeMomentum || d >= n.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                    } else {
                        for (var S = 0, P = t.slidesSizesGrid[0], O = 0; O < a.length; O += O < n.slidesPerGroupSkip ? 1 : n.slidesPerGroup) {
                            var A = O < n.slidesPerGroupSkip - 1 ? 1 : n.slidesPerGroup;
                            void 0 !== a[O + A] ? c >= a[O] && c < a[O + A] && (S = O, P = a[O + A] - a[O]) : c >= a[O] && (S = O, P = a[a.length - 1] - a[a.length - 2])
                        }
                        var M = (c - a[S]) / P, L = S < n.slidesPerGroupSkip - 1 ? 1 : n.slidesPerGroup;
                        if (d > n.longSwipesMs) {
                            if (!n.longSwipes) return void t.slideTo(t.activeIndex);
                            "next" === t.swipeDirection && (M >= n.longSwipesRatio ? t.slideTo(S + L) : t.slideTo(S)), "prev" === t.swipeDirection && (M > 1 - n.longSwipesRatio ? t.slideTo(S + L) : t.slideTo(S))
                        } else {
                            if (!n.shortSwipes) return void t.slideTo(t.activeIndex);
                            !t.navigation || u.target !== t.navigation.nextEl && u.target !== t.navigation.prevEl ? ("next" === t.swipeDirection && t.slideTo(S + L), "prev" === t.swipeDirection && t.slideTo(S)) : u.target === t.navigation.nextEl ? t.slideTo(S + L) : t.slideTo(S)
                        }
                    }
                }.bind(this), t.cssMode && (this.onScroll = function () {
                    var e = this.wrapperEl, t = this.rtlTranslate;
                    this.previousTranslate = this.translate, this.isHorizontal() ? this.translate = t ? e.scrollWidth - e.offsetWidth - e.scrollLeft : -e.scrollLeft : this.translate = -e.scrollTop, -0 === this.translate && (this.translate = 0), this.updateActiveIndex(), this.updateSlidesClasses();
                    var i = this.maxTranslate() - this.minTranslate();
                    (0 === i ? 0 : (this.translate - this.minTranslate()) / i) !== this.progress && this.updateProgress(t ? -this.translate : this.translate), this.emit("setTranslate", this.translate, !1)
                }.bind(this)), this.onClick = function (e) {
                    this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                }.bind(this);
                var a = !!t.nested;
                if (!o.touch && o.pointerEvents) n.addEventListener(i.start, this.onTouchStart, !1), e.addEventListener(i.move, this.onTouchMove, a), e.addEventListener(i.end, this.onTouchEnd, !1); else {
                    if (o.touch) {
                        var l = !("touchstart" !== i.start || !o.passiveListener || !t.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        n.addEventListener(i.start, this.onTouchStart, l), n.addEventListener(i.move, this.onTouchMove, o.passiveListener ? {
                            passive: !1,
                            capture: a
                        } : a), n.addEventListener(i.end, this.onTouchEnd, l), i.cancel && n.addEventListener(i.cancel, this.onTouchEnd, l), iu || (e.addEventListener("touchstart", nu), iu = !0)
                    }
                    (t.simulateTouch && !s.ios && !s.android || t.simulateTouch && !o.touch && s.ios) && (n.addEventListener("mousedown", this.onTouchStart, !1), e.addEventListener("mousemove", this.onTouchMove, a), e.addEventListener("mouseup", this.onTouchEnd, !1))
                }
                (t.preventClicks || t.preventClicksPropagation) && n.addEventListener("click", this.onClick, !0), t.cssMode && r.addEventListener("scroll", this.onScroll), t.updateOnWindowResize ? this.on(s.ios || s.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", tu, !0) : this.on("observerUpdate", tu, !0)
            }, detachEvents: function () {
                var e = Pl(), t = this.params, i = this.touchEvents, n = this.el, r = this.wrapperEl, s = this.device,
                    o = this.support, a = !!t.nested;
                if (!o.touch && o.pointerEvents) n.removeEventListener(i.start, this.onTouchStart, !1), e.removeEventListener(i.move, this.onTouchMove, a), e.removeEventListener(i.end, this.onTouchEnd, !1); else {
                    if (o.touch) {
                        var l = !("onTouchStart" !== i.start || !o.passiveListener || !t.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        n.removeEventListener(i.start, this.onTouchStart, l), n.removeEventListener(i.move, this.onTouchMove, a), n.removeEventListener(i.end, this.onTouchEnd, l), i.cancel && n.removeEventListener(i.cancel, this.onTouchEnd, l)
                    }
                    (t.simulateTouch && !s.ios && !s.android || t.simulateTouch && !o.touch && s.ios) && (n.removeEventListener("mousedown", this.onTouchStart, !1), e.removeEventListener("mousemove", this.onTouchMove, a), e.removeEventListener("mouseup", this.onTouchEnd, !1))
                }
                (t.preventClicks || t.preventClicksPropagation) && n.removeEventListener("click", this.onClick, !0), t.cssMode && r.removeEventListener("scroll", this.onScroll), this.off(s.ios || s.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", tu)
            }
        }, breakpoints: {
            setBreakpoint: function () {
                var e = this.activeIndex, t = this.initialized, i = this.loopedSlides, n = void 0 === i ? 0 : i,
                    r = this.params, s = this.$el, o = r.breakpoints;
                if (o && (!o || 0 !== Object.keys(o).length)) {
                    var a = this.getBreakpoint(o);
                    if (a && this.currentBreakpoint !== a) {
                        var l = a in o ? o[a] : void 0;
                        l && ["slidesPerView", "spaceBetween", "slidesPerGroup", "slidesPerGroupSkip", "slidesPerColumn"].forEach(function (e) {
                            var t = l[e];
                            void 0 !== t && (l[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto")
                        });
                        var u = l || this.originalParams, c = r.slidesPerColumn > 1, h = u.slidesPerColumn > 1;
                        c && !h ? (s.removeClass(r.containerModifierClass + "multirow " + r.containerModifierClass + "multirow-column"), this.emitContainerClasses()) : !c && h && (s.addClass(r.containerModifierClass + "multirow"), "column" === u.slidesPerColumnFill && s.addClass(r.containerModifierClass + "multirow-column"), this.emitContainerClasses());
                        var d = u.direction && u.direction !== r.direction,
                            p = r.loop && (u.slidesPerView !== r.slidesPerView || d);
                        d && t && this.changeDirection(), Kl(this.params, u), Kl(this, {
                            allowTouchMove: this.params.allowTouchMove,
                            allowSlideNext: this.params.allowSlideNext,
                            allowSlidePrev: this.params.allowSlidePrev
                        }), this.currentBreakpoint = a, p && t && (this.loopDestroy(), this.loopCreate(), this.updateSlides(), this.slideTo(e - n + this.loopedSlides, 0, !1)), this.emit("breakpoint", u)
                    }
                }
            }, getBreakpoint: function (e) {
                var t = Al();
                if (e) {
                    var i = !1, n = Object.keys(e).map(function (e) {
                        if ("string" == typeof e && 0 === e.indexOf("@")) {
                            var i = parseFloat(e.substr(1));
                            return {value: t.innerHeight * i, point: e}
                        }
                        return {value: e, point: e}
                    });
                    n.sort(function (e, t) {
                        return parseInt(e.value, 10) - parseInt(t.value, 10)
                    });
                    for (var r = 0; r < n.length; r += 1) {
                        var s = n[r], o = s.point;
                        s.value <= t.innerWidth && (i = o)
                    }
                    return i || "max"
                }
            }
        }, checkOverflow: {
            checkOverflow: function () {
                var e = this.params, t = this.isLocked,
                    i = this.slides.length > 0 && e.slidesOffsetBefore + e.spaceBetween * (this.slides.length - 1) + this.slides[0].offsetWidth * this.slides.length;
                e.slidesOffsetBefore && e.slidesOffsetAfter && i ? this.isLocked = i <= this.size : this.isLocked = 1 === this.snapGrid.length, this.allowSlideNext = !this.isLocked, this.allowSlidePrev = !this.isLocked, t !== this.isLocked && this.emit(this.isLocked ? "lock" : "unlock"), t && t !== this.isLocked && (this.isEnd = !1, this.navigation && this.navigation.update())
            }
        }, classes: {
            addClasses: function () {
                var e = this.classNames, t = this.params, i = this.rtl, n = this.$el, r = this.device, s = [];
                s.push("initialized"), s.push(t.direction), t.freeMode && s.push("free-mode"), t.autoHeight && s.push("autoheight"), i && s.push("rtl"), t.slidesPerColumn > 1 && (s.push("multirow"), "column" === t.slidesPerColumnFill && s.push("multirow-column")), r.android && s.push("android"), r.ios && s.push("ios"), t.cssMode && s.push("css-mode"), s.forEach(function (i) {
                    e.push(t.containerModifierClass + i)
                }), n.addClass(e.join(" ")), this.emitContainerClasses()
            }, removeClasses: function () {
                var e = this.$el, t = this.classNames;
                e.removeClass(t.join(" ")), this.emitContainerClasses()
            }
        }, images: {
            loadImage: function (e, t, i, n, r, s) {
                var o, a = Al();

                function l() {
                    s && s()
                }

                Yl(e).parent("picture")[0] || e.complete && r ? l() : t ? ((o = new a.Image).onload = l, o.onerror = l, n && (o.sizes = n), i && (o.srcset = i), t && (o.src = t)) : l()
            }, preloadImages: function () {
                var e = this;

                function t() {
                    null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")))
                }

                e.imagesToLoad = e.$el.find("img");
                for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                    var n = e.imagesToLoad[i];
                    e.loadImage(n, n.currentSrc || n.getAttribute("src"), n.srcset || n.getAttribute("srcset"), n.sizes || n.getAttribute("sizes"), !0, t)
                }
            }
        }
    }, ou = {}, au = function () {
        function e() {
            for (var t, i, n = arguments.length, r = new Array(n), s = 0; s < n; s++) r[s] = arguments[s];
            1 === r.length && r[0].constructor && r[0].constructor === Object ? i = r[0] : (t = r[0], i = r[1]), i || (i = {}), i = Kl({}, i), t && !i.el && (i.el = t);
            var o = this;
            o.support = Ql(), o.device = function (e) {
                return void 0 === e && (e = {}), Wl || (Wl = function (e) {
                    var t = (void 0 === e ? {} : e).userAgent, i = Ql(), n = Al(), r = n.navigator.platform,
                        s = t || n.navigator.userAgent, o = {ios: !1, android: !1}, a = n.screen.width,
                        l = n.screen.height, u = s.match(/(Android);?[\s\/]+([\d.]+)?/),
                        c = s.match(/(iPad).*OS\s([\d_]+)/), h = s.match(/(iPod)(.*OS\s([\d_]+))?/),
                        d = !c && s.match(/(iPhone\sOS|iOS)\s([\d_]+)/), p = "Win32" === r, f = "MacIntel" === r;
                    return !c && f && i.touch && ["1024x1366", "1366x1024", "834x1194", "1194x834", "834x1112", "1112x834", "768x1024", "1024x768"].indexOf(a + "x" + l) >= 0 && ((c = s.match(/(Version)\/([\d.]+)/)) || (c = [0, 1, "13_0_0"]), f = !1), u && !p && (o.os = "android", o.android = !0), (c || d || h) && (o.os = "ios", o.ios = !0), o
                }(e)), Wl
            }({userAgent: i.userAgent}), o.browser = function () {
                return Xl || (t = Al(), Xl = {
                    isEdge: !!t.navigator.userAgent.match(/Edge/g),
                    isSafari: (e = t.navigator.userAgent.toLowerCase(), e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0),
                    isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)
                }), Xl;
                var e, t
            }(), o.eventsListeners = {}, o.eventsAnyListeners = [], Object.keys(su).forEach(function (t) {
                Object.keys(su[t]).forEach(function (i) {
                    e.prototype[i] || (e.prototype[i] = su[t][i])
                })
            }), void 0 === o.modules && (o.modules = {}), Object.keys(o.modules).forEach(function (e) {
                var t = o.modules[e];
                if (t.params) {
                    var n = Object.keys(t.params)[0], r = t.params[n];
                    if ("object" != typeof r || null === r) return;
                    if (!(n in i && "enabled" in r)) return;
                    !0 === i[n] && (i[n] = {enabled: !0}), "object" != typeof i[n] || "enabled" in i[n] || (i[n].enabled = !0), i[n] || (i[n] = {enabled: !1})
                }
            });
            var a = Kl({}, ru);
            o.useParams(a), o.params = Kl({}, a, ou, i), o.originalParams = Kl({}, o.params), o.passedParams = Kl({}, i), o.params && o.params.on && Object.keys(o.params.on).forEach(function (e) {
                o.on(e, o.params.on[e])
            }), o.$ = Yl;
            var l = Yl(o.params.el);
            if (t = l[0]) {
                if (l.length > 1) {
                    var u = [];
                    return l.each(function (t) {
                        var n = Kl({}, i, {el: t});
                        u.push(new e(n))
                    }), u
                }
                var c, h, d;
                return t.swiper = o, t && t.shadowRoot && t.shadowRoot.querySelector ? (c = Yl(t.shadowRoot.querySelector("." + o.params.wrapperClass))).children = function (e) {
                    return l.children(e)
                } : c = l.children("." + o.params.wrapperClass), Kl(o, {
                    $el: l,
                    el: t,
                    $wrapperEl: c,
                    wrapperEl: c[0],
                    classNames: [],
                    slides: Yl(),
                    slidesGrid: [],
                    snapGrid: [],
                    slidesSizesGrid: [],
                    isHorizontal: function () {
                        return "horizontal" === o.params.direction
                    },
                    isVertical: function () {
                        return "vertical" === o.params.direction
                    },
                    rtl: "rtl" === t.dir.toLowerCase() || "rtl" === l.css("direction"),
                    rtlTranslate: "horizontal" === o.params.direction && ("rtl" === t.dir.toLowerCase() || "rtl" === l.css("direction")),
                    wrongRTL: "-webkit-box" === c.css("display"),
                    activeIndex: 0,
                    realIndex: 0,
                    isBeginning: !0,
                    isEnd: !1,
                    translate: 0,
                    previousTranslate: 0,
                    progress: 0,
                    velocity: 0,
                    animating: !1,
                    allowSlideNext: o.params.allowSlideNext,
                    allowSlidePrev: o.params.allowSlidePrev,
                    touchEvents: (h = ["touchstart", "touchmove", "touchend", "touchcancel"], d = ["mousedown", "mousemove", "mouseup"], o.support.pointerEvents && (d = ["pointerdown", "pointermove", "pointerup"]), o.touchEventsTouch = {
                        start: h[0],
                        move: h[1],
                        end: h[2],
                        cancel: h[3]
                    }, o.touchEventsDesktop = {
                        start: d[0],
                        move: d[1],
                        end: d[2]
                    }, o.support.touch || !o.params.simulateTouch ? o.touchEventsTouch : o.touchEventsDesktop),
                    touchEventsData: {
                        isTouched: void 0,
                        isMoved: void 0,
                        allowTouchCallbacks: void 0,
                        touchStartTime: void 0,
                        isScrolling: void 0,
                        currentTranslate: void 0,
                        startTranslate: void 0,
                        allowThresholdMove: void 0,
                        formElements: "input, select, option, textarea, button, video, label",
                        lastClickTime: Gl(),
                        clickTimeout: void 0,
                        velocities: [],
                        allowMomentumBounce: void 0,
                        isTouchEvent: void 0,
                        startMoving: void 0
                    },
                    allowClick: !0,
                    allowTouchMove: o.params.allowTouchMove,
                    touches: {startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0},
                    imagesToLoad: [],
                    imagesLoaded: 0
                }), o.useModules(), o.emit("_swiper"), o.params.init && o.init(), o
            }
        }

        var t, i, n = e.prototype;
        return n.emitContainerClasses = function () {
            var e = this;
            if (e.params._emitClasses && e.el) {
                var t = e.el.className.split(" ").filter(function (t) {
                    return 0 === t.indexOf("swiper-container") || 0 === t.indexOf(e.params.containerModifierClass)
                });
                e.emit("_containerClasses", t.join(" "))
            }
        }, n.emitSlidesClasses = function () {
            var e = this;
            e.params._emitClasses && e.el && e.slides.each(function (t) {
                var i = t.className.split(" ").filter(function (t) {
                    return 0 === t.indexOf("swiper-slide") || 0 === t.indexOf(e.params.slideClass)
                });
                e.emit("_slideClass", t, i.join(" "))
            })
        }, n.slidesPerViewDynamic = function () {
            var e = this.params, t = this.slides, i = this.slidesGrid, n = this.size, r = this.activeIndex, s = 1;
            if (e.centeredSlides) {
                for (var o, a = t[r].swiperSlideSize, l = r + 1; l < t.length; l += 1) t[l] && !o && (s += 1, (a += t[l].swiperSlideSize) > n && (o = !0));
                for (var u = r - 1; u >= 0; u -= 1) t[u] && !o && (s += 1, (a += t[u].swiperSlideSize) > n && (o = !0))
            } else for (var c = r + 1; c < t.length; c += 1) i[c] - i[r] < n && (s += 1);
            return s
        }, n.update = function () {
            var e = this;
            if (e && !e.destroyed) {
                var t = e.snapGrid, i = e.params;
                i.breakpoints && e.setBreakpoint(), e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses(), e.params.freeMode ? (n(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || n(), i.watchOverflow && t !== e.snapGrid && e.checkOverflow(), e.emit("update")
            }

            function n() {
                var t = e.rtlTranslate ? -1 * e.translate : e.translate,
                    i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
                e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses()
            }
        }, n.changeDirection = function (e, t) {
            void 0 === t && (t = !0);
            var i = this.params.direction;
            return e || (e = "horizontal" === i ? "vertical" : "horizontal"), e === i || "horizontal" !== e && "vertical" !== e || (this.$el.removeClass("" + this.params.containerModifierClass + i).addClass("" + this.params.containerModifierClass + e), this.emitContainerClasses(), this.params.direction = e, this.slides.each(function (t) {
                "vertical" === e ? t.style.width = "" : t.style.height = ""
            }), this.emit("changeDirection"), t && this.update()), this
        }, n.init = function () {
            this.initialized || (this.emit("beforeInit"), this.params.breakpoints && this.setBreakpoint(), this.addClasses(), this.params.loop && this.loopCreate(), this.updateSize(), this.updateSlides(), this.params.watchOverflow && this.checkOverflow(), this.params.grabCursor && this.setGrabCursor(), this.params.preloadImages && this.preloadImages(), this.params.loop ? this.slideTo(this.params.initialSlide + this.loopedSlides, 0, this.params.runCallbacksOnInit) : this.slideTo(this.params.initialSlide, 0, this.params.runCallbacksOnInit), this.attachEvents(), this.initialized = !0, this.emit("init"))
        }, n.destroy = function (e, t) {
            void 0 === e && (e = !0), void 0 === t && (t = !0);
            var i, n = this, r = n.params, s = n.$el, o = n.$wrapperEl, a = n.slides;
            return void 0 === n.params || n.destroyed || (n.emit("beforeDestroy"), n.initialized = !1, n.detachEvents(), r.loop && n.loopDestroy(), t && (n.removeClasses(), s.removeAttr("style"), o.removeAttr("style"), a && a.length && a.removeClass([r.slideVisibleClass, r.slideActiveClass, r.slideNextClass, r.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index")), n.emit("destroy"), Object.keys(n.eventsListeners).forEach(function (e) {
                n.off(e)
            }), !1 !== e && (n.$el[0].swiper = null, i = n, Object.keys(i).forEach(function (e) {
                try {
                    i[e] = null
                } catch (e) {
                }
                try {
                    delete i[e]
                } catch (e) {
                }
            })), n.destroyed = !0), null
        }, e.extendDefaults = function (e) {
            Kl(ou, e)
        }, e.installModule = function (t) {
            e.prototype.modules || (e.prototype.modules = {});
            var i = t.name || Object.keys(e.prototype.modules).length + "_" + Gl();
            e.prototype.modules[i] = t
        }, e.use = function (t) {
            return Array.isArray(t) ? (t.forEach(function (t) {
                return e.installModule(t)
            }), e) : (e.installModule(t), e)
        }, t = e, i = [{
            key: "extendedDefaults", get: function () {
                return ou
            }
        }, {
            key: "defaults", get: function () {
                return ru
            }
        }], null && Zl(t.prototype, null), i && Zl(t, i), e
    }(), lu = {
        attach: function (e, t) {
            void 0 === t && (t = {});
            var i = Al(), n = this, r = new (i.MutationObserver || i.WebkitMutationObserver)(function (e) {
                if (1 !== e.length) {
                    var t = function () {
                        n.emit("observerUpdate", e[0])
                    };
                    i.requestAnimationFrame ? i.requestAnimationFrame(t) : i.setTimeout(t, 0)
                } else n.emit("observerUpdate", e[0])
            });
            r.observe(e, {
                attributes: void 0 === t.attributes || t.attributes,
                childList: void 0 === t.childList || t.childList,
                characterData: void 0 === t.characterData || t.characterData
            }), n.observer.observers.push(r)
        }, init: function () {
            if (this.support.observer && this.params.observer) {
                if (this.params.observeParents) for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) this.observer.attach(e[t]);
                this.observer.attach(this.$el[0], {childList: this.params.observeSlideChildren}), this.observer.attach(this.$wrapperEl[0], {attributes: !1})
            }
        }, destroy: function () {
            this.observer.observers.forEach(function (e) {
                e.disconnect()
            }), this.observer.observers = []
        }
    }, uu = [{
        name: "resize", create: function () {
            var e = this;
            Kl(e, {
                resize: {
                    resizeHandler: function () {
                        e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"))
                    }, orientationChangeHandler: function () {
                        e && !e.destroyed && e.initialized && e.emit("orientationchange")
                    }
                }
            })
        }, on: {
            init: function (e) {
                var t = Al();
                t.addEventListener("resize", e.resize.resizeHandler), t.addEventListener("orientationchange", e.resize.orientationChangeHandler)
            }, destroy: function (e) {
                var t = Al();
                t.removeEventListener("resize", e.resize.resizeHandler), t.removeEventListener("orientationchange", e.resize.orientationChangeHandler)
            }
        }
    }, {
        name: "observer", params: {observer: !1, observeParents: !1, observeSlideChildren: !1}, create: function () {
            Jl(this, {observer: eu(eu({}, lu), {}, {observers: []})})
        }, on: {
            init: function (e) {
                e.observer.init()
            }, destroy: function (e) {
                e.observer.destroy()
            }
        }
    }];
    au.use(uu);
    var cu = au;

    function hu() {
        return (hu = Object.assign || function (e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
            }
            return e
        }).apply(this, arguments)
    }

    var du = {
        update: function () {
            var e = this.params.navigation;
            if (!this.params.loop) {
                var t = this.navigation, i = t.$nextEl, n = t.$prevEl;
                n && n.length > 0 && (this.isBeginning ? n.addClass(e.disabledClass) : n.removeClass(e.disabledClass), n[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass)), i && i.length > 0 && (this.isEnd ? i.addClass(e.disabledClass) : i.removeClass(e.disabledClass), i[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass))
            }
        }, onPrevClick: function (e) {
            e.preventDefault(), this.isBeginning && !this.params.loop || this.slidePrev()
        }, onNextClick: function (e) {
            e.preventDefault(), this.isEnd && !this.params.loop || this.slideNext()
        }, init: function () {
            var e, t, i = this.params.navigation;
            (i.nextEl || i.prevEl) && (i.nextEl && (e = Yl(i.nextEl), this.params.uniqueNavElements && "string" == typeof i.nextEl && e.length > 1 && 1 === this.$el.find(i.nextEl).length && (e = this.$el.find(i.nextEl))), i.prevEl && (t = Yl(i.prevEl), this.params.uniqueNavElements && "string" == typeof i.prevEl && t.length > 1 && 1 === this.$el.find(i.prevEl).length && (t = this.$el.find(i.prevEl))), e && e.length > 0 && e.on("click", this.navigation.onNextClick), t && t.length > 0 && t.on("click", this.navigation.onPrevClick), Kl(this.navigation, {
                $nextEl: e,
                nextEl: e && e[0],
                $prevEl: t,
                prevEl: t && t[0]
            }))
        }, destroy: function () {
            var e = this.navigation, t = e.$nextEl, i = e.$prevEl;
            t && t.length && (t.off("click", this.navigation.onNextClick), t.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click", this.navigation.onPrevClick), i.removeClass(this.params.navigation.disabledClass))
        }
    }, pu = {
        name: "navigation",
        params: {
            navigation: {
                nextEl: null,
                prevEl: null,
                hideOnClick: !1,
                disabledClass: "swiper-button-disabled",
                hiddenClass: "swiper-button-hidden",
                lockClass: "swiper-button-lock"
            }
        },
        create: function () {
            Jl(this, {navigation: hu({}, du)})
        },
        on: {
            init: function (e) {
                e.navigation.init(), e.navigation.update()
            }, toEdge: function (e) {
                e.navigation.update()
            }, fromEdge: function (e) {
                e.navigation.update()
            }, destroy: function (e) {
                e.navigation.destroy()
            }, click: function (e, t) {
                var i, n = e.navigation, r = n.$nextEl, s = n.$prevEl;
                !e.params.navigation.hideOnClick || Yl(t.target).is(s) || Yl(t.target).is(r) || (r ? i = r.hasClass(e.params.navigation.hiddenClass) : s && (i = s.hasClass(e.params.navigation.hiddenClass)), !0 === i ? e.emit("navigationShow") : e.emit("navigationHide"), r && r.toggleClass(e.params.navigation.hiddenClass), s && s.toggleClass(e.params.navigation.hiddenClass))
            }
        }
    };

    function fu() {
        return (fu = Object.assign || function (e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
            }
            return e
        }).apply(this, arguments)
    }

    var mu = {
        update: function () {
            var e = this.rtl, t = this.params.pagination;
            if (t.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                var i,
                    n = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                    r = this.pagination.$el,
                    s = this.params.loop ? Math.ceil((n - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length;
                if (this.params.loop ? ((i = Math.ceil((this.activeIndex - this.loopedSlides) / this.params.slidesPerGroup)) > n - 1 - 2 * this.loopedSlides && (i -= n - 2 * this.loopedSlides), i > s - 1 && (i -= s), i < 0 && "bullets" !== this.params.paginationType && (i = s + i)) : i = void 0 !== this.snapIndex ? this.snapIndex : this.activeIndex || 0, "bullets" === t.type && this.pagination.bullets && this.pagination.bullets.length > 0) {
                    var o, a, l, u = this.pagination.bullets;
                    if (t.dynamicBullets && (this.pagination.bulletSize = u.eq(0)[this.isHorizontal() ? "outerWidth" : "outerHeight"](!0), r.css(this.isHorizontal() ? "width" : "height", this.pagination.bulletSize * (t.dynamicMainBullets + 4) + "px"), t.dynamicMainBullets > 1 && void 0 !== this.previousIndex && (this.pagination.dynamicBulletIndex += i - this.previousIndex, this.pagination.dynamicBulletIndex > t.dynamicMainBullets - 1 ? this.pagination.dynamicBulletIndex = t.dynamicMainBullets - 1 : this.pagination.dynamicBulletIndex < 0 && (this.pagination.dynamicBulletIndex = 0)), o = i - this.pagination.dynamicBulletIndex, l = ((a = o + (Math.min(u.length, t.dynamicMainBullets) - 1)) + o) / 2), u.removeClass(t.bulletActiveClass + " " + t.bulletActiveClass + "-next " + t.bulletActiveClass + "-next-next " + t.bulletActiveClass + "-prev " + t.bulletActiveClass + "-prev-prev " + t.bulletActiveClass + "-main"), r.length > 1) u.each(function (e) {
                        var n = Yl(e), r = n.index();
                        r === i && n.addClass(t.bulletActiveClass), t.dynamicBullets && (r >= o && r <= a && n.addClass(t.bulletActiveClass + "-main"), r === o && n.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), r === a && n.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next"))
                    }); else {
                        var c = u.eq(i), h = c.index();
                        if (c.addClass(t.bulletActiveClass), t.dynamicBullets) {
                            for (var d = u.eq(o), p = u.eq(a), f = o; f <= a; f += 1) u.eq(f).addClass(t.bulletActiveClass + "-main");
                            if (this.params.loop) if (h >= u.length - t.dynamicMainBullets) {
                                for (var m = t.dynamicMainBullets; m >= 0; m -= 1) u.eq(u.length - m).addClass(t.bulletActiveClass + "-main");
                                u.eq(u.length - t.dynamicMainBullets - 1).addClass(t.bulletActiveClass + "-prev")
                            } else d.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), p.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next"); else d.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), p.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next")
                        }
                    }
                    if (t.dynamicBullets) {
                        var g = Math.min(u.length, t.dynamicMainBullets + 4),
                            v = (this.pagination.bulletSize * g - this.pagination.bulletSize) / 2 - l * this.pagination.bulletSize,
                            y = e ? "right" : "left";
                        u.css(this.isHorizontal() ? y : "top", v + "px")
                    }
                }
                if ("fraction" === t.type && (r.find("." + t.currentClass).text(t.formatFractionCurrent(i + 1)), r.find("." + t.totalClass).text(t.formatFractionTotal(s))), "progressbar" === t.type) {
                    var b;
                    b = t.progressbarOpposite ? this.isHorizontal() ? "vertical" : "horizontal" : this.isHorizontal() ? "horizontal" : "vertical";
                    var w = (i + 1) / s, D = 1, T = 1;
                    "horizontal" === b ? D = w : T = w, r.find("." + t.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + D + ") scaleY(" + T + ")").transition(this.params.speed)
                }
                "custom" === t.type && t.renderCustom ? (r.html(t.renderCustom(this, i + 1, s)), this.emit("paginationRender", r[0])) : this.emit("paginationUpdate", r[0]), r[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](t.lockClass)
            }
        }, render: function () {
            var e = this.params.pagination;
            if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                var t = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                    i = this.pagination.$el, n = "";
                if ("bullets" === e.type) {
                    for (var r = this.params.loop ? Math.ceil((t - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length, s = 0; s < r; s += 1) e.renderBullet ? n += e.renderBullet.call(this, s, e.bulletClass) : n += "<" + e.bulletElement + ' class="' + e.bulletClass + '"></' + e.bulletElement + ">";
                    i.html(n), this.pagination.bullets = i.find("." + e.bulletClass)
                }
                "fraction" === e.type && (n = e.renderFraction ? e.renderFraction.call(this, e.currentClass, e.totalClass) : '<span class="' + e.currentClass + '"></span> / <span class="' + e.totalClass + '"></span>', i.html(n)), "progressbar" === e.type && (n = e.renderProgressbar ? e.renderProgressbar.call(this, e.progressbarFillClass) : '<span class="' + e.progressbarFillClass + '"></span>', i.html(n)), "custom" !== e.type && this.emit("paginationRender", this.pagination.$el[0])
            }
        }, init: function () {
            var e = this, t = e.params.pagination;
            if (t.el) {
                var i = Yl(t.el);
                0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && i.length > 1 && (i = e.$el.find(t.el)), "bullets" === t.type && t.clickable && i.addClass(t.clickableClass), i.addClass(t.modifierClass + t.type), "bullets" === t.type && t.dynamicBullets && (i.addClass("" + t.modifierClass + t.type + "-dynamic"), e.pagination.dynamicBulletIndex = 0, t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)), "progressbar" === t.type && t.progressbarOpposite && i.addClass(t.progressbarOppositeClass), t.clickable && i.on("click", "." + t.bulletClass, function (t) {
                    t.preventDefault();
                    var i = Yl(this).index() * e.params.slidesPerGroup;
                    e.params.loop && (i += e.loopedSlides), e.slideTo(i)
                }), Kl(e.pagination, {$el: i, el: i[0]}))
            }
        }, destroy: function () {
            var e = this.params.pagination;
            if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                var t = this.pagination.$el;
                t.removeClass(e.hiddenClass), t.removeClass(e.modifierClass + e.type), this.pagination.bullets && this.pagination.bullets.removeClass(e.bulletActiveClass), e.clickable && t.off("click", "." + e.bulletClass)
            }
        }
    }, gu = {
        name: "pagination",
        params: {
            pagination: {
                el: null,
                bulletElement: "span",
                clickable: !1,
                hideOnClick: !1,
                renderBullet: null,
                renderProgressbar: null,
                renderFraction: null,
                renderCustom: null,
                progressbarOpposite: !1,
                type: "bullets",
                dynamicBullets: !1,
                dynamicMainBullets: 1,
                formatFractionCurrent: function (e) {
                    return e
                },
                formatFractionTotal: function (e) {
                    return e
                },
                bulletClass: "swiper-pagination-bullet",
                bulletActiveClass: "swiper-pagination-bullet-active",
                modifierClass: "swiper-pagination-",
                currentClass: "swiper-pagination-current",
                totalClass: "swiper-pagination-total",
                hiddenClass: "swiper-pagination-hidden",
                progressbarFillClass: "swiper-pagination-progressbar-fill",
                progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                clickableClass: "swiper-pagination-clickable",
                lockClass: "swiper-pagination-lock"
            }
        },
        create: function () {
            Jl(this, {pagination: fu({dynamicBulletIndex: 0}, mu)})
        },
        on: {
            init: function (e) {
                e.pagination.init(), e.pagination.render(), e.pagination.update()
            }, activeIndexChange: function (e) {
                (e.params.loop || void 0 === e.snapIndex) && e.pagination.update()
            }, snapIndexChange: function (e) {
                e.params.loop || e.pagination.update()
            }, slidesLengthChange: function (e) {
                e.params.loop && (e.pagination.render(), e.pagination.update())
            }, snapGridLengthChange: function (e) {
                e.params.loop || (e.pagination.render(), e.pagination.update())
            }, destroy: function (e) {
                e.pagination.destroy()
            }, click: function (e, t) {
                e.params.pagination.el && e.params.pagination.hideOnClick && e.pagination.$el.length > 0 && !Yl(t.target).hasClass(e.params.pagination.bulletClass) && (!0 === e.pagination.$el.hasClass(e.params.pagination.hiddenClass) ? e.emit("paginationShow") : e.emit("paginationHide"), e.pagination.$el.toggleClass(e.params.pagination.hiddenClass))
            }
        }
    };

    function vu() {
        return (vu = Object.assign || function (e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
            }
            return e
        }).apply(this, arguments)
    }

    var yu = {
        setTranslate: function () {
            for (var e = this.slides, t = 0; t < e.length; t += 1) {
                var i = this.slides.eq(t), n = -i[0].swiperSlideOffset;
                this.params.virtualTranslate || (n -= this.translate);
                var r = 0;
                this.isHorizontal() || (r = n, n = 0);
                var s = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);
                i.css({opacity: s}).transform("translate3d(" + n + "px, " + r + "px, 0px)")
            }
        }, setTransition: function (e) {
            var t = this, i = t.slides, n = t.$wrapperEl;
            if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
                var r = !1;
                i.transitionEnd(function () {
                    if (!r && t && !t.destroyed) {
                        r = !0, t.animating = !1;
                        for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) n.trigger(e[i])
                    }
                })
            }
        }
    }, bu = {
        name: "effect-fade", params: {fadeEffect: {crossFade: !1}}, create: function () {
            Jl(this, {fadeEffect: vu({}, yu)})
        }, on: {
            beforeInit: function (e) {
                if ("fade" === e.params.effect) {
                    e.classNames.push(e.params.containerModifierClass + "fade");
                    var t = {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        slidesPerGroup: 1,
                        watchSlidesProgress: !0,
                        spaceBetween: 0,
                        virtualTranslate: !0
                    };
                    Kl(e.params, t), Kl(e.originalParams, t)
                }
            }, setTranslate: function (e) {
                "fade" === e.params.effect && e.fadeEffect.setTranslate()
            }, setTransition: function (e, t) {
                "fade" === e.params.effect && e.fadeEffect.setTransition(t)
            }
        }
    };

    function wu() {
        return (wu = Object.assign || function (e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
            }
            return e
        }).apply(this, arguments)
    }

    var Du = {
        run: function () {
            var e = this, t = e.slides.eq(e.activeIndex), i = e.params.autoplay.delay;
            t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), clearTimeout(e.autoplay.timeout), e.autoplay.timeout = Ul(function () {
                e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")), e.params.cssMode && e.autoplay.running && e.autoplay.run()
            }, i)
        }, start: function () {
            return void 0 === this.autoplay.timeout && !this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0)
        }, stop: function () {
            return !!this.autoplay.running && void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0)
        }, pause: function (e) {
            this.autoplay.running && (this.autoplay.paused || (this.autoplay.timeout && clearTimeout(this.autoplay.timeout), this.autoplay.paused = !0, 0 !== e && this.params.autoplay.waitForTransition ? (this.$wrapperEl[0].addEventListener("transitionend", this.autoplay.onTransitionEnd), this.$wrapperEl[0].addEventListener("webkitTransitionEnd", this.autoplay.onTransitionEnd)) : (this.autoplay.paused = !1, this.autoplay.run())))
        }, onVisibilityChange: function () {
            var e = Pl();
            "hidden" === e.visibilityState && this.autoplay.running && this.autoplay.pause(), "visible" === e.visibilityState && this.autoplay.paused && (this.autoplay.run(), this.autoplay.paused = !1)
        }, onTransitionEnd: function (e) {
            this && !this.destroyed && this.$wrapperEl && e.target === this.$wrapperEl[0] && (this.$wrapperEl[0].removeEventListener("transitionend", this.autoplay.onTransitionEnd), this.$wrapperEl[0].removeEventListener("webkitTransitionEnd", this.autoplay.onTransitionEnd), this.autoplay.paused = !1, this.autoplay.running ? this.autoplay.run() : this.autoplay.stop())
        }
    }, Tu = {
        name: "autoplay",
        params: {
            autoplay: {
                enabled: !1,
                delay: 3e3,
                waitForTransition: !0,
                disableOnInteraction: !0,
                stopOnLastSlide: !1,
                reverseDirection: !1
            }
        },
        create: function () {
            Jl(this, {autoplay: wu(wu({}, Du), {}, {running: !1, paused: !1})})
        },
        on: {
            init: function (e) {
                e.params.autoplay.enabled && (e.autoplay.start(), Pl().addEventListener("visibilitychange", e.autoplay.onVisibilityChange))
            }, beforeTransitionStart: function (e, t, i) {
                e.autoplay.running && (i || !e.params.autoplay.disableOnInteraction ? e.autoplay.pause(t) : e.autoplay.stop())
            }, sliderFirstMove: function (e) {
                e.autoplay.running && (e.params.autoplay.disableOnInteraction ? e.autoplay.stop() : e.autoplay.pause())
            }, touchEnd: function (e) {
                e.params.cssMode && e.autoplay.paused && !e.params.autoplay.disableOnInteraction && e.autoplay.run()
            }, destroy: function (e) {
                e.autoplay.running && e.autoplay.stop(), Pl().removeEventListener("visibilitychange", e.autoplay.onVisibilityChange)
            }
        }
    };

    function Cu(e) {
        return (Cu = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function ku(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Eu(e, t, i) {
        return (Eu = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Su(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function _u(e, t) {
        return (_u = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function xu(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Su(e);
            if (t) {
                var r = Su(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Cu(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Su(e) {
        return (Su = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    cu.use([pu, gu, bu, Tu]);
    var Pu = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && _u(e, t)
        }(r, vl);
        var t, i, n = xu(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Eu(Su(r.prototype), "onEnter", this).call(this)
            }
        }, {
            key: "onLeave", value: function () {
                Eu(Su(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                var e = this;
                Eu(Su(r.prototype), "onEnterCompleted", this).call(this), Eu(Su(r.prototype), "onPageChange", this).call(this), this.clientSliderCol1 = new cu(".js-client-grid-one", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    freeMode: !0,
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 5},
                    speed: 1500,
                    centeredSlides: !0
                }), this.clientSliderCol2 = new cu(".js-client-grid-two", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 5},
                    speed: 2e3,
                    centeredSlides: !0
                }), this.clientSliderCol3 = new cu(".js-client-grid-three", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    freeMode: !0,
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 5},
                    speed: 1500,
                    centeredSlides: !0
                }), this.cardsSlider = new cu(".js-card-mobile", {
                    slidesPerView: "auto",
                    centeredSlides: !0,
                    spaceBetween: 30,
                    grabCursor: !0,
                    pagination: {
                        el: ".aw-pagination", clickable: !0, renderBullet: function (e, t) {
                            return '<span class="'.concat(t, '"></span>')
                        }
                    },
                    breakpoints: {650: {slidesPerView: "auto"}}
                }), this.quoteSlider = new cu(".swiper-container", {
                    effect: "fade",
                    fadeEffect: {crossFade: !0},
                    pagination: {
                        el: ".swiper-pagination", clickable: !0, renderBullet: function (e, t) {
                            return '<span class="'.concat(t, '"></span>')
                        }
                    },
                    breakpoints: {650: {}},
                    on: {
                        init: function () {
                            setTimeout(function () {
                                e.onPageChange()
                            }, 1e3)
                        }
                    }
                }), new Tl, o.isDesktop && ln(".js-moon") && new El
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Eu(Su(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && ku(t.prototype, i), r
    }();

    function Ou(e) {
        return (Ou = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function Au(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Mu(e, t, i) {
        return (Mu = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Bu(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function Lu(e, t) {
        return (Lu = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Fu(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Bu(e);
            if (t) {
                var r = Bu(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Ou(t) && "function" != typeof t ? ju(e) : t
            }(this, i)
        }
    }

    function ju(e) {
        if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return e
    }

    function Bu(e) {
        return (Bu = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    function Iu(e, t, i) {
        return t in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    var Ru = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && Lu(e, t)
        }(r, vl);
        var t, i, n = Fu(r);

        function r(e) {
            var t;
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), Iu(ju(t = n.call(this, e)), "setInitial", function () {
                var e = t.ui, i = e.dropdown, n = e.title, r = e.filters, s = e.close, o = e.flapOne, a = e.flapTwo;
                an.set(i, {autoAlpha: 0, pointerEvents: "none"}), an.set(r, {yPercent: 100}), an.set(n, {
                    yPercent: 100,
                    autoAlpha: 0
                }), an.set(s, {autoAlpha: 0, xPercent: 50}), an.set(o, {
                    autoAlpha: 0,
                    yPercent: 50
                }), an.set(a, {autoAlpha: 0, yPercent: 50})
            }), Iu(ju(t), "open", function () {
                o.flags.dropdownFilterCateogry = !0, document.body.classList.add("dcf-is-open"), t.tl && t.tl.kill(), t.tl = an.timeline();
                var e = t.ui, i = e.dropdown, n = e.title, r = e.filters, s = e.close, a = e.flapOne, l = e.flapTwo;
                t.tl.to([".js-inner-header", ".js-inner-header > .nav-logo", ".js-inner-header > .mobile-nav"], {
                    autoAlpha: 0,
                    pointerEvents: "none",
                    ease: "power4.out",
                    duration: .35
                }, 0).to(i, {
                    autoAlpha: 1,
                    pointerEvents: "auto",
                    duration: 1,
                    ease: "expo.out"
                }, .25).to(n, {
                    yPercent: 0,
                    autoAlpha: 1,
                    duration: 1.5,
                    ease: "expo.out"
                }, .45).to([a, l], {
                    autoAlpha: 1,
                    yPercent: 0,
                    duration: 1.2,
                    stagger: .2,
                    ease: "expo.out"
                }, .35).to(s, {autoAlpha: 1, xPercent: 0, duration: 1.2, ease: "expo.out"}, .75).to(r, {
                    autoAlpha: 1,
                    yPercent: 0,
                    duration: 1,
                    stagger: .1,
                    ease: "power3.out"
                }, .65)
            }), Iu(ju(t), "close", function () {
                o.flags.dropdownFilterCateogry = !1, document.body.classList.remove("dcf-is-open"), t.tl && t.tl.kill(), t.tl = an.timeline();
                var e = t.ui, i = e.dropdown, n = e.title, r = e.filters, s = e.close, a = e.flapOne, l = e.flapTwo;
                t.tl.to(s, {autoAlpha: 0, xPercent: 50, duration: 1.2, ease: "expo.out"}, 0).to(n, {
                    yPercent: 100,
                    autoAlpha: 0,
                    duration: 1.5,
                    ease: "expo.out"
                }, 0).to(r, {
                    autoAlpha: 0,
                    yPercent: 100,
                    duration: 1,
                    stagger: .075,
                    ease: "power3.out"
                }, .1).to([a, l], {
                    autoAlpha: 0,
                    yPercent: 50,
                    duration: 1.2,
                    stagger: .15,
                    ease: "expo.out"
                }, 0).to(i, {
                    autoAlpha: 0,
                    pointerEvents: "none",
                    duration: 1,
                    ease: "expo.out"
                }, .75).to([".js-inner-header", ".js-inner-header > .nav-logo", ".js-inner-header > .mobile-nav"], {
                    autoAlpha: 1,
                    pointerEvents: "auto",
                    ease: "expo.out",
                    duration: 1
                }, .85)
            }), t
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Mu(Bu(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "projects", this.ui = {
                    open: ln(".js-dcf-open"),
                    close: ln(".js-dcf-close"),
                    dropdown: ln(".js-dcf-dropdown"),
                    filters: un(".js-dcf-filter"),
                    title: ln(".js-dcf-title"),
                    flapOne: ln(".js-dcf-flap-one"),
                    flapTwo: ln(".js-dcf-flap-two")
                }, this.setInitial()
            }
        }, {
            key: "onLeave", value: function () {
                Mu(Bu(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Mu(Bu(r.prototype), "onEnterCompleted", this).call(this), Mu(Bu(r.prototype), "onPageChange", this).call(this), this.initWorks(), new Tl
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Mu(Bu(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }, {
            key: "initWorks", value: function () {
                this.worksFilter(), this.mobileWorksFilter(), this.addEvents()
            }
        }, {
            key: "worksFilter", value: function () {
                var e = this;
                this.categoriesBtns = un(".js-category-btn");
                var t, i = Array.from(un(".js-works"));
                Array.from(un(".js-works-description")), this.categoriesBtns.forEach(function (n) {
                    n.addEventListener("click", function () {
                        var r = n.getAttribute("data-category-id");
                        e.categoriesBtns.forEach(function (e) {
                            e.classList.remove("cat-active")
                        }), r != t ? (t = r, n.classList.add("cat-active")) : t = "all", e.updateWorkList(i, t)
                    })
                })
            }
        }, {
            key: "mobileWorksFilter", value: function () {
                var e = this;
                this.categoriesMobileBtns = un(".js-mobile-cateogry");
                var t, i = Array.from(un(".js-works"));
                this.categoriesMobileBtns.forEach(function (n) {
                    n.addEventListener("click", function () {
                        o.dom.body.classList.contains("dcf-is-open") ? (ln(".header").style.opacity = 0, ln(".header").style.pointerEvents = "none") : (ln(".header").style.opacity = 1, ln(".header").style.pointerEvents = "auto");
                        var r = n.getAttribute("data-category-id");
                        e.categoriesMobileBtns.forEach(function (e) {
                            e.classList.remove("cat-mobile-active")
                        }), r != t ? (t = r, n.classList.add("cat-mobile-active")) : t = "all", e.updateWorkList(i, t)
                    })
                })
            }
        }, {
            key: "updateWorkList", value: function (e, t) {
                var i = this;
                this.categoryActive = t, this.worksTohide = e.filter(function (e) {
                    return "none" != e.style.display
                }), this.workToShow = [], e.forEach(function (e) {
                    (e.getAttribute("data-category-id").includes(t) || "all" === i.categoryActive) && i.workToShow.push(e)
                }), o.dom.body.classList.contains("dcf-is-open") && this.close(), this.tlWorks && this.tlWorks.kill(), this.tlWorks = an.timeline(), this.tlWorks.to(this.worksTohide, {
                    autoAlpha: 0,
                    y: 50,
                    ease: "expo.out",
                    duration: .5,
                    onComplete: function () {
                        i.worksTohide.map(function (e) {
                            return e.style.display = "none"
                        }), i.tlWorks.fromTo(i.workToShow, {autoAlpha: 0, y: 30, display: "block"}, {
                            y: 0,
                            autoAlpha: 1,
                            duration: 2,
                            ease: "expo.out",
                            stagger: .15
                        }), setTimeout(function () {
                            Mu(Bu(r.prototype), "updateScrollBounds", i).call(i)
                        }, 500)
                    }
                })
            }
        }, {
            key: "addEvents", value: function () {
                this.ui.open && this.ui.close && (this.ui.open.addEventListener("click", this.open), this.ui.close.addEventListener("click", this.close))
            }
        }]) && Au(t.prototype, i), r
    }();

    function Nu(e) {
        return (Nu = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function zu(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Hu(e, t, i) {
        return (Hu = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Wu(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function Vu(e, t) {
        return (Vu = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function qu(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Wu(e);
            if (t) {
                var r = Wu(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Nu(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Wu(e) {
        return (Wu = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    cu.use([pu, gu, bu, Tu]);
    var Xu = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && Vu(e, t)
        }(r, vl);
        var t, i, n = qu(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Hu(Wu(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "about"
            }
        }, {
            key: "onLeave", value: function () {
                Hu(Wu(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Hu(Wu(r.prototype), "onEnterCompleted", this).call(this), Hu(Wu(r.prototype), "onPageChange", this).call(this), new Tl, this.cardsSlider = new cu(".js-card-mobile", {
                    slidesPerView: "auto",
                    centeredSlides: !0,
                    spaceBetween: 30,
                    grabCursor: !0,
                    pagination: {
                        el: ".aw-pagination", clickable: !0, renderBullet: function (e, t) {
                            return '<span class="'.concat(t, '"></span>')
                        }
                    },
                    breakpoints: {650: {slidesPerView: "auto"}}
                })
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Hu(Wu(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && zu(t.prototype, i), r
    }();

    function Yu(e) {
        return (Yu = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function Uu(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Gu(e, t, i) {
        return (Gu = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Ju(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function $u(e, t) {
        return ($u = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Ku(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Ju(e);
            if (t) {
                var r = Ju(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Yu(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Ju(e) {
        return (Ju = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var Qu = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && $u(e, t)
        }(r, vl);
        var t, i, n = Ku(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Gu(Ju(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "techniques"
            }
        }, {
            key: "onLeave", value: function () {
                Gu(Ju(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Gu(Ju(r.prototype), "onEnterCompleted", this).call(this), Gu(Ju(r.prototype), "onPageChange", this).call(this), new Tl, un(".js-technique-videos").forEach(function (e) {
                    new bl.a(e)
                })
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Gu(Ju(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && Uu(t.prototype, i), r
    }();

    function Zu(e) {
        return (Zu = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function ec(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function tc(e, t, i) {
        return (tc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = rc(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function ic(e, t) {
        return (ic = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function nc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = rc(e);
            if (t) {
                var r = rc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Zu(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function rc(e) {
        return (rc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var sc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && ic(e, t)
        }(r, vl);
        var t, i, n = nc(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                tc(rc(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "blog"
            }
        }, {
            key: "onLeave", value: function () {
                tc(rc(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                tc(rc(r.prototype), "onEnterCompleted", this).call(this), tc(rc(r.prototype), "onPageChange", this).call(this), ln(".js-load-more") && this.ajaxLoadMore()
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                tc(rc(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }, {
            key: "ajaxLoadMore", value: function () {
                var e = this;
                ln(".js-load-more") && ln(".js-load-more").addEventListener("click", function () {
                    var t = ln(".blog__load-more").dataset.url, i = ln(".blog__load-more").dataset.page,
                        n = (ln(".blog__load-more").dataset.max, new URLSearchParams);
                    n.append("action", "load_more_posts"), n.append("current_page", i), ts.a.post(t, n).then(function (t) {
                        var i = ln(".case__grid");
                        un(".case__item__grid").forEach(function (e) {
                            e.classList.add("prev__blogs")
                        }), i.innerHTML += t.data.data, setTimeout(function () {
                            an.fromTo(un(".case__item__grid:not(.prev__blogs)"), {autoAlpha: 0, y: 30}, {
                                autoAlpha: 1,
                                y: 0,
                                ease: "expo",
                                stagger: .1,
                                duration: 1
                            })
                        }, 500), ln(".blog__load-more").dataset.page == ln(".blog__load-more").dataset.max ? ln(".blog__load-more").remove() : ln(".blog__load-more").dataset.page++, e.updateScrollBounds()
                    })
                })
            }
        }]) && ec(t.prototype, i), r
    }();

    function oc(e) {
        return (oc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function ac(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function lc(e, t, i) {
        return (lc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = hc(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function uc(e, t) {
        return (uc = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function cc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = hc(e);
            if (t) {
                var r = hc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== oc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function hc(e) {
        return (hc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var dc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && uc(e, t)
        }(r, vl);
        var t, i, n = cc(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                lc(hc(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "single-blog", o.dom.body.classList.add("bgWhite"), ln(".logo-dark").style.display = "none", ln(".logo-light").style.display = "block", ln(".offWhite").style.color = "#fff"
            }
        }, {
            key: "onLeave", value: function () {
                lc(hc(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                lc(hc(r.prototype), "onEnterCompleted", this).call(this), lc(hc(r.prototype), "onPageChange", this).call(this), ln("#searchform") && ln("#searchform").addEventListener("submit", function (e) {
                    e.preventDefault();
                    var t = ln("#searchform").dataset.url, i = "".concat(t, "/?s=").concat(e.target[0].value);
                    Gc.redirect(i)
                })
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                lc(hc(r.prototype), "onLeaveCompleted", this).call(this), o.dom.body.classList.remove("bgWhite"), ln(".logo-dark").style.display = "block", ln(".logo-light").style.display = "none"
            }
        }]) && ac(t.prototype, i), r
    }();

    function pc(e) {
        return (pc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function fc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function mc(e, t, i) {
        return (mc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = yc(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function gc(e, t) {
        return (gc = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function vc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = yc(e);
            if (t) {
                var r = yc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== pc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function yc(e) {
        return (yc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var bc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && gc(e, t)
        }(r, vl);
        var t, i, n = vc(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                mc(yc(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "contact"
            }
        }, {
            key: "onLeave", value: function () {
                mc(yc(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                mc(yc(r.prototype), "onEnterCompleted", this).call(this), mc(yc(r.prototype), "onPageChange", this).call(this)
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                mc(yc(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && fc(t.prototype, i), r
    }();

    function wc(e) {
        return (wc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function Dc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Tc(e, t, i) {
        return (Tc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Ec(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function Cc(e, t) {
        return (Cc = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function kc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Ec(e);
            if (t) {
                var r = Ec(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== wc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Ec(e) {
        return (Ec = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var _c = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && Cc(e, t)
        }(r, vl);
        var t, i, n = kc(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Tc(Ec(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "thanks"
            }
        }, {
            key: "onLeave", value: function () {
                Tc(Ec(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Tc(Ec(r.prototype), "onEnterCompleted", this).call(this), Tc(Ec(r.prototype), "onPageChange", this).call(this)
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Tc(Ec(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && Dc(t.prototype, i), r
    }();

    function xc(e) {
        return (xc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function Sc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Pc(e, t, i) {
        return (Pc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Mc(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function Oc(e, t) {
        return (Oc = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Ac(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Mc(e);
            if (t) {
                var r = Mc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== xc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Mc(e) {
        return (Mc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var Lc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && Oc(e, t)
        }(r, vl);
        var t, i, n = Ac(r);

        function r(e) {
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), n.call(this, e)
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Pc(Mc(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "search", ln(".logo-dark").style.display = "none", ln(".logo-light").style.display = "block"
            }
        }, {
            key: "onLeave", value: function () {
                Pc(Mc(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Pc(Mc(r.prototype), "onEnterCompleted", this).call(this), Pc(Mc(r.prototype), "onPageChange", this).call(this)
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Pc(Mc(r.prototype), "onLeaveCompleted", this).call(this), ln(".logo-dark").style.display = "block", ln(".logo-light").style.display = "none"
            }
        }]) && Sc(t.prototype, i), r
    }();

    function Fc(e) {
        return (Fc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function jc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function Bc(e, t, i) {
        return (Bc = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, i) {
            var n = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = Nc(e));) ;
                return e
            }(e, t);
            if (n) {
                var r = Object.getOwnPropertyDescriptor(n, t);
                return r.get ? r.get.call(i) : r.value
            }
        })(e, t, i || e)
    }

    function Ic(e, t) {
        return (Ic = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Rc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Nc(e);
            if (t) {
                var r = Nc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Fc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Nc(e) {
        return (Nc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    cu.use([pu, gu, bu, Tu]);
    var zc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && Ic(e, t)
        }(r, vl);
        var t, i, n = Rc(r);

        function r(e) {
            var t;
            return function (e, t) {
                if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function")
            }(this), (t = n.call(this, e)).sequence1 = null, t.sequence3 = null, t
        }

        return t = r, (i = [{
            key: "onEnter", value: function () {
                Bc(Nc(r.prototype), "onEnter", this).call(this), o.flags.sectionView = "subhome"
            }
        }, {
            key: "onLeave", value: function () {
                Bc(Nc(r.prototype), "onLeave", this).call(this)
            }
        }, {
            key: "onEnterCompleted", value: function () {
                Bc(Nc(r.prototype), "onEnterCompleted", this).call(this), Bc(Nc(r.prototype), "onPageChange", this).call(this), this.clientSliderCol1 = new cu(".js-client-grid-one", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    freeMode: !0,
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 5},
                    speed: 1500,
                    centeredSlides: !0
                }), this.clientSliderCol2 = new cu(".js-client-grid-two", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 15},
                    speed: 2e3,
                    centeredSlides: !0
                }), this.clientSliderCol3 = new cu(".js-client-grid-three", {
                    slidesPerView: "auto",
                    direction: "horizontal",
                    freeMode: !0,
                    loop: !0,
                    spaceBetween: 30,
                    touchRatio: 0,
                    autoplay: {delay: 5},
                    speed: 1500,
                    centeredSlides: !0
                }), this.cardsSlider = new cu(".js-card-mobile", {
                    slidesPerView: "auto",
                    centeredSlides: !0,
                    spaceBetween: 30,
                    grabCursor: !0,
                    pagination: {
                        el: ".aw-pagination", clickable: !0, renderBullet: function (e, t) {
                            return '<span class="'.concat(t, '"></span>')
                        }
                    },
                    breakpoints: {650: {slidesPerView: "auto"}}
                }), this.quoteSlider = new cu(".swiper-container", {
                    effect: "fade",
                    fadeEffect: {crossFade: !0},
                    pagination: {
                        el: ".swiper-pagination", clickable: !0, renderBullet: function (e, t) {
                            return '<span class="'.concat(t, '"></span>')
                        }
                    },
                    breakpoints: {650: {}}
                }), new Tl, o.isDesktop && ln(".js-moon") && new El
            }
        }, {
            key: "onLeaveCompleted", value: function () {
                Bc(Nc(r.prototype), "onLeaveCompleted", this).call(this)
            }
        }]) && jc(t.prototype, i), r
    }();

    function Hc(e) {
        return (Hc = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function Vc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    function qc(e, t) {
        return (qc = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }

    function Wc(e) {
        var t = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
                })), !0
            } catch (e) {
                return !1
            }
        }();
        return function () {
            var i, n = Xc(e);
            if (t) {
                var r = Xc(this).constructor;
                i = Reflect.construct(n, arguments, r)
            } else i = n.apply(this, arguments);
            return function (e, t) {
                return !t || "object" !== Hc(t) && "function" != typeof t ? function (e) {
                    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return e
                }(e) : t
            }(this, i)
        }
    }

    function Xc(e) {
        return (Xc = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    var Yc = null, Uc = function (e) {
        !function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && qc(e, t)
        }(r, hr.Transition);
        var t, i, n = Wc(r);

        function r() {
            return function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, r), n.apply(this, arguments)
        }

        return t = r, (i = [{
            key: "in", value: function (e) {
                var t = e.from, i = (e.to, e.done);
                Yc && Yc.kill(), Yc = an.timeline({
                    paused: !0, onComplete: function () {
                        o.dom.body.classList.add("loaded")
                    }
                }).to(".js-mask-2", {
                    yPercent: -100,
                    duration: 1,
                    ease: "power4.inOut"
                }, .55), ns()(ln(".js-smooth"), function () {
                    t.remove(), Yc.play(), i()
                })
            }
        }, {
            key: "out", value: function (e) {
                e.from;
                var t = e.done;
                an.timeline({
                    onComplete: function () {
                        t(), o.dom.body.classList.remove("loaded")
                    }
                }).set([".js-mask", ".js-mask-2"], {autoAlpha: 1}).fromTo(".js-mask-2", {yPercent: 100}, {
                    yPercent: 0,
                    duration: 1.2,
                    ease: "power4.inOut"
                }, .15)
            }
        }]) && Vc(t.prototype, i), r
    }();
    hr.initial = !1;
    var Gc = new hr.Core({
        renderers: {
            home: Pu,
            works: Ru,
            about: Xu,
            blog: sc,
            contact: bc,
            techniques: Qu,
            singleBlog: dc,
            thanks: _c,
            subhome: zc,
            search: Lc,
            default: vl
        }, transitions: {default: Uc}
    });

    function $c(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    var Kc = new (function () {
        function e() {
            var t, i, n = this;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), i = function () {
                o.flags.dropdown = !0, document.body.classList.add("menu-is-open"), n.tl && n.tl.kill(), n.tl = an.timeline();
                var e = n.ui, t = e.dropdown, i = e.lines, r = e.links, s = e.openLines, a = e.closeLineOne,
                    l = e.closeLineTwo, u = e.flapOne, c = e.flapTwo;
                ln(".js-header").classList.contains("bg-add") && ln(".js-header").classList.remove("bg-add"), n.tl.to(s, {
                    scaleX: 0,
                    duration: .35,
                    stagger: -.05,
                    ease: "power1.in"
                }, 0).to(a, {scaleX: 1, duration: .75, ease: "expo.out"}, .55).to(l, {
                    scaleY: 1,
                    duration: .75,
                    ease: "expo.out"
                }, .56).to(t, {yPercent: 0, duration: 1.1, ease: "expo.inOut"}, .075, .5).to(i, {
                    yPercent: 0,
                    stagger: .075,
                    duration: 1.1,
                    ease: "expo.out"
                }, .85).to(r, {
                    autoAlpha: 0,
                    duration: .75,
                    yPercent: -100,
                    stagger: .075,
                    ease: "power2.inOut"
                }, .95).to([u, c], {autoAlpha: 1, yPercent: 0, duration: 1.2, stagger: .2, ease: "expo.out"}, .95)
            }, (t = "open") in this ? Object.defineProperty(this, t, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : this[t] = i, this.ui = {
                toggle: ln(".js-menu-toggle"),
                dropdown: ln(".js-dropdown"),
                lines: un(".js-dropdown-menu__line--main"),
                links: un(".js-site-link"),
                openLines: un(".js-burger__line"),
                closeLineOne: ln(".js-burger-close__line--1"),
                closeLineTwo: ln(".js-burger-close__line--2"),
                flapOne: ln(".js-d-flap-one"),
                flapTwo: ln(".js-d-flap-two")
            }
        }

        var t, i;
        return t = e, (i = [{
            key: "init", value: function () {
                this.setInitial(), this.addEvents()
            }
        }, {
            key: "setInitial", value: function () {
                var e = this.ui, t = e.dropdown, i = e.lines, n = e.flapOne, r = e.flapTwo;
                an.set(t, {yPercent: 101}), an.set(i, {yPercent: 100}), an.set(".js-burger-close__line--1", {scaleX: 0}), an.set(".js-burger-close__line--2", {scaleY: 0}), an.set(n, {
                    autoAlpha: 0,
                    yPercent: 50
                }), an.set(r, {autoAlpha: 0, yPercent: 50})
            }
        }, {
            key: "close", value: function () {
                o.flags.dropdown = !1, document.body.classList.remove("menu-is-open"), this.tl && this.tl.kill(), this.tl = an.timeline();
                var e = this.ui, t = e.dropdown, i = e.lines, n = e.links, r = e.openLines, s = e.closeLineOne,
                    a = e.closeLineTwo, l = e.flapOne, u = e.flapTwo;
                this.tl.to(s, {scaleX: 0, duration: .35, ease: "power1.in"}, 0).to(a, .35, {
                    scaleY: 0,
                    duration: .35,
                    ease: "power1.in"
                }, .05).to(t, {yPercent: 101, duration: 1.1, ease: "expo.inOut"}, 0).to([l, u], {
                    autoAlpha: 0,
                    yPercent: 50,
                    duration: 1.2,
                    stagger: .15,
                    ease: "expo.out"
                }, 0).to(i, {yPercent: 100, duration: .75, stagger: -.075, ease: "expo.inOut"}, 0).to(n, {
                    autoAlpha: 1,
                    yPercent: 0,
                    duration: .75,
                    stagger: -.075,
                    ease: "power2.inOut"
                }, 0).to(r, {scaleX: 1, duration: .5, stagger: -.05, ease: "expo.out"}, .75)
            }
        }, {
            key: "addEvents", value: function () {
                var e = this;
                this.ui.toggle.addEventListener("click", function () {
                    o.flags.dropdown ? e.close() : e.open()
                })
            }
        }]) && $c(t.prototype, i), e
    }());

    function Jc(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
    }

    "scrollRestoration" in history && (history.scrollRestoration = "manual");
    var Qc = function () {
        function e() {
            var t, i;
            !function (e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }(this, e), i = function () {
                window.addEventListener("focus", function () {
                    return o.flags.active = !0
                }), window.addEventListener("blur", function () {
                    return o.flags.active = !1
                })
            }, (t = "onLoad") in this ? Object.defineProperty(this, t, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : this[t] = i, this.init(), this.setup()
        }

        var t, i;
        return t = e, (i = [{
            key: "setup", value: function () {
                var e = this;
                r.a.addClasses(o.dom.body), window.onload = function () {
                    return e.onLoad()
                }
            }
        }, {
            key: "init", value: function () {
                o.flags.first = !0, o.flags.showVideo = !1, this.highwayEvents(), Kc.init(), ir.load().then(function () {
                });
                var e = an.timeline({paused: !0, defaults: {immediateRender: !0}});
                e.to(".js-l-logo", {
                    autoAlpha: 1,
                    duration: 1,
                    stagger: .1,
                    ease: "power4"
                }).fromTo(".js-l-rocketJump", {y: 20, autoAlpha: 0}, {
                    autoAlpha: 1,
                    y: -20,
                    duration: 1.2,
                    ease: "power4.inOut",
                    onComplete: function () {
                        e.fromTo(".js-l-rocketJump", {y: -20}, {y: 0, duration: 1.2, ease: "bounce.out"})
                    }
                }, ".75").to(".js-progress-svg", {
                    drawSVG: "0% ".concat(this.currentRounded, "%"),
                    duration: 2,
                    ease: "expo.inOut",
                    onComplete: function () {
                        an.fromTo(".js-progress-last-svg", {drawSVG: "0% 100%"}, {
                            drawSVG: "100% 100%",
                            duration: 1,
                            ease: "expo.inOut"
                        }), an.to(".js-l-logo", {
                            autoAlpha: 0,
                            delay: 2,
                            duration: 1,
                            stagger: .1,
                            ease: "power4"
                        }), an.to(".js-l-rocketJump", {
                            autoAlpha: 0,
                            delay: 2.5,
                            duration: 1,
                            ease: "power4"
                        }), an.to(".js-progress-svg", {
                            autoAlpha: 0,
                            delay: 3,
                            duration: 1,
                            stagger: .1,
                            ease: "power4"
                        }), an.to(".js-loader", {
                            yPercent: -100,
                            duration: .1,
                            delay: .1,
                            ease: "expo.inOut",
                            onComplete: function () {
                                ln(".js-loader").remove(), "home" === o.flags.sectionView && ln(".video__showreel") && ln(".video__showreel").play(), o.dom.body.classList.add("loaded")
                            }
                        })
                    }
                }, 0), setTimeout(function () {
                    an.to(".js-progress-svg", {drawSVG: "100%", duration: 1}), e.play()
                }, 800)
            }
        }, {
            key: "highwayEvents", value: function () {
                Gc.on("NAVIGATE_OUT", function () {

                    o.dom.body.classList.contains("menu-is-open") && Kc.close();
                    var e = un(".js-site-link"), t = un(".js-mobile-link");
                    e.forEach(function (e) {
                        e.classList.remove("link-is-active"), e.href === location.href && e.classList.add("link-is-active"),

                            (e.href === location.href && e.href == 'http://localhost:8000/about-us') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/products') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/contact-us') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/csr') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/product/detail/apparels') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/product/detail/balls') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/product/detail/gloves') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/product/detail/bags') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/go-green') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/research-&-development') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/events') ? window.location.reload() : '';
                        (e.href === location.href && e.href == 'http://localhost:8000/medical-care') ? window.location.reload() : '';
                    }), t.forEach(function (e) {
                        e.classList.remove("link-mobile-active"), e.href === location.href && e.classList.add("link-mobile-active")
                    })
                }), Gc.on("NAVIGATE_IN", function () {
                    window.scrollTo(0, 0), ln(".js-header") && ln(".js-header").classList.remove("hide")
                })
            }
        }]) && Jc(t.prototype, i), e
    }();
    window.addEventListener("keydown", function (e) {
        32 == e.keyCode && e.target == document.body && (e.preventDefault(), e.stopPropagation())
    }), new Qc
}]);
var isMobile = window.matchMedia("only screen and (max-width: 1024px)"),
    elementExists = document.getElementById("ir-video-home");
if (elementExists) if (isMobile.matches) ; else {
    for (var sources = document.querySelectorAll("video#ir-video-home source"), video = document.querySelector("video#ir-video-home"), i = 0; i < sources.length; i++) sources[i].setAttribute("src", sources[i].getAttribute("data-src"));
    video.load()
}
