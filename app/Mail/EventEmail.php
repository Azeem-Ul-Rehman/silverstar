<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {

        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        try {
            return $this->from($this->details['email'])->subject('Contact Us')->view('mails.contact')->with(['details' => $this->details]);
        } catch (Exception $e) {
            dd("error" + $e->getMessage());
        }

    }
}
