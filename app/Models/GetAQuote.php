<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GetAQuote extends Model
{
    protected $table = "get_a_quote";
    protected $guarded = [];
}
