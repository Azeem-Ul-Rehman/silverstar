<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $table = "product_variants";
    protected $guarded = [];

    public function getImagePathAttribute()
    {
        return asset('uploads/productVariant/' . $this->image);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
