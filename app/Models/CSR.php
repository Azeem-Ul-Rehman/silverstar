<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CSR extends Model
{
    protected $table = "c_s_r_s";
    protected $guarded = [];

    public function getBackgroundImagePathAttribute()
    {
        return asset('uploads/csr/' . $this->background_image);
    }

    public function getImagePathAttribute()
    {
        return asset('uploads/csr/' . $this->image);
    }

    public function getIconOnePathAttribute()
    {
        return asset('uploads/csr/' . $this->icon_one);
    }

    public function getIconTwoPathAttribute()
    {
        return asset('uploads/csr/' . $this->icon_two);
    }

    public function getIconThreePathAttribute()
    {
        return asset('uploads/csr/' . $this->icon_three);
    }

    public function getIconFourPathAttribute()
    {
        return asset('uploads/csr/' . $this->icon_four);
    }
}
