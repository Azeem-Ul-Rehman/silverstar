<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GetInTouch extends Model
{
    protected $table = "get_in_touches";
    protected $guarded = [];
}
