<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $userType = $user->user_type;

        if ($userType == 'admin') {
            return redirect()->route('admin.contacts.index');
        } else {
            Auth::logout();;
            return redirect()->route('index')->with([
                'flash_status' => 'success',
                'flash_message' => 'You are Unauthoirzed for this Request.'
            ]);

        }
    }
}
