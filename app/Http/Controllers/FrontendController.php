<?php

namespace App\Http\Controllers;

use App\Models\CSR;
use App\Models\Event;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function aboutUs()
    {
        return view('frontend.about');
    }

    public function contactUs()
    {
        return view('frontend.contact');
    }

    public function csr()
    {
        $csrs = CSR::all();
        return view('frontend.csr', compact('csrs'));
    }

    public function products()
    {
        return view('frontend.products');
    }

    public function productDetail($name)
    {
        $products = Product::where('type', $name)->with('productVariants')->get();
        return view('frontend.product-detail', compact('products','name'));
    }

    public function goGreen()
    {
        return view('frontend.gogreen');
    }

    public function researchAndDevelopment()
    {
        return view('frontend.research-and-development');
    }

    public function events()
    {
        $events = Event::all();
        return view('frontend.events', compact('events'));
    }

    public function medicalCare()
    {
        return view('frontend.medical-care');
    }
}
