<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsEmail;
use App\Mail\EventEmail;
use App\Models\GetAQuote;
use App\Models\GetInTouch;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class GetAQuoteController extends Controller
{
    public function index()
    {
        $contacts = GetAQuote::orderBy('id', 'DESC')->get();
        return view('backend.quote.index', compact('contacts'));

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required',
            'company_name' => 'required',
            'phone_number' => 'required',
            'message' => 'required'

        ]);
        $contact = new GetAQuote();
        $contact->name = $request->full_name;
        $contact->category = $request->category;
        $contact->email = $request->email;
        $contact->phone_number = $request->phone_number;
        $contact->company_name = $request->company_name;
        $contact->message = $request->message;
        $contact->save();


        $details = [
            'greeting' => 'Hi ' . $request->full_name,
            'category' => $request->category,
            'company_name' => $request->company_name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'message' => $request->message,
        ];
        Mail::to('admin@gmail.com')->send(new EventEmail($details));
        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Your message has been sent successfully.'
        ]);
    }
}
