<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\CSR;
use Illuminate\Http\Request;

class CSRController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {

        $csrs = CSR::all();
        return view('backend.csr.index', compact('csrs'));
    }

    public function create()
    {
        return view('backend.csr.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'background_image' => 'required',
            'image' => 'required',
            'icon_one' => 'required',
            'icon_two' => 'required',
            'icon_three' => 'required',
            'icon_four' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
            'background_image.required' => 'Background Image is required.',
            'image.required' => 'Image is required.',
            'icon_one.required' => 'Icon One is required.',
            'icon_two.required' => 'Icon Two is required.',
            'icon_three.required' => 'Icon Three is required.',
            'icon_four.required' => 'Icon Four is required.',
        ]);

        $var = date_create();
        $time = date_format($var, 'YmdHis');
        if ($request->has('background_image')) {
            $image = $request->file('background_image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $background_image = $name;
        } else {
            $background_image = 'default.png';
        }

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        if ($request->has('icon_one')) {
            $image = $request->file('icon_one');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_one = $name;
        } else {
            $icon_one = 'default.png';
        }

        if ($request->has('icon_two')) {
            $image = $request->file('icon_two');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_two = $name;
        } else {
            $icon_two = 'default.png';
        }

        if ($request->has('icon_three')) {
            $image = $request->file('icon_three');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_three = $name;
        } else {
            $icon_three = 'default.png';
        }
        if ($request->has('icon_four')) {
            $image = $request->file('icon_four');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_four = $name;
        } else {
            $icon_four = 'default.png';
        }

        $csr = CSR::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $profile_image,
            'background_image' => $background_image,
            'icon_one' => $icon_one,
            'icon_two' => $icon_two,
            'icon_three' => $icon_three,
            'icon_four' => $icon_four,
        ]);

        return redirect()->route('admin.csr.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'CSR created successfully.'
            ]);
    }

    public function edit($id)
    {
        $csr = CSR::find($id);
        return view('backend.csr.edit', compact('csr'));


    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
        ]);

        $csr = CSR::find($id);


        $var = date_create();
        $time = date_format($var, 'YmdHis');
        if ($request->has('background_image')) {
            $image = $request->file('background_image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $background_image = $name;
        } else {
            $background_image =  $csr->background_image;;
        }

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $csr->image;
        }

        if ($request->has('icon_one')) {
            $image = $request->file('icon_one');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_one = $name;
        } else {
            $icon_one = $csr->icon_one;
        }

        if ($request->has('icon_two')) {
            $image = $request->file('icon_two');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_two = $name;
        } else {
            $icon_two = $csr->icon_two;
        }

        if ($request->has('icon_three')) {
            $image = $request->file('icon_three');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_three = $name;
        } else {
            $icon_three = $csr->icon_three;
        }
        if ($request->has('icon_four')) {
            $image = $request->file('icon_four');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/csr');
            $image->move($destinationPath, $name);
            $icon_four = $name;
        } else {
            $icon_four = $csr->icon_four;
        }


        $csr->update([
            'title' => $request->title,
            'description' => $request->description,
            'background_image' => $background_image,
            'image' => $profile_image,
            'icon_one' => $icon_one,
            'icon_two' => $icon_two,
            'icon_three' => $icon_three,
            'icon_four' => $icon_four,
        ]);

        return redirect()->route('admin.csr.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'CSR updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $csr = CSR::findOrFail($id);
        $csr->delete();

        return redirect()->route('admin.csr.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'CSR has been deleted Successfully.'
            ]);
    }
}
