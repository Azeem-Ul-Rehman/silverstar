<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{
    public function productCategory(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ], [
            'type.required' => "Type is Required.",
        ]);

        if (!$validator->fails()) {
            $products = Product::where('type', $request->type)->get();

            $response['status'] = 'success';
            $response['data'] = [
                'products' => $products,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


}
