<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariant;
use Illuminate\Http\Request;

class ProductVariantController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $variants = ProductVariant::all();
        return view('backend.product-variants.index', compact('variants'));
    }

    public function create()
    {
        return view('backend.product-variants.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'color' => 'required',
            'product_id' => 'required',
            'image' => 'required',
        ], [
            'color.required' => 'Color is required.',
            'product_id.required' => 'Product is required.',
            'image.required' => 'Image is required.',

        ]);

        $var = date_create();
        $time = date_format($var, 'YmdHis');

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/productVariant');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }


        $variant = ProductVariant::create([
            'product_id' => $request->product_id,
            'color' => $request->color,
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.product-variants.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product Variant created successfully.'
            ]);
    }

    public function edit($id)
    {
        $variant = ProductVariant::find($id);
        return view('backend.product-variants.edit', compact('variant'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'color' => 'required',
            'product' => 'required',
        ], [
            'color.required' => 'Color is required.',
            'product.required' => 'Product is required.',

        ]);

        $variant = Product::find($id);
        $var = date_create();
        $time = date_format($var, 'YmdHis');
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/productVariant');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $variant->image;
        }
        $variant->update([
            'product_id' => $request->product_id,
            'color' => $request->color,
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.product-variants.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product Variant updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $variant = ProductVariant::findOrFail($id);
        $variant->delete();

        return redirect()->route('admin.product-variants.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product Variant has been deleted Successfully.'
            ]);
    }
}
