<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {

        $events = Event::all();
        return view('backend.events.index', compact('events'));
    }

    public function create()
    {
        return view('backend.events.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
            'image.required' => 'Image is required.',
        ]);

        $var = date_create();
        $time = date_format($var, 'YmdHis');

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/events');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }


        $event = Event::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $profile_image,

        ]);

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event created successfully.'
            ]);
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return view('backend.events.edit', compact('event'));


    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
        ]);

        $event = Event::find($id);


        $var = date_create();
        $time = date_format($var, 'YmdHis');


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/events');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $event->image;
        }


        $event->update([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event has been deleted Successfully.'
            ]);
    }
}
