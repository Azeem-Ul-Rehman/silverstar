<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $products = Product::all();
        return view('backend.products.index', compact('products'));
    }

    public function create()
    {
        return view('backend.products.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
            'image' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'type.required' => 'Type is required.',
            'image.required' => 'Image is required.',

        ]);

        $var = date_create();
        $time = date_format($var, 'YmdHis');

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/products');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }


        $product = Product::create([
            'title' => $request->title,
            'type' => $request->type,
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product created successfully.'
            ]);
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('backend.products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'type.required' => 'Type is required.',

        ]);

        $product = Product::find($id);
        $var = date_create();
        $time = date_format($var, 'YmdHis');
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $time . '-' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/products');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $product->image;
        }
        $product->update([
            'title' => $request->title,
            'type' => $request->type,
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product has been deleted Successfully.'
            ]);
    }
}
