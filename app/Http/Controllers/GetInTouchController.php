<?php

namespace App\Http\Controllers;

use App\Models\GetInTouch;

use Illuminate\Http\Request;


class GetInTouchController extends Controller
{
    public function index()
    {
        $contacts = GetInTouch::orderBy('id', 'DESC')->get();
        return view('backend.contacts.index', compact('contacts'));

    }

    public function create()
    {
//        $settings = Setting::take(3)->get();
//        return view('frontend.pages.contact-us', compact('settings'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name'             => 'required',
            'email'            => 'required',
            'phone_number'     => 'required',
            'message'          => 'required'

        ]);
        $contact  = new GetInTouch();
        $contact->name = $request->full_name;
        $contact->email = $request->email;
        $contact->phone_number = $request->phone_number;
        $contact->company_name = $request->company_name;
        $contact->message = $request->message;
        $contact->save();
        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Your message has been sent.'
        ]);
    }

    public function destroy($id)
    {
        $contactus = GetInTouch::findOrFail($id);
        $contactus->delete();

        return redirect()->route('admin.contacts.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Contact has been deleted'
            ]);
    }
}
