<?php

namespace App\Http\Middleware;

use App\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPayment
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $user = Auth::user();
        $setting = Setting::first();
        if (!is_null($user)) {
            if ($user->user_type == 'admin') {
                return $next($request);
            } else {
                if ($setting->payment_method == 'inactive') {
                    return $next($request);
                } else {
                    if ($user->status == 'pending') {
                        return redirect()->route('payment.index');
                    } else {
                        $todayDate = now();
                        $expiryDate = $user->expiry_date;
                        if ($todayDate >= $expiryDate) {
                            return redirect()->route('payment.index')->with([
                                'flash_status' => 'success',
                                'flash_message' => 'Your Monthly Subscription is Expired.'
                            ]);
                        } else {
                            return $next($request);
                        }
                    }
                }
            }

        } else {
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthoirzed for this request.'
            ]);
        }


    }
}
